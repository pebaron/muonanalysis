//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Thu Apr 25 13:21:28 2019 by ROOT version 6.16/00
// from TTree TPTree_MuonProbe_OC_LooseProbes_noProbeIP/TP
// found on file: muontp_mc_pp.root
//////////////////////////////////////////////////////////

#ifndef PBClassTrig_h
#define PBClassTrig_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class PBClassTrig {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           BCID;
   Float_t         EventBadMuonVetoVar_HighPt_ID;
   Float_t         EventBadMuonVetoVar_HighPt_ME;
   Float_t         EventBadMuonVetoVar_Loose_ID;
   Float_t         EventBadMuonVetoVar_Loose_ME;
   Float_t         EventBadMuonVetoVar_Medium_ID;
   Float_t         EventBadMuonVetoVar_Medium_ME;
   Float_t         EventBadMuonVetoVar_Tight_ID;
   Float_t         EventBadMuonVetoVar_Tight_ME;
   Float_t         EventBadMuonVetoVar_VeryLoose_ID;
   Float_t         EventBadMuonVetoVar_VeryLoose_ME;
   Int_t           PV_n;
   Float_t         actual_mu;
   Float_t         average_mu;
   Float_t         calib_jet_probe_pt;
   Float_t         calib_jet_probe_eta;
   Float_t         calib_jet_probe_phi;
   UChar_t         calib_jet_probe_index;
   Float_t         calib_jet_probe_m;
   Float_t         calib_jet_probe_emfrac;
   Float_t         calib_jet_probe_jvt;
   Float_t         calib_jet_closest_ptrel_probe;
   Float_t         calib_jet_tag_pt;
   Float_t         calib_jet_tag_eta;
   Float_t         calib_jet_tag_phi;
   UChar_t         calib_jet_tag_index;
   Float_t         calib_jet_tag_m;
   Float_t         calib_jet_dR_tag;
   UInt_t          calib_jet_nJets20;
   Float_t         calib_jet_dR_probe;
   Float_t         calib_jet_closest_chargedfrac_probe;
   UInt_t          calib_jet_ntrks500_probe;
   UInt_t          calib_jet_n_bad_jets;
   Float_t         dilep_deta;
   Float_t         dilep_dphi;
   Int_t           dilep_match_2L1RoI_mu6_mu4;
   Int_t           dilep_match_2L1RoI_mu6_mu6;
   Float_t         dilep_mll;
   Float_t         dilep_pt;
   Float_t         energyDensity_central;
   Float_t         energyDensity_forward;
   ULong64_t       eventNumber;
   UInt_t          lumiblock;
   Int_t           mcChannelNumber;
   Double_t        mcEventWeight;
   UInt_t          n_Trks_PrimVtx;
   Float_t         probe_pt;
   Float_t         probe_eta;
   Float_t         probe_phi;
   UChar_t         probe_index;
   Float_t         probe_CaloLRLikelihood;
   Int_t           probe_CaloMuonIDTag;
   Float_t         probe_EnergyLoss;
   Float_t         probe_LowPtBDT_PromptTagger;
   Float_t         probe_LowPtBDT_PromptVeto;
   Float_t         probe_MeasEnergyLoss;
   Float_t         probe_ParamEnergyLoss;
   Float_t         probe_PromptLeptonInput_DL1mu;
   Float_t         probe_PromptLeptonInput_DRlj;
   Float_t         probe_PromptLeptonInput_LepJetPtFrac;
   Float_t         probe_PromptLeptonInput_PtFrac;
   Float_t         probe_PromptLeptonInput_PtRel;
   Float_t         probe_PromptLeptonInput_PtVarCone30Rel;
   Float_t         probe_PromptLeptonInput_TopoEtCone30Rel;
   Short_t         probe_PromptLeptonInput_TrackJetNTrack;
   Float_t         probe_PromptLeptonInput_ip2;
   Float_t         probe_PromptLeptonInput_ip3;
   Float_t         probe_PromptLeptonInput_rnnip;
   Short_t         probe_PromptLeptonInput_sv1_jf_ntrkv;
   Float_t         probe_PromptLeptonIso;
   Float_t         probe_PromptLeptonVeto;
   UShort_t        probe_allAuthors;
   UShort_t        probe_author;
   UChar_t         probe_combinedTrackOutBoundsPrecisionHits;
   Float_t         probe_d0;
   Float_t         probe_d0err;
   Float_t         probe_eta_exTP;
   Float_t         probe_etcone20;
   Float_t         probe_etcone30;
   Float_t         probe_etcone40;
   UChar_t         probe_extendedClosePrecisionHits;
   UChar_t         probe_extendedLargeHits;
   UChar_t         probe_extendedLargeHoles;
   UChar_t         probe_extendedOutBoundsPrecisionHits;
   UChar_t         probe_extendedSmallHits;
   UChar_t         probe_extendedSmallHoles;
   UChar_t         probe_innerClosePrecisionHits;
   UChar_t         probe_innerLargeHits;
   UChar_t         probe_innerLargeHoles;
   UChar_t         probe_innerOutBoundsPrecisionHits;
   UChar_t         probe_innerSmallHits;
   UChar_t         probe_innerSmallHoles;
   UChar_t         probe_isEndcapGoodLayers;
   UChar_t         probe_isSmallGoodSectors;
   UChar_t         probe_middleClosePrecisionHits;
   UChar_t         probe_middleLargeHits;
   UChar_t         probe_middleLargeHoles;
   UChar_t         probe_middleOutBoundsPrecisionHits;
   UChar_t         probe_middleSmallHits;
   UChar_t         probe_middleSmallHoles;
   Float_t         probe_momentumBalanceSignificance;
   UChar_t         probe_nUnspoiledCscHits;
   Float_t         probe_neflowisol20;
   Float_t         probe_neflowisol30;
   Float_t         probe_neflowisol40;
   UChar_t         probe_numberOfGoodPrecisionLayers;
   UChar_t         probe_numberOfOutliersOnTrack;
   UChar_t         probe_numberOfPhiHoleLayers;
   UChar_t         probe_numberOfPhiLayers;
   UChar_t         probe_numberOfPrecisionHoleLayers;
   UChar_t         probe_numberOfPrecisionLayers;
   UChar_t         probe_numberOfTriggerEtaHoleLayers;
   UChar_t         probe_numberOfTriggerEtaLayers;
   UChar_t         probe_outerClosePrecisionHits;
   UChar_t         probe_outerLargeHits;
   UChar_t         probe_outerLargeHoles;
   UChar_t         probe_outerOutBoundsPrecisionHits;
   UChar_t         probe_outerSmallHits;
   UChar_t         probe_outerSmallHoles;
   UChar_t         probe_phiLayer1Hits;
   UChar_t         probe_phiLayer1Holes;
   UChar_t         probe_phiLayer2Hits;
   UChar_t         probe_phiLayer2Holes;
   UChar_t         probe_phiLayer3Hits;
   UChar_t         probe_phiLayer3Holes;
   UChar_t         probe_phiLayer4Hits;
   UChar_t         probe_phiLayer4Holes;
   Float_t         probe_phi_exTP;
   UChar_t         probe_primarySector;
   Float_t         probe_ptcone20;
   Float_t         probe_ptcone20_LooseTTVA_pt500;
   Float_t         probe_ptcone20_TightTTVA_pt1000;
   Float_t         probe_ptcone20_TightTTVA_pt500;
   Float_t         probe_ptcone30;
   Float_t         probe_ptcone30_LooseTTVA_pt500;
   Float_t         probe_ptcone30_TightTTVA_pt1000;
   Float_t         probe_ptcone30_TightTTVA_pt500;
   Float_t         probe_ptcone40;
   Float_t         probe_ptcone40_LooseTTVA_pt500;
   Float_t         probe_ptcone40_TightTTVA_pt1000;
   Float_t         probe_ptcone40_TightTTVA_pt500;
   Float_t         probe_ptvarcone20;
   Float_t         probe_ptvarcone20_LooseTTVA_pt500;
   Float_t         probe_ptvarcone20_TightTTVA_pt1000;
   Float_t         probe_ptvarcone20_TightTTVA_pt500;
   Float_t         probe_ptvarcone30;
   Float_t         probe_ptvarcone30_LooseTTVA_pt500;
   Float_t         probe_ptvarcone30_TightTTVA_pt1000;
   Float_t         probe_ptvarcone30_TightTTVA_pt500;
   Float_t         probe_ptvarcone40;
   Float_t         probe_ptvarcone40_LooseTTVA_pt500;
   Float_t         probe_ptvarcone40_TightTTVA_pt1000;
   Float_t         probe_ptvarcone40_TightTTVA_pt500;
   Int_t           probe_q;
   Float_t         probe_scatteringCurvatureSignificance;
   Float_t         probe_scatteringNeighbourSignificance;
   UChar_t         probe_secondarySector;
   Float_t         probe_segmentDeltaEta;
   Float_t         probe_segmentDeltaPhi;
   Float_t         probe_topocore;
   Float_t         probe_topoetcone20;
   Float_t         probe_topoetcone30;
   Float_t         probe_topoetcone40;
   Int_t           probe_truth_origin;
   Int_t           probe_truth_type;
   UShort_t        probe_type;
   Float_t         probe_z0;
   Float_t         probe_Segment1_chi2;
   UInt_t          probe_Segment1_chmbIdx;
   Float_t         probe_Segment1_nDoF;
   Float_t         probe_Segment2_chi2;
   UInt_t          probe_Segment2_chmbIdx;
   Float_t         probe_Segment2_nDoF;
   Float_t         probe_chi2_PR;
   Float_t         probe_dRMatch_HLT_2mu10_OBSR;
   Float_t         probe_dRMatch_HLT_2mu10_nomucomb_OBSR;
   Float_t         probe_dRMatch_HLT_2mu14_OBSR;
   Float_t         probe_dRMatch_HLT_2mu14_nomucomb_OBSR;
   Float_t         probe_dRMatch_HLT_2mu15_OBSR;
   Float_t         probe_dRMatch_HLT_2mu15_nomucomb_OBSR;
   Float_t         probe_dRMatch_HLT_2mu6_bJpsimumu;
   Float_t         probe_dRMatch_HLT_2mu6_bJpsimumui_noL2;
   Float_t         probe_dRMatch_HLT_AllTriggers;
   Float_t         probe_dRMatch_HLT_mu10;
   Float_t         probe_dRMatch_HLT_mu10_RM;
   Float_t         probe_dRMatch_HLT_mu10_idperf;
   Float_t         probe_dRMatch_HLT_mu10_idperf_RM;
   Float_t         probe_dRMatch_HLT_mu10_msonly;
   Float_t         probe_dRMatch_HLT_mu10_msonly_RM;
   Float_t         probe_dRMatch_HLT_mu10_nomucomb_RM;
   Float_t         probe_dRMatch_HLT_mu13_mu13_idperf_Zmumu;
   Float_t         probe_dRMatch_HLT_mu14;
   Float_t         probe_dRMatch_HLT_mu14_RM;
   Float_t         probe_dRMatch_HLT_mu14_ivarloose;
   Float_t         probe_dRMatch_HLT_mu14_nomucomb_RM;
   Float_t         probe_dRMatch_HLT_mu15noL1;
   Float_t         probe_dRMatch_HLT_mu15noL1_RM;
   Float_t         probe_dRMatch_HLT_mu18;
   Float_t         probe_dRMatch_HLT_mu18_2mu0noL1_JpsimumuFS;
   Float_t         probe_dRMatch_HLT_mu18_2mu4noL1_JpsimumuL2;
   Float_t         probe_dRMatch_HLT_mu18_RM;
   Float_t         probe_dRMatch_HLT_mu18_mu8noL1_OBSR;
   Float_t         probe_dRMatch_HLT_mu18noL1;
   Float_t         probe_dRMatch_HLT_mu18noL1_RM;
   Float_t         probe_dRMatch_HLT_mu20;
   Float_t         probe_dRMatch_HLT_mu20_L1MU15_RM;
   Float_t         probe_dRMatch_HLT_mu20_RM;
   Float_t         probe_dRMatch_HLT_mu20_idperf;
   Float_t         probe_dRMatch_HLT_mu20_idperf_RM;
   Float_t         probe_dRMatch_HLT_mu20_iloose_L1MU15;
   Float_t         probe_dRMatch_HLT_mu20_ivarloose_L1MU15;
   Float_t         probe_dRMatch_HLT_mu20_msonly;
   Float_t         probe_dRMatch_HLT_mu20_msonly_RM;
   Float_t         probe_dRMatch_HLT_mu20_mu8noL1_OBSR;
   Float_t         probe_dRMatch_HLT_mu20_mu8noL1_calotag_0eta010_OBSR;
   Float_t         probe_dRMatch_HLT_mu22;
   Float_t         probe_dRMatch_HLT_mu22_RM;
   Float_t         probe_dRMatch_HLT_mu22_mu8noL1_OBSR;
   Float_t         probe_dRMatch_HLT_mu22_mu8noL1_RM;
   Float_t         probe_dRMatch_HLT_mu22_mu8noL1_calotag_0eta010_OBSR;
   Float_t         probe_dRMatch_HLT_mu24;
   Float_t         probe_dRMatch_HLT_mu24_2mu4noL1_OBSR;
   Float_t         probe_dRMatch_HLT_mu24_L1MU15_RM;
   Float_t         probe_dRMatch_HLT_mu24_RM;
   Float_t         probe_dRMatch_HLT_mu24_iloose;
   Float_t         probe_dRMatch_HLT_mu24_iloose_L1MU15;
   Float_t         probe_dRMatch_HLT_mu24_imedium;
   Float_t         probe_dRMatch_HLT_mu24_ivarloose;
   Float_t         probe_dRMatch_HLT_mu24_ivarloose_L1MU15;
   Float_t         probe_dRMatch_HLT_mu24_ivarmedium;
   Float_t         probe_dRMatch_HLT_mu24_mu10noL1_calotag_0eta010_OBSR;
   Float_t         probe_dRMatch_HLT_mu24_mu8noL1_OBSR;
   Float_t         probe_dRMatch_HLT_mu24_mu8noL1_calotag_0eta010_OBSR;
   Float_t         probe_dRMatch_HLT_mu26_RM;
   Float_t         probe_dRMatch_HLT_mu26_imedium;
   Float_t         probe_dRMatch_HLT_mu26_ivarmedium;
   Float_t         probe_dRMatch_HLT_mu26_mu10noL1_calotag_0eta010_OBSR;
   Float_t         probe_dRMatch_HLT_mu26_mu8noL1_OBSR;
   Float_t         probe_dRMatch_HLT_mu26_mu8noL1_calotag_0eta010_OBSR;
   Float_t         probe_dRMatch_HLT_mu28_imedium;
   Float_t         probe_dRMatch_HLT_mu28_ivarmedium;
   Float_t         probe_dRMatch_HLT_mu28_mu8noL1_calotag_0eta010_OBSR;
   Float_t         probe_dRMatch_HLT_mu4;
   Float_t         probe_dRMatch_HLT_mu40;
   Float_t         probe_dRMatch_HLT_mu4_RM;
   Float_t         probe_dRMatch_HLT_mu4_idperf;
   Float_t         probe_dRMatch_HLT_mu4_idperf_RM;
   Float_t         probe_dRMatch_HLT_mu4_msonly;
   Float_t         probe_dRMatch_HLT_mu4_msonly_RM;
   Float_t         probe_dRMatch_HLT_mu4_mu4_idperf_bJpsimumu_noid;
   Float_t         probe_dRMatch_HLT_mu50;
   Float_t         probe_dRMatch_HLT_mu6;
   Float_t         probe_dRMatch_HLT_mu60;
   Float_t         probe_dRMatch_HLT_mu60_0eta105_msonly_OBSR;
   Float_t         probe_dRMatch_HLT_mu6_RM;
   Float_t         probe_dRMatch_HLT_mu6_bJpsi_TrkPEB;
   Float_t         probe_dRMatch_HLT_mu6_bJpsi_Trkloose;
   Float_t         probe_dRMatch_HLT_mu6_idperf;
   Float_t         probe_dRMatch_HLT_mu6_idperf_RM;
   Float_t         probe_dRMatch_HLT_mu6_msonly;
   Float_t         probe_dRMatch_HLT_mu6_msonly_RM;
   Float_t         probe_dRMatch_HLT_mu6_mu4_bJpsimumu;
   Float_t         probe_dRMatch_HLT_mu6_mu4_bJpsimumu_noL2;
   Float_t         probe_dRMatch_HLT_mu8_RM;
   Float_t         probe_dRMatch_HLT_mu8noL1;
   Float_t         probe_dRMatch_HLT_noalg_L1MU;
   Float_t         probe_dRMatch_HLT_noalg_L1MU10;
   Float_t         probe_dRMatch_HLT_noalg_L1MU11;
   Float_t         probe_dRMatch_HLT_noalg_L1MU20;
   Float_t         probe_dRMatch_HLT_noalg_L1MU21;
   Float_t         probe_dRMatch_L1RoI_mu4;
   Float_t         probe_dRMatch_L1RoI_mu6;
   Float_t         probe_dRMatch_L1_MU10;
   Float_t         probe_dRMatch_L1_MU11;
   Float_t         probe_dRMatch_L1_MU15;
   Float_t         probe_dRMatch_L1_MU20;
   Float_t         probe_dRMatch_L1_MU21;
   Float_t         probe_dRMatch_L1_MU4;
   Float_t         probe_dRMatch_L1_MU6;
   Float_t         probe_etaMS;
   Bool_t          probe_matched_CaloTag;
   Bool_t          probe_matched_HLT_2mu6_bJpsimumu;
   Bool_t          probe_matched_HLT_2mu6_bJpsimumui_noL2;
   Bool_t          probe_matched_HLT_AllTriggers;
   Bool_t          probe_matched_HLT_mu10;
   Bool_t          probe_matched_HLT_mu10_idperf;
   Bool_t          probe_matched_HLT_mu10_msonly;
   Bool_t          probe_matched_HLT_mu13_mu13_idperf_Zmumu;
   Bool_t          probe_matched_HLT_mu14;
   Bool_t          probe_matched_HLT_mu14_ivarloose;
   Bool_t          probe_matched_HLT_mu15noL1;
   Bool_t          probe_matched_HLT_mu18;
   Bool_t          probe_matched_HLT_mu18_2mu0noL1_JpsimumuFS;
   Bool_t          probe_matched_HLT_mu18_2mu4noL1_JpsimumuL2;
   Bool_t          probe_matched_HLT_mu18noL1;
   Bool_t          probe_matched_HLT_mu20;
   Bool_t          probe_matched_HLT_mu20_idperf;
   Bool_t          probe_matched_HLT_mu20_iloose_L1MU15;
   Bool_t          probe_matched_HLT_mu20_ivarloose_L1MU15;
   Bool_t          probe_matched_HLT_mu20_msonly;
   Bool_t          probe_matched_HLT_mu22;
   Bool_t          probe_matched_HLT_mu24;
   Bool_t          probe_matched_HLT_mu24_iloose;
   Bool_t          probe_matched_HLT_mu24_iloose_L1MU15;
   Bool_t          probe_matched_HLT_mu24_imedium;
   Bool_t          probe_matched_HLT_mu24_ivarloose;
   Bool_t          probe_matched_HLT_mu24_ivarloose_L1MU15;
   Bool_t          probe_matched_HLT_mu24_ivarmedium;
   Bool_t          probe_matched_HLT_mu26_imedium;
   Bool_t          probe_matched_HLT_mu26_ivarmedium;
   Bool_t          probe_matched_HLT_mu28_imedium;
   Bool_t          probe_matched_HLT_mu28_ivarmedium;
   Bool_t          probe_matched_HLT_mu4;
   Bool_t          probe_matched_HLT_mu40;
   Bool_t          probe_matched_HLT_mu4_idperf;
   Bool_t          probe_matched_HLT_mu4_msonly;
   Bool_t          probe_matched_HLT_mu4_mu4_idperf_bJpsimumu_noid;
   Bool_t          probe_matched_HLT_mu50;
   Bool_t          probe_matched_HLT_mu6;
   Bool_t          probe_matched_HLT_mu60;
   Bool_t          probe_matched_HLT_mu6_bJpsi_TrkPEB;
   Bool_t          probe_matched_HLT_mu6_bJpsi_Trkloose;
   Bool_t          probe_matched_HLT_mu6_idperf;
   Bool_t          probe_matched_HLT_mu6_msonly;
   Bool_t          probe_matched_HLT_mu6_mu4_bJpsimumu;
   Bool_t          probe_matched_HLT_mu6_mu4_bJpsimumu_noL2;
   Bool_t          probe_matched_HLT_mu8noL1;
   Bool_t          probe_matched_HLT_noalg_L1MU;
   Bool_t          probe_matched_HLT_noalg_L1MU10;
   Bool_t          probe_matched_HLT_noalg_L1MU11;
   Bool_t          probe_matched_HLT_noalg_L1MU20;
   Bool_t          probe_matched_HLT_noalg_L1MU21;
   Bool_t          probe_matched_HighPt;
   Bool_t          probe_matched_IsoFCLoose;
   Bool_t          probe_matched_IsoFCLoose_FixedRad;
   Bool_t          probe_matched_IsoFCTight;
   Bool_t          probe_matched_IsoFCTightTrackOnly;
   Bool_t          probe_matched_IsoFCTightTrackOnly_FixedRad;
   Bool_t          probe_matched_IsoFCTight_FixedRad;
   Bool_t          probe_matched_IsoFixedCutHighPtTrackOnly;
   Bool_t          probe_matched_IsoFixedCutPflowLoose;
   Bool_t          probe_matched_IsoFixedCutPflowTight;
   Bool_t          probe_matched_L1_MU10;
   Bool_t          probe_matched_L1_MU11;
   Bool_t          probe_matched_L1_MU15;
   Bool_t          probe_matched_L1_MU20;
   Bool_t          probe_matched_L1_MU21;
   Bool_t          probe_matched_L1_MU4;
   Bool_t          probe_matched_L1_MU6;
   Bool_t          probe_matched_Loose;
   Bool_t          probe_matched_LowPt;
   Bool_t          probe_matched_Medium;
   Bool_t          probe_matched_TRTCut;
   Bool_t          probe_matched_Tight;
   Float_t         probe_nDof_PR;
   Float_t         probe_qoverp_CB;
   Float_t         probe_qoverp_ID;
   Float_t         probe_qoverp_ME;
   Float_t         probe_qoverp_MS;
   Float_t         probe_qoverp_err_CB;
   Float_t         probe_qoverp_err_ID;
   Float_t         probe_qoverp_err_ME;
   Float_t         probe_qoverp_err_MS;
   Float_t         probe_qoverp_err_modified;
   Float_t         probe_qoverp_modified;
   Int_t           probe_quality;
   Double_t        prwWeight;
   UInt_t          randomLumiBlockNumber;
   UInt_t          randomRunNumber;
   UInt_t          runNumber;
   Float_t         tag_pt;
   Float_t         tag_eta;
   Float_t         tag_phi;
   UChar_t         tag_index;
   Float_t         tag_d0;
   Float_t         tag_d0err;
   Float_t         tag_dR_exTP;
   Float_t         tag_deta_exTP;
   Float_t         tag_dphi_exTP;
   Float_t         tag_etcone20;
   Float_t         tag_etcone30;
   Float_t         tag_etcone40;
   Bool_t          tag_matched_IsoFCLoose;
   Bool_t          tag_matched_IsoFCLoose_FixedRad;
   Bool_t          tag_matched_IsoFCTight;
   Bool_t          tag_matched_IsoFCTightTrackOnly;
   Bool_t          tag_matched_IsoFCTightTrackOnly_FixedRad;
   Bool_t          tag_matched_IsoFCTight_FixedRad;
   Bool_t          tag_matched_IsoFixedCutHighPtTrackOnly;
   Bool_t          tag_matched_IsoFixedCutPflowLoose;
   Bool_t          tag_matched_IsoFixedCutPflowTight;
   Float_t         tag_neflowisol20;
   Float_t         tag_neflowisol30;
   Float_t         tag_neflowisol40;
   Float_t         tag_ptcone20;
   Float_t         tag_ptcone20_LooseTTVA_pt500;
   Float_t         tag_ptcone20_TightTTVA_pt1000;
   Float_t         tag_ptcone20_TightTTVA_pt500;
   Float_t         tag_ptcone30;
   Float_t         tag_ptcone30_LooseTTVA_pt500;
   Float_t         tag_ptcone30_TightTTVA_pt1000;
   Float_t         tag_ptcone30_TightTTVA_pt500;
   Float_t         tag_ptcone40;
   Float_t         tag_ptcone40_LooseTTVA_pt500;
   Float_t         tag_ptcone40_TightTTVA_pt1000;
   Float_t         tag_ptcone40_TightTTVA_pt500;
   Float_t         tag_ptvarcone20;
   Float_t         tag_ptvarcone20_LooseTTVA_pt500;
   Float_t         tag_ptvarcone20_TightTTVA_pt1000;
   Float_t         tag_ptvarcone20_TightTTVA_pt500;
   Float_t         tag_ptvarcone30;
   Float_t         tag_ptvarcone30_LooseTTVA_pt500;
   Float_t         tag_ptvarcone30_TightTTVA_pt1000;
   Float_t         tag_ptvarcone30_TightTTVA_pt500;
   Float_t         tag_ptvarcone40;
   Float_t         tag_ptvarcone40_LooseTTVA_pt500;
   Float_t         tag_ptvarcone40_TightTTVA_pt1000;
   Float_t         tag_ptvarcone40_TightTTVA_pt500;
   Int_t           tag_q;
   Float_t         tag_topocore;
   Float_t         tag_topoetcone20;
   Float_t         tag_topoetcone30;
   Float_t         tag_topoetcone40;
   Int_t           tag_truth_origin;
   Int_t           tag_truth_type;
   Float_t         tag_z0;
   Float_t         tag_dRMatch_HLT_2mu10_OBSR;
   Float_t         tag_dRMatch_HLT_2mu10_nomucomb_OBSR;
   Float_t         tag_dRMatch_HLT_2mu14_OBSR;
   Float_t         tag_dRMatch_HLT_2mu14_nomucomb_OBSR;
   Float_t         tag_dRMatch_HLT_2mu15_OBSR;
   Float_t         tag_dRMatch_HLT_2mu15_nomucomb_OBSR;
   Float_t         tag_dRMatch_HLT_2mu6_bJpsimumu;
   Float_t         tag_dRMatch_HLT_2mu6_bJpsimumui_noL2;
   Float_t         tag_dRMatch_HLT_AllTriggers;
   Float_t         tag_dRMatch_HLT_mu10;
   Float_t         tag_dRMatch_HLT_mu10_RM;
   Float_t         tag_dRMatch_HLT_mu10_idperf;
   Float_t         tag_dRMatch_HLT_mu10_idperf_RM;
   Float_t         tag_dRMatch_HLT_mu10_msonly;
   Float_t         tag_dRMatch_HLT_mu10_msonly_RM;
   Float_t         tag_dRMatch_HLT_mu10_nomucomb_RM;
   Float_t         tag_dRMatch_HLT_mu13_mu13_idperf_Zmumu;
   Float_t         tag_dRMatch_HLT_mu14;
   Float_t         tag_dRMatch_HLT_mu14_RM;
   Float_t         tag_dRMatch_HLT_mu14_ivarloose;
   Float_t         tag_dRMatch_HLT_mu14_nomucomb_RM;
   Float_t         tag_dRMatch_HLT_mu15noL1;
   Float_t         tag_dRMatch_HLT_mu15noL1_RM;
   Float_t         tag_dRMatch_HLT_mu18;
   Float_t         tag_dRMatch_HLT_mu18_2mu0noL1_JpsimumuFS;
   Float_t         tag_dRMatch_HLT_mu18_2mu4noL1_JpsimumuL2;
   Float_t         tag_dRMatch_HLT_mu18_RM;
   Float_t         tag_dRMatch_HLT_mu18_mu8noL1_OBSR;
   Float_t         tag_dRMatch_HLT_mu18noL1;
   Float_t         tag_dRMatch_HLT_mu18noL1_RM;
   Float_t         tag_dRMatch_HLT_mu20;
   Float_t         tag_dRMatch_HLT_mu20_L1MU15_RM;
   Float_t         tag_dRMatch_HLT_mu20_RM;
   Float_t         tag_dRMatch_HLT_mu20_idperf;
   Float_t         tag_dRMatch_HLT_mu20_idperf_RM;
   Float_t         tag_dRMatch_HLT_mu20_iloose_L1MU15;
   Float_t         tag_dRMatch_HLT_mu20_ivarloose_L1MU15;
   Float_t         tag_dRMatch_HLT_mu20_msonly;
   Float_t         tag_dRMatch_HLT_mu20_msonly_RM;
   Float_t         tag_dRMatch_HLT_mu20_mu8noL1_OBSR;
   Float_t         tag_dRMatch_HLT_mu20_mu8noL1_calotag_0eta010_OBSR;
   Float_t         tag_dRMatch_HLT_mu22;
   Float_t         tag_dRMatch_HLT_mu22_RM;
   Float_t         tag_dRMatch_HLT_mu22_mu8noL1_OBSR;
   Float_t         tag_dRMatch_HLT_mu22_mu8noL1_RM;
   Float_t         tag_dRMatch_HLT_mu22_mu8noL1_calotag_0eta010_OBSR;
   Float_t         tag_dRMatch_HLT_mu24;
   Float_t         tag_dRMatch_HLT_mu24_2mu4noL1_OBSR;
   Float_t         tag_dRMatch_HLT_mu24_L1MU15_RM;
   Float_t         tag_dRMatch_HLT_mu24_RM;
   Float_t         tag_dRMatch_HLT_mu24_iloose;
   Float_t         tag_dRMatch_HLT_mu24_iloose_L1MU15;
   Float_t         tag_dRMatch_HLT_mu24_imedium;
   Float_t         tag_dRMatch_HLT_mu24_ivarloose;
   Float_t         tag_dRMatch_HLT_mu24_ivarloose_L1MU15;
   Float_t         tag_dRMatch_HLT_mu24_ivarmedium;
   Float_t         tag_dRMatch_HLT_mu24_mu10noL1_calotag_0eta010_OBSR;
   Float_t         tag_dRMatch_HLT_mu24_mu8noL1_OBSR;
   Float_t         tag_dRMatch_HLT_mu24_mu8noL1_calotag_0eta010_OBSR;
   Float_t         tag_dRMatch_HLT_mu26_RM;
   Float_t         tag_dRMatch_HLT_mu26_imedium;
   Float_t         tag_dRMatch_HLT_mu26_ivarmedium;
   Float_t         tag_dRMatch_HLT_mu26_mu10noL1_calotag_0eta010_OBSR;
   Float_t         tag_dRMatch_HLT_mu26_mu8noL1_OBSR;
   Float_t         tag_dRMatch_HLT_mu26_mu8noL1_calotag_0eta010_OBSR;
   Float_t         tag_dRMatch_HLT_mu28_imedium;
   Float_t         tag_dRMatch_HLT_mu28_ivarmedium;
   Float_t         tag_dRMatch_HLT_mu28_mu8noL1_calotag_0eta010_OBSR;
   Float_t         tag_dRMatch_HLT_mu4;
   Float_t         tag_dRMatch_HLT_mu40;
   Float_t         tag_dRMatch_HLT_mu4_RM;
   Float_t         tag_dRMatch_HLT_mu4_idperf;
   Float_t         tag_dRMatch_HLT_mu4_idperf_RM;
   Float_t         tag_dRMatch_HLT_mu4_msonly;
   Float_t         tag_dRMatch_HLT_mu4_msonly_RM;
   Float_t         tag_dRMatch_HLT_mu4_mu4_idperf_bJpsimumu_noid;
   Float_t         tag_dRMatch_HLT_mu50;
   Float_t         tag_dRMatch_HLT_mu6;
   Float_t         tag_dRMatch_HLT_mu60;
   Float_t         tag_dRMatch_HLT_mu60_0eta105_msonly_OBSR;
   Float_t         tag_dRMatch_HLT_mu6_RM;
   Float_t         tag_dRMatch_HLT_mu6_bJpsi_TrkPEB;
   Float_t         tag_dRMatch_HLT_mu6_bJpsi_Trkloose;
   Float_t         tag_dRMatch_HLT_mu6_idperf;
   Float_t         tag_dRMatch_HLT_mu6_idperf_RM;
   Float_t         tag_dRMatch_HLT_mu6_msonly;
   Float_t         tag_dRMatch_HLT_mu6_msonly_RM;
   Float_t         tag_dRMatch_HLT_mu6_mu4_bJpsimumu;
   Float_t         tag_dRMatch_HLT_mu6_mu4_bJpsimumu_noL2;
   Float_t         tag_dRMatch_HLT_mu8_RM;
   Float_t         tag_dRMatch_HLT_mu8noL1;
   Float_t         tag_dRMatch_HLT_noalg_L1MU;
   Float_t         tag_dRMatch_HLT_noalg_L1MU10;
   Float_t         tag_dRMatch_HLT_noalg_L1MU11;
   Float_t         tag_dRMatch_HLT_noalg_L1MU20;
   Float_t         tag_dRMatch_HLT_noalg_L1MU21;
   Float_t         tag_dRMatch_L1RoI_mu6;
   Bool_t          tag_matched_HLT_2mu6_bJpsimumu;
   Bool_t          tag_matched_HLT_2mu6_bJpsimumui_noL2;
   Bool_t          tag_matched_HLT_AllTriggers;
   Bool_t          tag_matched_HLT_mu10;
   Bool_t          tag_matched_HLT_mu10_idperf;
   Bool_t          tag_matched_HLT_mu10_msonly;
   Bool_t          tag_matched_HLT_mu13_mu13_idperf_Zmumu;
   Bool_t          tag_matched_HLT_mu14;
   Bool_t          tag_matched_HLT_mu14_ivarloose;
   Bool_t          tag_matched_HLT_mu15noL1;
   Bool_t          tag_matched_HLT_mu18;
   Bool_t          tag_matched_HLT_mu18_2mu0noL1_JpsimumuFS;
   Bool_t          tag_matched_HLT_mu18_2mu4noL1_JpsimumuL2;
   Bool_t          tag_matched_HLT_mu18noL1;
   Bool_t          tag_matched_HLT_mu20;
   Bool_t          tag_matched_HLT_mu20_idperf;
   Bool_t          tag_matched_HLT_mu20_iloose_L1MU15;
   Bool_t          tag_matched_HLT_mu20_ivarloose_L1MU15;
   Bool_t          tag_matched_HLT_mu20_msonly;
   Bool_t          tag_matched_HLT_mu22;
   Bool_t          tag_matched_HLT_mu24;
   Bool_t          tag_matched_HLT_mu24_iloose;
   Bool_t          tag_matched_HLT_mu24_iloose_L1MU15;
   Bool_t          tag_matched_HLT_mu24_imedium;
   Bool_t          tag_matched_HLT_mu24_ivarloose;
   Bool_t          tag_matched_HLT_mu24_ivarloose_L1MU15;
   Bool_t          tag_matched_HLT_mu24_ivarmedium;
   Bool_t          tag_matched_HLT_mu26_imedium;
   Bool_t          tag_matched_HLT_mu26_ivarmedium;
   Bool_t          tag_matched_HLT_mu28_imedium;
   Bool_t          tag_matched_HLT_mu28_ivarmedium;
   Bool_t          tag_matched_HLT_mu4;
   Bool_t          tag_matched_HLT_mu40;
   Bool_t          tag_matched_HLT_mu4_idperf;
   Bool_t          tag_matched_HLT_mu4_msonly;
   Bool_t          tag_matched_HLT_mu4_mu4_idperf_bJpsimumu_noid;
   Bool_t          tag_matched_HLT_mu50;
   Bool_t          tag_matched_HLT_mu6;
   Bool_t          tag_matched_HLT_mu60;
   Bool_t          tag_matched_HLT_mu6_bJpsi_TrkPEB;
   Bool_t          tag_matched_HLT_mu6_bJpsi_Trkloose;
   Bool_t          tag_matched_HLT_mu6_idperf;
   Bool_t          tag_matched_HLT_mu6_msonly;
   Bool_t          tag_matched_HLT_mu6_mu4_bJpsimumu;
   Bool_t          tag_matched_HLT_mu6_mu4_bJpsimumu_noL2;
   Bool_t          tag_matched_HLT_mu8noL1;
   Bool_t          tag_matched_HLT_noalg_L1MU;
   Bool_t          tag_matched_HLT_noalg_L1MU10;
   Bool_t          tag_matched_HLT_noalg_L1MU11;
   Bool_t          tag_matched_HLT_noalg_L1MU20;
   Bool_t          tag_matched_HLT_noalg_L1MU21;
   Float_t         trackMET_abs;
   Float_t         trackMET_phi;
   Bool_t          trig_HLT_2mu10_OBSR;
   Bool_t          trig_HLT_2mu10_nomucomb_OBSR;
   Bool_t          trig_HLT_2mu14_OBSR;
   Bool_t          trig_HLT_2mu14_nomucomb_OBSR;
   Bool_t          trig_HLT_2mu15_OBSR;
   Bool_t          trig_HLT_2mu15_nomucomb_OBSR;
   Bool_t          trig_HLT_2mu6_bJpsimumu;
   Bool_t          trig_HLT_2mu6_bJpsimumui_noL2;
   Bool_t          trig_HLT_AllTriggers;
   Bool_t          trig_HLT_mu10;
   Bool_t          trig_HLT_mu10_RM;
   Bool_t          trig_HLT_mu10_idperf;
   Bool_t          trig_HLT_mu10_idperf_RM;
   Bool_t          trig_HLT_mu10_msonly;
   Bool_t          trig_HLT_mu10_msonly_RM;
   Bool_t          trig_HLT_mu10_nomucomb_RM;
   Bool_t          trig_HLT_mu13_mu13_idperf_Zmumu;
   Bool_t          trig_HLT_mu14;
   Bool_t          trig_HLT_mu14_RM;
   Bool_t          trig_HLT_mu14_ivarloose;
   Bool_t          trig_HLT_mu14_nomucomb_RM;
   Bool_t          trig_HLT_mu15noL1;
   Bool_t          trig_HLT_mu15noL1_RM;
   Bool_t          trig_HLT_mu18;
   Bool_t          trig_HLT_mu18_2mu0noL1_JpsimumuFS;
   Bool_t          trig_HLT_mu18_2mu4noL1_JpsimumuL2;
   Bool_t          trig_HLT_mu18_RM;
   Bool_t          trig_HLT_mu18_mu8noL1_OBSR;
   Bool_t          trig_HLT_mu18noL1;
   Bool_t          trig_HLT_mu18noL1_RM;
   Bool_t          trig_HLT_mu20;
   Bool_t          trig_HLT_mu20_L1MU15_RM;
   Bool_t          trig_HLT_mu20_RM;
   Bool_t          trig_HLT_mu20_idperf;
   Bool_t          trig_HLT_mu20_idperf_RM;
   Bool_t          trig_HLT_mu20_iloose_L1MU15;
   Bool_t          trig_HLT_mu20_ivarloose_L1MU15;
   Bool_t          trig_HLT_mu20_msonly;
   Bool_t          trig_HLT_mu20_msonly_RM;
   Bool_t          trig_HLT_mu20_mu8noL1_OBSR;
   Bool_t          trig_HLT_mu20_mu8noL1_calotag_0eta010_OBSR;
   Bool_t          trig_HLT_mu22;
   Bool_t          trig_HLT_mu22_RM;
   Bool_t          trig_HLT_mu22_mu8noL1_OBSR;
   Bool_t          trig_HLT_mu22_mu8noL1_RM;
   Bool_t          trig_HLT_mu22_mu8noL1_calotag_0eta010_OBSR;
   Bool_t          trig_HLT_mu24;
   Bool_t          trig_HLT_mu24_2mu4noL1_OBSR;
   Bool_t          trig_HLT_mu24_L1MU15_RM;
   Bool_t          trig_HLT_mu24_RM;
   Bool_t          trig_HLT_mu24_iloose;
   Bool_t          trig_HLT_mu24_iloose_L1MU15;
   Bool_t          trig_HLT_mu24_imedium;
   Bool_t          trig_HLT_mu24_ivarloose;
   Bool_t          trig_HLT_mu24_ivarloose_L1MU15;
   Bool_t          trig_HLT_mu24_ivarmedium;
   Bool_t          trig_HLT_mu24_mu10noL1_calotag_0eta010_OBSR;
   Bool_t          trig_HLT_mu24_mu8noL1_OBSR;
   Bool_t          trig_HLT_mu24_mu8noL1_calotag_0eta010_OBSR;
   Bool_t          trig_HLT_mu26_RM;
   Bool_t          trig_HLT_mu26_imedium;
   Bool_t          trig_HLT_mu26_ivarmedium;
   Bool_t          trig_HLT_mu26_mu10noL1_calotag_0eta010_OBSR;
   Bool_t          trig_HLT_mu26_mu8noL1_OBSR;
   Bool_t          trig_HLT_mu26_mu8noL1_calotag_0eta010_OBSR;
   Bool_t          trig_HLT_mu28_imedium;
   Bool_t          trig_HLT_mu28_ivarmedium;
   Bool_t          trig_HLT_mu28_mu8noL1_calotag_0eta010_OBSR;
   Bool_t          trig_HLT_mu4;
   Bool_t          trig_HLT_mu40;
   Bool_t          trig_HLT_mu4_RM;
   Bool_t          trig_HLT_mu4_idperf;
   Bool_t          trig_HLT_mu4_idperf_RM;
   Bool_t          trig_HLT_mu4_msonly;
   Bool_t          trig_HLT_mu4_msonly_RM;
   Bool_t          trig_HLT_mu4_mu4_idperf_bJpsimumu_noid;
   Bool_t          trig_HLT_mu50;
   Bool_t          trig_HLT_mu6;
   Bool_t          trig_HLT_mu60;
   Bool_t          trig_HLT_mu60_0eta105_msonly_OBSR;
   Bool_t          trig_HLT_mu6_RM;
   Bool_t          trig_HLT_mu6_bJpsi_TrkPEB;
   Bool_t          trig_HLT_mu6_bJpsi_Trkloose;
   Bool_t          trig_HLT_mu6_idperf;
   Bool_t          trig_HLT_mu6_idperf_RM;
   Bool_t          trig_HLT_mu6_msonly;
   Bool_t          trig_HLT_mu6_msonly_RM;
   Bool_t          trig_HLT_mu6_mu4_bJpsimumu;
   Bool_t          trig_HLT_mu6_mu4_bJpsimumu_noL2;
   Bool_t          trig_HLT_mu8_RM;
   Bool_t          trig_HLT_mu8noL1;
   Bool_t          trig_HLT_noalg_L1MU;
   Bool_t          trig_HLT_noalg_L1MU10;
   Bool_t          trig_HLT_noalg_L1MU11;
   Bool_t          trig_HLT_noalg_L1MU20;
   Bool_t          trig_HLT_noalg_L1MU21;
   Float_t         vertex_density;

   // List of branches
   TBranch        *b_BCID;   //!
   TBranch        *b_EventBadMuonVetoVar_HighPt_ID;   //!
   TBranch        *b_EventBadMuonVetoVar_HighPt_ME;   //!
   TBranch        *b_EventBadMuonVetoVar_Loose_ID;   //!
   TBranch        *b_EventBadMuonVetoVar_Loose_ME;   //!
   TBranch        *b_EventBadMuonVetoVar_Medium_ID;   //!
   TBranch        *b_EventBadMuonVetoVar_Medium_ME;   //!
   TBranch        *b_EventBadMuonVetoVar_Tight_ID;   //!
   TBranch        *b_EventBadMuonVetoVar_Tight_ME;   //!
   TBranch        *b_EventBadMuonVetoVar_VeryLoose_ID;   //!
   TBranch        *b_EventBadMuonVetoVar_VeryLoose_ME;   //!
   TBranch        *b_PV_n;   //!
   TBranch        *b_actual_mu;   //!
   TBranch        *b_average_mu;   //!
   TBranch        *b_calib_jet_probe_pt;   //!
   TBranch        *b_calib_jet_probe_eta;   //!
   TBranch        *b_calib_jet_probe_phi;   //!
   TBranch        *b_calib_jet_probe_index;   //!
   TBranch        *b_calib_jet_probe_m;   //!
   TBranch        *b_calib_jet_probe_emfrac;   //!
   TBranch        *b_calib_jet_probe_jvt;   //!
   TBranch        *b_calib_jet_closest_ptrel_probe;   //!
   TBranch        *b_calib_jet_tag_pt;   //!
   TBranch        *b_calib_jet_tag_eta;   //!
   TBranch        *b_calib_jet_tag_phi;   //!
   TBranch        *b_calib_jet_tag_index;   //!
   TBranch        *b_calib_jet_tag_m;   //!
   TBranch        *b_calib_jet_dR_tag;   //!
   TBranch        *b_calib_jet_nJets20;   //!
   TBranch        *b_calib_jet_dR_probe;   //!
   TBranch        *b_calib_jet_closest_chargedfrac_probe;   //!
   TBranch        *b_calib_jet_ntrks500_probe;   //!
   TBranch        *b_calib_jet_n_bad_jets;   //!
   TBranch        *b_dilep_deta;   //!
   TBranch        *b_dilep_dphi;   //!
   TBranch        *b_dilep_match_2L1RoI_mu6_mu4;   //!
   TBranch        *b_dilep_match_2L1RoI_mu6_mu6;   //!
   TBranch        *b_dilep_mll;   //!
   TBranch        *b_dilep_pt;   //!
   TBranch        *b_energyDensity_central;   //!
   TBranch        *b_energyDensity_forward;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_lumiblock;   //!
   TBranch        *b_mcChannelNumber;   //!
   TBranch        *b_mcEventWeight;   //!
   TBranch        *b_n_Trks_PrimVtx;   //!
   TBranch        *b_probe_pt;   //!
   TBranch        *b_probe_eta;   //!
   TBranch        *b_probe_phi;   //!
   TBranch        *b_probe_index;   //!
   TBranch        *b_probe_CaloLRLikelihood;   //!
   TBranch        *b_probe_CaloMuonIDTag;   //!
   TBranch        *b_probe_EnergyLoss;   //!
   TBranch        *b_probe_LowPtBDT_PromptTagger;   //!
   TBranch        *b_probe_LowPtBDT_PromptVeto;   //!
   TBranch        *b_probe_MeasEnergyLoss;   //!
   TBranch        *b_probe_ParamEnergyLoss;   //!
   TBranch        *b_probe_PromptLeptonInput_DL1mu;   //!
   TBranch        *b_probe_PromptLeptonInput_DRlj;   //!
   TBranch        *b_probe_PromptLeptonInput_LepJetPtFrac;   //!
   TBranch        *b_probe_PromptLeptonInput_PtFrac;   //!
   TBranch        *b_probe_PromptLeptonInput_PtRel;   //!
   TBranch        *b_probe_PromptLeptonInput_PtVarCone30Rel;   //!
   TBranch        *b_probe_PromptLeptonInput_TopoEtCone30Rel;   //!
   TBranch        *b_probe_PromptLeptonInput_TrackJetNTrack;   //!
   TBranch        *b_probe_PromptLeptonInput_ip2;   //!
   TBranch        *b_probe_PromptLeptonInput_ip3;   //!
   TBranch        *b_probe_PromptLeptonInput_rnnip;   //!
   TBranch        *b_probe_PromptLeptonInput_sv1_jf_ntrkv;   //!
   TBranch        *b_probe_PromptLeptonIso;   //!
   TBranch        *b_probe_PromptLeptonVeto;   //!
   TBranch        *b_probe_allAuthors;   //!
   TBranch        *b_probe_author;   //!
   TBranch        *b_probe_combinedTrackOutBoundsPrecisionHits;   //!
   TBranch        *b_probe_d0;   //!
   TBranch        *b_probe_d0err;   //!
   TBranch        *b_probe_eta_exTP;   //!
   TBranch        *b_probe_etcone20;   //!
   TBranch        *b_probe_etcone30;   //!
   TBranch        *b_probe_etcone40;   //!
   TBranch        *b_probe_extendedClosePrecisionHits;   //!
   TBranch        *b_probe_extendedLargeHits;   //!
   TBranch        *b_probe_extendedLargeHoles;   //!
   TBranch        *b_probe_extendedOutBoundsPrecisionHits;   //!
   TBranch        *b_probe_extendedSmallHits;   //!
   TBranch        *b_probe_extendedSmallHoles;   //!
   TBranch        *b_probe_innerClosePrecisionHits;   //!
   TBranch        *b_probe_innerLargeHits;   //!
   TBranch        *b_probe_innerLargeHoles;   //!
   TBranch        *b_probe_innerOutBoundsPrecisionHits;   //!
   TBranch        *b_probe_innerSmallHits;   //!
   TBranch        *b_probe_innerSmallHoles;   //!
   TBranch        *b_probe_isEndcapGoodLayers;   //!
   TBranch        *b_probe_isSmallGoodSectors;   //!
   TBranch        *b_probe_middleClosePrecisionHits;   //!
   TBranch        *b_probe_middleLargeHits;   //!
   TBranch        *b_probe_middleLargeHoles;   //!
   TBranch        *b_probe_middleOutBoundsPrecisionHits;   //!
   TBranch        *b_probe_middleSmallHits;   //!
   TBranch        *b_probe_middleSmallHoles;   //!
   TBranch        *b_probe_momentumBalanceSignificance;   //!
   TBranch        *b_probe_nUnspoiledCscHits;   //!
   TBranch        *b_probe_neflowisol20;   //!
   TBranch        *b_probe_neflowisol30;   //!
   TBranch        *b_probe_neflowisol40;   //!
   TBranch        *b_probe_numberOfGoodPrecisionLayers;   //!
   TBranch        *b_probe_numberOfOutliersOnTrack;   //!
   TBranch        *b_probe_numberOfPhiHoleLayers;   //!
   TBranch        *b_probe_numberOfPhiLayers;   //!
   TBranch        *b_probe_numberOfPrecisionHoleLayers;   //!
   TBranch        *b_probe_numberOfPrecisionLayers;   //!
   TBranch        *b_probe_numberOfTriggerEtaHoleLayers;   //!
   TBranch        *b_probe_numberOfTriggerEtaLayers;   //!
   TBranch        *b_probe_outerClosePrecisionHits;   //!
   TBranch        *b_probe_outerLargeHits;   //!
   TBranch        *b_probe_outerLargeHoles;   //!
   TBranch        *b_probe_outerOutBoundsPrecisionHits;   //!
   TBranch        *b_probe_outerSmallHits;   //!
   TBranch        *b_probe_outerSmallHoles;   //!
   TBranch        *b_probe_phiLayer1Hits;   //!
   TBranch        *b_probe_phiLayer1Holes;   //!
   TBranch        *b_probe_phiLayer2Hits;   //!
   TBranch        *b_probe_phiLayer2Holes;   //!
   TBranch        *b_probe_phiLayer3Hits;   //!
   TBranch        *b_probe_phiLayer3Holes;   //!
   TBranch        *b_probe_phiLayer4Hits;   //!
   TBranch        *b_probe_phiLayer4Holes;   //!
   TBranch        *b_probe_phi_exTP;   //!
   TBranch        *b_probe_primarySector;   //!
   TBranch        *b_probe_ptcone20;   //!
   TBranch        *b_probe_ptcone20_LooseTTVA_pt500;   //!
   TBranch        *b_probe_ptcone20_TightTTVA_pt1000;   //!
   TBranch        *b_probe_ptcone20_TightTTVA_pt500;   //!
   TBranch        *b_probe_ptcone30;   //!
   TBranch        *b_probe_ptcone30_LooseTTVA_pt500;   //!
   TBranch        *b_probe_ptcone30_TightTTVA_pt1000;   //!
   TBranch        *b_probe_ptcone30_TightTTVA_pt500;   //!
   TBranch        *b_probe_ptcone40;   //!
   TBranch        *b_probe_ptcone40_LooseTTVA_pt500;   //!
   TBranch        *b_probe_ptcone40_TightTTVA_pt1000;   //!
   TBranch        *b_probe_ptcone40_TightTTVA_pt500;   //!
   TBranch        *b_probe_ptvarcone20;   //!
   TBranch        *b_probe_ptvarcone20_LooseTTVA_pt500;   //!
   TBranch        *b_probe_ptvarcone20_TightTTVA_pt1000;   //!
   TBranch        *b_probe_ptvarcone20_TightTTVA_pt500;   //!
   TBranch        *b_probe_ptvarcone30;   //!
   TBranch        *b_probe_ptvarcone30_LooseTTVA_pt500;   //!
   TBranch        *b_probe_ptvarcone30_TightTTVA_pt1000;   //!
   TBranch        *b_probe_ptvarcone30_TightTTVA_pt500;   //!
   TBranch        *b_probe_ptvarcone40;   //!
   TBranch        *b_probe_ptvarcone40_LooseTTVA_pt500;   //!
   TBranch        *b_probe_ptvarcone40_TightTTVA_pt1000;   //!
   TBranch        *b_probe_ptvarcone40_TightTTVA_pt500;   //!
   TBranch        *b_probe_q;   //!
   TBranch        *b_probe_scatteringCurvatureSignificance;   //!
   TBranch        *b_probe_scatteringNeighbourSignificance;   //!
   TBranch        *b_probe_secondarySector;   //!
   TBranch        *b_probe_segmentDeltaEta;   //!
   TBranch        *b_probe_segmentDeltaPhi;   //!
   TBranch        *b_probe_topocore;   //!
   TBranch        *b_probe_topoetcone20;   //!
   TBranch        *b_probe_topoetcone30;   //!
   TBranch        *b_probe_topoetcone40;   //!
   TBranch        *b_probe_truth_origin;   //!
   TBranch        *b_probe_truth_type;   //!
   TBranch        *b_probe_type;   //!
   TBranch        *b_probe_z0;   //!
   TBranch        *b_probe_Segment1_chi2;   //!
   TBranch        *b_probe_Segment1_chmbIdx;   //!
   TBranch        *b_probe_Segment1_nDoF;   //!
   TBranch        *b_probe_Segment2_chi2;   //!
   TBranch        *b_probe_Segment2_chmbIdx;   //!
   TBranch        *b_probe_Segment2_nDoF;   //!
   TBranch        *b_probe_chi2_PR;   //!
   TBranch        *b_probe_dRMatch_HLT_2mu10_OBSR;   //!
   TBranch        *b_probe_dRMatch_HLT_2mu10_nomucomb_OBSR;   //!
   TBranch        *b_probe_dRMatch_HLT_2mu14_OBSR;   //!
   TBranch        *b_probe_dRMatch_HLT_2mu14_nomucomb_OBSR;   //!
   TBranch        *b_probe_dRMatch_HLT_2mu15_OBSR;   //!
   TBranch        *b_probe_dRMatch_HLT_2mu15_nomucomb_OBSR;   //!
   TBranch        *b_probe_dRMatch_HLT_2mu6_bJpsimumu;   //!
   TBranch        *b_probe_dRMatch_HLT_2mu6_bJpsimumui_noL2;   //!
   TBranch        *b_probe_dRMatch_HLT_AllTriggers;   //!
   TBranch        *b_probe_dRMatch_HLT_mu10;   //!
   TBranch        *b_probe_dRMatch_HLT_mu10_RM;   //!
   TBranch        *b_probe_dRMatch_HLT_mu10_idperf;   //!
   TBranch        *b_probe_dRMatch_HLT_mu10_idperf_RM;   //!
   TBranch        *b_probe_dRMatch_HLT_mu10_msonly;   //!
   TBranch        *b_probe_dRMatch_HLT_mu10_msonly_RM;   //!
   TBranch        *b_probe_dRMatch_HLT_mu10_nomucomb_RM;   //!
   TBranch        *b_probe_dRMatch_HLT_mu13_mu13_idperf_Zmumu;   //!
   TBranch        *b_probe_dRMatch_HLT_mu14;   //!
   TBranch        *b_probe_dRMatch_HLT_mu14_RM;   //!
   TBranch        *b_probe_dRMatch_HLT_mu14_ivarloose;   //!
   TBranch        *b_probe_dRMatch_HLT_mu14_nomucomb_RM;   //!
   TBranch        *b_probe_dRMatch_HLT_mu15noL1;   //!
   TBranch        *b_probe_dRMatch_HLT_mu15noL1_RM;   //!
   TBranch        *b_probe_dRMatch_HLT_mu18;   //!
   TBranch        *b_probe_dRMatch_HLT_mu18_2mu0noL1_JpsimumuFS;   //!
   TBranch        *b_probe_dRMatch_HLT_mu18_2mu4noL1_JpsimumuL2;   //!
   TBranch        *b_probe_dRMatch_HLT_mu18_RM;   //!
   TBranch        *b_probe_dRMatch_HLT_mu18_mu8noL1_OBSR;   //!
   TBranch        *b_probe_dRMatch_HLT_mu18noL1;   //!
   TBranch        *b_probe_dRMatch_HLT_mu18noL1_RM;   //!
   TBranch        *b_probe_dRMatch_HLT_mu20;   //!
   TBranch        *b_probe_dRMatch_HLT_mu20_L1MU15_RM;   //!
   TBranch        *b_probe_dRMatch_HLT_mu20_RM;   //!
   TBranch        *b_probe_dRMatch_HLT_mu20_idperf;   //!
   TBranch        *b_probe_dRMatch_HLT_mu20_idperf_RM;   //!
   TBranch        *b_probe_dRMatch_HLT_mu20_iloose_L1MU15;   //!
   TBranch        *b_probe_dRMatch_HLT_mu20_ivarloose_L1MU15;   //!
   TBranch        *b_probe_dRMatch_HLT_mu20_msonly;   //!
   TBranch        *b_probe_dRMatch_HLT_mu20_msonly_RM;   //!
   TBranch        *b_probe_dRMatch_HLT_mu20_mu8noL1_OBSR;   //!
   TBranch        *b_probe_dRMatch_HLT_mu20_mu8noL1_calotag_0eta010_OBSR;   //!
   TBranch        *b_probe_dRMatch_HLT_mu22;   //!
   TBranch        *b_probe_dRMatch_HLT_mu22_RM;   //!
   TBranch        *b_probe_dRMatch_HLT_mu22_mu8noL1_OBSR;   //!
   TBranch        *b_probe_dRMatch_HLT_mu22_mu8noL1_RM;   //!
   TBranch        *b_probe_dRMatch_HLT_mu22_mu8noL1_calotag_0eta010_OBSR;   //!
   TBranch        *b_probe_dRMatch_HLT_mu24;   //!
   TBranch        *b_probe_dRMatch_HLT_mu24_2mu4noL1_OBSR;   //!
   TBranch        *b_probe_dRMatch_HLT_mu24_L1MU15_RM;   //!
   TBranch        *b_probe_dRMatch_HLT_mu24_RM;   //!
   TBranch        *b_probe_dRMatch_HLT_mu24_iloose;   //!
   TBranch        *b_probe_dRMatch_HLT_mu24_iloose_L1MU15;   //!
   TBranch        *b_probe_dRMatch_HLT_mu24_imedium;   //!
   TBranch        *b_probe_dRMatch_HLT_mu24_ivarloose;   //!
   TBranch        *b_probe_dRMatch_HLT_mu24_ivarloose_L1MU15;   //!
   TBranch        *b_probe_dRMatch_HLT_mu24_ivarmedium;   //!
   TBranch        *b_probe_dRMatch_HLT_mu24_mu10noL1_calotag_0eta010_OBSR;   //!
   TBranch        *b_probe_dRMatch_HLT_mu24_mu8noL1_OBSR;   //!
   TBranch        *b_probe_dRMatch_HLT_mu24_mu8noL1_calotag_0eta010_OBSR;   //!
   TBranch        *b_probe_dRMatch_HLT_mu26_RM;   //!
   TBranch        *b_probe_dRMatch_HLT_mu26_imedium;   //!
   TBranch        *b_probe_dRMatch_HLT_mu26_ivarmedium;   //!
   TBranch        *b_probe_dRMatch_HLT_mu26_mu10noL1_calotag_0eta010_OBSR;   //!
   TBranch        *b_probe_dRMatch_HLT_mu26_mu8noL1_OBSR;   //!
   TBranch        *b_probe_dRMatch_HLT_mu26_mu8noL1_calotag_0eta010_OBSR;   //!
   TBranch        *b_probe_dRMatch_HLT_mu28_imedium;   //!
   TBranch        *b_probe_dRMatch_HLT_mu28_ivarmedium;   //!
   TBranch        *b_probe_dRMatch_HLT_mu28_mu8noL1_calotag_0eta010_OBSR;   //!
   TBranch        *b_probe_dRMatch_HLT_mu4;   //!
   TBranch        *b_probe_dRMatch_HLT_mu40;   //!
   TBranch        *b_probe_dRMatch_HLT_mu4_RM;   //!
   TBranch        *b_probe_dRMatch_HLT_mu4_idperf;   //!
   TBranch        *b_probe_dRMatch_HLT_mu4_idperf_RM;   //!
   TBranch        *b_probe_dRMatch_HLT_mu4_msonly;   //!
   TBranch        *b_probe_dRMatch_HLT_mu4_msonly_RM;   //!
   TBranch        *b_probe_dRMatch_HLT_mu4_mu4_idperf_bJpsimumu_noid;   //!
   TBranch        *b_probe_dRMatch_HLT_mu50;   //!
   TBranch        *b_probe_dRMatch_HLT_mu6;   //!
   TBranch        *b_probe_dRMatch_HLT_mu60;   //!
   TBranch        *b_probe_dRMatch_HLT_mu60_0eta105_msonly_OBSR;   //!
   TBranch        *b_probe_dRMatch_HLT_mu6_RM;   //!
   TBranch        *b_probe_dRMatch_HLT_mu6_bJpsi_TrkPEB;   //!
   TBranch        *b_probe_dRMatch_HLT_mu6_bJpsi_Trkloose;   //!
   TBranch        *b_probe_dRMatch_HLT_mu6_idperf;   //!
   TBranch        *b_probe_dRMatch_HLT_mu6_idperf_RM;   //!
   TBranch        *b_probe_dRMatch_HLT_mu6_msonly;   //!
   TBranch        *b_probe_dRMatch_HLT_mu6_msonly_RM;   //!
   TBranch        *b_probe_dRMatch_HLT_mu6_mu4_bJpsimumu;   //!
   TBranch        *b_probe_dRMatch_HLT_mu6_mu4_bJpsimumu_noL2;   //!
   TBranch        *b_probe_dRMatch_HLT_mu8_RM;   //!
   TBranch        *b_probe_dRMatch_HLT_mu8noL1;   //!
   TBranch        *b_probe_dRMatch_HLT_noalg_L1MU;   //!
   TBranch        *b_probe_dRMatch_HLT_noalg_L1MU10;   //!
   TBranch        *b_probe_dRMatch_HLT_noalg_L1MU11;   //!
   TBranch        *b_probe_dRMatch_HLT_noalg_L1MU20;   //!
   TBranch        *b_probe_dRMatch_HLT_noalg_L1MU21;   //!
   TBranch        *b_probe_dRMatch_L1RoI_mu4;   //!
   TBranch        *b_probe_dRMatch_L1RoI_mu6;   //!
   TBranch        *b_probe_dRMatch_L1_MU10;   //!
   TBranch        *b_probe_dRMatch_L1_MU11;   //!
   TBranch        *b_probe_dRMatch_L1_MU15;   //!
   TBranch        *b_probe_dRMatch_L1_MU20;   //!
   TBranch        *b_probe_dRMatch_L1_MU21;   //!
   TBranch        *b_probe_dRMatch_L1_MU4;   //!
   TBranch        *b_probe_dRMatch_L1_MU6;   //!
   TBranch        *b_probe_etaMS;   //!
   TBranch        *b_probe_matched_CaloTag;   //!
   TBranch        *b_probe_matched_HLT_2mu6_bJpsimumu;   //!
   TBranch        *b_probe_matched_HLT_2mu6_bJpsimumui_noL2;   //!
   TBranch        *b_probe_matched_HLT_AllTriggers;   //!
   TBranch        *b_probe_matched_HLT_mu10;   //!
   TBranch        *b_probe_matched_HLT_mu10_idperf;   //!
   TBranch        *b_probe_matched_HLT_mu10_msonly;   //!
   TBranch        *b_probe_matched_HLT_mu13_mu13_idperf_Zmumu;   //!
   TBranch        *b_probe_matched_HLT_mu14;   //!
   TBranch        *b_probe_matched_HLT_mu14_ivarloose;   //!
   TBranch        *b_probe_matched_HLT_mu15noL1;   //!
   TBranch        *b_probe_matched_HLT_mu18;   //!
   TBranch        *b_probe_matched_HLT_mu18_2mu0noL1_JpsimumuFS;   //!
   TBranch        *b_probe_matched_HLT_mu18_2mu4noL1_JpsimumuL2;   //!
   TBranch        *b_probe_matched_HLT_mu18noL1;   //!
   TBranch        *b_probe_matched_HLT_mu20;   //!
   TBranch        *b_probe_matched_HLT_mu20_idperf;   //!
   TBranch        *b_probe_matched_HLT_mu20_iloose_L1MU15;   //!
   TBranch        *b_probe_matched_HLT_mu20_ivarloose_L1MU15;   //!
   TBranch        *b_probe_matched_HLT_mu20_msonly;   //!
   TBranch        *b_probe_matched_HLT_mu22;   //!
   TBranch        *b_probe_matched_HLT_mu24;   //!
   TBranch        *b_probe_matched_HLT_mu24_iloose;   //!
   TBranch        *b_probe_matched_HLT_mu24_iloose_L1MU15;   //!
   TBranch        *b_probe_matched_HLT_mu24_imedium;   //!
   TBranch        *b_probe_matched_HLT_mu24_ivarloose;   //!
   TBranch        *b_probe_matched_HLT_mu24_ivarloose_L1MU15;   //!
   TBranch        *b_probe_matched_HLT_mu24_ivarmedium;   //!
   TBranch        *b_probe_matched_HLT_mu26_imedium;   //!
   TBranch        *b_probe_matched_HLT_mu26_ivarmedium;   //!
   TBranch        *b_probe_matched_HLT_mu28_imedium;   //!
   TBranch        *b_probe_matched_HLT_mu28_ivarmedium;   //!
   TBranch        *b_probe_matched_HLT_mu4;   //!
   TBranch        *b_probe_matched_HLT_mu40;   //!
   TBranch        *b_probe_matched_HLT_mu4_idperf;   //!
   TBranch        *b_probe_matched_HLT_mu4_msonly;   //!
   TBranch        *b_probe_matched_HLT_mu4_mu4_idperf_bJpsimumu_noid;   //!
   TBranch        *b_probe_matched_HLT_mu50;   //!
   TBranch        *b_probe_matched_HLT_mu6;   //!
   TBranch        *b_probe_matched_HLT_mu60;   //!
   TBranch        *b_probe_matched_HLT_mu6_bJpsi_TrkPEB;   //!
   TBranch        *b_probe_matched_HLT_mu6_bJpsi_Trkloose;   //!
   TBranch        *b_probe_matched_HLT_mu6_idperf;   //!
   TBranch        *b_probe_matched_HLT_mu6_msonly;   //!
   TBranch        *b_probe_matched_HLT_mu6_mu4_bJpsimumu;   //!
   TBranch        *b_probe_matched_HLT_mu6_mu4_bJpsimumu_noL2;   //!
   TBranch        *b_probe_matched_HLT_mu8noL1;   //!
   TBranch        *b_probe_matched_HLT_noalg_L1MU;   //!
   TBranch        *b_probe_matched_HLT_noalg_L1MU10;   //!
   TBranch        *b_probe_matched_HLT_noalg_L1MU11;   //!
   TBranch        *b_probe_matched_HLT_noalg_L1MU20;   //!
   TBranch        *b_probe_matched_HLT_noalg_L1MU21;   //!
   TBranch        *b_probe_matched_HighPt;   //!
   TBranch        *b_probe_matched_IsoFCLoose;   //!
   TBranch        *b_probe_matched_IsoFCLoose_FixedRad;   //!
   TBranch        *b_probe_matched_IsoFCTight;   //!
   TBranch        *b_probe_matched_IsoFCTightTrackOnly;   //!
   TBranch        *b_probe_matched_IsoFCTightTrackOnly_FixedRad;   //!
   TBranch        *b_probe_matched_IsoFCTight_FixedRad;   //!
   TBranch        *b_probe_matched_IsoFixedCutHighPtTrackOnly;   //!
   TBranch        *b_probe_matched_IsoFixedCutPflowLoose;   //!
   TBranch        *b_probe_matched_IsoFixedCutPflowTight;   //!
   TBranch        *b_probe_matched_L1_MU10;   //!
   TBranch        *b_probe_matched_L1_MU11;   //!
   TBranch        *b_probe_matched_L1_MU15;   //!
   TBranch        *b_probe_matched_L1_MU20;   //!
   TBranch        *b_probe_matched_L1_MU21;   //!
   TBranch        *b_probe_matched_L1_MU4;   //!
   TBranch        *b_probe_matched_L1_MU6;   //!
   TBranch        *b_probe_matched_Loose;   //!
   TBranch        *b_probe_matched_LowPt;   //!
   TBranch        *b_probe_matched_Medium;   //!
   TBranch        *b_probe_matched_TRTCut;   //!
   TBranch        *b_probe_matched_Tight;   //!
   TBranch        *b_probe_nDof_PR;   //!
   TBranch        *b_probe_qoverp_CB;   //!
   TBranch        *b_probe_qoverp_ID;   //!
   TBranch        *b_probe_qoverp_ME;   //!
   TBranch        *b_probe_qoverp_MS;   //!
   TBranch        *b_probe_qoverp_err_CB;   //!
   TBranch        *b_probe_qoverp_err_ID;   //!
   TBranch        *b_probe_qoverp_err_ME;   //!
   TBranch        *b_probe_qoverp_err_MS;   //!
   TBranch        *b_probe_qoverp_err_modified;   //!
   TBranch        *b_probe_qoverp_modified;   //!
   TBranch        *b_probe_quality;   //!
   TBranch        *b_prwWeight;   //!
   TBranch        *b_randomLumiBlockNumber;   //!
   TBranch        *b_randomRunNumber;   //!
   TBranch        *b_runNumber;   //!
   TBranch        *b_tag_pt;   //!
   TBranch        *b_tag_eta;   //!
   TBranch        *b_tag_phi;   //!
   TBranch        *b_tag_index;   //!
   TBranch        *b_tag_d0;   //!
   TBranch        *b_tag_d0err;   //!
   TBranch        *b_tag_dR_exTP;   //!
   TBranch        *b_tag_deta_exTP;   //!
   TBranch        *b_tag_dphi_exTP;   //!
   TBranch        *b_tag_etcone20;   //!
   TBranch        *b_tag_etcone30;   //!
   TBranch        *b_tag_etcone40;   //!
   TBranch        *b_tag_matched_IsoFCLoose;   //!
   TBranch        *b_tag_matched_IsoFCLoose_FixedRad;   //!
   TBranch        *b_tag_matched_IsoFCTight;   //!
   TBranch        *b_tag_matched_IsoFCTightTrackOnly;   //!
   TBranch        *b_tag_matched_IsoFCTightTrackOnly_FixedRad;   //!
   TBranch        *b_tag_matched_IsoFCTight_FixedRad;   //!
   TBranch        *b_tag_matched_IsoFixedCutHighPtTrackOnly;   //!
   TBranch        *b_tag_matched_IsoFixedCutPflowLoose;   //!
   TBranch        *b_tag_matched_IsoFixedCutPflowTight;   //!
   TBranch        *b_tag_neflowisol20;   //!
   TBranch        *b_tag_neflowisol30;   //!
   TBranch        *b_tag_neflowisol40;   //!
   TBranch        *b_tag_ptcone20;   //!
   TBranch        *b_tag_ptcone20_LooseTTVA_pt500;   //!
   TBranch        *b_tag_ptcone20_TightTTVA_pt1000;   //!
   TBranch        *b_tag_ptcone20_TightTTVA_pt500;   //!
   TBranch        *b_tag_ptcone30;   //!
   TBranch        *b_tag_ptcone30_LooseTTVA_pt500;   //!
   TBranch        *b_tag_ptcone30_TightTTVA_pt1000;   //!
   TBranch        *b_tag_ptcone30_TightTTVA_pt500;   //!
   TBranch        *b_tag_ptcone40;   //!
   TBranch        *b_tag_ptcone40_LooseTTVA_pt500;   //!
   TBranch        *b_tag_ptcone40_TightTTVA_pt1000;   //!
   TBranch        *b_tag_ptcone40_TightTTVA_pt500;   //!
   TBranch        *b_tag_ptvarcone20;   //!
   TBranch        *b_tag_ptvarcone20_LooseTTVA_pt500;   //!
   TBranch        *b_tag_ptvarcone20_TightTTVA_pt1000;   //!
   TBranch        *b_tag_ptvarcone20_TightTTVA_pt500;   //!
   TBranch        *b_tag_ptvarcone30;   //!
   TBranch        *b_tag_ptvarcone30_LooseTTVA_pt500;   //!
   TBranch        *b_tag_ptvarcone30_TightTTVA_pt1000;   //!
   TBranch        *b_tag_ptvarcone30_TightTTVA_pt500;   //!
   TBranch        *b_tag_ptvarcone40;   //!
   TBranch        *b_tag_ptvarcone40_LooseTTVA_pt500;   //!
   TBranch        *b_tag_ptvarcone40_TightTTVA_pt1000;   //!
   TBranch        *b_tag_ptvarcone40_TightTTVA_pt500;   //!
   TBranch        *b_tag_q;   //!
   TBranch        *b_tag_topocore;   //!
   TBranch        *b_tag_topoetcone20;   //!
   TBranch        *b_tag_topoetcone30;   //!
   TBranch        *b_tag_topoetcone40;   //!
   TBranch        *b_tag_truth_origin;   //!
   TBranch        *b_tag_truth_type;   //!
   TBranch        *b_tag_z0;   //!
   TBranch        *b_tag_dRMatch_HLT_2mu10_OBSR;   //!
   TBranch        *b_tag_dRMatch_HLT_2mu10_nomucomb_OBSR;   //!
   TBranch        *b_tag_dRMatch_HLT_2mu14_OBSR;   //!
   TBranch        *b_tag_dRMatch_HLT_2mu14_nomucomb_OBSR;   //!
   TBranch        *b_tag_dRMatch_HLT_2mu15_OBSR;   //!
   TBranch        *b_tag_dRMatch_HLT_2mu15_nomucomb_OBSR;   //!
   TBranch        *b_tag_dRMatch_HLT_2mu6_bJpsimumu;   //!
   TBranch        *b_tag_dRMatch_HLT_2mu6_bJpsimumui_noL2;   //!
   TBranch        *b_tag_dRMatch_HLT_AllTriggers;   //!
   TBranch        *b_tag_dRMatch_HLT_mu10;   //!
   TBranch        *b_tag_dRMatch_HLT_mu10_RM;   //!
   TBranch        *b_tag_dRMatch_HLT_mu10_idperf;   //!
   TBranch        *b_tag_dRMatch_HLT_mu10_idperf_RM;   //!
   TBranch        *b_tag_dRMatch_HLT_mu10_msonly;   //!
   TBranch        *b_tag_dRMatch_HLT_mu10_msonly_RM;   //!
   TBranch        *b_tag_dRMatch_HLT_mu10_nomucomb_RM;   //!
   TBranch        *b_tag_dRMatch_HLT_mu13_mu13_idperf_Zmumu;   //!
   TBranch        *b_tag_dRMatch_HLT_mu14;   //!
   TBranch        *b_tag_dRMatch_HLT_mu14_RM;   //!
   TBranch        *b_tag_dRMatch_HLT_mu14_ivarloose;   //!
   TBranch        *b_tag_dRMatch_HLT_mu14_nomucomb_RM;   //!
   TBranch        *b_tag_dRMatch_HLT_mu15noL1;   //!
   TBranch        *b_tag_dRMatch_HLT_mu15noL1_RM;   //!
   TBranch        *b_tag_dRMatch_HLT_mu18;   //!
   TBranch        *b_tag_dRMatch_HLT_mu18_2mu0noL1_JpsimumuFS;   //!
   TBranch        *b_tag_dRMatch_HLT_mu18_2mu4noL1_JpsimumuL2;   //!
   TBranch        *b_tag_dRMatch_HLT_mu18_RM;   //!
   TBranch        *b_tag_dRMatch_HLT_mu18_mu8noL1_OBSR;   //!
   TBranch        *b_tag_dRMatch_HLT_mu18noL1;   //!
   TBranch        *b_tag_dRMatch_HLT_mu18noL1_RM;   //!
   TBranch        *b_tag_dRMatch_HLT_mu20;   //!
   TBranch        *b_tag_dRMatch_HLT_mu20_L1MU15_RM;   //!
   TBranch        *b_tag_dRMatch_HLT_mu20_RM;   //!
   TBranch        *b_tag_dRMatch_HLT_mu20_idperf;   //!
   TBranch        *b_tag_dRMatch_HLT_mu20_idperf_RM;   //!
   TBranch        *b_tag_dRMatch_HLT_mu20_iloose_L1MU15;   //!
   TBranch        *b_tag_dRMatch_HLT_mu20_ivarloose_L1MU15;   //!
   TBranch        *b_tag_dRMatch_HLT_mu20_msonly;   //!
   TBranch        *b_tag_dRMatch_HLT_mu20_msonly_RM;   //!
   TBranch        *b_tag_dRMatch_HLT_mu20_mu8noL1_OBSR;   //!
   TBranch        *b_tag_dRMatch_HLT_mu20_mu8noL1_calotag_0eta010_OBSR;   //!
   TBranch        *b_tag_dRMatch_HLT_mu22;   //!
   TBranch        *b_tag_dRMatch_HLT_mu22_RM;   //!
   TBranch        *b_tag_dRMatch_HLT_mu22_mu8noL1_OBSR;   //!
   TBranch        *b_tag_dRMatch_HLT_mu22_mu8noL1_RM;   //!
   TBranch        *b_tag_dRMatch_HLT_mu22_mu8noL1_calotag_0eta010_OBSR;   //!
   TBranch        *b_tag_dRMatch_HLT_mu24;   //!
   TBranch        *b_tag_dRMatch_HLT_mu24_2mu4noL1_OBSR;   //!
   TBranch        *b_tag_dRMatch_HLT_mu24_L1MU15_RM;   //!
   TBranch        *b_tag_dRMatch_HLT_mu24_RM;   //!
   TBranch        *b_tag_dRMatch_HLT_mu24_iloose;   //!
   TBranch        *b_tag_dRMatch_HLT_mu24_iloose_L1MU15;   //!
   TBranch        *b_tag_dRMatch_HLT_mu24_imedium;   //!
   TBranch        *b_tag_dRMatch_HLT_mu24_ivarloose;   //!
   TBranch        *b_tag_dRMatch_HLT_mu24_ivarloose_L1MU15;   //!
   TBranch        *b_tag_dRMatch_HLT_mu24_ivarmedium;   //!
   TBranch        *b_tag_dRMatch_HLT_mu24_mu10noL1_calotag_0eta010_OBSR;   //!
   TBranch        *b_tag_dRMatch_HLT_mu24_mu8noL1_OBSR;   //!
   TBranch        *b_tag_dRMatch_HLT_mu24_mu8noL1_calotag_0eta010_OBSR;   //!
   TBranch        *b_tag_dRMatch_HLT_mu26_RM;   //!
   TBranch        *b_tag_dRMatch_HLT_mu26_imedium;   //!
   TBranch        *b_tag_dRMatch_HLT_mu26_ivarmedium;   //!
   TBranch        *b_tag_dRMatch_HLT_mu26_mu10noL1_calotag_0eta010_OBSR;   //!
   TBranch        *b_tag_dRMatch_HLT_mu26_mu8noL1_OBSR;   //!
   TBranch        *b_tag_dRMatch_HLT_mu26_mu8noL1_calotag_0eta010_OBSR;   //!
   TBranch        *b_tag_dRMatch_HLT_mu28_imedium;   //!
   TBranch        *b_tag_dRMatch_HLT_mu28_ivarmedium;   //!
   TBranch        *b_tag_dRMatch_HLT_mu28_mu8noL1_calotag_0eta010_OBSR;   //!
   TBranch        *b_tag_dRMatch_HLT_mu4;   //!
   TBranch        *b_tag_dRMatch_HLT_mu40;   //!
   TBranch        *b_tag_dRMatch_HLT_mu4_RM;   //!
   TBranch        *b_tag_dRMatch_HLT_mu4_idperf;   //!
   TBranch        *b_tag_dRMatch_HLT_mu4_idperf_RM;   //!
   TBranch        *b_tag_dRMatch_HLT_mu4_msonly;   //!
   TBranch        *b_tag_dRMatch_HLT_mu4_msonly_RM;   //!
   TBranch        *b_tag_dRMatch_HLT_mu4_mu4_idperf_bJpsimumu_noid;   //!
   TBranch        *b_tag_dRMatch_HLT_mu50;   //!
   TBranch        *b_tag_dRMatch_HLT_mu6;   //!
   TBranch        *b_tag_dRMatch_HLT_mu60;   //!
   TBranch        *b_tag_dRMatch_HLT_mu60_0eta105_msonly_OBSR;   //!
   TBranch        *b_tag_dRMatch_HLT_mu6_RM;   //!
   TBranch        *b_tag_dRMatch_HLT_mu6_bJpsi_TrkPEB;   //!
   TBranch        *b_tag_dRMatch_HLT_mu6_bJpsi_Trkloose;   //!
   TBranch        *b_tag_dRMatch_HLT_mu6_idperf;   //!
   TBranch        *b_tag_dRMatch_HLT_mu6_idperf_RM;   //!
   TBranch        *b_tag_dRMatch_HLT_mu6_msonly;   //!
   TBranch        *b_tag_dRMatch_HLT_mu6_msonly_RM;   //!
   TBranch        *b_tag_dRMatch_HLT_mu6_mu4_bJpsimumu;   //!
   TBranch        *b_tag_dRMatch_HLT_mu6_mu4_bJpsimumu_noL2;   //!
   TBranch        *b_tag_dRMatch_HLT_mu8_RM;   //!
   TBranch        *b_tag_dRMatch_HLT_mu8noL1;   //!
   TBranch        *b_tag_dRMatch_HLT_noalg_L1MU;   //!
   TBranch        *b_tag_dRMatch_HLT_noalg_L1MU10;   //!
   TBranch        *b_tag_dRMatch_HLT_noalg_L1MU11;   //!
   TBranch        *b_tag_dRMatch_HLT_noalg_L1MU20;   //!
   TBranch        *b_tag_dRMatch_HLT_noalg_L1MU21;   //!
   TBranch        *b_tag_dRMatch_L1RoI_mu6;   //!
   TBranch        *b_tag_matched_HLT_2mu6_bJpsimumu;   //!
   TBranch        *b_tag_matched_HLT_2mu6_bJpsimumui_noL2;   //!
   TBranch        *b_tag_matched_HLT_AllTriggers;   //!
   TBranch        *b_tag_matched_HLT_mu10;   //!
   TBranch        *b_tag_matched_HLT_mu10_idperf;   //!
   TBranch        *b_tag_matched_HLT_mu10_msonly;   //!
   TBranch        *b_tag_matched_HLT_mu13_mu13_idperf_Zmumu;   //!
   TBranch        *b_tag_matched_HLT_mu14;   //!
   TBranch        *b_tag_matched_HLT_mu14_ivarloose;   //!
   TBranch        *b_tag_matched_HLT_mu15noL1;   //!
   TBranch        *b_tag_matched_HLT_mu18;   //!
   TBranch        *b_tag_matched_HLT_mu18_2mu0noL1_JpsimumuFS;   //!
   TBranch        *b_tag_matched_HLT_mu18_2mu4noL1_JpsimumuL2;   //!
   TBranch        *b_tag_matched_HLT_mu18noL1;   //!
   TBranch        *b_tag_matched_HLT_mu20;   //!
   TBranch        *b_tag_matched_HLT_mu20_idperf;   //!
   TBranch        *b_tag_matched_HLT_mu20_iloose_L1MU15;   //!
   TBranch        *b_tag_matched_HLT_mu20_ivarloose_L1MU15;   //!
   TBranch        *b_tag_matched_HLT_mu20_msonly;   //!
   TBranch        *b_tag_matched_HLT_mu22;   //!
   TBranch        *b_tag_matched_HLT_mu24;   //!
   TBranch        *b_tag_matched_HLT_mu24_iloose;   //!
   TBranch        *b_tag_matched_HLT_mu24_iloose_L1MU15;   //!
   TBranch        *b_tag_matched_HLT_mu24_imedium;   //!
   TBranch        *b_tag_matched_HLT_mu24_ivarloose;   //!
   TBranch        *b_tag_matched_HLT_mu24_ivarloose_L1MU15;   //!
   TBranch        *b_tag_matched_HLT_mu24_ivarmedium;   //!
   TBranch        *b_tag_matched_HLT_mu26_imedium;   //!
   TBranch        *b_tag_matched_HLT_mu26_ivarmedium;   //!
   TBranch        *b_tag_matched_HLT_mu28_imedium;   //!
   TBranch        *b_tag_matched_HLT_mu28_ivarmedium;   //!
   TBranch        *b_tag_matched_HLT_mu4;   //!
   TBranch        *b_tag_matched_HLT_mu40;   //!
   TBranch        *b_tag_matched_HLT_mu4_idperf;   //!
   TBranch        *b_tag_matched_HLT_mu4_msonly;   //!
   TBranch        *b_tag_matched_HLT_mu4_mu4_idperf_bJpsimumu_noid;   //!
   TBranch        *b_tag_matched_HLT_mu50;   //!
   TBranch        *b_tag_matched_HLT_mu6;   //!
   TBranch        *b_tag_matched_HLT_mu60;   //!
   TBranch        *b_tag_matched_HLT_mu6_bJpsi_TrkPEB;   //!
   TBranch        *b_tag_matched_HLT_mu6_bJpsi_Trkloose;   //!
   TBranch        *b_tag_matched_HLT_mu6_idperf;   //!
   TBranch        *b_tag_matched_HLT_mu6_msonly;   //!
   TBranch        *b_tag_matched_HLT_mu6_mu4_bJpsimumu;   //!
   TBranch        *b_tag_matched_HLT_mu6_mu4_bJpsimumu_noL2;   //!
   TBranch        *b_tag_matched_HLT_mu8noL1;   //!
   TBranch        *b_tag_matched_HLT_noalg_L1MU;   //!
   TBranch        *b_tag_matched_HLT_noalg_L1MU10;   //!
   TBranch        *b_tag_matched_HLT_noalg_L1MU11;   //!
   TBranch        *b_tag_matched_HLT_noalg_L1MU20;   //!
   TBranch        *b_tag_matched_HLT_noalg_L1MU21;   //!
   TBranch        *b_trackMET_abs;   //!
   TBranch        *b_trackMET_phi;   //!
   TBranch        *b_trig_HLT_2mu10_OBSR;   //!
   TBranch        *b_trig_HLT_2mu10_nomucomb_OBSR;   //!
   TBranch        *b_trig_HLT_2mu14_OBSR;   //!
   TBranch        *b_trig_HLT_2mu14_nomucomb_OBSR;   //!
   TBranch        *b_trig_HLT_2mu15_OBSR;   //!
   TBranch        *b_trig_HLT_2mu15_nomucomb_OBSR;   //!
   TBranch        *b_trig_HLT_2mu6_bJpsimumu;   //!
   TBranch        *b_trig_HLT_2mu6_bJpsimumui_noL2;   //!
   TBranch        *b_trig_HLT_AllTriggers;   //!
   TBranch        *b_trig_HLT_mu10;   //!
   TBranch        *b_trig_HLT_mu10_RM;   //!
   TBranch        *b_trig_HLT_mu10_idperf;   //!
   TBranch        *b_trig_HLT_mu10_idperf_RM;   //!
   TBranch        *b_trig_HLT_mu10_msonly;   //!
   TBranch        *b_trig_HLT_mu10_msonly_RM;   //!
   TBranch        *b_trig_HLT_mu10_nomucomb_RM;   //!
   TBranch        *b_trig_HLT_mu13_mu13_idperf_Zmumu;   //!
   TBranch        *b_trig_HLT_mu14;   //!
   TBranch        *b_trig_HLT_mu14_RM;   //!
   TBranch        *b_trig_HLT_mu14_ivarloose;   //!
   TBranch        *b_trig_HLT_mu14_nomucomb_RM;   //!
   TBranch        *b_trig_HLT_mu15noL1;   //!
   TBranch        *b_trig_HLT_mu15noL1_RM;   //!
   TBranch        *b_trig_HLT_mu18;   //!
   TBranch        *b_trig_HLT_mu18_2mu0noL1_JpsimumuFS;   //!
   TBranch        *b_trig_HLT_mu18_2mu4noL1_JpsimumuL2;   //!
   TBranch        *b_trig_HLT_mu18_RM;   //!
   TBranch        *b_trig_HLT_mu18_mu8noL1_OBSR;   //!
   TBranch        *b_trig_HLT_mu18noL1;   //!
   TBranch        *b_trig_HLT_mu18noL1_RM;   //!
   TBranch        *b_trig_HLT_mu20;   //!
   TBranch        *b_trig_HLT_mu20_L1MU15_RM;   //!
   TBranch        *b_trig_HLT_mu20_RM;   //!
   TBranch        *b_trig_HLT_mu20_idperf;   //!
   TBranch        *b_trig_HLT_mu20_idperf_RM;   //!
   TBranch        *b_trig_HLT_mu20_iloose_L1MU15;   //!
   TBranch        *b_trig_HLT_mu20_ivarloose_L1MU15;   //!
   TBranch        *b_trig_HLT_mu20_msonly;   //!
   TBranch        *b_trig_HLT_mu20_msonly_RM;   //!
   TBranch        *b_trig_HLT_mu20_mu8noL1_OBSR;   //!
   TBranch        *b_trig_HLT_mu20_mu8noL1_calotag_0eta010_OBSR;   //!
   TBranch        *b_trig_HLT_mu22;   //!
   TBranch        *b_trig_HLT_mu22_RM;   //!
   TBranch        *b_trig_HLT_mu22_mu8noL1_OBSR;   //!
   TBranch        *b_trig_HLT_mu22_mu8noL1_RM;   //!
   TBranch        *b_trig_HLT_mu22_mu8noL1_calotag_0eta010_OBSR;   //!
   TBranch        *b_trig_HLT_mu24;   //!
   TBranch        *b_trig_HLT_mu24_2mu4noL1_OBSR;   //!
   TBranch        *b_trig_HLT_mu24_L1MU15_RM;   //!
   TBranch        *b_trig_HLT_mu24_RM;   //!
   TBranch        *b_trig_HLT_mu24_iloose;   //!
   TBranch        *b_trig_HLT_mu24_iloose_L1MU15;   //!
   TBranch        *b_trig_HLT_mu24_imedium;   //!
   TBranch        *b_trig_HLT_mu24_ivarloose;   //!
   TBranch        *b_trig_HLT_mu24_ivarloose_L1MU15;   //!
   TBranch        *b_trig_HLT_mu24_ivarmedium;   //!
   TBranch        *b_trig_HLT_mu24_mu10noL1_calotag_0eta010_OBSR;   //!
   TBranch        *b_trig_HLT_mu24_mu8noL1_OBSR;   //!
   TBranch        *b_trig_HLT_mu24_mu8noL1_calotag_0eta010_OBSR;   //!
   TBranch        *b_trig_HLT_mu26_RM;   //!
   TBranch        *b_trig_HLT_mu26_imedium;   //!
   TBranch        *b_trig_HLT_mu26_ivarmedium;   //!
   TBranch        *b_trig_HLT_mu26_mu10noL1_calotag_0eta010_OBSR;   //!
   TBranch        *b_trig_HLT_mu26_mu8noL1_OBSR;   //!
   TBranch        *b_trig_HLT_mu26_mu8noL1_calotag_0eta010_OBSR;   //!
   TBranch        *b_trig_HLT_mu28_imedium;   //!
   TBranch        *b_trig_HLT_mu28_ivarmedium;   //!
   TBranch        *b_trig_HLT_mu28_mu8noL1_calotag_0eta010_OBSR;   //!
   TBranch        *b_trig_HLT_mu4;   //!
   TBranch        *b_trig_HLT_mu40;   //!
   TBranch        *b_trig_HLT_mu4_RM;   //!
   TBranch        *b_trig_HLT_mu4_idperf;   //!
   TBranch        *b_trig_HLT_mu4_idperf_RM;   //!
   TBranch        *b_trig_HLT_mu4_msonly;   //!
   TBranch        *b_trig_HLT_mu4_msonly_RM;   //!
   TBranch        *b_trig_HLT_mu4_mu4_idperf_bJpsimumu_noid;   //!
   TBranch        *b_trig_HLT_mu50;   //!
   TBranch        *b_trig_HLT_mu6;   //!
   TBranch        *b_trig_HLT_mu60;   //!
   TBranch        *b_trig_HLT_mu60_0eta105_msonly_OBSR;   //!
   TBranch        *b_trig_HLT_mu6_RM;   //!
   TBranch        *b_trig_HLT_mu6_bJpsi_TrkPEB;   //!
   TBranch        *b_trig_HLT_mu6_bJpsi_Trkloose;   //!
   TBranch        *b_trig_HLT_mu6_idperf;   //!
   TBranch        *b_trig_HLT_mu6_idperf_RM;   //!
   TBranch        *b_trig_HLT_mu6_msonly;   //!
   TBranch        *b_trig_HLT_mu6_msonly_RM;   //!
   TBranch        *b_trig_HLT_mu6_mu4_bJpsimumu;   //!
   TBranch        *b_trig_HLT_mu6_mu4_bJpsimumu_noL2;   //!
   TBranch        *b_trig_HLT_mu8_RM;   //!
   TBranch        *b_trig_HLT_mu8noL1;   //!
   TBranch        *b_trig_HLT_noalg_L1MU;   //!
   TBranch        *b_trig_HLT_noalg_L1MU10;   //!
   TBranch        *b_trig_HLT_noalg_L1MU11;   //!
   TBranch        *b_trig_HLT_noalg_L1MU20;   //!
   TBranch        *b_trig_HLT_noalg_L1MU21;   //!
   TBranch        *b_vertex_density;   //!

   PBClassTrig(TTree *tree=0, TString Input="/home/petr/Analysis_iwona/SFAnalysis/rootfiles/muontp_mc_pp.root", TString NameOfTree="TPTree_MuonProbe_OC_LooseProbes_noProbeIP", TString PathToTree="ZmumuTPMuon/Trees/MuonProbe_OC_LooseProbes_noProbeIP");
   virtual ~PBClassTrig();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop(TString Output="out.root", TString Tag="");
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif
