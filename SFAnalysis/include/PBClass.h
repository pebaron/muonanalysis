//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Apr  2 17:03:06 2019 by ROOT version 5.34/30
// from TTree TPTree_IDProbes_OC/TP
// found on file: muontp_mc_pp.root
//////////////////////////////////////////////////////////

#ifndef PBClass_h
#define PBClass_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

// Fixed size dimensions of array or collections stored in the TTree if any.

class PBClass {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   Int_t           BCID;
   Float_t         EventBadMuonVetoVar_HighPt_ID;
   Float_t         EventBadMuonVetoVar_HighPt_ME;
   Float_t         EventBadMuonVetoVar_Loose_ID;
   Float_t         EventBadMuonVetoVar_Loose_ME;
   Float_t         EventBadMuonVetoVar_Medium_ID;
   Float_t         EventBadMuonVetoVar_Medium_ME;
   Float_t         EventBadMuonVetoVar_Tight_ID;
   Float_t         EventBadMuonVetoVar_Tight_ME;
   Float_t         EventBadMuonVetoVar_VeryLoose_ID;
   Float_t         EventBadMuonVetoVar_VeryLoose_ME;
   Int_t           PV_n;
   Float_t         actual_mu;
   Float_t         average_mu;
   Float_t         calib_jet_probe_pt;
   Float_t         calib_jet_probe_eta;
   Float_t         calib_jet_probe_phi;
   UChar_t         calib_jet_probe_index;
   Float_t         calib_jet_probe_m;
   Float_t         calib_jet_probe_emfrac;
   Float_t         calib_jet_probe_jvt;
   Float_t         calib_jet_closest_ptrel_probe;
   Float_t         calib_jet_tag_pt;
   Float_t         calib_jet_tag_eta;
   Float_t         calib_jet_tag_phi;
   UChar_t         calib_jet_tag_index;
   Float_t         calib_jet_tag_m;
   Float_t         calib_jet_dR_tag;
   UInt_t          calib_jet_nJets20;
   Float_t         calib_jet_dR_probe;
   Float_t         calib_jet_closest_chargedfrac_probe;
   UInt_t          calib_jet_ntrks500_probe;
   UInt_t          calib_jet_n_bad_jets;
   Float_t         dilep_deta;
   Float_t         dilep_dphi;
   Float_t         dilep_mll;
   Float_t         dilep_pt;
   Float_t         energyDensity_central;
   Float_t         energyDensity_forward;
   ULong64_t       eventNumber;
   UInt_t          lumiblock;
   Float_t         matchobj_muon_CaloLRLikelihood;
   Int_t           matchobj_muon_CaloMuonIDTag;
   Float_t         matchobj_muon_EnergyLoss;
   Float_t         matchobj_muon_MeasEnergyLoss;
   Float_t         matchobj_muon_ParamEnergyLoss;
   UShort_t        matchobj_muon_allAuthors;
   UShort_t        matchobj_muon_author;
   Float_t         matchobj_muon_momentumBalanceSignificance;
   Float_t         matchobj_muon_scatteringCurvatureSignificance;
   Float_t         matchobj_muon_scatteringNeighbourSignificance;
   Float_t         matchobj_muon_segmentDeltaEta;
   Float_t         matchobj_muon_segmentDeltaPhi;
   UShort_t        matchobj_muon_type;
   Int_t           mcChannelNumber;
   Double_t        mcEventWeight;
   UInt_t          n_Trks_PrimVtx;
   Float_t         probe_pt;
   Float_t         probe_eta;
   Float_t         probe_phi;
   UChar_t         probe_index;
   Float_t         probe_d0;
   Float_t         probe_d0err;
   Float_t         probe_eta_exTP;
   Float_t         probe_etcone20;
   Float_t         probe_etcone30;
   Float_t         probe_etcone40;
   Float_t         probe_neflowisol20;
   Float_t         probe_neflowisol30;
   Float_t         probe_neflowisol40;
   Float_t         probe_phi_exTP;
   Float_t         probe_ptcone20;
   Float_t         probe_ptcone20_LooseTTVA_pt500;
   Float_t         probe_ptcone20_TightTTVA_pt1000;
   Float_t         probe_ptcone20_TightTTVA_pt500;
   Float_t         probe_ptcone30;
   Float_t         probe_ptcone30_LooseTTVA_pt500;
   Float_t         probe_ptcone30_TightTTVA_pt1000;
   Float_t         probe_ptcone30_TightTTVA_pt500;
   Float_t         probe_ptcone40;
   Float_t         probe_ptcone40_LooseTTVA_pt500;
   Float_t         probe_ptcone40_TightTTVA_pt1000;
   Float_t         probe_ptcone40_TightTTVA_pt500;
   Float_t         probe_ptvarcone20;
   Float_t         probe_ptvarcone20_LooseTTVA_pt500;
   Float_t         probe_ptvarcone20_TightTTVA_pt1000;
   Float_t         probe_ptvarcone20_TightTTVA_pt500;
   Float_t         probe_ptvarcone30;
   Float_t         probe_ptvarcone30_LooseTTVA_pt500;
   Float_t         probe_ptvarcone30_TightTTVA_pt1000;
   Float_t         probe_ptvarcone30_TightTTVA_pt500;
   Float_t         probe_ptvarcone40;
   Float_t         probe_ptvarcone40_LooseTTVA_pt500;
   Float_t         probe_ptvarcone40_TightTTVA_pt1000;
   Float_t         probe_ptvarcone40_TightTTVA_pt500;
   Int_t           probe_q;
   Float_t         probe_topocore;
   Float_t         probe_topoetcone20;
   Float_t         probe_topoetcone30;
   Float_t         probe_topoetcone40;
   Int_t           probe_truth_origin;
   Int_t           probe_truth_type;
   Float_t         probe_z0;
   Float_t         probe_dRMatch_CaloTag;
   Float_t         probe_dRMatch_CombinedMuons;
   Float_t         probe_dRMatch_HighPtMuons;
   Float_t         probe_dRMatch_LooseMuons;
   Float_t         probe_dRMatch_LooseMuons_noCaloTag;
   Float_t         probe_dRMatch_LowPtMuons;
   Float_t         probe_dRMatch_MediumMuons;
   Float_t         probe_dRMatch_StandaloneMuons;
   Float_t         probe_dRMatch_TightMuons;
   Bool_t          probe_matched_CaloTag;
   Bool_t          probe_matched_CaloTag_PtrMatching;
   Bool_t          probe_matched_CombinedMuons;
   Bool_t          probe_matched_HighPtMuons;
   Bool_t          probe_matched_HighPtMuons_PtrMatching;
   Bool_t          probe_matched_IsoFCLoose;
   Bool_t          probe_matched_IsoFCLoose_FixedRad;
   Bool_t          probe_matched_IsoFCTight;
   Bool_t          probe_matched_IsoFCTightTrackOnly;
   Bool_t          probe_matched_IsoFCTightTrackOnly_FixedRad;
   Bool_t          probe_matched_IsoFCTight_FixedRad;
   Bool_t          probe_matched_IsoFixedCutHighPtTrackOnly;
   Bool_t          probe_matched_LooseMuons;
   Bool_t          probe_matched_LooseMuons_PtrMatching;
   Bool_t          probe_matched_LooseMuons_noCaloTag;
   Bool_t          probe_matched_LooseMuons_noCaloTag_PtrMatching;
   Bool_t          probe_matched_LowPtMuons;
   Bool_t          probe_matched_LowPtMuons_PtrMatching;
   Bool_t          probe_matched_MediumMuons;
   Bool_t          probe_matched_MediumMuons_PtrMatching;
   Bool_t          probe_matched_StandaloneMuons;
   Bool_t          probe_matched_StandaloneMuons_PtrMatching;
   Bool_t          probe_matched_TRTCut;
   Bool_t          probe_matched_TightMuons;
   Bool_t          probe_matched_TightMuons_PtrMatching;
   Bool_t          probe_matched_IDTracks; //added by PB
   Bool_t          probe_matched_HLT_mu10; //added by PB
   Bool_t          probe_quality; //added by PB
   Bool_t          probe_matched_HLT_mu20_iloose_L1MU15; //added by PB
   Double_t        prwWeight;
   UInt_t          randomLumiBlockNumber;
   UInt_t          randomRunNumber;
   UInt_t          runNumber;
   Float_t         tag_pt;
   Float_t         tag_eta;
   Float_t         tag_phi;
   UChar_t         tag_index;
   Float_t         tag_d0;
   Float_t         tag_d0err;
   Float_t         tag_dR_exTP;
   Float_t         tag_deta_exTP;
   Float_t         tag_dphi_exTP;
   Float_t         tag_etcone20;
   Float_t         tag_etcone30;
   Float_t         tag_etcone40;
   Bool_t          tag_matched_IsoFCLoose;
   Bool_t          tag_matched_IsoFCLoose_FixedRad;
   Bool_t          tag_matched_IsoFCTight;
   Bool_t          tag_matched_IsoFCTightTrackOnly;
   Bool_t          tag_matched_IsoFCTightTrackOnly_FixedRad;
   Bool_t          tag_matched_IsoFCTight_FixedRad;
   Bool_t          tag_matched_IsoFixedCutHighPtTrackOnly;
   Float_t         tag_neflowisol20;
   Float_t         tag_neflowisol30;
   Float_t         tag_neflowisol40;
   Float_t         tag_ptcone20;
   Float_t         tag_ptcone20_LooseTTVA_pt500;
   Float_t         tag_ptcone20_TightTTVA_pt1000;
   Float_t         tag_ptcone20_TightTTVA_pt500;
   Float_t         tag_ptcone30;
   Float_t         tag_ptcone30_LooseTTVA_pt500;
   Float_t         tag_ptcone30_TightTTVA_pt1000;
   Float_t         tag_ptcone30_TightTTVA_pt500;
   Float_t         tag_ptcone40;
   Float_t         tag_ptcone40_LooseTTVA_pt500;
   Float_t         tag_ptcone40_TightTTVA_pt1000;
   Float_t         tag_ptcone40_TightTTVA_pt500;
   Float_t         tag_ptvarcone20;
   Float_t         tag_ptvarcone20_LooseTTVA_pt500;
   Float_t         tag_ptvarcone20_TightTTVA_pt1000;
   Float_t         tag_ptvarcone20_TightTTVA_pt500;
   Float_t         tag_ptvarcone30;
   Float_t         tag_ptvarcone30_LooseTTVA_pt500;
   Float_t         tag_ptvarcone30_TightTTVA_pt1000;
   Float_t         tag_ptvarcone30_TightTTVA_pt500;
   Float_t         tag_ptvarcone40;
   Float_t         tag_ptvarcone40_LooseTTVA_pt500;
   Float_t         tag_ptvarcone40_TightTTVA_pt1000;
   Float_t         tag_ptvarcone40_TightTTVA_pt500;
   Int_t           tag_q;
   Float_t         tag_topocore;
   Float_t         tag_topoetcone20;
   Float_t         tag_topoetcone30;
   Float_t         tag_topoetcone40;
   Int_t           tag_truth_origin;
   Int_t           tag_truth_type;
   Float_t         tag_z0;
   Float_t         tag_dRMatch_HLT_2mu6_bJpsimumu;
   Float_t         tag_dRMatch_HLT_2mu6_bJpsimumui_noL2;
   Float_t         tag_dRMatch_HLT_AllTriggers;
   Float_t         tag_dRMatch_HLT_mu10;
   Float_t         tag_dRMatch_HLT_mu10_idperf;
   Float_t         tag_dRMatch_HLT_mu10_msonly;
   Float_t         tag_dRMatch_HLT_mu13_mu13_idperf_Zmumu;
   Float_t         tag_dRMatch_HLT_mu14;
   Float_t         tag_dRMatch_HLT_mu14_ivarloose;
   Float_t         tag_dRMatch_HLT_mu15noL1;
   Float_t         tag_dRMatch_HLT_mu18;
   Float_t         tag_dRMatch_HLT_mu18_2mu0noL1_JpsimumuFS;
   Float_t         tag_dRMatch_HLT_mu18_2mu4noL1_JpsimumuL2;
   Float_t         tag_dRMatch_HLT_mu18noL1;
   Float_t         tag_dRMatch_HLT_mu20;
   Float_t         tag_dRMatch_HLT_mu20_idperf;
   Float_t         tag_dRMatch_HLT_mu20_iloose_L1MU15;
   Float_t         tag_dRMatch_HLT_mu20_ivarloose_L1MU15;
   Float_t         tag_dRMatch_HLT_mu20_msonly;
   Float_t         tag_dRMatch_HLT_mu22;
   Float_t         tag_dRMatch_HLT_mu24;
   Float_t         tag_dRMatch_HLT_mu24_iloose;
   Float_t         tag_dRMatch_HLT_mu24_iloose_L1MU15;
   Float_t         tag_dRMatch_HLT_mu24_imedium;
   Float_t         tag_dRMatch_HLT_mu24_ivarloose;
   Float_t         tag_dRMatch_HLT_mu24_ivarloose_L1MU15;
   Float_t         tag_dRMatch_HLT_mu24_ivarmedium;
   Float_t         tag_dRMatch_HLT_mu26_imedium;
   Float_t         tag_dRMatch_HLT_mu26_ivarmedium;
   Float_t         tag_dRMatch_HLT_mu28_imedium;
   Float_t         tag_dRMatch_HLT_mu28_ivarmedium;
   Float_t         tag_dRMatch_HLT_mu4;
   Float_t         tag_dRMatch_HLT_mu40;
   Float_t         tag_dRMatch_HLT_mu4_idperf;
   Float_t         tag_dRMatch_HLT_mu4_msonly;
   Float_t         tag_dRMatch_HLT_mu4_mu4_idperf_bJpsimumu_noid;
   Float_t         tag_dRMatch_HLT_mu50;
   Float_t         tag_dRMatch_HLT_mu6;
   Float_t         tag_dRMatch_HLT_mu60;
   Float_t         tag_dRMatch_HLT_mu6_bJpsi_TrkPEB;
   Float_t         tag_dRMatch_HLT_mu6_bJpsi_Trkloose;
   Float_t         tag_dRMatch_HLT_mu6_idperf;
   Float_t         tag_dRMatch_HLT_mu6_msonly;
   Float_t         tag_dRMatch_HLT_mu6_mu4_bJpsimumu;
   Float_t         tag_dRMatch_HLT_mu6_mu4_bJpsimumu_noL2;
   Float_t         tag_dRMatch_HLT_mu8noL1;
   Float_t         tag_dRMatch_HLT_noalg_L1MU;
   Float_t         tag_dRMatch_HLT_noalg_L1MU10;
   Float_t         tag_dRMatch_HLT_noalg_L1MU11;
   Float_t         tag_dRMatch_HLT_noalg_L1MU20;
   Float_t         tag_dRMatch_HLT_noalg_L1MU21;
   Bool_t          tag_matched_HLT_2mu6_bJpsimumu;
   Bool_t          tag_matched_HLT_2mu6_bJpsimumui_noL2;
   Bool_t          tag_matched_HLT_AllTriggers;
   Bool_t          tag_matched_HLT_mu10;
   Bool_t          tag_matched_HLT_mu10_idperf;
   Bool_t          tag_matched_HLT_mu10_msonly;
   Bool_t          tag_matched_HLT_mu13_mu13_idperf_Zmumu;
   Bool_t          tag_matched_HLT_mu14;
   Bool_t          tag_matched_HLT_mu14_ivarloose;
   Bool_t          tag_matched_HLT_mu15noL1;
   Bool_t          tag_matched_HLT_mu18;
   Bool_t          tag_matched_HLT_mu18_2mu0noL1_JpsimumuFS;
   Bool_t          tag_matched_HLT_mu18_2mu4noL1_JpsimumuL2;
   Bool_t          tag_matched_HLT_mu18noL1;
   Bool_t          tag_matched_HLT_mu20;
   Bool_t          tag_matched_HLT_mu20_idperf;
   Bool_t          tag_matched_HLT_mu20_iloose_L1MU15;
   Bool_t          tag_matched_HLT_mu20_ivarloose_L1MU15;
   Bool_t          tag_matched_HLT_mu20_msonly;
   Bool_t          tag_matched_HLT_mu22;
   Bool_t          tag_matched_HLT_mu24;
   Bool_t          tag_matched_HLT_mu24_iloose;
   Bool_t          tag_matched_HLT_mu24_iloose_L1MU15;
   Bool_t          tag_matched_HLT_mu24_imedium;
   Bool_t          tag_matched_HLT_mu24_ivarloose;
   Bool_t          tag_matched_HLT_mu24_ivarloose_L1MU15;
   Bool_t          tag_matched_HLT_mu24_ivarmedium;
   Bool_t          tag_matched_HLT_mu26_imedium;
   Bool_t          tag_matched_HLT_mu26_ivarmedium;
   Bool_t          tag_matched_HLT_mu28_imedium;
   Bool_t          tag_matched_HLT_mu28_ivarmedium;
   Bool_t          tag_matched_HLT_mu4;
   Bool_t          tag_matched_HLT_mu40;
   Bool_t          tag_matched_HLT_mu4_idperf;
   Bool_t          tag_matched_HLT_mu4_msonly;
   Bool_t          tag_matched_HLT_mu4_mu4_idperf_bJpsimumu_noid;
   Bool_t          tag_matched_HLT_mu50;
   Bool_t          tag_matched_HLT_mu6;
   Bool_t          tag_matched_HLT_mu60;
   Bool_t          tag_matched_HLT_mu6_bJpsi_TrkPEB;
   Bool_t          tag_matched_HLT_mu6_bJpsi_Trkloose;
   Bool_t          tag_matched_HLT_mu6_idperf;
   Bool_t          tag_matched_HLT_mu6_msonly;
   Bool_t          tag_matched_HLT_mu6_mu4_bJpsimumu;
   Bool_t          tag_matched_HLT_mu6_mu4_bJpsimumu_noL2;
   Bool_t          tag_matched_HLT_mu8noL1;
   Bool_t          tag_matched_HLT_noalg_L1MU;
   Bool_t          tag_matched_HLT_noalg_L1MU10;
   Bool_t          tag_matched_HLT_noalg_L1MU11;
   Bool_t          tag_matched_HLT_noalg_L1MU20;
   Bool_t          tag_matched_HLT_noalg_L1MU21;
   Float_t         trackMET_abs;
   Float_t         trackMET_phi;
   Bool_t          trig_HLT_2mu6_bJpsimumu;
   Bool_t          trig_HLT_2mu6_bJpsimumui_noL2;
   Bool_t          trig_HLT_AllTriggers;
   Bool_t          trig_HLT_mu10;
   Bool_t          trig_HLT_mu10_idperf;
   Bool_t          trig_HLT_mu10_msonly;
   Bool_t          trig_HLT_mu13_mu13_idperf_Zmumu;
   Bool_t          trig_HLT_mu14;
   Bool_t          trig_HLT_mu14_ivarloose;
   Bool_t          trig_HLT_mu15noL1;
   Bool_t          trig_HLT_mu18;
   Bool_t          trig_HLT_mu18_2mu0noL1_JpsimumuFS;
   Bool_t          trig_HLT_mu18_2mu4noL1_JpsimumuL2;
   Bool_t          trig_HLT_mu18noL1;
   Bool_t          trig_HLT_mu20;
   Bool_t          trig_HLT_mu20_idperf;
   Bool_t          trig_HLT_mu20_iloose_L1MU15;
   Bool_t          trig_HLT_mu20_ivarloose_L1MU15;
   Bool_t          trig_HLT_mu20_msonly;
   Bool_t          trig_HLT_mu22;
   Bool_t          trig_HLT_mu24;
   Bool_t          trig_HLT_mu24_iloose;
   Bool_t          trig_HLT_mu24_iloose_L1MU15;
   Bool_t          trig_HLT_mu24_imedium;
   Bool_t          trig_HLT_mu24_ivarloose;
   Bool_t          trig_HLT_mu24_ivarloose_L1MU15;
   Bool_t          trig_HLT_mu24_ivarmedium;
   Bool_t          trig_HLT_mu26_imedium;
   Bool_t          trig_HLT_mu26_ivarmedium;
   Bool_t          trig_HLT_mu28_imedium;
   Bool_t          trig_HLT_mu28_ivarmedium;
   Bool_t          trig_HLT_mu4;
   Bool_t          trig_HLT_mu40;
   Bool_t          trig_HLT_mu4_idperf;
   Bool_t          trig_HLT_mu4_msonly;
   Bool_t          trig_HLT_mu4_mu4_idperf_bJpsimumu_noid;
   Bool_t          trig_HLT_mu50;
   Bool_t          trig_HLT_mu6;
   Bool_t          trig_HLT_mu60;
   Bool_t          trig_HLT_mu6_bJpsi_TrkPEB;
   Bool_t          trig_HLT_mu6_bJpsi_Trkloose;
   Bool_t          trig_HLT_mu6_idperf;
   Bool_t          trig_HLT_mu6_msonly;
   Bool_t          trig_HLT_mu6_mu4_bJpsimumu;
   Bool_t          trig_HLT_mu6_mu4_bJpsimumu_noL2;
   Bool_t          trig_HLT_mu8noL1;
   Bool_t          trig_HLT_noalg_L1MU;
   Bool_t          trig_HLT_noalg_L1MU10;
   Bool_t          trig_HLT_noalg_L1MU11;
   Bool_t          trig_HLT_noalg_L1MU20;
   Bool_t          trig_HLT_noalg_L1MU21;
   Float_t         vertex_density;

   // List of branches
   TBranch        *b_BCID;   //!
   TBranch        *b_EventBadMuonVetoVar_HighPt_ID;   //!
   TBranch        *b_EventBadMuonVetoVar_HighPt_ME;   //!
   TBranch        *b_EventBadMuonVetoVar_Loose_ID;   //!
   TBranch        *b_EventBadMuonVetoVar_Loose_ME;   //!
   TBranch        *b_EventBadMuonVetoVar_Medium_ID;   //!
   TBranch        *b_EventBadMuonVetoVar_Medium_ME;   //!
   TBranch        *b_EventBadMuonVetoVar_Tight_ID;   //!
   TBranch        *b_EventBadMuonVetoVar_Tight_ME;   //!
   TBranch        *b_EventBadMuonVetoVar_VeryLoose_ID;   //!
   TBranch        *b_EventBadMuonVetoVar_VeryLoose_ME;   //!
   TBranch        *b_PV_n;   //!
   TBranch        *b_actual_mu;   //!
   TBranch        *b_average_mu;   //!
   TBranch        *b_calib_jet_probe_pt;   //!
   TBranch        *b_calib_jet_probe_eta;   //!
   TBranch        *b_calib_jet_probe_phi;   //!
   TBranch        *b_calib_jet_probe_index;   //!
   TBranch        *b_calib_jet_probe_m;   //!
   TBranch        *b_calib_jet_probe_emfrac;   //!
   TBranch        *b_calib_jet_probe_jvt;   //!
   TBranch        *b_calib_jet_closest_ptrel_probe;   //!
   TBranch        *b_calib_jet_tag_pt;   //!
   TBranch        *b_calib_jet_tag_eta;   //!
   TBranch        *b_calib_jet_tag_phi;   //!
   TBranch        *b_calib_jet_tag_index;   //!
   TBranch        *b_calib_jet_tag_m;   //!
   TBranch        *b_calib_jet_dR_tag;   //!
   TBranch        *b_calib_jet_nJets20;   //!
   TBranch        *b_calib_jet_dR_probe;   //!
   TBranch        *b_calib_jet_closest_chargedfrac_probe;   //!
   TBranch        *b_calib_jet_ntrks500_probe;   //!
   TBranch        *b_calib_jet_n_bad_jets;   //!
   TBranch        *b_dilep_deta;   //!
   TBranch        *b_dilep_dphi;   //!
   TBranch        *b_dilep_mll;   //!
   TBranch        *b_dilep_pt;   //!
   TBranch        *b_energyDensity_central;   //!
   TBranch        *b_energyDensity_forward;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_lumiblock;   //!
   TBranch        *b_matchobj_muon_CaloLRLikelihood;   //!
   TBranch        *b_matchobj_muon_CaloMuonIDTag;   //!
   TBranch        *b_matchobj_muon_EnergyLoss;   //!
   TBranch        *b_matchobj_muon_MeasEnergyLoss;   //!
   TBranch        *b_matchobj_muon_ParamEnergyLoss;   //!
   TBranch        *b_matchobj_muon_allAuthors;   //!
   TBranch        *b_matchobj_muon_author;   //!
   TBranch        *b_matchobj_muon_momentumBalanceSignificance;   //!
   TBranch        *b_matchobj_muon_scatteringCurvatureSignificance;   //!
   TBranch        *b_matchobj_muon_scatteringNeighbourSignificance;   //!
   TBranch        *b_matchobj_muon_segmentDeltaEta;   //!
   TBranch        *b_matchobj_muon_segmentDeltaPhi;   //!
   TBranch        *b_matchobj_muon_type;   //!
   TBranch        *b_mcChannelNumber;   //!
   TBranch        *b_mcEventWeight;   //!
   TBranch        *b_n_Trks_PrimVtx;   //!
   TBranch        *b_probe_pt;   //!
   TBranch        *b_probe_eta;   //!
   TBranch        *b_probe_phi;   //!
   TBranch        *b_probe_index;   //!
   TBranch        *b_probe_d0;   //!
   TBranch        *b_probe_d0err;   //!
   TBranch        *b_probe_eta_exTP;   //!
   TBranch        *b_probe_etcone20;   //!
   TBranch        *b_probe_etcone30;   //!
   TBranch        *b_probe_etcone40;   //!
   TBranch        *b_probe_neflowisol20;   //!
   TBranch        *b_probe_neflowisol30;   //!
   TBranch        *b_probe_neflowisol40;   //!
   TBranch        *b_probe_phi_exTP;   //!
   TBranch        *b_probe_ptcone20;   //!
   TBranch        *b_probe_ptcone20_LooseTTVA_pt500;   //!
   TBranch        *b_probe_ptcone20_TightTTVA_pt1000;   //!
   TBranch        *b_probe_ptcone20_TightTTVA_pt500;   //!
   TBranch        *b_probe_ptcone30;   //!
   TBranch        *b_probe_ptcone30_LooseTTVA_pt500;   //!
   TBranch        *b_probe_ptcone30_TightTTVA_pt1000;   //!
   TBranch        *b_probe_ptcone30_TightTTVA_pt500;   //!
   TBranch        *b_probe_ptcone40;   //!
   TBranch        *b_probe_ptcone40_LooseTTVA_pt500;   //!
   TBranch        *b_probe_ptcone40_TightTTVA_pt1000;   //!
   TBranch        *b_probe_ptcone40_TightTTVA_pt500;   //!
   TBranch        *b_probe_ptvarcone20;   //!
   TBranch        *b_probe_ptvarcone20_LooseTTVA_pt500;   //!
   TBranch        *b_probe_ptvarcone20_TightTTVA_pt1000;   //!
   TBranch        *b_probe_ptvarcone20_TightTTVA_pt500;   //!
   TBranch        *b_probe_ptvarcone30;   //!
   TBranch        *b_probe_ptvarcone30_LooseTTVA_pt500;   //!
   TBranch        *b_probe_ptvarcone30_TightTTVA_pt1000;   //!
   TBranch        *b_probe_ptvarcone30_TightTTVA_pt500;   //!
   TBranch        *b_probe_ptvarcone40;   //!
   TBranch        *b_probe_ptvarcone40_LooseTTVA_pt500;   //!
   TBranch        *b_probe_ptvarcone40_TightTTVA_pt1000;   //!
   TBranch        *b_probe_ptvarcone40_TightTTVA_pt500;   //!
   TBranch        *b_probe_q;   //!
   TBranch        *b_probe_topocore;   //!
   TBranch        *b_probe_topoetcone20;   //!
   TBranch        *b_probe_topoetcone30;   //!
   TBranch        *b_probe_topoetcone40;   //!
   TBranch        *b_probe_truth_origin;   //!
   TBranch        *b_probe_truth_type;   //!
   TBranch        *b_probe_z0;   //!
   TBranch        *b_probe_dRMatch_CaloTag;   //!
   TBranch        *b_probe_dRMatch_CombinedMuons;   //!
   TBranch        *b_probe_dRMatch_HighPtMuons;   //!
   TBranch        *b_probe_dRMatch_LooseMuons;   //!
   TBranch        *b_probe_dRMatch_LooseMuons_noCaloTag;   //!
   TBranch        *b_probe_dRMatch_LowPtMuons;   //!
   TBranch        *b_probe_dRMatch_MediumMuons;   //!
   TBranch        *b_probe_dRMatch_StandaloneMuons;   //!
   TBranch        *b_probe_dRMatch_TightMuons;   //!
   TBranch        *b_probe_matched_CaloTag;   //!
   TBranch        *b_probe_matched_CaloTag_PtrMatching;   //!
   TBranch        *b_probe_matched_CombinedMuons;   //!
   TBranch        *b_probe_matched_HighPtMuons;   //!
   TBranch        *b_probe_matched_HighPtMuons_PtrMatching;   //!
   TBranch        *b_probe_matched_IsoFCLoose;   //!
   TBranch        *b_probe_matched_IsoFCLoose_FixedRad;   //!
   TBranch        *b_probe_matched_IsoFCTight;   //!
   TBranch        *b_probe_matched_IsoFCTightTrackOnly;   //!
   TBranch        *b_probe_matched_IsoFCTightTrackOnly_FixedRad;   //!
   TBranch        *b_probe_matched_IsoFCTight_FixedRad;   //!
   TBranch        *b_probe_matched_IsoFixedCutHighPtTrackOnly;   //!
   TBranch        *b_probe_matched_LooseMuons;   //!
   TBranch        *b_probe_matched_LooseMuons_PtrMatching;   //!
   TBranch        *b_probe_matched_LooseMuons_noCaloTag;   //!
   TBranch        *b_probe_matched_LooseMuons_noCaloTag_PtrMatching;   //!
   TBranch        *b_probe_matched_LowPtMuons;   //!
   TBranch        *b_probe_matched_LowPtMuons_PtrMatching;   //!
   TBranch        *b_probe_matched_MediumMuons;   //!
   TBranch        *b_probe_matched_MediumMuons_PtrMatching;   //!
   TBranch        *b_probe_matched_StandaloneMuons;   //!
   TBranch        *b_probe_matched_StandaloneMuons_PtrMatching;   //!
   TBranch        *b_probe_matched_TRTCut;   //!
   TBranch        *b_probe_matched_TightMuons;   //!
   TBranch        *b_probe_matched_TightMuons_PtrMatching;   //!
   TBranch        *b_probe_matched_IDTracks;   //! added by PB
   TBranch        *b_probe_matched_HLT_mu10; //! added by PB
   TBranch        *b_probe_quality; //! added by PB
   TBranch        *b_probe_matched_HLT_mu20_iloose_L1MU15; //! added by PB
   TBranch        *b_prwWeight;   //!
   TBranch        *b_randomLumiBlockNumber;   //!
   TBranch        *b_randomRunNumber;   //!
   TBranch        *b_runNumber;   //!
   TBranch        *b_tag_pt;   //!
   TBranch        *b_tag_eta;   //!
   TBranch        *b_tag_phi;   //!
   TBranch        *b_tag_index;   //!
   TBranch        *b_tag_d0;   //!
   TBranch        *b_tag_d0err;   //!
   TBranch        *b_tag_dR_exTP;   //!
   TBranch        *b_tag_deta_exTP;   //!
   TBranch        *b_tag_dphi_exTP;   //!
   TBranch        *b_tag_etcone20;   //!
   TBranch        *b_tag_etcone30;   //!
   TBranch        *b_tag_etcone40;   //!
   TBranch        *b_tag_matched_IsoFCLoose;   //!
   TBranch        *b_tag_matched_IsoFCLoose_FixedRad;   //!
   TBranch        *b_tag_matched_IsoFCTight;   //!
   TBranch        *b_tag_matched_IsoFCTightTrackOnly;   //!
   TBranch        *b_tag_matched_IsoFCTightTrackOnly_FixedRad;   //!
   TBranch        *b_tag_matched_IsoFCTight_FixedRad;   //!
   TBranch        *b_tag_matched_IsoFixedCutHighPtTrackOnly;   //!
   TBranch        *b_tag_neflowisol20;   //!
   TBranch        *b_tag_neflowisol30;   //!
   TBranch        *b_tag_neflowisol40;   //!
   TBranch        *b_tag_ptcone20;   //!
   TBranch        *b_tag_ptcone20_LooseTTVA_pt500;   //!
   TBranch        *b_tag_ptcone20_TightTTVA_pt1000;   //!
   TBranch        *b_tag_ptcone20_TightTTVA_pt500;   //!
   TBranch        *b_tag_ptcone30;   //!
   TBranch        *b_tag_ptcone30_LooseTTVA_pt500;   //!
   TBranch        *b_tag_ptcone30_TightTTVA_pt1000;   //!
   TBranch        *b_tag_ptcone30_TightTTVA_pt500;   //!
   TBranch        *b_tag_ptcone40;   //!
   TBranch        *b_tag_ptcone40_LooseTTVA_pt500;   //!
   TBranch        *b_tag_ptcone40_TightTTVA_pt1000;   //!
   TBranch        *b_tag_ptcone40_TightTTVA_pt500;   //!
   TBranch        *b_tag_ptvarcone20;   //!
   TBranch        *b_tag_ptvarcone20_LooseTTVA_pt500;   //!
   TBranch        *b_tag_ptvarcone20_TightTTVA_pt1000;   //!
   TBranch        *b_tag_ptvarcone20_TightTTVA_pt500;   //!
   TBranch        *b_tag_ptvarcone30;   //!
   TBranch        *b_tag_ptvarcone30_LooseTTVA_pt500;   //!
   TBranch        *b_tag_ptvarcone30_TightTTVA_pt1000;   //!
   TBranch        *b_tag_ptvarcone30_TightTTVA_pt500;   //!
   TBranch        *b_tag_ptvarcone40;   //!
   TBranch        *b_tag_ptvarcone40_LooseTTVA_pt500;   //!
   TBranch        *b_tag_ptvarcone40_TightTTVA_pt1000;   //!
   TBranch        *b_tag_ptvarcone40_TightTTVA_pt500;   //!
   TBranch        *b_tag_q;   //!
   TBranch        *b_tag_topocore;   //!
   TBranch        *b_tag_topoetcone20;   //!
   TBranch        *b_tag_topoetcone30;   //!
   TBranch        *b_tag_topoetcone40;   //!
   TBranch        *b_tag_truth_origin;   //!
   TBranch        *b_tag_truth_type;   //!
   TBranch        *b_tag_z0;   //!
   TBranch        *b_tag_dRMatch_HLT_2mu6_bJpsimumu;   //!
   TBranch        *b_tag_dRMatch_HLT_2mu6_bJpsimumui_noL2;   //!
   TBranch        *b_tag_dRMatch_HLT_AllTriggers;   //!
   TBranch        *b_tag_dRMatch_HLT_mu10;   //!
   TBranch        *b_tag_dRMatch_HLT_mu10_idperf;   //!
   TBranch        *b_tag_dRMatch_HLT_mu10_msonly;   //!
   TBranch        *b_tag_dRMatch_HLT_mu13_mu13_idperf_Zmumu;   //!
   TBranch        *b_tag_dRMatch_HLT_mu14;   //!
   TBranch        *b_tag_dRMatch_HLT_mu14_ivarloose;   //!
   TBranch        *b_tag_dRMatch_HLT_mu15noL1;   //!
   TBranch        *b_tag_dRMatch_HLT_mu18;   //!
   TBranch        *b_tag_dRMatch_HLT_mu18_2mu0noL1_JpsimumuFS;   //!
   TBranch        *b_tag_dRMatch_HLT_mu18_2mu4noL1_JpsimumuL2;   //!
   TBranch        *b_tag_dRMatch_HLT_mu18noL1;   //!
   TBranch        *b_tag_dRMatch_HLT_mu20;   //!
   TBranch        *b_tag_dRMatch_HLT_mu20_idperf;   //!
   TBranch        *b_tag_dRMatch_HLT_mu20_iloose_L1MU15;   //!
   TBranch        *b_tag_dRMatch_HLT_mu20_ivarloose_L1MU15;   //!
   TBranch        *b_tag_dRMatch_HLT_mu20_msonly;   //!
   TBranch        *b_tag_dRMatch_HLT_mu22;   //!
   TBranch        *b_tag_dRMatch_HLT_mu24;   //!
   TBranch        *b_tag_dRMatch_HLT_mu24_iloose;   //!
   TBranch        *b_tag_dRMatch_HLT_mu24_iloose_L1MU15;   //!
   TBranch        *b_tag_dRMatch_HLT_mu24_imedium;   //!
   TBranch        *b_tag_dRMatch_HLT_mu24_ivarloose;   //!
   TBranch        *b_tag_dRMatch_HLT_mu24_ivarloose_L1MU15;   //!
   TBranch        *b_tag_dRMatch_HLT_mu24_ivarmedium;   //!
   TBranch        *b_tag_dRMatch_HLT_mu26_imedium;   //!
   TBranch        *b_tag_dRMatch_HLT_mu26_ivarmedium;   //!
   TBranch        *b_tag_dRMatch_HLT_mu28_imedium;   //!
   TBranch        *b_tag_dRMatch_HLT_mu28_ivarmedium;   //!
   TBranch        *b_tag_dRMatch_HLT_mu4;   //!
   TBranch        *b_tag_dRMatch_HLT_mu40;   //!
   TBranch        *b_tag_dRMatch_HLT_mu4_idperf;   //!
   TBranch        *b_tag_dRMatch_HLT_mu4_msonly;   //!
   TBranch        *b_tag_dRMatch_HLT_mu4_mu4_idperf_bJpsimumu_noid;   //!
   TBranch        *b_tag_dRMatch_HLT_mu50;   //!
   TBranch        *b_tag_dRMatch_HLT_mu6;   //!
   TBranch        *b_tag_dRMatch_HLT_mu60;   //!
   TBranch        *b_tag_dRMatch_HLT_mu6_bJpsi_TrkPEB;   //!
   TBranch        *b_tag_dRMatch_HLT_mu6_bJpsi_Trkloose;   //!
   TBranch        *b_tag_dRMatch_HLT_mu6_idperf;   //!
   TBranch        *b_tag_dRMatch_HLT_mu6_msonly;   //!
   TBranch        *b_tag_dRMatch_HLT_mu6_mu4_bJpsimumu;   //!
   TBranch        *b_tag_dRMatch_HLT_mu6_mu4_bJpsimumu_noL2;   //!
   TBranch        *b_tag_dRMatch_HLT_mu8noL1;   //!
   TBranch        *b_tag_dRMatch_HLT_noalg_L1MU;   //!
   TBranch        *b_tag_dRMatch_HLT_noalg_L1MU10;   //!
   TBranch        *b_tag_dRMatch_HLT_noalg_L1MU11;   //!
   TBranch        *b_tag_dRMatch_HLT_noalg_L1MU20;   //!
   TBranch        *b_tag_dRMatch_HLT_noalg_L1MU21;   //!
   TBranch        *b_tag_matched_HLT_2mu6_bJpsimumu;   //!
   TBranch        *b_tag_matched_HLT_2mu6_bJpsimumui_noL2;   //!
   TBranch        *b_tag_matched_HLT_AllTriggers;   //!
   TBranch        *b_tag_matched_HLT_mu10;   //!
   TBranch        *b_tag_matched_HLT_mu10_idperf;   //!
   TBranch        *b_tag_matched_HLT_mu10_msonly;   //!
   TBranch        *b_tag_matched_HLT_mu13_mu13_idperf_Zmumu;   //!
   TBranch        *b_tag_matched_HLT_mu14;   //!
   TBranch        *b_tag_matched_HLT_mu14_ivarloose;   //!
   TBranch        *b_tag_matched_HLT_mu15noL1;   //!
   TBranch        *b_tag_matched_HLT_mu18;   //!
   TBranch        *b_tag_matched_HLT_mu18_2mu0noL1_JpsimumuFS;   //!
   TBranch        *b_tag_matched_HLT_mu18_2mu4noL1_JpsimumuL2;   //!
   TBranch        *b_tag_matched_HLT_mu18noL1;   //!
   TBranch        *b_tag_matched_HLT_mu20;   //!
   TBranch        *b_tag_matched_HLT_mu20_idperf;   //!
   TBranch        *b_tag_matched_HLT_mu20_iloose_L1MU15;   //!
   TBranch        *b_tag_matched_HLT_mu20_ivarloose_L1MU15;   //!
   TBranch        *b_tag_matched_HLT_mu20_msonly;   //!
   TBranch        *b_tag_matched_HLT_mu22;   //!
   TBranch        *b_tag_matched_HLT_mu24;   //!
   TBranch        *b_tag_matched_HLT_mu24_iloose;   //!
   TBranch        *b_tag_matched_HLT_mu24_iloose_L1MU15;   //!
   TBranch        *b_tag_matched_HLT_mu24_imedium;   //!
   TBranch        *b_tag_matched_HLT_mu24_ivarloose;   //!
   TBranch        *b_tag_matched_HLT_mu24_ivarloose_L1MU15;   //!
   TBranch        *b_tag_matched_HLT_mu24_ivarmedium;   //!
   TBranch        *b_tag_matched_HLT_mu26_imedium;   //!
   TBranch        *b_tag_matched_HLT_mu26_ivarmedium;   //!
   TBranch        *b_tag_matched_HLT_mu28_imedium;   //!
   TBranch        *b_tag_matched_HLT_mu28_ivarmedium;   //!
   TBranch        *b_tag_matched_HLT_mu4;   //!
   TBranch        *b_tag_matched_HLT_mu40;   //!
   TBranch        *b_tag_matched_HLT_mu4_idperf;   //!
   TBranch        *b_tag_matched_HLT_mu4_msonly;   //!
   TBranch        *b_tag_matched_HLT_mu4_mu4_idperf_bJpsimumu_noid;   //!
   TBranch        *b_tag_matched_HLT_mu50;   //!
   TBranch        *b_tag_matched_HLT_mu6;   //!
   TBranch        *b_tag_matched_HLT_mu60;   //!
   TBranch        *b_tag_matched_HLT_mu6_bJpsi_TrkPEB;   //!
   TBranch        *b_tag_matched_HLT_mu6_bJpsi_Trkloose;   //!
   TBranch        *b_tag_matched_HLT_mu6_idperf;   //!
   TBranch        *b_tag_matched_HLT_mu6_msonly;   //!
   TBranch        *b_tag_matched_HLT_mu6_mu4_bJpsimumu;   //!
   TBranch        *b_tag_matched_HLT_mu6_mu4_bJpsimumu_noL2;   //!
   TBranch        *b_tag_matched_HLT_mu8noL1;   //!
   TBranch        *b_tag_matched_HLT_noalg_L1MU;   //!
   TBranch        *b_tag_matched_HLT_noalg_L1MU10;   //!
   TBranch        *b_tag_matched_HLT_noalg_L1MU11;   //!
   TBranch        *b_tag_matched_HLT_noalg_L1MU20;   //!
   TBranch        *b_tag_matched_HLT_noalg_L1MU21;   //!
   TBranch        *b_trackMET_abs;   //!
   TBranch        *b_trackMET_phi;   //!
   TBranch        *b_trig_HLT_2mu6_bJpsimumu;   //!
   TBranch        *b_trig_HLT_2mu6_bJpsimumui_noL2;   //!
   TBranch        *b_trig_HLT_AllTriggers;   //!
   TBranch        *b_trig_HLT_mu10;   //!
   TBranch        *b_trig_HLT_mu10_idperf;   //!
   TBranch        *b_trig_HLT_mu10_msonly;   //!
   TBranch        *b_trig_HLT_mu13_mu13_idperf_Zmumu;   //!
   TBranch        *b_trig_HLT_mu14;   //!
   TBranch        *b_trig_HLT_mu14_ivarloose;   //!
   TBranch        *b_trig_HLT_mu15noL1;   //!
   TBranch        *b_trig_HLT_mu18;   //!
   TBranch        *b_trig_HLT_mu18_2mu0noL1_JpsimumuFS;   //!
   TBranch        *b_trig_HLT_mu18_2mu4noL1_JpsimumuL2;   //!
   TBranch        *b_trig_HLT_mu18noL1;   //!
   TBranch        *b_trig_HLT_mu20;   //!
   TBranch        *b_trig_HLT_mu20_idperf;   //!
   TBranch        *b_trig_HLT_mu20_iloose_L1MU15;   //!
   TBranch        *b_trig_HLT_mu20_ivarloose_L1MU15;   //!
   TBranch        *b_trig_HLT_mu20_msonly;   //!
   TBranch        *b_trig_HLT_mu22;   //!
   TBranch        *b_trig_HLT_mu24;   //!
   TBranch        *b_trig_HLT_mu24_iloose;   //!
   TBranch        *b_trig_HLT_mu24_iloose_L1MU15;   //!
   TBranch        *b_trig_HLT_mu24_imedium;   //!
   TBranch        *b_trig_HLT_mu24_ivarloose;   //!
   TBranch        *b_trig_HLT_mu24_ivarloose_L1MU15;   //!
   TBranch        *b_trig_HLT_mu24_ivarmedium;   //!
   TBranch        *b_trig_HLT_mu26_imedium;   //!
   TBranch        *b_trig_HLT_mu26_ivarmedium;   //!
   TBranch        *b_trig_HLT_mu28_imedium;   //!
   TBranch        *b_trig_HLT_mu28_ivarmedium;   //!
   TBranch        *b_trig_HLT_mu4;   //!
   TBranch        *b_trig_HLT_mu40;   //!
   TBranch        *b_trig_HLT_mu4_idperf;   //!
   TBranch        *b_trig_HLT_mu4_msonly;   //!
   TBranch        *b_trig_HLT_mu4_mu4_idperf_bJpsimumu_noid;   //!
   TBranch        *b_trig_HLT_mu50;   //!
   TBranch        *b_trig_HLT_mu6;   //!
   TBranch        *b_trig_HLT_mu60;   //!
   TBranch        *b_trig_HLT_mu6_bJpsi_TrkPEB;   //!
   TBranch        *b_trig_HLT_mu6_bJpsi_Trkloose;   //!
   TBranch        *b_trig_HLT_mu6_idperf;   //!
   TBranch        *b_trig_HLT_mu6_msonly;   //!
   TBranch        *b_trig_HLT_mu6_mu4_bJpsimumu;   //!
   TBranch        *b_trig_HLT_mu6_mu4_bJpsimumu_noL2;   //!
   TBranch        *b_trig_HLT_mu8noL1;   //!
   TBranch        *b_trig_HLT_noalg_L1MU;   //!
   TBranch        *b_trig_HLT_noalg_L1MU10;   //!
   TBranch        *b_trig_HLT_noalg_L1MU11;   //!
   TBranch        *b_trig_HLT_noalg_L1MU20;   //!
   TBranch        *b_trig_HLT_noalg_L1MU21;   //!
   TBranch        *b_vertex_density;   //!

   PBClass(TTree *tree=0, TString Input="/home/petr/Analysis_iwona/SFAnalysis/rootfiles/muontp_mc_pp.root", TString NameOfTree="TPTree_IDProbes_OC", TString PathToTree="ZmumuTPReco/Trees/IDProbes_OC");
   virtual ~PBClass();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop(TString Output="out.root", TString Tag="");
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

//#ifdef PBClass_cxx

//#endif // #ifdef PBClass_cxx
