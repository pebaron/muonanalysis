//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri Oct 19 12:51:15 2018 by ROOT version 6.14/04
// from TTree nominal/tree
// found on file: mc_Zmumu.root
//////////////////////////////////////////////////////////

#ifndef nominal_h
#define nominal_h

#include "TROOT.h"
#include "TChain.h"
#include "TFile.h"
#include "TString.h"
#include <vector>
#include <iostream> 
using namespace std;

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"

class nominal {
public :
   TChain          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   ULong64_t       eventNumber;
   UInt_t          runNumber;
   UInt_t          mcChannelNumber;
   Float_t         mu;
   Float_t         mu_original_xAOD;
   UInt_t          backgroundFlags;
   UInt_t          hasBadMuon;
   vector<float>   *el_pt;
   vector<float>   *el_eta;
   vector<float>   *el_cl_eta;
   vector<float>   *el_phi;
   vector<float>   *el_e;
   vector<float>   *el_charge;
   vector<float>   *el_topoetcone20;
   vector<float>   *el_ptvarcone20;
   vector<char>    *el_CF;
   vector<float>   *el_d0sig;
   vector<float>   *el_delta_z0_sintheta;
   vector<float>   *mu_pt;
   vector<float>   *mu_eta;
   vector<float>   *mu_phi;
   vector<float>   *mu_e;
   vector<float>   *mu_charge;
   vector<float>   *mu_topoetcone20;
   vector<float>   *mu_ptvarcone30;
   vector<float>   *mu_d0sig;
   vector<float>   *mu_delta_z0_sintheta;
   vector<float>   *jet_pt;
   vector<float>   *jet_eta;
   vector<float>   *jet_phi;
   vector<float>   *jet_e;
   vector<float>   *jet_mv2c00;
   vector<float>   *jet_mv2c10;
   vector<float>   *jet_mv2c20;
   vector<float>   *jet_ip3dsv1;
   vector<float>   *jet_jvt;
   vector<char>    *jet_passfjvt;
   vector<char>    *jet_isbtagged_MV2c10_77;
   Float_t         met_met;
   Float_t         met_phi;
   Int_t           emu_2015;
   Int_t           emu_2016;
   Int_t           emu_particle;
   Int_t           ee_2015;
   Int_t           ee_2016;
   Int_t           ee_particle;
   Int_t           mumu_2015;
   Int_t           mumu_2016;
   Int_t           mumu_particle;
   //I have added due to the W boson selection
   Int_t           ejets_2015;
   Int_t           ejets_2016;
   Int_t           ejets_particle;
   Int_t           mujets_2015;
   Int_t           mujets_2016;
   Int_t           mujets_particle;
   //
   Char_t          HLT_mu15_L1MU6;
   Char_t          HLT_mu15_L1MU10;
   Char_t          HLT_mu15;
   Char_t          HLT_e15_lhloose_nod0;
   Char_t          HLT_e15_lhloose;
   vector<char>    *el_trigMatch_HLT_e15_lhloose_nod0;
   vector<char>    *el_trigMatch_HLT_e15_lhloose;
   vector<char>    *mu_trigMatch_HLT_mu15_L1MU6;
   vector<char>    *mu_trigMatch_HLT_mu15_L1MU10;
   vector<char>    *mu_trigMatch_HLT_mu15;

   // List of branchesejets_2015

   // I have added due to the W selection
   TBranch        *b_ejets_2015;   //!
   TBranch        *b_ejets_2016;   //!
   TBranch        *b_ejets_particle;   //!
   TBranch        *b_mujets_2015;   //!
   TBranch        *b_mujets_2016;   //!
   TBranch        *b_mujets_particle;   //!
   //
   TBranch        *b_eventNumber;   //!
   TBranch        *b_runNumber;   //!
   TBranch        *b_mcChannelNumber;   //!
   TBranch        *b_mu;   //!
   TBranch        *b_mu_original_xAOD;   //!
   TBranch        *b_backgroundFlags;   //!
   TBranch        *b_hasBadMuon;   //!
   TBranch        *b_el_pt;   //!
   TBranch        *b_el_eta;   //!
   TBranch        *b_el_cl_eta;   //!
   TBranch        *b_el_phi;   //!
   TBranch        *b_el_e;   //!
   TBranch        *b_el_charge;   //!
   TBranch        *b_el_topoetcone20;   //!
   TBranch        *b_el_ptvarcone20;   //!
   TBranch        *b_el_CF;   //!
   TBranch        *b_el_d0sig;   //!
   TBranch        *b_el_delta_z0_sintheta;   //!
   TBranch        *b_mu_pt;   //!
   TBranch        *b_mu_eta;   //!
   TBranch        *b_mu_phi;   //!
   TBranch        *b_mu_e;   //!
   TBranch        *b_mu_charge;   //!
   TBranch        *b_mu_topoetcone20;   //!
   TBranch        *b_mu_ptvarcone30;   //!
   TBranch        *b_mu_d0sig;   //!
   TBranch        *b_mu_delta_z0_sintheta;   //!
   TBranch        *b_jet_pt;   //!
   TBranch        *b_jet_eta;   //!
   TBranch        *b_jet_phi;   //!
   TBranch        *b_jet_e;   //!
   TBranch        *b_jet_mv2c00;   //!
   TBranch        *b_jet_mv2c10;   //!
   TBranch        *b_jet_mv2c20;   //!
   TBranch        *b_jet_ip3dsv1;   //!
   TBranch        *b_jet_jvt;   //!
   TBranch        *b_jet_passfjvt;   //!
   TBranch        *b_jet_isbtagged_MV2c10_77;   //!
   TBranch        *b_met_met;   //!
   TBranch        *b_met_phi;   //!
   TBranch        *b_emu_2015;   //!
   TBranch        *b_emu_2016;   //!
   TBranch        *b_emu_particle;   //!
   TBranch        *b_ee_2015;   //!
   TBranch        *b_ee_2016;   //!
   TBranch        *b_ee_particle;   //!
   TBranch        *b_mumu_2015;   //!
   TBranch        *b_mumu_2016;   //!
   TBranch        *b_mumu_particle;   //!
   TBranch        *b_HLT_mu15_L1MU6;   //!
   TBranch        *b_HLT_mu15_L1MU10;   //!
   TBranch        *b_HLT_mu15;   //!
   TBranch        *b_HLT_e15_lhloose_nod0;   //!
   TBranch        *b_HLT_e15_lhloose;   //!
   TBranch        *b_el_trigMatch_HLT_e15_lhloose_nod0;   //!
   TBranch        *b_el_trigMatch_HLT_e15_lhloose;   //!
   TBranch        *b_mu_trigMatch_HLT_mu15_L1MU6;   //!
   TBranch        *b_mu_trigMatch_HLT_mu15_L1MU10;   //!
   TBranch        *b_mu_trigMatch_HLT_mu15;   //!

   nominal(TTree *tree=0, TString Input="/home/petr/Analysis_iwona/ZAnalysis/rootfiles/out_data.root", TString NameOfTree="nominal");
   virtual ~nominal();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop(TString Output="out_data_nominal.root", TString Tag="");
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif