#include "PBClass.h"
//#include "nominal.cpp"
#include "TH2.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TString.h"
#include "TLorentzVector.h"
#include "TROOT.h"
#include "math.h"
#include <iostream>  
using namespace std;

int main (int argc, char *argv[]) { 
    //cout << "argc je: " << argc << endl;
    //cout << "Sizeof(argv) je: " << sizeof(argv) << endl;
    for (int i=1; i<argc; i++){
    cout << "argv["<< i << "] je: " << argv[i] << endl;
    }
    TString TestString;
    TestString = argv[2];
    cout << "TestString je: " << TestString << endl;
    // argc ukazuje pocet argumentu + 1

    TString MyInput("/home/petr/Analysis_iwona/SFAnalysis/rootfiles/muontp_mc_pp.root"), MyTree("TPTree_CaloProbes_OC"), MyOutput("out_data_nominal.root"), MyPathToTree("ZmumuTPReco/Trees/IDProbes_OC");

    if (argc > 1){
        MyInput = argv[1];
        MyOutput = argv[2];
        MyTree = argv[3];
        MyPathToTree = argv[4];
    }
    
    PBClass * t = new PBClass(0, MyInput, MyTree, MyPathToTree);
    t->Loop(MyOutput, MyTree);
 } 