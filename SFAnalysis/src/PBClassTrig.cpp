#define PBClassTrig_cxx
#include "PBClassTrig.h"
#include "TH2.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TLorentzVector.h"
#include "TROOT.h"
#include "TString.h"
#include "TEfficiency.h"
#include "math.h"
#include "vector"
#include <iostream>
#include <vector>
using namespace std;

void PBClassTrig::Loop(TString Output, TString Tag)
{
   cout << "SIGNe: Start of the function Loop()." << endl;
   if (fChain == 0) return;
   TFile *outfile = new TFile(Output, "recreate");
   cout << "Output: " << Output << endl;
   
   //MUMU HISTOGRAMS
   
   TH1D *Zmass_mumu = new TH1D("Zmass_mumu", "m_{Z mumu}; [GeV]; Events", 60, 60, 120);

   TH1D *Zmass_mumu_Trig_nom_barrel = new TH1D("Zmass_mumu_Trig_nom_barrel", "m_{Z mumu}_cuts_nom; [GeV]; Events", 60, 60, 120);
   TH1D *Zmass_mumu_Trig_denom_barrel = new TH1D("Zmass_mumu_Trig_denom_barrel", "m_{Z mumu}_cuts_denom; [GeV]; Events", 60, 60, 120);

   TH1D *Zmass_mumu_Trig_nom_endcup = new TH1D("Zmass_mumu_Trig_nom_endcup", "m_{Z mumu}_cuts_nom; [GeV]; Events", 60, 60, 120);
   TH1D *Zmass_mumu_Trig_denom_endcup = new TH1D("Zmass_mumu_Trig_denom_endcup", "m_{Z mumu}_cuts_denom; [GeV]; Events", 60, 60, 120);

   TH1D *muon0_pt_nomin_both_barrel = new TH1D("muon0_pt_nomin_both_barrel","muon0_pt_nomin_both_barrel;[GeV]; Events",11,10,100);
   TH1D *muon0_pt_denomin_both_barrel = new TH1D("muon0_pt_denomin_both_barrel","muon0_pt_denomin_both_barrel;[GeV]; Events",11,10,100);
   TH1D *muon0_eta_nomin_both = new TH1D("muon0_eta_nomin_both","muon0_eta_nomin_both;[GeV]; Events",20,-2.5,2.5);
   TH1D *muon0_eta_denomin_both = new TH1D("muon0_eta_denomin_both","muon0_eta_denomin_both;[GeV]; Events",20,-2.5,2.5);

   TH1D *muon0_eta_nomin_plus = new TH1D("muon0_eta_nomin_plus","muon0_eta_nomin_plus;[GeV]; Events",20,-2.5,2.5);
   TH1D *muon0_eta_nomin_minus = new TH1D("muon0_eta_nomin_minus","muon0_eta_nomin_minus;[GeV]; Events",20,-2.5,2.5);
   TH1D *muon0_eta_denomin_plus = new TH1D("muon0_eta_denomin_plus","muon0_eta_denomin_plus;[GeV]; Events",20,-2.5,2.5);
   TH1D *muon0_eta_denomin_minus = new TH1D("muon0_eta_denomin_minus","muon0_eta_denomin_minus;[GeV]; Events",20,-2.5,2.5);
   
   TH1D *muon0_pt_nomin_both_endcup = new TH1D("muon0_pt_nomin_both_endcup","muon0_pt_nomin_both_endcup;[GeV]; Events",11,10,100);
   TH1D *muon0_pt_denomin_both_endcup = new TH1D("muon0_pt_denomin_both_endcup","muon0_pt_denomin_both_endcup;[GeV]; Events",11,10,100);

   TH1D *muon0_pt_nomin_plus_barrel = new TH1D("muon0_pt_nomin_plus_barrel","muon0_pt_nomin_plus_barrel;[GeV]; Events",11,10,100);
   TH1D *muon0_pt_denomin_plus_barrel = new TH1D("muon0_pt_denomin_plus_barrel","muon0_pt_denomin_plus_barrel;[GeV]; Events",11,10,100);
   TH1D *muon0_pt_nomin_plus_endcup = new TH1D("muon0_pt_nomin_plus_endcup","muon0_pt_nomin_plus_endcup;[GeV]; Events",11,10,100);
   TH1D *muon0_pt_denomin_plus_endcup = new TH1D("muon0_pt_denomin_plus_endcup","muon0_pt_denomin_plus_endcup;[GeV]; Events",11,10,100);

   TH1D *muon0_pt_nomin_minus_barrel = new TH1D("muon0_pt_nomin_minus_barrel","muon0_pt_nomin_minus_barrel;[GeV]; Events",11,10,100);
   TH1D *muon0_pt_denomin_minus_barrel = new TH1D("muon0_pt_denomin_minus_barrel","muon0_pt_denomin_minus_barrel;[GeV]; Events",11,10,100);
   TH1D *muon0_pt_nomin_minus_endcup = new TH1D("muon0_pt_nomin_minus_endcup","muon0_pt_nomin_minus_endcup;[GeV]; Events",11,10,100);
   TH1D *muon0_pt_denomin_minus_endcup = new TH1D("muon0_pt_denomin_minus_endcup","muon0_pt_denomin_minus_endcup;[GeV]; Events",11,10,100);

   //FOR ISOLATION TRIGGERS

   TH1D *Zmass_mumu_iso_nom = new TH1D("Zmass_mumu_iso_nom", "Zmass_mumu_iso_nom; [GeV]; Events", 60, 60, 120);
   TH1D *Zmass_mumu_iso_denom = new TH1D("Zmass_mumu_iso_denom", "Zmass_mumu_iso_denom; [GeV]; Events", 60, 60, 120);

   TH1D *muon0_pt_nomin_both_iso = new TH1D("muon0_pt_nomin_both_iso","muon0_pt_nomin_both_iso;[GeV]; Events",15,10,500);
   TH1D *muon0_pt_denomin_both_iso = new TH1D("muon0_pt_denomin_both_iso","muon0_pt_denomin_both_iso;[GeV]; Events",15,10,500);
   
   //TH1D *muon0_eta_nomin_plus = new TH1D("muon0_eta_nomin_plus","muon0_eta_nomin_plus;[GeV]; Events",20,-2.5,2.5);
   TH1D *muon0_eta_nomin_both_iso = new TH1D("muon0_eta_nomin_both_iso","   muon0_eta_nomin_both_iso;[GeV]; Events",20,-2.5,2.5);
   TH1D *muon0_eta_denomin_both_iso = new TH1D("muon0_eta_denomin_both_iso","   muon0_eta_denomin_both_iso;[GeV]; Events",20,-2.5,2.5);
   TH1D *muon0_eta_nomin_plus_iso = new TH1D("muon0_eta_nomin_plus_iso","   muon0_eta_nomin_plus_iso;[GeV]; Events",20,-2.5,2.5);
   TH1D *muon0_eta_denomin_plus_iso = new TH1D("muon0_eta_denomin_plus_iso","   muon0_eta_denomin_plus_iso;[GeV]; Events",20,-2.5,2.5);
   TH1D *muon0_eta_nomin_minus_iso = new TH1D("muon0_eta_nomin_minus_iso","   muon0_eta_nomin_minus_iso;[GeV]; Events",20,-2.5,2.5);
   TH1D *muon0_eta_denomin_minus_iso = new TH1D("muon0_eta_denomin_minus_iso","   muon0_eta_denomin_minus_iso;[GeV]; Events",20,-2.5,2.5);

   //TH1D *muon0_pt_nomin_plus = new TH1D("muon0_pt_nomin_plus","muon0_pt_nomin_plus;[GeV]; Events",11,10,100);
   //TH1D *muon0_pt_denomin_plus = new TH1D("muon0_pt_denomin_plus","muon0_pt_denomin_plus;[GeV]; Events",11,10,100);
   //TH1D *muon0_pt_nomin_minus = new TH1D("muon0_pt_nomin_minus","muon0_pt_nomin_minus;[GeV]; Events",11,10,100);
   //TH1D *muon0_pt_denomin_minus = new TH1D("muon0_pt_denomin_minus","muon0_pt_denomin_minus;[GeV]; Events",11,10,100);
    
   //TH1D *muon0_eta_nomin_plus = new TH1D("muon0_eta_nomin_plus","muon0_eta_nomin_plus;[GeV]; Events",20,-2.5,2.5);
   //TH1D *muon0_eta_denomin_plus = new TH1D("muon0_eta_denomin_plus","muon0_eta_denomin_plus;[GeV]; Events",20,-2.5,2.5);
   //TH1D *muon0_eta_nomin_minus = new TH1D("muon0_eta_nomin_minus","muon0_eta_nomin_minus;[GeV]; Events",20,-2.5,2.5);
   //TH1D *muon0_eta_denomin_minus = new TH1D("muon0_eta_denomin_minus","muon0_eta_denomin_minus;[GeV]; Events",20,-2.5,2.5);

   //TH2D *check = new TH2D("check","check",60,0.0,6.28,60,0.0,6.28);

   Long64_t nentries = fChain->GetEntriesFast();
   cout << "Number of entries: " << nentries << ", Name " << fChain->GetName() << endl;

   Long64_t nbytes = 0, nb = 0;
   const float GeV = 0.001;
   // CYCLE OVER EVENTS
   for (Long64_t jentry=0; jentry<nentries;jentry++) {
      Long64_t ientry = LoadTree(jentry);
      if (ientry < 0) break;
      nb = fChain->GetEntry(jentry);   nbytes += nb;
	if (jentry % 10000 == 0) 
  		cout << "SIGN: Processing entry " << jentry << endl;
   Zmass_mumu->Fill(dilep_mll*GeV);

   bool CutProbeTriger, CutTagTriger;
   CutProbeTriger =  ((probe_quality == 0.0) || (probe_quality == 1.0))  && (probe_pt*GeV > 4.0) && (fabs(probe_eta) < 2.4) && (fabs(probe_d0/probe_d0err) < 3.0) && (probe_z0 < 10.0) ;
   CutTagTriger =  (tag_pt*GeV > 24.0) && (fabs(tag_eta) < 2.5) && (tag_matched_HLT_mu20_iloose_L1MU15) && (fabs(tag_d0/tag_d0err) < 3.0) && (tag_z0 < 10.0);

   if ((Tag == "TPTree_MuonProbe_OC_LooseProbes_noProbeIP")||(Tag =="TPTree_MuonProbe_SC_LooseProbes_noProbeIP")){
            //Plot Zmass note figure 15
            //cout << CutProbeTriger << " " << CutTagTriger << endl; 
            
      if ( (CutTagTriger) && (CutProbeTriger) ) {
         //if ((fabs(probe_eta) > 0.1) && (fabs(probe_eta) < 2.4)){  uz si nepamatuji, proc jsem mel cut > 0.1 v ethe
         if (fabs(probe_eta) < 2.4){
            if (probe_matched_HLT_mu20_iloose_L1MU15){
              if (fabs(probe_eta) < 1.05){
                 Zmass_mumu_Trig_nom_barrel->Fill(dilep_mll*GeV); 
                 muon0_pt_nomin_both_barrel->Fill(probe_pt*GeV);
                 if (probe_q > 0) muon0_pt_nomin_plus_barrel->Fill(probe_pt*GeV);
                 if (probe_q < 0) muon0_pt_nomin_minus_barrel->Fill(probe_pt*GeV);
              }
              if (fabs(probe_eta) > 1.05){
                 Zmass_mumu_Trig_nom_endcup->Fill(dilep_mll*GeV); 
                 muon0_pt_nomin_both_endcup->Fill(probe_pt*GeV);
                 if (probe_q > 0) muon0_pt_nomin_plus_endcup->Fill(probe_pt*GeV);
                 if (probe_q < 0) muon0_pt_nomin_minus_endcup->Fill(probe_pt*GeV);
              }
              if (probe_pt*GeV > 25.0) {
                  muon0_eta_nomin_both->Fill(probe_eta);
                  if (probe_q > 0) muon0_eta_nomin_plus->Fill(probe_eta);
                  if (probe_q < 0) muon0_eta_nomin_minus->Fill(probe_eta);
                  }
            }
            if (fabs(probe_eta) < 1.05){
            Zmass_mumu_Trig_denom_barrel->Fill(dilep_mll*GeV); 
            muon0_pt_denomin_both_barrel->Fill(probe_pt*GeV);
            if (probe_q > 0) muon0_pt_denomin_plus_barrel->Fill(probe_pt*GeV);
            if (probe_q < 0) muon0_pt_denomin_minus_barrel->Fill(probe_pt*GeV);
            }
            if (fabs(probe_eta) > 1.05){
            Zmass_mumu_Trig_denom_endcup->Fill(dilep_mll*GeV); 
            muon0_pt_denomin_both_endcup->Fill(probe_pt*GeV);
            if (probe_q > 0) muon0_pt_denomin_plus_endcup->Fill(probe_pt*GeV);
            if (probe_q < 0) muon0_pt_denomin_minus_endcup->Fill(probe_pt*GeV);
            }
            if (probe_pt*GeV > 25.0) {
               muon0_eta_denomin_both->Fill(probe_eta);
               if (probe_q > 0) muon0_eta_denomin_plus->Fill(probe_eta);
               if (probe_q < 0) muon0_eta_denomin_minus->Fill(probe_eta);
            }
         // ted zacnu isolation cut
         //if (probe_matched_IsoFCLoose){
         if (probe_matched_HLT_mu20_iloose_L1MU15){
            if (probe_pt > 10*GeV) Zmass_mumu_iso_nom->Fill(dilep_mll*GeV);
            if (probe_pt > 10*GeV) muon0_pt_nomin_both_iso->Fill(probe_pt*GeV);
            if (probe_pt > 25*GeV) muon0_eta_nomin_both_iso->Fill(probe_eta);
            if (probe_pt > 25*GeV) {
               if (probe_q > 0) muon0_eta_nomin_plus_iso->Fill(probe_eta);
               if (probe_q < 0) muon0_eta_nomin_minus_iso->Fill(probe_eta);
               }
         }
         if (probe_pt > 10*GeV) Zmass_mumu_iso_denom->Fill(dilep_mll*GeV);
         if (probe_pt > 10*GeV) muon0_pt_denomin_both_iso->Fill(probe_pt*GeV);
         if (probe_pt > 25*GeV) muon0_eta_denomin_both_iso->Fill(probe_eta);
         if (probe_pt > 25*GeV) {
            if (probe_q > 0) muon0_eta_denomin_plus_iso->Fill(probe_eta);
            if (probe_q < 0) muon0_eta_denomin_minus_iso->Fill(probe_eta);
            }
         ////////////////////////
         }
      }
      //if ( (dilep_mll*GeV > 81) && (dilep_mll*GeV < 101.0) ) {
         //check->Fill(2.0*atan(exp(probe_eta)), 2.0*atan(exp(tag_eta)));
      //}
   
   }

   }

   cout << "SIGN: End of the Loop() function." << endl;
   outfile->Write();
}


//END OF LOOP FUNCTION

PBClassTrig::PBClassTrig(TTree *tree, TString Input, TString NameOfTree, TString PathToTree): fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(Input);
      if (!f || !f->IsOpen()) {
         f = new TFile(Input);
      }
      TDirectory * dir = (TDirectory*)f->Get(Input+":/"+PathToTree);
      dir->GetObject(NameOfTree,tree);

   }
   Init(tree);
}

PBClassTrig::~PBClassTrig()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t PBClassTrig::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t PBClassTrig::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void PBClassTrig::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("BCID", &BCID, &b_BCID);
   fChain->SetBranchAddress("EventBadMuonVetoVar_HighPt_ID", &EventBadMuonVetoVar_HighPt_ID, &b_EventBadMuonVetoVar_HighPt_ID);
   fChain->SetBranchAddress("EventBadMuonVetoVar_HighPt_ME", &EventBadMuonVetoVar_HighPt_ME, &b_EventBadMuonVetoVar_HighPt_ME);
   fChain->SetBranchAddress("EventBadMuonVetoVar_Loose_ID", &EventBadMuonVetoVar_Loose_ID, &b_EventBadMuonVetoVar_Loose_ID);
   fChain->SetBranchAddress("EventBadMuonVetoVar_Loose_ME", &EventBadMuonVetoVar_Loose_ME, &b_EventBadMuonVetoVar_Loose_ME);
   fChain->SetBranchAddress("EventBadMuonVetoVar_Medium_ID", &EventBadMuonVetoVar_Medium_ID, &b_EventBadMuonVetoVar_Medium_ID);
   fChain->SetBranchAddress("EventBadMuonVetoVar_Medium_ME", &EventBadMuonVetoVar_Medium_ME, &b_EventBadMuonVetoVar_Medium_ME);
   fChain->SetBranchAddress("EventBadMuonVetoVar_Tight_ID", &EventBadMuonVetoVar_Tight_ID, &b_EventBadMuonVetoVar_Tight_ID);
   fChain->SetBranchAddress("EventBadMuonVetoVar_Tight_ME", &EventBadMuonVetoVar_Tight_ME, &b_EventBadMuonVetoVar_Tight_ME);
   fChain->SetBranchAddress("EventBadMuonVetoVar_VeryLoose_ID", &EventBadMuonVetoVar_VeryLoose_ID, &b_EventBadMuonVetoVar_VeryLoose_ID);
   fChain->SetBranchAddress("EventBadMuonVetoVar_VeryLoose_ME", &EventBadMuonVetoVar_VeryLoose_ME, &b_EventBadMuonVetoVar_VeryLoose_ME);
   fChain->SetBranchAddress("PV_n", &PV_n, &b_PV_n);
   fChain->SetBranchAddress("actual_mu", &actual_mu, &b_actual_mu);
   fChain->SetBranchAddress("average_mu", &average_mu, &b_average_mu);
   fChain->SetBranchAddress("calib_jet_probe_pt", &calib_jet_probe_pt, &b_calib_jet_probe_pt);
   fChain->SetBranchAddress("calib_jet_probe_eta", &calib_jet_probe_eta, &b_calib_jet_probe_eta);
   fChain->SetBranchAddress("calib_jet_probe_phi", &calib_jet_probe_phi, &b_calib_jet_probe_phi);
   fChain->SetBranchAddress("calib_jet_probe_index", &calib_jet_probe_index, &b_calib_jet_probe_index);
   fChain->SetBranchAddress("calib_jet_probe_m", &calib_jet_probe_m, &b_calib_jet_probe_m);
   fChain->SetBranchAddress("calib_jet_probe_emfrac", &calib_jet_probe_emfrac, &b_calib_jet_probe_emfrac);
   fChain->SetBranchAddress("calib_jet_probe_jvt", &calib_jet_probe_jvt, &b_calib_jet_probe_jvt);
   fChain->SetBranchAddress("calib_jet_closest_ptrel_probe", &calib_jet_closest_ptrel_probe, &b_calib_jet_closest_ptrel_probe);
   fChain->SetBranchAddress("calib_jet_tag_pt", &calib_jet_tag_pt, &b_calib_jet_tag_pt);
   fChain->SetBranchAddress("calib_jet_tag_eta", &calib_jet_tag_eta, &b_calib_jet_tag_eta);
   fChain->SetBranchAddress("calib_jet_tag_phi", &calib_jet_tag_phi, &b_calib_jet_tag_phi);
   fChain->SetBranchAddress("calib_jet_tag_index", &calib_jet_tag_index, &b_calib_jet_tag_index);
   fChain->SetBranchAddress("calib_jet_tag_m", &calib_jet_tag_m, &b_calib_jet_tag_m);
   fChain->SetBranchAddress("calib_jet_dR_tag", &calib_jet_dR_tag, &b_calib_jet_dR_tag);
   fChain->SetBranchAddress("calib_jet_nJets20", &calib_jet_nJets20, &b_calib_jet_nJets20);
   fChain->SetBranchAddress("calib_jet_dR_probe", &calib_jet_dR_probe, &b_calib_jet_dR_probe);
   fChain->SetBranchAddress("calib_jet_closest_chargedfrac_probe", &calib_jet_closest_chargedfrac_probe, &b_calib_jet_closest_chargedfrac_probe);
   fChain->SetBranchAddress("calib_jet_ntrks500_probe", &calib_jet_ntrks500_probe, &b_calib_jet_ntrks500_probe);
   fChain->SetBranchAddress("calib_jet_n_bad_jets", &calib_jet_n_bad_jets, &b_calib_jet_n_bad_jets);
   fChain->SetBranchAddress("dilep_deta", &dilep_deta, &b_dilep_deta);
   fChain->SetBranchAddress("dilep_dphi", &dilep_dphi, &b_dilep_dphi);
   fChain->SetBranchAddress("dilep_match_2L1RoI_mu6_mu4", &dilep_match_2L1RoI_mu6_mu4, &b_dilep_match_2L1RoI_mu6_mu4);
   fChain->SetBranchAddress("dilep_match_2L1RoI_mu6_mu6", &dilep_match_2L1RoI_mu6_mu6, &b_dilep_match_2L1RoI_mu6_mu6);
   fChain->SetBranchAddress("dilep_mll", &dilep_mll, &b_dilep_mll);
   fChain->SetBranchAddress("dilep_pt", &dilep_pt, &b_dilep_pt);
   fChain->SetBranchAddress("energyDensity_central", &energyDensity_central, &b_energyDensity_central);
   fChain->SetBranchAddress("energyDensity_forward", &energyDensity_forward, &b_energyDensity_forward);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("lumiblock", &lumiblock, &b_lumiblock);
   fChain->SetBranchAddress("mcChannelNumber", &mcChannelNumber, &b_mcChannelNumber);
   fChain->SetBranchAddress("mcEventWeight", &mcEventWeight, &b_mcEventWeight);
   fChain->SetBranchAddress("n_Trks_PrimVtx", &n_Trks_PrimVtx, &b_n_Trks_PrimVtx);
   fChain->SetBranchAddress("probe_pt", &probe_pt, &b_probe_pt);
   fChain->SetBranchAddress("probe_eta", &probe_eta, &b_probe_eta);
   fChain->SetBranchAddress("probe_phi", &probe_phi, &b_probe_phi);
   fChain->SetBranchAddress("probe_index", &probe_index, &b_probe_index);
   fChain->SetBranchAddress("probe_CaloLRLikelihood", &probe_CaloLRLikelihood, &b_probe_CaloLRLikelihood);
   fChain->SetBranchAddress("probe_CaloMuonIDTag", &probe_CaloMuonIDTag, &b_probe_CaloMuonIDTag);
   fChain->SetBranchAddress("probe_EnergyLoss", &probe_EnergyLoss, &b_probe_EnergyLoss);
   fChain->SetBranchAddress("probe_LowPtBDT_PromptTagger", &probe_LowPtBDT_PromptTagger, &b_probe_LowPtBDT_PromptTagger);
   fChain->SetBranchAddress("probe_LowPtBDT_PromptVeto", &probe_LowPtBDT_PromptVeto, &b_probe_LowPtBDT_PromptVeto);
   fChain->SetBranchAddress("probe_MeasEnergyLoss", &probe_MeasEnergyLoss, &b_probe_MeasEnergyLoss);
   fChain->SetBranchAddress("probe_ParamEnergyLoss", &probe_ParamEnergyLoss, &b_probe_ParamEnergyLoss);
   fChain->SetBranchAddress("probe_PromptLeptonInput_DL1mu", &probe_PromptLeptonInput_DL1mu, &b_probe_PromptLeptonInput_DL1mu);
   fChain->SetBranchAddress("probe_PromptLeptonInput_DRlj", &probe_PromptLeptonInput_DRlj, &b_probe_PromptLeptonInput_DRlj);
   fChain->SetBranchAddress("probe_PromptLeptonInput_LepJetPtFrac", &probe_PromptLeptonInput_LepJetPtFrac, &b_probe_PromptLeptonInput_LepJetPtFrac);
   fChain->SetBranchAddress("probe_PromptLeptonInput_PtFrac", &probe_PromptLeptonInput_PtFrac, &b_probe_PromptLeptonInput_PtFrac);
   fChain->SetBranchAddress("probe_PromptLeptonInput_PtRel", &probe_PromptLeptonInput_PtRel, &b_probe_PromptLeptonInput_PtRel);
   fChain->SetBranchAddress("probe_PromptLeptonInput_PtVarCone30Rel", &probe_PromptLeptonInput_PtVarCone30Rel, &b_probe_PromptLeptonInput_PtVarCone30Rel);
   fChain->SetBranchAddress("probe_PromptLeptonInput_TopoEtCone30Rel", &probe_PromptLeptonInput_TopoEtCone30Rel, &b_probe_PromptLeptonInput_TopoEtCone30Rel);
   fChain->SetBranchAddress("probe_PromptLeptonInput_TrackJetNTrack", &probe_PromptLeptonInput_TrackJetNTrack, &b_probe_PromptLeptonInput_TrackJetNTrack);
   fChain->SetBranchAddress("probe_PromptLeptonInput_ip2", &probe_PromptLeptonInput_ip2, &b_probe_PromptLeptonInput_ip2);
   fChain->SetBranchAddress("probe_PromptLeptonInput_ip3", &probe_PromptLeptonInput_ip3, &b_probe_PromptLeptonInput_ip3);
   fChain->SetBranchAddress("probe_PromptLeptonInput_rnnip", &probe_PromptLeptonInput_rnnip, &b_probe_PromptLeptonInput_rnnip);
   fChain->SetBranchAddress("probe_PromptLeptonInput_sv1_jf_ntrkv", &probe_PromptLeptonInput_sv1_jf_ntrkv, &b_probe_PromptLeptonInput_sv1_jf_ntrkv);
   fChain->SetBranchAddress("probe_PromptLeptonIso", &probe_PromptLeptonIso, &b_probe_PromptLeptonIso);
   fChain->SetBranchAddress("probe_PromptLeptonVeto", &probe_PromptLeptonVeto, &b_probe_PromptLeptonVeto);
   fChain->SetBranchAddress("probe_allAuthors", &probe_allAuthors, &b_probe_allAuthors);
   fChain->SetBranchAddress("probe_author", &probe_author, &b_probe_author);
   fChain->SetBranchAddress("probe_combinedTrackOutBoundsPrecisionHits", &probe_combinedTrackOutBoundsPrecisionHits, &b_probe_combinedTrackOutBoundsPrecisionHits);
   fChain->SetBranchAddress("probe_d0", &probe_d0, &b_probe_d0);
   fChain->SetBranchAddress("probe_d0err", &probe_d0err, &b_probe_d0err);
   fChain->SetBranchAddress("probe_eta_exTP", &probe_eta_exTP, &b_probe_eta_exTP);
   fChain->SetBranchAddress("probe_etcone20", &probe_etcone20, &b_probe_etcone20);
   fChain->SetBranchAddress("probe_etcone30", &probe_etcone30, &b_probe_etcone30);
   fChain->SetBranchAddress("probe_etcone40", &probe_etcone40, &b_probe_etcone40);
   fChain->SetBranchAddress("probe_extendedClosePrecisionHits", &probe_extendedClosePrecisionHits, &b_probe_extendedClosePrecisionHits);
   fChain->SetBranchAddress("probe_extendedLargeHits", &probe_extendedLargeHits, &b_probe_extendedLargeHits);
   fChain->SetBranchAddress("probe_extendedLargeHoles", &probe_extendedLargeHoles, &b_probe_extendedLargeHoles);
   fChain->SetBranchAddress("probe_extendedOutBoundsPrecisionHits", &probe_extendedOutBoundsPrecisionHits, &b_probe_extendedOutBoundsPrecisionHits);
   fChain->SetBranchAddress("probe_extendedSmallHits", &probe_extendedSmallHits, &b_probe_extendedSmallHits);
   fChain->SetBranchAddress("probe_extendedSmallHoles", &probe_extendedSmallHoles, &b_probe_extendedSmallHoles);
   fChain->SetBranchAddress("probe_innerClosePrecisionHits", &probe_innerClosePrecisionHits, &b_probe_innerClosePrecisionHits);
   fChain->SetBranchAddress("probe_innerLargeHits", &probe_innerLargeHits, &b_probe_innerLargeHits);
   fChain->SetBranchAddress("probe_innerLargeHoles", &probe_innerLargeHoles, &b_probe_innerLargeHoles);
   fChain->SetBranchAddress("probe_innerOutBoundsPrecisionHits", &probe_innerOutBoundsPrecisionHits, &b_probe_innerOutBoundsPrecisionHits);
   fChain->SetBranchAddress("probe_innerSmallHits", &probe_innerSmallHits, &b_probe_innerSmallHits);
   fChain->SetBranchAddress("probe_innerSmallHoles", &probe_innerSmallHoles, &b_probe_innerSmallHoles);
   fChain->SetBranchAddress("probe_isEndcapGoodLayers", &probe_isEndcapGoodLayers, &b_probe_isEndcapGoodLayers);
   fChain->SetBranchAddress("probe_isSmallGoodSectors", &probe_isSmallGoodSectors, &b_probe_isSmallGoodSectors);
   fChain->SetBranchAddress("probe_middleClosePrecisionHits", &probe_middleClosePrecisionHits, &b_probe_middleClosePrecisionHits);
   fChain->SetBranchAddress("probe_middleLargeHits", &probe_middleLargeHits, &b_probe_middleLargeHits);
   fChain->SetBranchAddress("probe_middleLargeHoles", &probe_middleLargeHoles, &b_probe_middleLargeHoles);
   fChain->SetBranchAddress("probe_middleOutBoundsPrecisionHits", &probe_middleOutBoundsPrecisionHits, &b_probe_middleOutBoundsPrecisionHits);
   fChain->SetBranchAddress("probe_middleSmallHits", &probe_middleSmallHits, &b_probe_middleSmallHits);
   fChain->SetBranchAddress("probe_middleSmallHoles", &probe_middleSmallHoles, &b_probe_middleSmallHoles);
   fChain->SetBranchAddress("probe_momentumBalanceSignificance", &probe_momentumBalanceSignificance, &b_probe_momentumBalanceSignificance);
   fChain->SetBranchAddress("probe_nUnspoiledCscHits", &probe_nUnspoiledCscHits, &b_probe_nUnspoiledCscHits);
   fChain->SetBranchAddress("probe_neflowisol20", &probe_neflowisol20, &b_probe_neflowisol20);
   fChain->SetBranchAddress("probe_neflowisol30", &probe_neflowisol30, &b_probe_neflowisol30);
   fChain->SetBranchAddress("probe_neflowisol40", &probe_neflowisol40, &b_probe_neflowisol40);
   fChain->SetBranchAddress("probe_numberOfGoodPrecisionLayers", &probe_numberOfGoodPrecisionLayers, &b_probe_numberOfGoodPrecisionLayers);
   fChain->SetBranchAddress("probe_numberOfOutliersOnTrack", &probe_numberOfOutliersOnTrack, &b_probe_numberOfOutliersOnTrack);
   fChain->SetBranchAddress("probe_numberOfPhiHoleLayers", &probe_numberOfPhiHoleLayers, &b_probe_numberOfPhiHoleLayers);
   fChain->SetBranchAddress("probe_numberOfPhiLayers", &probe_numberOfPhiLayers, &b_probe_numberOfPhiLayers);
   fChain->SetBranchAddress("probe_numberOfPrecisionHoleLayers", &probe_numberOfPrecisionHoleLayers, &b_probe_numberOfPrecisionHoleLayers);
   fChain->SetBranchAddress("probe_numberOfPrecisionLayers", &probe_numberOfPrecisionLayers, &b_probe_numberOfPrecisionLayers);
   fChain->SetBranchAddress("probe_numberOfTriggerEtaHoleLayers", &probe_numberOfTriggerEtaHoleLayers, &b_probe_numberOfTriggerEtaHoleLayers);
   fChain->SetBranchAddress("probe_numberOfTriggerEtaLayers", &probe_numberOfTriggerEtaLayers, &b_probe_numberOfTriggerEtaLayers);
   fChain->SetBranchAddress("probe_outerClosePrecisionHits", &probe_outerClosePrecisionHits, &b_probe_outerClosePrecisionHits);
   fChain->SetBranchAddress("probe_outerLargeHits", &probe_outerLargeHits, &b_probe_outerLargeHits);
   fChain->SetBranchAddress("probe_outerLargeHoles", &probe_outerLargeHoles, &b_probe_outerLargeHoles);
   fChain->SetBranchAddress("probe_outerOutBoundsPrecisionHits", &probe_outerOutBoundsPrecisionHits, &b_probe_outerOutBoundsPrecisionHits);
   fChain->SetBranchAddress("probe_outerSmallHits", &probe_outerSmallHits, &b_probe_outerSmallHits);
   fChain->SetBranchAddress("probe_outerSmallHoles", &probe_outerSmallHoles, &b_probe_outerSmallHoles);
   fChain->SetBranchAddress("probe_phiLayer1Hits", &probe_phiLayer1Hits, &b_probe_phiLayer1Hits);
   fChain->SetBranchAddress("probe_phiLayer1Holes", &probe_phiLayer1Holes, &b_probe_phiLayer1Holes);
   fChain->SetBranchAddress("probe_phiLayer2Hits", &probe_phiLayer2Hits, &b_probe_phiLayer2Hits);
   fChain->SetBranchAddress("probe_phiLayer2Holes", &probe_phiLayer2Holes, &b_probe_phiLayer2Holes);
   fChain->SetBranchAddress("probe_phiLayer3Hits", &probe_phiLayer3Hits, &b_probe_phiLayer3Hits);
   fChain->SetBranchAddress("probe_phiLayer3Holes", &probe_phiLayer3Holes, &b_probe_phiLayer3Holes);
   fChain->SetBranchAddress("probe_phiLayer4Hits", &probe_phiLayer4Hits, &b_probe_phiLayer4Hits);
   fChain->SetBranchAddress("probe_phiLayer4Holes", &probe_phiLayer4Holes, &b_probe_phiLayer4Holes);
   fChain->SetBranchAddress("probe_phi_exTP", &probe_phi_exTP, &b_probe_phi_exTP);
   fChain->SetBranchAddress("probe_primarySector", &probe_primarySector, &b_probe_primarySector);
   fChain->SetBranchAddress("probe_ptcone20", &probe_ptcone20, &b_probe_ptcone20);
   fChain->SetBranchAddress("probe_ptcone20_LooseTTVA_pt500", &probe_ptcone20_LooseTTVA_pt500, &b_probe_ptcone20_LooseTTVA_pt500);
   fChain->SetBranchAddress("probe_ptcone20_TightTTVA_pt1000", &probe_ptcone20_TightTTVA_pt1000, &b_probe_ptcone20_TightTTVA_pt1000);
   fChain->SetBranchAddress("probe_ptcone20_TightTTVA_pt500", &probe_ptcone20_TightTTVA_pt500, &b_probe_ptcone20_TightTTVA_pt500);
   fChain->SetBranchAddress("probe_ptcone30", &probe_ptcone30, &b_probe_ptcone30);
   fChain->SetBranchAddress("probe_ptcone30_LooseTTVA_pt500", &probe_ptcone30_LooseTTVA_pt500, &b_probe_ptcone30_LooseTTVA_pt500);
   fChain->SetBranchAddress("probe_ptcone30_TightTTVA_pt1000", &probe_ptcone30_TightTTVA_pt1000, &b_probe_ptcone30_TightTTVA_pt1000);
   fChain->SetBranchAddress("probe_ptcone30_TightTTVA_pt500", &probe_ptcone30_TightTTVA_pt500, &b_probe_ptcone30_TightTTVA_pt500);
   fChain->SetBranchAddress("probe_ptcone40", &probe_ptcone40, &b_probe_ptcone40);
   fChain->SetBranchAddress("probe_ptcone40_LooseTTVA_pt500", &probe_ptcone40_LooseTTVA_pt500, &b_probe_ptcone40_LooseTTVA_pt500);
   fChain->SetBranchAddress("probe_ptcone40_TightTTVA_pt1000", &probe_ptcone40_TightTTVA_pt1000, &b_probe_ptcone40_TightTTVA_pt1000);
   fChain->SetBranchAddress("probe_ptcone40_TightTTVA_pt500", &probe_ptcone40_TightTTVA_pt500, &b_probe_ptcone40_TightTTVA_pt500);
   fChain->SetBranchAddress("probe_ptvarcone20", &probe_ptvarcone20, &b_probe_ptvarcone20);
   fChain->SetBranchAddress("probe_ptvarcone20_LooseTTVA_pt500", &probe_ptvarcone20_LooseTTVA_pt500, &b_probe_ptvarcone20_LooseTTVA_pt500);
   fChain->SetBranchAddress("probe_ptvarcone20_TightTTVA_pt1000", &probe_ptvarcone20_TightTTVA_pt1000, &b_probe_ptvarcone20_TightTTVA_pt1000);
   fChain->SetBranchAddress("probe_ptvarcone20_TightTTVA_pt500", &probe_ptvarcone20_TightTTVA_pt500, &b_probe_ptvarcone20_TightTTVA_pt500);
   fChain->SetBranchAddress("probe_ptvarcone30", &probe_ptvarcone30, &b_probe_ptvarcone30);
   fChain->SetBranchAddress("probe_ptvarcone30_LooseTTVA_pt500", &probe_ptvarcone30_LooseTTVA_pt500, &b_probe_ptvarcone30_LooseTTVA_pt500);
   fChain->SetBranchAddress("probe_ptvarcone30_TightTTVA_pt1000", &probe_ptvarcone30_TightTTVA_pt1000, &b_probe_ptvarcone30_TightTTVA_pt1000);
   fChain->SetBranchAddress("probe_ptvarcone30_TightTTVA_pt500", &probe_ptvarcone30_TightTTVA_pt500, &b_probe_ptvarcone30_TightTTVA_pt500);
   fChain->SetBranchAddress("probe_ptvarcone40", &probe_ptvarcone40, &b_probe_ptvarcone40);
   fChain->SetBranchAddress("probe_ptvarcone40_LooseTTVA_pt500", &probe_ptvarcone40_LooseTTVA_pt500, &b_probe_ptvarcone40_LooseTTVA_pt500);
   fChain->SetBranchAddress("probe_ptvarcone40_TightTTVA_pt1000", &probe_ptvarcone40_TightTTVA_pt1000, &b_probe_ptvarcone40_TightTTVA_pt1000);
   fChain->SetBranchAddress("probe_ptvarcone40_TightTTVA_pt500", &probe_ptvarcone40_TightTTVA_pt500, &b_probe_ptvarcone40_TightTTVA_pt500);
   fChain->SetBranchAddress("probe_q", &probe_q, &b_probe_q);
   fChain->SetBranchAddress("probe_scatteringCurvatureSignificance", &probe_scatteringCurvatureSignificance, &b_probe_scatteringCurvatureSignificance);
   fChain->SetBranchAddress("probe_scatteringNeighbourSignificance", &probe_scatteringNeighbourSignificance, &b_probe_scatteringNeighbourSignificance);
   fChain->SetBranchAddress("probe_secondarySector", &probe_secondarySector, &b_probe_secondarySector);
   fChain->SetBranchAddress("probe_segmentDeltaEta", &probe_segmentDeltaEta, &b_probe_segmentDeltaEta);
   fChain->SetBranchAddress("probe_segmentDeltaPhi", &probe_segmentDeltaPhi, &b_probe_segmentDeltaPhi);
   fChain->SetBranchAddress("probe_topocore", &probe_topocore, &b_probe_topocore);
   fChain->SetBranchAddress("probe_topoetcone20", &probe_topoetcone20, &b_probe_topoetcone20);
   fChain->SetBranchAddress("probe_topoetcone30", &probe_topoetcone30, &b_probe_topoetcone30);
   fChain->SetBranchAddress("probe_topoetcone40", &probe_topoetcone40, &b_probe_topoetcone40);
   fChain->SetBranchAddress("probe_truth_origin", &probe_truth_origin, &b_probe_truth_origin);
   fChain->SetBranchAddress("probe_truth_type", &probe_truth_type, &b_probe_truth_type);
   fChain->SetBranchAddress("probe_type", &probe_type, &b_probe_type);
   fChain->SetBranchAddress("probe_z0", &probe_z0, &b_probe_z0);
   fChain->SetBranchAddress("probe_Segment1_chi2", &probe_Segment1_chi2, &b_probe_Segment1_chi2);
   fChain->SetBranchAddress("probe_Segment1_chmbIdx", &probe_Segment1_chmbIdx, &b_probe_Segment1_chmbIdx);
   fChain->SetBranchAddress("probe_Segment1_nDoF", &probe_Segment1_nDoF, &b_probe_Segment1_nDoF);
   fChain->SetBranchAddress("probe_Segment2_chi2", &probe_Segment2_chi2, &b_probe_Segment2_chi2);
   fChain->SetBranchAddress("probe_Segment2_chmbIdx", &probe_Segment2_chmbIdx, &b_probe_Segment2_chmbIdx);
   fChain->SetBranchAddress("probe_Segment2_nDoF", &probe_Segment2_nDoF, &b_probe_Segment2_nDoF);
   fChain->SetBranchAddress("probe_chi2_PR", &probe_chi2_PR, &b_probe_chi2_PR);
   fChain->SetBranchAddress("probe_dRMatch_HLT_2mu10_OBSR", &probe_dRMatch_HLT_2mu10_OBSR, &b_probe_dRMatch_HLT_2mu10_OBSR);
   fChain->SetBranchAddress("probe_dRMatch_HLT_2mu10_nomucomb_OBSR", &probe_dRMatch_HLT_2mu10_nomucomb_OBSR, &b_probe_dRMatch_HLT_2mu10_nomucomb_OBSR);
   fChain->SetBranchAddress("probe_dRMatch_HLT_2mu14_OBSR", &probe_dRMatch_HLT_2mu14_OBSR, &b_probe_dRMatch_HLT_2mu14_OBSR);
   fChain->SetBranchAddress("probe_dRMatch_HLT_2mu14_nomucomb_OBSR", &probe_dRMatch_HLT_2mu14_nomucomb_OBSR, &b_probe_dRMatch_HLT_2mu14_nomucomb_OBSR);
   fChain->SetBranchAddress("probe_dRMatch_HLT_2mu15_OBSR", &probe_dRMatch_HLT_2mu15_OBSR, &b_probe_dRMatch_HLT_2mu15_OBSR);
   fChain->SetBranchAddress("probe_dRMatch_HLT_2mu15_nomucomb_OBSR", &probe_dRMatch_HLT_2mu15_nomucomb_OBSR, &b_probe_dRMatch_HLT_2mu15_nomucomb_OBSR);
   fChain->SetBranchAddress("probe_dRMatch_HLT_2mu6_bJpsimumu", &probe_dRMatch_HLT_2mu6_bJpsimumu, &b_probe_dRMatch_HLT_2mu6_bJpsimumu);
   fChain->SetBranchAddress("probe_dRMatch_HLT_2mu6_bJpsimumui_noL2", &probe_dRMatch_HLT_2mu6_bJpsimumui_noL2, &b_probe_dRMatch_HLT_2mu6_bJpsimumui_noL2);
   fChain->SetBranchAddress("probe_dRMatch_HLT_AllTriggers", &probe_dRMatch_HLT_AllTriggers, &b_probe_dRMatch_HLT_AllTriggers);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu10", &probe_dRMatch_HLT_mu10, &b_probe_dRMatch_HLT_mu10);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu10_RM", &probe_dRMatch_HLT_mu10_RM, &b_probe_dRMatch_HLT_mu10_RM);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu10_idperf", &probe_dRMatch_HLT_mu10_idperf, &b_probe_dRMatch_HLT_mu10_idperf);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu10_idperf_RM", &probe_dRMatch_HLT_mu10_idperf_RM, &b_probe_dRMatch_HLT_mu10_idperf_RM);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu10_msonly", &probe_dRMatch_HLT_mu10_msonly, &b_probe_dRMatch_HLT_mu10_msonly);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu10_msonly_RM", &probe_dRMatch_HLT_mu10_msonly_RM, &b_probe_dRMatch_HLT_mu10_msonly_RM);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu10_nomucomb_RM", &probe_dRMatch_HLT_mu10_nomucomb_RM, &b_probe_dRMatch_HLT_mu10_nomucomb_RM);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu13_mu13_idperf_Zmumu", &probe_dRMatch_HLT_mu13_mu13_idperf_Zmumu, &b_probe_dRMatch_HLT_mu13_mu13_idperf_Zmumu);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu14", &probe_dRMatch_HLT_mu14, &b_probe_dRMatch_HLT_mu14);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu14_RM", &probe_dRMatch_HLT_mu14_RM, &b_probe_dRMatch_HLT_mu14_RM);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu14_ivarloose", &probe_dRMatch_HLT_mu14_ivarloose, &b_probe_dRMatch_HLT_mu14_ivarloose);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu14_nomucomb_RM", &probe_dRMatch_HLT_mu14_nomucomb_RM, &b_probe_dRMatch_HLT_mu14_nomucomb_RM);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu15noL1", &probe_dRMatch_HLT_mu15noL1, &b_probe_dRMatch_HLT_mu15noL1);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu15noL1_RM", &probe_dRMatch_HLT_mu15noL1_RM, &b_probe_dRMatch_HLT_mu15noL1_RM);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu18", &probe_dRMatch_HLT_mu18, &b_probe_dRMatch_HLT_mu18);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu18_2mu0noL1_JpsimumuFS", &probe_dRMatch_HLT_mu18_2mu0noL1_JpsimumuFS, &b_probe_dRMatch_HLT_mu18_2mu0noL1_JpsimumuFS);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu18_2mu4noL1_JpsimumuL2", &probe_dRMatch_HLT_mu18_2mu4noL1_JpsimumuL2, &b_probe_dRMatch_HLT_mu18_2mu4noL1_JpsimumuL2);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu18_RM", &probe_dRMatch_HLT_mu18_RM, &b_probe_dRMatch_HLT_mu18_RM);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu18_mu8noL1_OBSR", &probe_dRMatch_HLT_mu18_mu8noL1_OBSR, &b_probe_dRMatch_HLT_mu18_mu8noL1_OBSR);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu18noL1", &probe_dRMatch_HLT_mu18noL1, &b_probe_dRMatch_HLT_mu18noL1);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu18noL1_RM", &probe_dRMatch_HLT_mu18noL1_RM, &b_probe_dRMatch_HLT_mu18noL1_RM);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu20", &probe_dRMatch_HLT_mu20, &b_probe_dRMatch_HLT_mu20);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu20_L1MU15_RM", &probe_dRMatch_HLT_mu20_L1MU15_RM, &b_probe_dRMatch_HLT_mu20_L1MU15_RM);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu20_RM", &probe_dRMatch_HLT_mu20_RM, &b_probe_dRMatch_HLT_mu20_RM);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu20_idperf", &probe_dRMatch_HLT_mu20_idperf, &b_probe_dRMatch_HLT_mu20_idperf);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu20_idperf_RM", &probe_dRMatch_HLT_mu20_idperf_RM, &b_probe_dRMatch_HLT_mu20_idperf_RM);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu20_iloose_L1MU15", &probe_dRMatch_HLT_mu20_iloose_L1MU15, &b_probe_dRMatch_HLT_mu20_iloose_L1MU15);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu20_ivarloose_L1MU15", &probe_dRMatch_HLT_mu20_ivarloose_L1MU15, &b_probe_dRMatch_HLT_mu20_ivarloose_L1MU15);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu20_msonly", &probe_dRMatch_HLT_mu20_msonly, &b_probe_dRMatch_HLT_mu20_msonly);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu20_msonly_RM", &probe_dRMatch_HLT_mu20_msonly_RM, &b_probe_dRMatch_HLT_mu20_msonly_RM);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu20_mu8noL1_OBSR", &probe_dRMatch_HLT_mu20_mu8noL1_OBSR, &b_probe_dRMatch_HLT_mu20_mu8noL1_OBSR);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu20_mu8noL1_calotag_0eta010_OBSR", &probe_dRMatch_HLT_mu20_mu8noL1_calotag_0eta010_OBSR, &b_probe_dRMatch_HLT_mu20_mu8noL1_calotag_0eta010_OBSR);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu22", &probe_dRMatch_HLT_mu22, &b_probe_dRMatch_HLT_mu22);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu22_RM", &probe_dRMatch_HLT_mu22_RM, &b_probe_dRMatch_HLT_mu22_RM);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu22_mu8noL1_OBSR", &probe_dRMatch_HLT_mu22_mu8noL1_OBSR, &b_probe_dRMatch_HLT_mu22_mu8noL1_OBSR);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu22_mu8noL1_RM", &probe_dRMatch_HLT_mu22_mu8noL1_RM, &b_probe_dRMatch_HLT_mu22_mu8noL1_RM);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu22_mu8noL1_calotag_0eta010_OBSR", &probe_dRMatch_HLT_mu22_mu8noL1_calotag_0eta010_OBSR, &b_probe_dRMatch_HLT_mu22_mu8noL1_calotag_0eta010_OBSR);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu24", &probe_dRMatch_HLT_mu24, &b_probe_dRMatch_HLT_mu24);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu24_2mu4noL1_OBSR", &probe_dRMatch_HLT_mu24_2mu4noL1_OBSR, &b_probe_dRMatch_HLT_mu24_2mu4noL1_OBSR);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu24_L1MU15_RM", &probe_dRMatch_HLT_mu24_L1MU15_RM, &b_probe_dRMatch_HLT_mu24_L1MU15_RM);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu24_RM", &probe_dRMatch_HLT_mu24_RM, &b_probe_dRMatch_HLT_mu24_RM);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu24_iloose", &probe_dRMatch_HLT_mu24_iloose, &b_probe_dRMatch_HLT_mu24_iloose);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu24_iloose_L1MU15", &probe_dRMatch_HLT_mu24_iloose_L1MU15, &b_probe_dRMatch_HLT_mu24_iloose_L1MU15);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu24_imedium", &probe_dRMatch_HLT_mu24_imedium, &b_probe_dRMatch_HLT_mu24_imedium);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu24_ivarloose", &probe_dRMatch_HLT_mu24_ivarloose, &b_probe_dRMatch_HLT_mu24_ivarloose);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu24_ivarloose_L1MU15", &probe_dRMatch_HLT_mu24_ivarloose_L1MU15, &b_probe_dRMatch_HLT_mu24_ivarloose_L1MU15);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu24_ivarmedium", &probe_dRMatch_HLT_mu24_ivarmedium, &b_probe_dRMatch_HLT_mu24_ivarmedium);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu24_mu10noL1_calotag_0eta010_OBSR", &probe_dRMatch_HLT_mu24_mu10noL1_calotag_0eta010_OBSR, &b_probe_dRMatch_HLT_mu24_mu10noL1_calotag_0eta010_OBSR);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu24_mu8noL1_OBSR", &probe_dRMatch_HLT_mu24_mu8noL1_OBSR, &b_probe_dRMatch_HLT_mu24_mu8noL1_OBSR);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu24_mu8noL1_calotag_0eta010_OBSR", &probe_dRMatch_HLT_mu24_mu8noL1_calotag_0eta010_OBSR, &b_probe_dRMatch_HLT_mu24_mu8noL1_calotag_0eta010_OBSR);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu26_RM", &probe_dRMatch_HLT_mu26_RM, &b_probe_dRMatch_HLT_mu26_RM);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu26_imedium", &probe_dRMatch_HLT_mu26_imedium, &b_probe_dRMatch_HLT_mu26_imedium);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu26_ivarmedium", &probe_dRMatch_HLT_mu26_ivarmedium, &b_probe_dRMatch_HLT_mu26_ivarmedium);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu26_mu10noL1_calotag_0eta010_OBSR", &probe_dRMatch_HLT_mu26_mu10noL1_calotag_0eta010_OBSR, &b_probe_dRMatch_HLT_mu26_mu10noL1_calotag_0eta010_OBSR);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu26_mu8noL1_OBSR", &probe_dRMatch_HLT_mu26_mu8noL1_OBSR, &b_probe_dRMatch_HLT_mu26_mu8noL1_OBSR);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu26_mu8noL1_calotag_0eta010_OBSR", &probe_dRMatch_HLT_mu26_mu8noL1_calotag_0eta010_OBSR, &b_probe_dRMatch_HLT_mu26_mu8noL1_calotag_0eta010_OBSR);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu28_imedium", &probe_dRMatch_HLT_mu28_imedium, &b_probe_dRMatch_HLT_mu28_imedium);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu28_ivarmedium", &probe_dRMatch_HLT_mu28_ivarmedium, &b_probe_dRMatch_HLT_mu28_ivarmedium);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu28_mu8noL1_calotag_0eta010_OBSR", &probe_dRMatch_HLT_mu28_mu8noL1_calotag_0eta010_OBSR, &b_probe_dRMatch_HLT_mu28_mu8noL1_calotag_0eta010_OBSR);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu4", &probe_dRMatch_HLT_mu4, &b_probe_dRMatch_HLT_mu4);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu40", &probe_dRMatch_HLT_mu40, &b_probe_dRMatch_HLT_mu40);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu4_RM", &probe_dRMatch_HLT_mu4_RM, &b_probe_dRMatch_HLT_mu4_RM);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu4_idperf", &probe_dRMatch_HLT_mu4_idperf, &b_probe_dRMatch_HLT_mu4_idperf);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu4_idperf_RM", &probe_dRMatch_HLT_mu4_idperf_RM, &b_probe_dRMatch_HLT_mu4_idperf_RM);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu4_msonly", &probe_dRMatch_HLT_mu4_msonly, &b_probe_dRMatch_HLT_mu4_msonly);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu4_msonly_RM", &probe_dRMatch_HLT_mu4_msonly_RM, &b_probe_dRMatch_HLT_mu4_msonly_RM);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu4_mu4_idperf_bJpsimumu_noid", &probe_dRMatch_HLT_mu4_mu4_idperf_bJpsimumu_noid, &b_probe_dRMatch_HLT_mu4_mu4_idperf_bJpsimumu_noid);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu50", &probe_dRMatch_HLT_mu50, &b_probe_dRMatch_HLT_mu50);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu6", &probe_dRMatch_HLT_mu6, &b_probe_dRMatch_HLT_mu6);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu60", &probe_dRMatch_HLT_mu60, &b_probe_dRMatch_HLT_mu60);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu60_0eta105_msonly_OBSR", &probe_dRMatch_HLT_mu60_0eta105_msonly_OBSR, &b_probe_dRMatch_HLT_mu60_0eta105_msonly_OBSR);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu6_RM", &probe_dRMatch_HLT_mu6_RM, &b_probe_dRMatch_HLT_mu6_RM);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu6_bJpsi_TrkPEB", &probe_dRMatch_HLT_mu6_bJpsi_TrkPEB, &b_probe_dRMatch_HLT_mu6_bJpsi_TrkPEB);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu6_bJpsi_Trkloose", &probe_dRMatch_HLT_mu6_bJpsi_Trkloose, &b_probe_dRMatch_HLT_mu6_bJpsi_Trkloose);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu6_idperf", &probe_dRMatch_HLT_mu6_idperf, &b_probe_dRMatch_HLT_mu6_idperf);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu6_idperf_RM", &probe_dRMatch_HLT_mu6_idperf_RM, &b_probe_dRMatch_HLT_mu6_idperf_RM);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu6_msonly", &probe_dRMatch_HLT_mu6_msonly, &b_probe_dRMatch_HLT_mu6_msonly);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu6_msonly_RM", &probe_dRMatch_HLT_mu6_msonly_RM, &b_probe_dRMatch_HLT_mu6_msonly_RM);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu6_mu4_bJpsimumu", &probe_dRMatch_HLT_mu6_mu4_bJpsimumu, &b_probe_dRMatch_HLT_mu6_mu4_bJpsimumu);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu6_mu4_bJpsimumu_noL2", &probe_dRMatch_HLT_mu6_mu4_bJpsimumu_noL2, &b_probe_dRMatch_HLT_mu6_mu4_bJpsimumu_noL2);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu8_RM", &probe_dRMatch_HLT_mu8_RM, &b_probe_dRMatch_HLT_mu8_RM);
   fChain->SetBranchAddress("probe_dRMatch_HLT_mu8noL1", &probe_dRMatch_HLT_mu8noL1, &b_probe_dRMatch_HLT_mu8noL1);
   fChain->SetBranchAddress("probe_dRMatch_HLT_noalg_L1MU", &probe_dRMatch_HLT_noalg_L1MU, &b_probe_dRMatch_HLT_noalg_L1MU);
   fChain->SetBranchAddress("probe_dRMatch_HLT_noalg_L1MU10", &probe_dRMatch_HLT_noalg_L1MU10, &b_probe_dRMatch_HLT_noalg_L1MU10);
   fChain->SetBranchAddress("probe_dRMatch_HLT_noalg_L1MU11", &probe_dRMatch_HLT_noalg_L1MU11, &b_probe_dRMatch_HLT_noalg_L1MU11);
   fChain->SetBranchAddress("probe_dRMatch_HLT_noalg_L1MU20", &probe_dRMatch_HLT_noalg_L1MU20, &b_probe_dRMatch_HLT_noalg_L1MU20);
   fChain->SetBranchAddress("probe_dRMatch_HLT_noalg_L1MU21", &probe_dRMatch_HLT_noalg_L1MU21, &b_probe_dRMatch_HLT_noalg_L1MU21);
   fChain->SetBranchAddress("probe_dRMatch_L1RoI_mu4", &probe_dRMatch_L1RoI_mu4, &b_probe_dRMatch_L1RoI_mu4);
   fChain->SetBranchAddress("probe_dRMatch_L1RoI_mu6", &probe_dRMatch_L1RoI_mu6, &b_probe_dRMatch_L1RoI_mu6);
   fChain->SetBranchAddress("probe_dRMatch_L1_MU10", &probe_dRMatch_L1_MU10, &b_probe_dRMatch_L1_MU10);
   fChain->SetBranchAddress("probe_dRMatch_L1_MU11", &probe_dRMatch_L1_MU11, &b_probe_dRMatch_L1_MU11);
   fChain->SetBranchAddress("probe_dRMatch_L1_MU15", &probe_dRMatch_L1_MU15, &b_probe_dRMatch_L1_MU15);
   fChain->SetBranchAddress("probe_dRMatch_L1_MU20", &probe_dRMatch_L1_MU20, &b_probe_dRMatch_L1_MU20);
   fChain->SetBranchAddress("probe_dRMatch_L1_MU21", &probe_dRMatch_L1_MU21, &b_probe_dRMatch_L1_MU21);
   fChain->SetBranchAddress("probe_dRMatch_L1_MU4", &probe_dRMatch_L1_MU4, &b_probe_dRMatch_L1_MU4);
   fChain->SetBranchAddress("probe_dRMatch_L1_MU6", &probe_dRMatch_L1_MU6, &b_probe_dRMatch_L1_MU6);
   fChain->SetBranchAddress("probe_etaMS", &probe_etaMS, &b_probe_etaMS);
   fChain->SetBranchAddress("probe_matched_CaloTag", &probe_matched_CaloTag, &b_probe_matched_CaloTag);
   fChain->SetBranchAddress("probe_matched_HLT_2mu6_bJpsimumu", &probe_matched_HLT_2mu6_bJpsimumu, &b_probe_matched_HLT_2mu6_bJpsimumu);
   fChain->SetBranchAddress("probe_matched_HLT_2mu6_bJpsimumui_noL2", &probe_matched_HLT_2mu6_bJpsimumui_noL2, &b_probe_matched_HLT_2mu6_bJpsimumui_noL2);
   fChain->SetBranchAddress("probe_matched_HLT_AllTriggers", &probe_matched_HLT_AllTriggers, &b_probe_matched_HLT_AllTriggers);
   fChain->SetBranchAddress("probe_matched_HLT_mu10", &probe_matched_HLT_mu10, &b_probe_matched_HLT_mu10);
   fChain->SetBranchAddress("probe_matched_HLT_mu10_idperf", &probe_matched_HLT_mu10_idperf, &b_probe_matched_HLT_mu10_idperf);
   fChain->SetBranchAddress("probe_matched_HLT_mu10_msonly", &probe_matched_HLT_mu10_msonly, &b_probe_matched_HLT_mu10_msonly);
   fChain->SetBranchAddress("probe_matched_HLT_mu13_mu13_idperf_Zmumu", &probe_matched_HLT_mu13_mu13_idperf_Zmumu, &b_probe_matched_HLT_mu13_mu13_idperf_Zmumu);
   fChain->SetBranchAddress("probe_matched_HLT_mu14", &probe_matched_HLT_mu14, &b_probe_matched_HLT_mu14);
   fChain->SetBranchAddress("probe_matched_HLT_mu14_ivarloose", &probe_matched_HLT_mu14_ivarloose, &b_probe_matched_HLT_mu14_ivarloose);
   fChain->SetBranchAddress("probe_matched_HLT_mu15noL1", &probe_matched_HLT_mu15noL1, &b_probe_matched_HLT_mu15noL1);
   fChain->SetBranchAddress("probe_matched_HLT_mu18", &probe_matched_HLT_mu18, &b_probe_matched_HLT_mu18);
   fChain->SetBranchAddress("probe_matched_HLT_mu18_2mu0noL1_JpsimumuFS", &probe_matched_HLT_mu18_2mu0noL1_JpsimumuFS, &b_probe_matched_HLT_mu18_2mu0noL1_JpsimumuFS);
   fChain->SetBranchAddress("probe_matched_HLT_mu18_2mu4noL1_JpsimumuL2", &probe_matched_HLT_mu18_2mu4noL1_JpsimumuL2, &b_probe_matched_HLT_mu18_2mu4noL1_JpsimumuL2);
   fChain->SetBranchAddress("probe_matched_HLT_mu18noL1", &probe_matched_HLT_mu18noL1, &b_probe_matched_HLT_mu18noL1);
   fChain->SetBranchAddress("probe_matched_HLT_mu20", &probe_matched_HLT_mu20, &b_probe_matched_HLT_mu20);
   fChain->SetBranchAddress("probe_matched_HLT_mu20_idperf", &probe_matched_HLT_mu20_idperf, &b_probe_matched_HLT_mu20_idperf);
   fChain->SetBranchAddress("probe_matched_HLT_mu20_iloose_L1MU15", &probe_matched_HLT_mu20_iloose_L1MU15, &b_probe_matched_HLT_mu20_iloose_L1MU15);
   fChain->SetBranchAddress("probe_matched_HLT_mu20_ivarloose_L1MU15", &probe_matched_HLT_mu20_ivarloose_L1MU15, &b_probe_matched_HLT_mu20_ivarloose_L1MU15);
   fChain->SetBranchAddress("probe_matched_HLT_mu20_msonly", &probe_matched_HLT_mu20_msonly, &b_probe_matched_HLT_mu20_msonly);
   fChain->SetBranchAddress("probe_matched_HLT_mu22", &probe_matched_HLT_mu22, &b_probe_matched_HLT_mu22);
   fChain->SetBranchAddress("probe_matched_HLT_mu24", &probe_matched_HLT_mu24, &b_probe_matched_HLT_mu24);
   fChain->SetBranchAddress("probe_matched_HLT_mu24_iloose", &probe_matched_HLT_mu24_iloose, &b_probe_matched_HLT_mu24_iloose);
   fChain->SetBranchAddress("probe_matched_HLT_mu24_iloose_L1MU15", &probe_matched_HLT_mu24_iloose_L1MU15, &b_probe_matched_HLT_mu24_iloose_L1MU15);
   fChain->SetBranchAddress("probe_matched_HLT_mu24_imedium", &probe_matched_HLT_mu24_imedium, &b_probe_matched_HLT_mu24_imedium);
   fChain->SetBranchAddress("probe_matched_HLT_mu24_ivarloose", &probe_matched_HLT_mu24_ivarloose, &b_probe_matched_HLT_mu24_ivarloose);
   fChain->SetBranchAddress("probe_matched_HLT_mu24_ivarloose_L1MU15", &probe_matched_HLT_mu24_ivarloose_L1MU15, &b_probe_matched_HLT_mu24_ivarloose_L1MU15);
   fChain->SetBranchAddress("probe_matched_HLT_mu24_ivarmedium", &probe_matched_HLT_mu24_ivarmedium, &b_probe_matched_HLT_mu24_ivarmedium);
   fChain->SetBranchAddress("probe_matched_HLT_mu26_imedium", &probe_matched_HLT_mu26_imedium, &b_probe_matched_HLT_mu26_imedium);
   fChain->SetBranchAddress("probe_matched_HLT_mu26_ivarmedium", &probe_matched_HLT_mu26_ivarmedium, &b_probe_matched_HLT_mu26_ivarmedium);
   fChain->SetBranchAddress("probe_matched_HLT_mu28_imedium", &probe_matched_HLT_mu28_imedium, &b_probe_matched_HLT_mu28_imedium);
   fChain->SetBranchAddress("probe_matched_HLT_mu28_ivarmedium", &probe_matched_HLT_mu28_ivarmedium, &b_probe_matched_HLT_mu28_ivarmedium);
   fChain->SetBranchAddress("probe_matched_HLT_mu4", &probe_matched_HLT_mu4, &b_probe_matched_HLT_mu4);
   fChain->SetBranchAddress("probe_matched_HLT_mu40", &probe_matched_HLT_mu40, &b_probe_matched_HLT_mu40);
   fChain->SetBranchAddress("probe_matched_HLT_mu4_idperf", &probe_matched_HLT_mu4_idperf, &b_probe_matched_HLT_mu4_idperf);
   fChain->SetBranchAddress("probe_matched_HLT_mu4_msonly", &probe_matched_HLT_mu4_msonly, &b_probe_matched_HLT_mu4_msonly);
   fChain->SetBranchAddress("probe_matched_HLT_mu4_mu4_idperf_bJpsimumu_noid", &probe_matched_HLT_mu4_mu4_idperf_bJpsimumu_noid, &b_probe_matched_HLT_mu4_mu4_idperf_bJpsimumu_noid);
   fChain->SetBranchAddress("probe_matched_HLT_mu50", &probe_matched_HLT_mu50, &b_probe_matched_HLT_mu50);
   fChain->SetBranchAddress("probe_matched_HLT_mu6", &probe_matched_HLT_mu6, &b_probe_matched_HLT_mu6);
   fChain->SetBranchAddress("probe_matched_HLT_mu60", &probe_matched_HLT_mu60, &b_probe_matched_HLT_mu60);
   fChain->SetBranchAddress("probe_matched_HLT_mu6_bJpsi_TrkPEB", &probe_matched_HLT_mu6_bJpsi_TrkPEB, &b_probe_matched_HLT_mu6_bJpsi_TrkPEB);
   fChain->SetBranchAddress("probe_matched_HLT_mu6_bJpsi_Trkloose", &probe_matched_HLT_mu6_bJpsi_Trkloose, &b_probe_matched_HLT_mu6_bJpsi_Trkloose);
   fChain->SetBranchAddress("probe_matched_HLT_mu6_idperf", &probe_matched_HLT_mu6_idperf, &b_probe_matched_HLT_mu6_idperf);
   fChain->SetBranchAddress("probe_matched_HLT_mu6_msonly", &probe_matched_HLT_mu6_msonly, &b_probe_matched_HLT_mu6_msonly);
   fChain->SetBranchAddress("probe_matched_HLT_mu6_mu4_bJpsimumu", &probe_matched_HLT_mu6_mu4_bJpsimumu, &b_probe_matched_HLT_mu6_mu4_bJpsimumu);
   fChain->SetBranchAddress("probe_matched_HLT_mu6_mu4_bJpsimumu_noL2", &probe_matched_HLT_mu6_mu4_bJpsimumu_noL2, &b_probe_matched_HLT_mu6_mu4_bJpsimumu_noL2);
   fChain->SetBranchAddress("probe_matched_HLT_mu8noL1", &probe_matched_HLT_mu8noL1, &b_probe_matched_HLT_mu8noL1);
   fChain->SetBranchAddress("probe_matched_HLT_noalg_L1MU", &probe_matched_HLT_noalg_L1MU, &b_probe_matched_HLT_noalg_L1MU);
   fChain->SetBranchAddress("probe_matched_HLT_noalg_L1MU10", &probe_matched_HLT_noalg_L1MU10, &b_probe_matched_HLT_noalg_L1MU10);
   fChain->SetBranchAddress("probe_matched_HLT_noalg_L1MU11", &probe_matched_HLT_noalg_L1MU11, &b_probe_matched_HLT_noalg_L1MU11);
   fChain->SetBranchAddress("probe_matched_HLT_noalg_L1MU20", &probe_matched_HLT_noalg_L1MU20, &b_probe_matched_HLT_noalg_L1MU20);
   fChain->SetBranchAddress("probe_matched_HLT_noalg_L1MU21", &probe_matched_HLT_noalg_L1MU21, &b_probe_matched_HLT_noalg_L1MU21);
   fChain->SetBranchAddress("probe_matched_HighPt", &probe_matched_HighPt, &b_probe_matched_HighPt);
   fChain->SetBranchAddress("probe_matched_IsoFCLoose", &probe_matched_IsoFCLoose, &b_probe_matched_IsoFCLoose);
   fChain->SetBranchAddress("probe_matched_IsoFCLoose_FixedRad", &probe_matched_IsoFCLoose_FixedRad, &b_probe_matched_IsoFCLoose_FixedRad);
   fChain->SetBranchAddress("probe_matched_IsoFCTight", &probe_matched_IsoFCTight, &b_probe_matched_IsoFCTight);
   fChain->SetBranchAddress("probe_matched_IsoFCTightTrackOnly", &probe_matched_IsoFCTightTrackOnly, &b_probe_matched_IsoFCTightTrackOnly);
   fChain->SetBranchAddress("probe_matched_IsoFCTightTrackOnly_FixedRad", &probe_matched_IsoFCTightTrackOnly_FixedRad, &b_probe_matched_IsoFCTightTrackOnly_FixedRad);
   fChain->SetBranchAddress("probe_matched_IsoFCTight_FixedRad", &probe_matched_IsoFCTight_FixedRad, &b_probe_matched_IsoFCTight_FixedRad);
   fChain->SetBranchAddress("probe_matched_IsoFixedCutHighPtTrackOnly", &probe_matched_IsoFixedCutHighPtTrackOnly, &b_probe_matched_IsoFixedCutHighPtTrackOnly);
   fChain->SetBranchAddress("probe_matched_IsoFixedCutPflowLoose", &probe_matched_IsoFixedCutPflowLoose, &b_probe_matched_IsoFixedCutPflowLoose);
   fChain->SetBranchAddress("probe_matched_IsoFixedCutPflowTight", &probe_matched_IsoFixedCutPflowTight, &b_probe_matched_IsoFixedCutPflowTight);
   fChain->SetBranchAddress("probe_matched_L1_MU10", &probe_matched_L1_MU10, &b_probe_matched_L1_MU10);
   fChain->SetBranchAddress("probe_matched_L1_MU11", &probe_matched_L1_MU11, &b_probe_matched_L1_MU11);
   fChain->SetBranchAddress("probe_matched_L1_MU15", &probe_matched_L1_MU15, &b_probe_matched_L1_MU15);
   fChain->SetBranchAddress("probe_matched_L1_MU20", &probe_matched_L1_MU20, &b_probe_matched_L1_MU20);
   fChain->SetBranchAddress("probe_matched_L1_MU21", &probe_matched_L1_MU21, &b_probe_matched_L1_MU21);
   fChain->SetBranchAddress("probe_matched_L1_MU4", &probe_matched_L1_MU4, &b_probe_matched_L1_MU4);
   fChain->SetBranchAddress("probe_matched_L1_MU6", &probe_matched_L1_MU6, &b_probe_matched_L1_MU6);
   fChain->SetBranchAddress("probe_matched_Loose", &probe_matched_Loose, &b_probe_matched_Loose);
   fChain->SetBranchAddress("probe_matched_LowPt", &probe_matched_LowPt, &b_probe_matched_LowPt);
   fChain->SetBranchAddress("probe_matched_Medium", &probe_matched_Medium, &b_probe_matched_Medium);
   fChain->SetBranchAddress("probe_matched_TRTCut", &probe_matched_TRTCut, &b_probe_matched_TRTCut);
   fChain->SetBranchAddress("probe_matched_Tight", &probe_matched_Tight, &b_probe_matched_Tight);
   fChain->SetBranchAddress("probe_nDof_PR", &probe_nDof_PR, &b_probe_nDof_PR);
   fChain->SetBranchAddress("probe_qoverp_CB", &probe_qoverp_CB, &b_probe_qoverp_CB);
   fChain->SetBranchAddress("probe_qoverp_ID", &probe_qoverp_ID, &b_probe_qoverp_ID);
   fChain->SetBranchAddress("probe_qoverp_ME", &probe_qoverp_ME, &b_probe_qoverp_ME);
   fChain->SetBranchAddress("probe_qoverp_MS", &probe_qoverp_MS, &b_probe_qoverp_MS);
   fChain->SetBranchAddress("probe_qoverp_err_CB", &probe_qoverp_err_CB, &b_probe_qoverp_err_CB);
   fChain->SetBranchAddress("probe_qoverp_err_ID", &probe_qoverp_err_ID, &b_probe_qoverp_err_ID);
   fChain->SetBranchAddress("probe_qoverp_err_ME", &probe_qoverp_err_ME, &b_probe_qoverp_err_ME);
   fChain->SetBranchAddress("probe_qoverp_err_MS", &probe_qoverp_err_MS, &b_probe_qoverp_err_MS);
   fChain->SetBranchAddress("probe_qoverp_err_modified", &probe_qoverp_err_modified, &b_probe_qoverp_err_modified);
   fChain->SetBranchAddress("probe_qoverp_modified", &probe_qoverp_modified, &b_probe_qoverp_modified);
   fChain->SetBranchAddress("probe_quality", &probe_quality, &b_probe_quality);
   fChain->SetBranchAddress("prwWeight", &prwWeight, &b_prwWeight);
   fChain->SetBranchAddress("randomLumiBlockNumber", &randomLumiBlockNumber, &b_randomLumiBlockNumber);
   fChain->SetBranchAddress("randomRunNumber", &randomRunNumber, &b_randomRunNumber);
   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("tag_pt", &tag_pt, &b_tag_pt);
   fChain->SetBranchAddress("tag_eta", &tag_eta, &b_tag_eta);
   fChain->SetBranchAddress("tag_phi", &tag_phi, &b_tag_phi);
   fChain->SetBranchAddress("tag_index", &tag_index, &b_tag_index);
   fChain->SetBranchAddress("tag_d0", &tag_d0, &b_tag_d0);
   fChain->SetBranchAddress("tag_d0err", &tag_d0err, &b_tag_d0err);
   fChain->SetBranchAddress("tag_dR_exTP", &tag_dR_exTP, &b_tag_dR_exTP);
   fChain->SetBranchAddress("tag_deta_exTP", &tag_deta_exTP, &b_tag_deta_exTP);
   fChain->SetBranchAddress("tag_dphi_exTP", &tag_dphi_exTP, &b_tag_dphi_exTP);
   fChain->SetBranchAddress("tag_etcone20", &tag_etcone20, &b_tag_etcone20);
   fChain->SetBranchAddress("tag_etcone30", &tag_etcone30, &b_tag_etcone30);
   fChain->SetBranchAddress("tag_etcone40", &tag_etcone40, &b_tag_etcone40);
   fChain->SetBranchAddress("tag_matched_IsoFCLoose", &tag_matched_IsoFCLoose, &b_tag_matched_IsoFCLoose);
   fChain->SetBranchAddress("tag_matched_IsoFCLoose_FixedRad", &tag_matched_IsoFCLoose_FixedRad, &b_tag_matched_IsoFCLoose_FixedRad);
   fChain->SetBranchAddress("tag_matched_IsoFCTight", &tag_matched_IsoFCTight, &b_tag_matched_IsoFCTight);
   fChain->SetBranchAddress("tag_matched_IsoFCTightTrackOnly", &tag_matched_IsoFCTightTrackOnly, &b_tag_matched_IsoFCTightTrackOnly);
   fChain->SetBranchAddress("tag_matched_IsoFCTightTrackOnly_FixedRad", &tag_matched_IsoFCTightTrackOnly_FixedRad, &b_tag_matched_IsoFCTightTrackOnly_FixedRad);
   fChain->SetBranchAddress("tag_matched_IsoFCTight_FixedRad", &tag_matched_IsoFCTight_FixedRad, &b_tag_matched_IsoFCTight_FixedRad);
   fChain->SetBranchAddress("tag_matched_IsoFixedCutHighPtTrackOnly", &tag_matched_IsoFixedCutHighPtTrackOnly, &b_tag_matched_IsoFixedCutHighPtTrackOnly);
   fChain->SetBranchAddress("tag_matched_IsoFixedCutPflowLoose", &tag_matched_IsoFixedCutPflowLoose, &b_tag_matched_IsoFixedCutPflowLoose);
   fChain->SetBranchAddress("tag_matched_IsoFixedCutPflowTight", &tag_matched_IsoFixedCutPflowTight, &b_tag_matched_IsoFixedCutPflowTight);
   fChain->SetBranchAddress("tag_neflowisol20", &tag_neflowisol20, &b_tag_neflowisol20);
   fChain->SetBranchAddress("tag_neflowisol30", &tag_neflowisol30, &b_tag_neflowisol30);
   fChain->SetBranchAddress("tag_neflowisol40", &tag_neflowisol40, &b_tag_neflowisol40);
   fChain->SetBranchAddress("tag_ptcone20", &tag_ptcone20, &b_tag_ptcone20);
   fChain->SetBranchAddress("tag_ptcone20_LooseTTVA_pt500", &tag_ptcone20_LooseTTVA_pt500, &b_tag_ptcone20_LooseTTVA_pt500);
   fChain->SetBranchAddress("tag_ptcone20_TightTTVA_pt1000", &tag_ptcone20_TightTTVA_pt1000, &b_tag_ptcone20_TightTTVA_pt1000);
   fChain->SetBranchAddress("tag_ptcone20_TightTTVA_pt500", &tag_ptcone20_TightTTVA_pt500, &b_tag_ptcone20_TightTTVA_pt500);
   fChain->SetBranchAddress("tag_ptcone30", &tag_ptcone30, &b_tag_ptcone30);
   fChain->SetBranchAddress("tag_ptcone30_LooseTTVA_pt500", &tag_ptcone30_LooseTTVA_pt500, &b_tag_ptcone30_LooseTTVA_pt500);
   fChain->SetBranchAddress("tag_ptcone30_TightTTVA_pt1000", &tag_ptcone30_TightTTVA_pt1000, &b_tag_ptcone30_TightTTVA_pt1000);
   fChain->SetBranchAddress("tag_ptcone30_TightTTVA_pt500", &tag_ptcone30_TightTTVA_pt500, &b_tag_ptcone30_TightTTVA_pt500);
   fChain->SetBranchAddress("tag_ptcone40", &tag_ptcone40, &b_tag_ptcone40);
   fChain->SetBranchAddress("tag_ptcone40_LooseTTVA_pt500", &tag_ptcone40_LooseTTVA_pt500, &b_tag_ptcone40_LooseTTVA_pt500);
   fChain->SetBranchAddress("tag_ptcone40_TightTTVA_pt1000", &tag_ptcone40_TightTTVA_pt1000, &b_tag_ptcone40_TightTTVA_pt1000);
   fChain->SetBranchAddress("tag_ptcone40_TightTTVA_pt500", &tag_ptcone40_TightTTVA_pt500, &b_tag_ptcone40_TightTTVA_pt500);
   fChain->SetBranchAddress("tag_ptvarcone20", &tag_ptvarcone20, &b_tag_ptvarcone20);
   fChain->SetBranchAddress("tag_ptvarcone20_LooseTTVA_pt500", &tag_ptvarcone20_LooseTTVA_pt500, &b_tag_ptvarcone20_LooseTTVA_pt500);
   fChain->SetBranchAddress("tag_ptvarcone20_TightTTVA_pt1000", &tag_ptvarcone20_TightTTVA_pt1000, &b_tag_ptvarcone20_TightTTVA_pt1000);
   fChain->SetBranchAddress("tag_ptvarcone20_TightTTVA_pt500", &tag_ptvarcone20_TightTTVA_pt500, &b_tag_ptvarcone20_TightTTVA_pt500);
   fChain->SetBranchAddress("tag_ptvarcone30", &tag_ptvarcone30, &b_tag_ptvarcone30);
   fChain->SetBranchAddress("tag_ptvarcone30_LooseTTVA_pt500", &tag_ptvarcone30_LooseTTVA_pt500, &b_tag_ptvarcone30_LooseTTVA_pt500);
   fChain->SetBranchAddress("tag_ptvarcone30_TightTTVA_pt1000", &tag_ptvarcone30_TightTTVA_pt1000, &b_tag_ptvarcone30_TightTTVA_pt1000);
   fChain->SetBranchAddress("tag_ptvarcone30_TightTTVA_pt500", &tag_ptvarcone30_TightTTVA_pt500, &b_tag_ptvarcone30_TightTTVA_pt500);
   fChain->SetBranchAddress("tag_ptvarcone40", &tag_ptvarcone40, &b_tag_ptvarcone40);
   fChain->SetBranchAddress("tag_ptvarcone40_LooseTTVA_pt500", &tag_ptvarcone40_LooseTTVA_pt500, &b_tag_ptvarcone40_LooseTTVA_pt500);
   fChain->SetBranchAddress("tag_ptvarcone40_TightTTVA_pt1000", &tag_ptvarcone40_TightTTVA_pt1000, &b_tag_ptvarcone40_TightTTVA_pt1000);
   fChain->SetBranchAddress("tag_ptvarcone40_TightTTVA_pt500", &tag_ptvarcone40_TightTTVA_pt500, &b_tag_ptvarcone40_TightTTVA_pt500);
   fChain->SetBranchAddress("tag_q", &tag_q, &b_tag_q);
   fChain->SetBranchAddress("tag_topocore", &tag_topocore, &b_tag_topocore);
   fChain->SetBranchAddress("tag_topoetcone20", &tag_topoetcone20, &b_tag_topoetcone20);
   fChain->SetBranchAddress("tag_topoetcone30", &tag_topoetcone30, &b_tag_topoetcone30);
   fChain->SetBranchAddress("tag_topoetcone40", &tag_topoetcone40, &b_tag_topoetcone40);
   fChain->SetBranchAddress("tag_truth_origin", &tag_truth_origin, &b_tag_truth_origin);
   fChain->SetBranchAddress("tag_truth_type", &tag_truth_type, &b_tag_truth_type);
   fChain->SetBranchAddress("tag_z0", &tag_z0, &b_tag_z0);
   fChain->SetBranchAddress("tag_dRMatch_HLT_2mu10_OBSR", &tag_dRMatch_HLT_2mu10_OBSR, &b_tag_dRMatch_HLT_2mu10_OBSR);
   fChain->SetBranchAddress("tag_dRMatch_HLT_2mu10_nomucomb_OBSR", &tag_dRMatch_HLT_2mu10_nomucomb_OBSR, &b_tag_dRMatch_HLT_2mu10_nomucomb_OBSR);
   fChain->SetBranchAddress("tag_dRMatch_HLT_2mu14_OBSR", &tag_dRMatch_HLT_2mu14_OBSR, &b_tag_dRMatch_HLT_2mu14_OBSR);
   fChain->SetBranchAddress("tag_dRMatch_HLT_2mu14_nomucomb_OBSR", &tag_dRMatch_HLT_2mu14_nomucomb_OBSR, &b_tag_dRMatch_HLT_2mu14_nomucomb_OBSR);
   fChain->SetBranchAddress("tag_dRMatch_HLT_2mu15_OBSR", &tag_dRMatch_HLT_2mu15_OBSR, &b_tag_dRMatch_HLT_2mu15_OBSR);
   fChain->SetBranchAddress("tag_dRMatch_HLT_2mu15_nomucomb_OBSR", &tag_dRMatch_HLT_2mu15_nomucomb_OBSR, &b_tag_dRMatch_HLT_2mu15_nomucomb_OBSR);
   fChain->SetBranchAddress("tag_dRMatch_HLT_2mu6_bJpsimumu", &tag_dRMatch_HLT_2mu6_bJpsimumu, &b_tag_dRMatch_HLT_2mu6_bJpsimumu);
   fChain->SetBranchAddress("tag_dRMatch_HLT_2mu6_bJpsimumui_noL2", &tag_dRMatch_HLT_2mu6_bJpsimumui_noL2, &b_tag_dRMatch_HLT_2mu6_bJpsimumui_noL2);
   fChain->SetBranchAddress("tag_dRMatch_HLT_AllTriggers", &tag_dRMatch_HLT_AllTriggers, &b_tag_dRMatch_HLT_AllTriggers);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu10", &tag_dRMatch_HLT_mu10, &b_tag_dRMatch_HLT_mu10);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu10_RM", &tag_dRMatch_HLT_mu10_RM, &b_tag_dRMatch_HLT_mu10_RM);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu10_idperf", &tag_dRMatch_HLT_mu10_idperf, &b_tag_dRMatch_HLT_mu10_idperf);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu10_idperf_RM", &tag_dRMatch_HLT_mu10_idperf_RM, &b_tag_dRMatch_HLT_mu10_idperf_RM);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu10_msonly", &tag_dRMatch_HLT_mu10_msonly, &b_tag_dRMatch_HLT_mu10_msonly);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu10_msonly_RM", &tag_dRMatch_HLT_mu10_msonly_RM, &b_tag_dRMatch_HLT_mu10_msonly_RM);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu10_nomucomb_RM", &tag_dRMatch_HLT_mu10_nomucomb_RM, &b_tag_dRMatch_HLT_mu10_nomucomb_RM);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu13_mu13_idperf_Zmumu", &tag_dRMatch_HLT_mu13_mu13_idperf_Zmumu, &b_tag_dRMatch_HLT_mu13_mu13_idperf_Zmumu);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu14", &tag_dRMatch_HLT_mu14, &b_tag_dRMatch_HLT_mu14);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu14_RM", &tag_dRMatch_HLT_mu14_RM, &b_tag_dRMatch_HLT_mu14_RM);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu14_ivarloose", &tag_dRMatch_HLT_mu14_ivarloose, &b_tag_dRMatch_HLT_mu14_ivarloose);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu14_nomucomb_RM", &tag_dRMatch_HLT_mu14_nomucomb_RM, &b_tag_dRMatch_HLT_mu14_nomucomb_RM);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu15noL1", &tag_dRMatch_HLT_mu15noL1, &b_tag_dRMatch_HLT_mu15noL1);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu15noL1_RM", &tag_dRMatch_HLT_mu15noL1_RM, &b_tag_dRMatch_HLT_mu15noL1_RM);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu18", &tag_dRMatch_HLT_mu18, &b_tag_dRMatch_HLT_mu18);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu18_2mu0noL1_JpsimumuFS", &tag_dRMatch_HLT_mu18_2mu0noL1_JpsimumuFS, &b_tag_dRMatch_HLT_mu18_2mu0noL1_JpsimumuFS);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu18_2mu4noL1_JpsimumuL2", &tag_dRMatch_HLT_mu18_2mu4noL1_JpsimumuL2, &b_tag_dRMatch_HLT_mu18_2mu4noL1_JpsimumuL2);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu18_RM", &tag_dRMatch_HLT_mu18_RM, &b_tag_dRMatch_HLT_mu18_RM);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu18_mu8noL1_OBSR", &tag_dRMatch_HLT_mu18_mu8noL1_OBSR, &b_tag_dRMatch_HLT_mu18_mu8noL1_OBSR);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu18noL1", &tag_dRMatch_HLT_mu18noL1, &b_tag_dRMatch_HLT_mu18noL1);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu18noL1_RM", &tag_dRMatch_HLT_mu18noL1_RM, &b_tag_dRMatch_HLT_mu18noL1_RM);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu20", &tag_dRMatch_HLT_mu20, &b_tag_dRMatch_HLT_mu20);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu20_L1MU15_RM", &tag_dRMatch_HLT_mu20_L1MU15_RM, &b_tag_dRMatch_HLT_mu20_L1MU15_RM);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu20_RM", &tag_dRMatch_HLT_mu20_RM, &b_tag_dRMatch_HLT_mu20_RM);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu20_idperf", &tag_dRMatch_HLT_mu20_idperf, &b_tag_dRMatch_HLT_mu20_idperf);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu20_idperf_RM", &tag_dRMatch_HLT_mu20_idperf_RM, &b_tag_dRMatch_HLT_mu20_idperf_RM);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu20_iloose_L1MU15", &tag_dRMatch_HLT_mu20_iloose_L1MU15, &b_tag_dRMatch_HLT_mu20_iloose_L1MU15);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu20_ivarloose_L1MU15", &tag_dRMatch_HLT_mu20_ivarloose_L1MU15, &b_tag_dRMatch_HLT_mu20_ivarloose_L1MU15);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu20_msonly", &tag_dRMatch_HLT_mu20_msonly, &b_tag_dRMatch_HLT_mu20_msonly);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu20_msonly_RM", &tag_dRMatch_HLT_mu20_msonly_RM, &b_tag_dRMatch_HLT_mu20_msonly_RM);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu20_mu8noL1_OBSR", &tag_dRMatch_HLT_mu20_mu8noL1_OBSR, &b_tag_dRMatch_HLT_mu20_mu8noL1_OBSR);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu20_mu8noL1_calotag_0eta010_OBSR", &tag_dRMatch_HLT_mu20_mu8noL1_calotag_0eta010_OBSR, &b_tag_dRMatch_HLT_mu20_mu8noL1_calotag_0eta010_OBSR);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu22", &tag_dRMatch_HLT_mu22, &b_tag_dRMatch_HLT_mu22);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu22_RM", &tag_dRMatch_HLT_mu22_RM, &b_tag_dRMatch_HLT_mu22_RM);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu22_mu8noL1_OBSR", &tag_dRMatch_HLT_mu22_mu8noL1_OBSR, &b_tag_dRMatch_HLT_mu22_mu8noL1_OBSR);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu22_mu8noL1_RM", &tag_dRMatch_HLT_mu22_mu8noL1_RM, &b_tag_dRMatch_HLT_mu22_mu8noL1_RM);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu22_mu8noL1_calotag_0eta010_OBSR", &tag_dRMatch_HLT_mu22_mu8noL1_calotag_0eta010_OBSR, &b_tag_dRMatch_HLT_mu22_mu8noL1_calotag_0eta010_OBSR);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu24", &tag_dRMatch_HLT_mu24, &b_tag_dRMatch_HLT_mu24);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu24_2mu4noL1_OBSR", &tag_dRMatch_HLT_mu24_2mu4noL1_OBSR, &b_tag_dRMatch_HLT_mu24_2mu4noL1_OBSR);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu24_L1MU15_RM", &tag_dRMatch_HLT_mu24_L1MU15_RM, &b_tag_dRMatch_HLT_mu24_L1MU15_RM);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu24_RM", &tag_dRMatch_HLT_mu24_RM, &b_tag_dRMatch_HLT_mu24_RM);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu24_iloose", &tag_dRMatch_HLT_mu24_iloose, &b_tag_dRMatch_HLT_mu24_iloose);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu24_iloose_L1MU15", &tag_dRMatch_HLT_mu24_iloose_L1MU15, &b_tag_dRMatch_HLT_mu24_iloose_L1MU15);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu24_imedium", &tag_dRMatch_HLT_mu24_imedium, &b_tag_dRMatch_HLT_mu24_imedium);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu24_ivarloose", &tag_dRMatch_HLT_mu24_ivarloose, &b_tag_dRMatch_HLT_mu24_ivarloose);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu24_ivarloose_L1MU15", &tag_dRMatch_HLT_mu24_ivarloose_L1MU15, &b_tag_dRMatch_HLT_mu24_ivarloose_L1MU15);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu24_ivarmedium", &tag_dRMatch_HLT_mu24_ivarmedium, &b_tag_dRMatch_HLT_mu24_ivarmedium);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu24_mu10noL1_calotag_0eta010_OBSR", &tag_dRMatch_HLT_mu24_mu10noL1_calotag_0eta010_OBSR, &b_tag_dRMatch_HLT_mu24_mu10noL1_calotag_0eta010_OBSR);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu24_mu8noL1_OBSR", &tag_dRMatch_HLT_mu24_mu8noL1_OBSR, &b_tag_dRMatch_HLT_mu24_mu8noL1_OBSR);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu24_mu8noL1_calotag_0eta010_OBSR", &tag_dRMatch_HLT_mu24_mu8noL1_calotag_0eta010_OBSR, &b_tag_dRMatch_HLT_mu24_mu8noL1_calotag_0eta010_OBSR);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu26_RM", &tag_dRMatch_HLT_mu26_RM, &b_tag_dRMatch_HLT_mu26_RM);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu26_imedium", &tag_dRMatch_HLT_mu26_imedium, &b_tag_dRMatch_HLT_mu26_imedium);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu26_ivarmedium", &tag_dRMatch_HLT_mu26_ivarmedium, &b_tag_dRMatch_HLT_mu26_ivarmedium);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu26_mu10noL1_calotag_0eta010_OBSR", &tag_dRMatch_HLT_mu26_mu10noL1_calotag_0eta010_OBSR, &b_tag_dRMatch_HLT_mu26_mu10noL1_calotag_0eta010_OBSR);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu26_mu8noL1_OBSR", &tag_dRMatch_HLT_mu26_mu8noL1_OBSR, &b_tag_dRMatch_HLT_mu26_mu8noL1_OBSR);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu26_mu8noL1_calotag_0eta010_OBSR", &tag_dRMatch_HLT_mu26_mu8noL1_calotag_0eta010_OBSR, &b_tag_dRMatch_HLT_mu26_mu8noL1_calotag_0eta010_OBSR);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu28_imedium", &tag_dRMatch_HLT_mu28_imedium, &b_tag_dRMatch_HLT_mu28_imedium);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu28_ivarmedium", &tag_dRMatch_HLT_mu28_ivarmedium, &b_tag_dRMatch_HLT_mu28_ivarmedium);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu28_mu8noL1_calotag_0eta010_OBSR", &tag_dRMatch_HLT_mu28_mu8noL1_calotag_0eta010_OBSR, &b_tag_dRMatch_HLT_mu28_mu8noL1_calotag_0eta010_OBSR);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu4", &tag_dRMatch_HLT_mu4, &b_tag_dRMatch_HLT_mu4);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu40", &tag_dRMatch_HLT_mu40, &b_tag_dRMatch_HLT_mu40);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu4_RM", &tag_dRMatch_HLT_mu4_RM, &b_tag_dRMatch_HLT_mu4_RM);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu4_idperf", &tag_dRMatch_HLT_mu4_idperf, &b_tag_dRMatch_HLT_mu4_idperf);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu4_idperf_RM", &tag_dRMatch_HLT_mu4_idperf_RM, &b_tag_dRMatch_HLT_mu4_idperf_RM);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu4_msonly", &tag_dRMatch_HLT_mu4_msonly, &b_tag_dRMatch_HLT_mu4_msonly);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu4_msonly_RM", &tag_dRMatch_HLT_mu4_msonly_RM, &b_tag_dRMatch_HLT_mu4_msonly_RM);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu4_mu4_idperf_bJpsimumu_noid", &tag_dRMatch_HLT_mu4_mu4_idperf_bJpsimumu_noid, &b_tag_dRMatch_HLT_mu4_mu4_idperf_bJpsimumu_noid);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu50", &tag_dRMatch_HLT_mu50, &b_tag_dRMatch_HLT_mu50);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu6", &tag_dRMatch_HLT_mu6, &b_tag_dRMatch_HLT_mu6);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu60", &tag_dRMatch_HLT_mu60, &b_tag_dRMatch_HLT_mu60);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu60_0eta105_msonly_OBSR", &tag_dRMatch_HLT_mu60_0eta105_msonly_OBSR, &b_tag_dRMatch_HLT_mu60_0eta105_msonly_OBSR);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu6_RM", &tag_dRMatch_HLT_mu6_RM, &b_tag_dRMatch_HLT_mu6_RM);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu6_bJpsi_TrkPEB", &tag_dRMatch_HLT_mu6_bJpsi_TrkPEB, &b_tag_dRMatch_HLT_mu6_bJpsi_TrkPEB);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu6_bJpsi_Trkloose", &tag_dRMatch_HLT_mu6_bJpsi_Trkloose, &b_tag_dRMatch_HLT_mu6_bJpsi_Trkloose);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu6_idperf", &tag_dRMatch_HLT_mu6_idperf, &b_tag_dRMatch_HLT_mu6_idperf);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu6_idperf_RM", &tag_dRMatch_HLT_mu6_idperf_RM, &b_tag_dRMatch_HLT_mu6_idperf_RM);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu6_msonly", &tag_dRMatch_HLT_mu6_msonly, &b_tag_dRMatch_HLT_mu6_msonly);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu6_msonly_RM", &tag_dRMatch_HLT_mu6_msonly_RM, &b_tag_dRMatch_HLT_mu6_msonly_RM);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu6_mu4_bJpsimumu", &tag_dRMatch_HLT_mu6_mu4_bJpsimumu, &b_tag_dRMatch_HLT_mu6_mu4_bJpsimumu);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu6_mu4_bJpsimumu_noL2", &tag_dRMatch_HLT_mu6_mu4_bJpsimumu_noL2, &b_tag_dRMatch_HLT_mu6_mu4_bJpsimumu_noL2);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu8_RM", &tag_dRMatch_HLT_mu8_RM, &b_tag_dRMatch_HLT_mu8_RM);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu8noL1", &tag_dRMatch_HLT_mu8noL1, &b_tag_dRMatch_HLT_mu8noL1);
   fChain->SetBranchAddress("tag_dRMatch_HLT_noalg_L1MU", &tag_dRMatch_HLT_noalg_L1MU, &b_tag_dRMatch_HLT_noalg_L1MU);
   fChain->SetBranchAddress("tag_dRMatch_HLT_noalg_L1MU10", &tag_dRMatch_HLT_noalg_L1MU10, &b_tag_dRMatch_HLT_noalg_L1MU10);
   fChain->SetBranchAddress("tag_dRMatch_HLT_noalg_L1MU11", &tag_dRMatch_HLT_noalg_L1MU11, &b_tag_dRMatch_HLT_noalg_L1MU11);
   fChain->SetBranchAddress("tag_dRMatch_HLT_noalg_L1MU20", &tag_dRMatch_HLT_noalg_L1MU20, &b_tag_dRMatch_HLT_noalg_L1MU20);
   fChain->SetBranchAddress("tag_dRMatch_HLT_noalg_L1MU21", &tag_dRMatch_HLT_noalg_L1MU21, &b_tag_dRMatch_HLT_noalg_L1MU21);
   fChain->SetBranchAddress("tag_dRMatch_L1RoI_mu6", &tag_dRMatch_L1RoI_mu6, &b_tag_dRMatch_L1RoI_mu6);
   fChain->SetBranchAddress("tag_matched_HLT_2mu6_bJpsimumu", &tag_matched_HLT_2mu6_bJpsimumu, &b_tag_matched_HLT_2mu6_bJpsimumu);
   fChain->SetBranchAddress("tag_matched_HLT_2mu6_bJpsimumui_noL2", &tag_matched_HLT_2mu6_bJpsimumui_noL2, &b_tag_matched_HLT_2mu6_bJpsimumui_noL2);
   fChain->SetBranchAddress("tag_matched_HLT_AllTriggers", &tag_matched_HLT_AllTriggers, &b_tag_matched_HLT_AllTriggers);
   fChain->SetBranchAddress("tag_matched_HLT_mu10", &tag_matched_HLT_mu10, &b_tag_matched_HLT_mu10);
   fChain->SetBranchAddress("tag_matched_HLT_mu10_idperf", &tag_matched_HLT_mu10_idperf, &b_tag_matched_HLT_mu10_idperf);
   fChain->SetBranchAddress("tag_matched_HLT_mu10_msonly", &tag_matched_HLT_mu10_msonly, &b_tag_matched_HLT_mu10_msonly);
   fChain->SetBranchAddress("tag_matched_HLT_mu13_mu13_idperf_Zmumu", &tag_matched_HLT_mu13_mu13_idperf_Zmumu, &b_tag_matched_HLT_mu13_mu13_idperf_Zmumu);
   fChain->SetBranchAddress("tag_matched_HLT_mu14", &tag_matched_HLT_mu14, &b_tag_matched_HLT_mu14);
   fChain->SetBranchAddress("tag_matched_HLT_mu14_ivarloose", &tag_matched_HLT_mu14_ivarloose, &b_tag_matched_HLT_mu14_ivarloose);
   fChain->SetBranchAddress("tag_matched_HLT_mu15noL1", &tag_matched_HLT_mu15noL1, &b_tag_matched_HLT_mu15noL1);
   fChain->SetBranchAddress("tag_matched_HLT_mu18", &tag_matched_HLT_mu18, &b_tag_matched_HLT_mu18);
   fChain->SetBranchAddress("tag_matched_HLT_mu18_2mu0noL1_JpsimumuFS", &tag_matched_HLT_mu18_2mu0noL1_JpsimumuFS, &b_tag_matched_HLT_mu18_2mu0noL1_JpsimumuFS);
   fChain->SetBranchAddress("tag_matched_HLT_mu18_2mu4noL1_JpsimumuL2", &tag_matched_HLT_mu18_2mu4noL1_JpsimumuL2, &b_tag_matched_HLT_mu18_2mu4noL1_JpsimumuL2);
   fChain->SetBranchAddress("tag_matched_HLT_mu18noL1", &tag_matched_HLT_mu18noL1, &b_tag_matched_HLT_mu18noL1);
   fChain->SetBranchAddress("tag_matched_HLT_mu20", &tag_matched_HLT_mu20, &b_tag_matched_HLT_mu20);
   fChain->SetBranchAddress("tag_matched_HLT_mu20_idperf", &tag_matched_HLT_mu20_idperf, &b_tag_matched_HLT_mu20_idperf);
   fChain->SetBranchAddress("tag_matched_HLT_mu20_iloose_L1MU15", &tag_matched_HLT_mu20_iloose_L1MU15, &b_tag_matched_HLT_mu20_iloose_L1MU15);
   fChain->SetBranchAddress("tag_matched_HLT_mu20_ivarloose_L1MU15", &tag_matched_HLT_mu20_ivarloose_L1MU15, &b_tag_matched_HLT_mu20_ivarloose_L1MU15);
   fChain->SetBranchAddress("tag_matched_HLT_mu20_msonly", &tag_matched_HLT_mu20_msonly, &b_tag_matched_HLT_mu20_msonly);
   fChain->SetBranchAddress("tag_matched_HLT_mu22", &tag_matched_HLT_mu22, &b_tag_matched_HLT_mu22);
   fChain->SetBranchAddress("tag_matched_HLT_mu24", &tag_matched_HLT_mu24, &b_tag_matched_HLT_mu24);
   fChain->SetBranchAddress("tag_matched_HLT_mu24_iloose", &tag_matched_HLT_mu24_iloose, &b_tag_matched_HLT_mu24_iloose);
   fChain->SetBranchAddress("tag_matched_HLT_mu24_iloose_L1MU15", &tag_matched_HLT_mu24_iloose_L1MU15, &b_tag_matched_HLT_mu24_iloose_L1MU15);
   fChain->SetBranchAddress("tag_matched_HLT_mu24_imedium", &tag_matched_HLT_mu24_imedium, &b_tag_matched_HLT_mu24_imedium);
   fChain->SetBranchAddress("tag_matched_HLT_mu24_ivarloose", &tag_matched_HLT_mu24_ivarloose, &b_tag_matched_HLT_mu24_ivarloose);
   fChain->SetBranchAddress("tag_matched_HLT_mu24_ivarloose_L1MU15", &tag_matched_HLT_mu24_ivarloose_L1MU15, &b_tag_matched_HLT_mu24_ivarloose_L1MU15);
   fChain->SetBranchAddress("tag_matched_HLT_mu24_ivarmedium", &tag_matched_HLT_mu24_ivarmedium, &b_tag_matched_HLT_mu24_ivarmedium);
   fChain->SetBranchAddress("tag_matched_HLT_mu26_imedium", &tag_matched_HLT_mu26_imedium, &b_tag_matched_HLT_mu26_imedium);
   fChain->SetBranchAddress("tag_matched_HLT_mu26_ivarmedium", &tag_matched_HLT_mu26_ivarmedium, &b_tag_matched_HLT_mu26_ivarmedium);
   fChain->SetBranchAddress("tag_matched_HLT_mu28_imedium", &tag_matched_HLT_mu28_imedium, &b_tag_matched_HLT_mu28_imedium);
   fChain->SetBranchAddress("tag_matched_HLT_mu28_ivarmedium", &tag_matched_HLT_mu28_ivarmedium, &b_tag_matched_HLT_mu28_ivarmedium);
   fChain->SetBranchAddress("tag_matched_HLT_mu4", &tag_matched_HLT_mu4, &b_tag_matched_HLT_mu4);
   fChain->SetBranchAddress("tag_matched_HLT_mu40", &tag_matched_HLT_mu40, &b_tag_matched_HLT_mu40);
   fChain->SetBranchAddress("tag_matched_HLT_mu4_idperf", &tag_matched_HLT_mu4_idperf, &b_tag_matched_HLT_mu4_idperf);
   fChain->SetBranchAddress("tag_matched_HLT_mu4_msonly", &tag_matched_HLT_mu4_msonly, &b_tag_matched_HLT_mu4_msonly);
   fChain->SetBranchAddress("tag_matched_HLT_mu4_mu4_idperf_bJpsimumu_noid", &tag_matched_HLT_mu4_mu4_idperf_bJpsimumu_noid, &b_tag_matched_HLT_mu4_mu4_idperf_bJpsimumu_noid);
   fChain->SetBranchAddress("tag_matched_HLT_mu50", &tag_matched_HLT_mu50, &b_tag_matched_HLT_mu50);
   fChain->SetBranchAddress("tag_matched_HLT_mu6", &tag_matched_HLT_mu6, &b_tag_matched_HLT_mu6);
   fChain->SetBranchAddress("tag_matched_HLT_mu60", &tag_matched_HLT_mu60, &b_tag_matched_HLT_mu60);
   fChain->SetBranchAddress("tag_matched_HLT_mu6_bJpsi_TrkPEB", &tag_matched_HLT_mu6_bJpsi_TrkPEB, &b_tag_matched_HLT_mu6_bJpsi_TrkPEB);
   fChain->SetBranchAddress("tag_matched_HLT_mu6_bJpsi_Trkloose", &tag_matched_HLT_mu6_bJpsi_Trkloose, &b_tag_matched_HLT_mu6_bJpsi_Trkloose);
   fChain->SetBranchAddress("tag_matched_HLT_mu6_idperf", &tag_matched_HLT_mu6_idperf, &b_tag_matched_HLT_mu6_idperf);
   fChain->SetBranchAddress("tag_matched_HLT_mu6_msonly", &tag_matched_HLT_mu6_msonly, &b_tag_matched_HLT_mu6_msonly);
   fChain->SetBranchAddress("tag_matched_HLT_mu6_mu4_bJpsimumu", &tag_matched_HLT_mu6_mu4_bJpsimumu, &b_tag_matched_HLT_mu6_mu4_bJpsimumu);
   fChain->SetBranchAddress("tag_matched_HLT_mu6_mu4_bJpsimumu_noL2", &tag_matched_HLT_mu6_mu4_bJpsimumu_noL2, &b_tag_matched_HLT_mu6_mu4_bJpsimumu_noL2);
   fChain->SetBranchAddress("tag_matched_HLT_mu8noL1", &tag_matched_HLT_mu8noL1, &b_tag_matched_HLT_mu8noL1);
   fChain->SetBranchAddress("tag_matched_HLT_noalg_L1MU", &tag_matched_HLT_noalg_L1MU, &b_tag_matched_HLT_noalg_L1MU);
   fChain->SetBranchAddress("tag_matched_HLT_noalg_L1MU10", &tag_matched_HLT_noalg_L1MU10, &b_tag_matched_HLT_noalg_L1MU10);
   fChain->SetBranchAddress("tag_matched_HLT_noalg_L1MU11", &tag_matched_HLT_noalg_L1MU11, &b_tag_matched_HLT_noalg_L1MU11);
   fChain->SetBranchAddress("tag_matched_HLT_noalg_L1MU20", &tag_matched_HLT_noalg_L1MU20, &b_tag_matched_HLT_noalg_L1MU20);
   fChain->SetBranchAddress("tag_matched_HLT_noalg_L1MU21", &tag_matched_HLT_noalg_L1MU21, &b_tag_matched_HLT_noalg_L1MU21);
   fChain->SetBranchAddress("trackMET_abs", &trackMET_abs, &b_trackMET_abs);
   fChain->SetBranchAddress("trackMET_phi", &trackMET_phi, &b_trackMET_phi);
   fChain->SetBranchAddress("trig_HLT_2mu10_OBSR", &trig_HLT_2mu10_OBSR, &b_trig_HLT_2mu10_OBSR);
   fChain->SetBranchAddress("trig_HLT_2mu10_nomucomb_OBSR", &trig_HLT_2mu10_nomucomb_OBSR, &b_trig_HLT_2mu10_nomucomb_OBSR);
   fChain->SetBranchAddress("trig_HLT_2mu14_OBSR", &trig_HLT_2mu14_OBSR, &b_trig_HLT_2mu14_OBSR);
   fChain->SetBranchAddress("trig_HLT_2mu14_nomucomb_OBSR", &trig_HLT_2mu14_nomucomb_OBSR, &b_trig_HLT_2mu14_nomucomb_OBSR);
   fChain->SetBranchAddress("trig_HLT_2mu15_OBSR", &trig_HLT_2mu15_OBSR, &b_trig_HLT_2mu15_OBSR);
   fChain->SetBranchAddress("trig_HLT_2mu15_nomucomb_OBSR", &trig_HLT_2mu15_nomucomb_OBSR, &b_trig_HLT_2mu15_nomucomb_OBSR);
   fChain->SetBranchAddress("trig_HLT_2mu6_bJpsimumu", &trig_HLT_2mu6_bJpsimumu, &b_trig_HLT_2mu6_bJpsimumu);
   fChain->SetBranchAddress("trig_HLT_2mu6_bJpsimumui_noL2", &trig_HLT_2mu6_bJpsimumui_noL2, &b_trig_HLT_2mu6_bJpsimumui_noL2);
   fChain->SetBranchAddress("trig_HLT_AllTriggers", &trig_HLT_AllTriggers, &b_trig_HLT_AllTriggers);
   fChain->SetBranchAddress("trig_HLT_mu10", &trig_HLT_mu10, &b_trig_HLT_mu10);
   fChain->SetBranchAddress("trig_HLT_mu10_RM", &trig_HLT_mu10_RM, &b_trig_HLT_mu10_RM);
   fChain->SetBranchAddress("trig_HLT_mu10_idperf", &trig_HLT_mu10_idperf, &b_trig_HLT_mu10_idperf);
   fChain->SetBranchAddress("trig_HLT_mu10_idperf_RM", &trig_HLT_mu10_idperf_RM, &b_trig_HLT_mu10_idperf_RM);
   fChain->SetBranchAddress("trig_HLT_mu10_msonly", &trig_HLT_mu10_msonly, &b_trig_HLT_mu10_msonly);
   fChain->SetBranchAddress("trig_HLT_mu10_msonly_RM", &trig_HLT_mu10_msonly_RM, &b_trig_HLT_mu10_msonly_RM);
   fChain->SetBranchAddress("trig_HLT_mu10_nomucomb_RM", &trig_HLT_mu10_nomucomb_RM, &b_trig_HLT_mu10_nomucomb_RM);
   fChain->SetBranchAddress("trig_HLT_mu13_mu13_idperf_Zmumu", &trig_HLT_mu13_mu13_idperf_Zmumu, &b_trig_HLT_mu13_mu13_idperf_Zmumu);
   fChain->SetBranchAddress("trig_HLT_mu14", &trig_HLT_mu14, &b_trig_HLT_mu14);
   fChain->SetBranchAddress("trig_HLT_mu14_RM", &trig_HLT_mu14_RM, &b_trig_HLT_mu14_RM);
   fChain->SetBranchAddress("trig_HLT_mu14_ivarloose", &trig_HLT_mu14_ivarloose, &b_trig_HLT_mu14_ivarloose);
   fChain->SetBranchAddress("trig_HLT_mu14_nomucomb_RM", &trig_HLT_mu14_nomucomb_RM, &b_trig_HLT_mu14_nomucomb_RM);
   fChain->SetBranchAddress("trig_HLT_mu15noL1", &trig_HLT_mu15noL1, &b_trig_HLT_mu15noL1);
   fChain->SetBranchAddress("trig_HLT_mu15noL1_RM", &trig_HLT_mu15noL1_RM, &b_trig_HLT_mu15noL1_RM);
   fChain->SetBranchAddress("trig_HLT_mu18", &trig_HLT_mu18, &b_trig_HLT_mu18);
   fChain->SetBranchAddress("trig_HLT_mu18_2mu0noL1_JpsimumuFS", &trig_HLT_mu18_2mu0noL1_JpsimumuFS, &b_trig_HLT_mu18_2mu0noL1_JpsimumuFS);
   fChain->SetBranchAddress("trig_HLT_mu18_2mu4noL1_JpsimumuL2", &trig_HLT_mu18_2mu4noL1_JpsimumuL2, &b_trig_HLT_mu18_2mu4noL1_JpsimumuL2);
   fChain->SetBranchAddress("trig_HLT_mu18_RM", &trig_HLT_mu18_RM, &b_trig_HLT_mu18_RM);
   fChain->SetBranchAddress("trig_HLT_mu18_mu8noL1_OBSR", &trig_HLT_mu18_mu8noL1_OBSR, &b_trig_HLT_mu18_mu8noL1_OBSR);
   fChain->SetBranchAddress("trig_HLT_mu18noL1", &trig_HLT_mu18noL1, &b_trig_HLT_mu18noL1);
   fChain->SetBranchAddress("trig_HLT_mu18noL1_RM", &trig_HLT_mu18noL1_RM, &b_trig_HLT_mu18noL1_RM);
   fChain->SetBranchAddress("trig_HLT_mu20", &trig_HLT_mu20, &b_trig_HLT_mu20);
   fChain->SetBranchAddress("trig_HLT_mu20_L1MU15_RM", &trig_HLT_mu20_L1MU15_RM, &b_trig_HLT_mu20_L1MU15_RM);
   fChain->SetBranchAddress("trig_HLT_mu20_RM", &trig_HLT_mu20_RM, &b_trig_HLT_mu20_RM);
   fChain->SetBranchAddress("trig_HLT_mu20_idperf", &trig_HLT_mu20_idperf, &b_trig_HLT_mu20_idperf);
   fChain->SetBranchAddress("trig_HLT_mu20_idperf_RM", &trig_HLT_mu20_idperf_RM, &b_trig_HLT_mu20_idperf_RM);
   fChain->SetBranchAddress("trig_HLT_mu20_iloose_L1MU15", &trig_HLT_mu20_iloose_L1MU15, &b_trig_HLT_mu20_iloose_L1MU15);
   fChain->SetBranchAddress("trig_HLT_mu20_ivarloose_L1MU15", &trig_HLT_mu20_ivarloose_L1MU15, &b_trig_HLT_mu20_ivarloose_L1MU15);
   fChain->SetBranchAddress("trig_HLT_mu20_msonly", &trig_HLT_mu20_msonly, &b_trig_HLT_mu20_msonly);
   fChain->SetBranchAddress("trig_HLT_mu20_msonly_RM", &trig_HLT_mu20_msonly_RM, &b_trig_HLT_mu20_msonly_RM);
   fChain->SetBranchAddress("trig_HLT_mu20_mu8noL1_OBSR", &trig_HLT_mu20_mu8noL1_OBSR, &b_trig_HLT_mu20_mu8noL1_OBSR);
   fChain->SetBranchAddress("trig_HLT_mu20_mu8noL1_calotag_0eta010_OBSR", &trig_HLT_mu20_mu8noL1_calotag_0eta010_OBSR, &b_trig_HLT_mu20_mu8noL1_calotag_0eta010_OBSR);
   fChain->SetBranchAddress("trig_HLT_mu22", &trig_HLT_mu22, &b_trig_HLT_mu22);
   fChain->SetBranchAddress("trig_HLT_mu22_RM", &trig_HLT_mu22_RM, &b_trig_HLT_mu22_RM);
   fChain->SetBranchAddress("trig_HLT_mu22_mu8noL1_OBSR", &trig_HLT_mu22_mu8noL1_OBSR, &b_trig_HLT_mu22_mu8noL1_OBSR);
   fChain->SetBranchAddress("trig_HLT_mu22_mu8noL1_RM", &trig_HLT_mu22_mu8noL1_RM, &b_trig_HLT_mu22_mu8noL1_RM);
   fChain->SetBranchAddress("trig_HLT_mu22_mu8noL1_calotag_0eta010_OBSR", &trig_HLT_mu22_mu8noL1_calotag_0eta010_OBSR, &b_trig_HLT_mu22_mu8noL1_calotag_0eta010_OBSR);
   fChain->SetBranchAddress("trig_HLT_mu24", &trig_HLT_mu24, &b_trig_HLT_mu24);
   fChain->SetBranchAddress("trig_HLT_mu24_2mu4noL1_OBSR", &trig_HLT_mu24_2mu4noL1_OBSR, &b_trig_HLT_mu24_2mu4noL1_OBSR);
   fChain->SetBranchAddress("trig_HLT_mu24_L1MU15_RM", &trig_HLT_mu24_L1MU15_RM, &b_trig_HLT_mu24_L1MU15_RM);
   fChain->SetBranchAddress("trig_HLT_mu24_RM", &trig_HLT_mu24_RM, &b_trig_HLT_mu24_RM);
   fChain->SetBranchAddress("trig_HLT_mu24_iloose", &trig_HLT_mu24_iloose, &b_trig_HLT_mu24_iloose);
   fChain->SetBranchAddress("trig_HLT_mu24_iloose_L1MU15", &trig_HLT_mu24_iloose_L1MU15, &b_trig_HLT_mu24_iloose_L1MU15);
   fChain->SetBranchAddress("trig_HLT_mu24_imedium", &trig_HLT_mu24_imedium, &b_trig_HLT_mu24_imedium);
   fChain->SetBranchAddress("trig_HLT_mu24_ivarloose", &trig_HLT_mu24_ivarloose, &b_trig_HLT_mu24_ivarloose);
   fChain->SetBranchAddress("trig_HLT_mu24_ivarloose_L1MU15", &trig_HLT_mu24_ivarloose_L1MU15, &b_trig_HLT_mu24_ivarloose_L1MU15);
   fChain->SetBranchAddress("trig_HLT_mu24_ivarmedium", &trig_HLT_mu24_ivarmedium, &b_trig_HLT_mu24_ivarmedium);
   fChain->SetBranchAddress("trig_HLT_mu24_mu10noL1_calotag_0eta010_OBSR", &trig_HLT_mu24_mu10noL1_calotag_0eta010_OBSR, &b_trig_HLT_mu24_mu10noL1_calotag_0eta010_OBSR);
   fChain->SetBranchAddress("trig_HLT_mu24_mu8noL1_OBSR", &trig_HLT_mu24_mu8noL1_OBSR, &b_trig_HLT_mu24_mu8noL1_OBSR);
   fChain->SetBranchAddress("trig_HLT_mu24_mu8noL1_calotag_0eta010_OBSR", &trig_HLT_mu24_mu8noL1_calotag_0eta010_OBSR, &b_trig_HLT_mu24_mu8noL1_calotag_0eta010_OBSR);
   fChain->SetBranchAddress("trig_HLT_mu26_RM", &trig_HLT_mu26_RM, &b_trig_HLT_mu26_RM);
   fChain->SetBranchAddress("trig_HLT_mu26_imedium", &trig_HLT_mu26_imedium, &b_trig_HLT_mu26_imedium);
   fChain->SetBranchAddress("trig_HLT_mu26_ivarmedium", &trig_HLT_mu26_ivarmedium, &b_trig_HLT_mu26_ivarmedium);
   fChain->SetBranchAddress("trig_HLT_mu26_mu10noL1_calotag_0eta010_OBSR", &trig_HLT_mu26_mu10noL1_calotag_0eta010_OBSR, &b_trig_HLT_mu26_mu10noL1_calotag_0eta010_OBSR);
   fChain->SetBranchAddress("trig_HLT_mu26_mu8noL1_OBSR", &trig_HLT_mu26_mu8noL1_OBSR, &b_trig_HLT_mu26_mu8noL1_OBSR);
   fChain->SetBranchAddress("trig_HLT_mu26_mu8noL1_calotag_0eta010_OBSR", &trig_HLT_mu26_mu8noL1_calotag_0eta010_OBSR, &b_trig_HLT_mu26_mu8noL1_calotag_0eta010_OBSR);
   fChain->SetBranchAddress("trig_HLT_mu28_imedium", &trig_HLT_mu28_imedium, &b_trig_HLT_mu28_imedium);
   fChain->SetBranchAddress("trig_HLT_mu28_ivarmedium", &trig_HLT_mu28_ivarmedium, &b_trig_HLT_mu28_ivarmedium);
   fChain->SetBranchAddress("trig_HLT_mu28_mu8noL1_calotag_0eta010_OBSR", &trig_HLT_mu28_mu8noL1_calotag_0eta010_OBSR, &b_trig_HLT_mu28_mu8noL1_calotag_0eta010_OBSR);
   fChain->SetBranchAddress("trig_HLT_mu4", &trig_HLT_mu4, &b_trig_HLT_mu4);
   fChain->SetBranchAddress("trig_HLT_mu40", &trig_HLT_mu40, &b_trig_HLT_mu40);
   fChain->SetBranchAddress("trig_HLT_mu4_RM", &trig_HLT_mu4_RM, &b_trig_HLT_mu4_RM);
   fChain->SetBranchAddress("trig_HLT_mu4_idperf", &trig_HLT_mu4_idperf, &b_trig_HLT_mu4_idperf);
   fChain->SetBranchAddress("trig_HLT_mu4_idperf_RM", &trig_HLT_mu4_idperf_RM, &b_trig_HLT_mu4_idperf_RM);
   fChain->SetBranchAddress("trig_HLT_mu4_msonly", &trig_HLT_mu4_msonly, &b_trig_HLT_mu4_msonly);
   fChain->SetBranchAddress("trig_HLT_mu4_msonly_RM", &trig_HLT_mu4_msonly_RM, &b_trig_HLT_mu4_msonly_RM);
   fChain->SetBranchAddress("trig_HLT_mu4_mu4_idperf_bJpsimumu_noid", &trig_HLT_mu4_mu4_idperf_bJpsimumu_noid, &b_trig_HLT_mu4_mu4_idperf_bJpsimumu_noid);
   fChain->SetBranchAddress("trig_HLT_mu50", &trig_HLT_mu50, &b_trig_HLT_mu50);
   fChain->SetBranchAddress("trig_HLT_mu6", &trig_HLT_mu6, &b_trig_HLT_mu6);
   fChain->SetBranchAddress("trig_HLT_mu60", &trig_HLT_mu60, &b_trig_HLT_mu60);
   fChain->SetBranchAddress("trig_HLT_mu60_0eta105_msonly_OBSR", &trig_HLT_mu60_0eta105_msonly_OBSR, &b_trig_HLT_mu60_0eta105_msonly_OBSR);
   fChain->SetBranchAddress("trig_HLT_mu6_RM", &trig_HLT_mu6_RM, &b_trig_HLT_mu6_RM);
   fChain->SetBranchAddress("trig_HLT_mu6_bJpsi_TrkPEB", &trig_HLT_mu6_bJpsi_TrkPEB, &b_trig_HLT_mu6_bJpsi_TrkPEB);
   fChain->SetBranchAddress("trig_HLT_mu6_bJpsi_Trkloose", &trig_HLT_mu6_bJpsi_Trkloose, &b_trig_HLT_mu6_bJpsi_Trkloose);
   fChain->SetBranchAddress("trig_HLT_mu6_idperf", &trig_HLT_mu6_idperf, &b_trig_HLT_mu6_idperf);
   fChain->SetBranchAddress("trig_HLT_mu6_idperf_RM", &trig_HLT_mu6_idperf_RM, &b_trig_HLT_mu6_idperf_RM);
   fChain->SetBranchAddress("trig_HLT_mu6_msonly", &trig_HLT_mu6_msonly, &b_trig_HLT_mu6_msonly);
   fChain->SetBranchAddress("trig_HLT_mu6_msonly_RM", &trig_HLT_mu6_msonly_RM, &b_trig_HLT_mu6_msonly_RM);
   fChain->SetBranchAddress("trig_HLT_mu6_mu4_bJpsimumu", &trig_HLT_mu6_mu4_bJpsimumu, &b_trig_HLT_mu6_mu4_bJpsimumu);
   fChain->SetBranchAddress("trig_HLT_mu6_mu4_bJpsimumu_noL2", &trig_HLT_mu6_mu4_bJpsimumu_noL2, &b_trig_HLT_mu6_mu4_bJpsimumu_noL2);
   fChain->SetBranchAddress("trig_HLT_mu8_RM", &trig_HLT_mu8_RM, &b_trig_HLT_mu8_RM);
   fChain->SetBranchAddress("trig_HLT_mu8noL1", &trig_HLT_mu8noL1, &b_trig_HLT_mu8noL1);
   fChain->SetBranchAddress("trig_HLT_noalg_L1MU", &trig_HLT_noalg_L1MU, &b_trig_HLT_noalg_L1MU);
   fChain->SetBranchAddress("trig_HLT_noalg_L1MU10", &trig_HLT_noalg_L1MU10, &b_trig_HLT_noalg_L1MU10);
   fChain->SetBranchAddress("trig_HLT_noalg_L1MU11", &trig_HLT_noalg_L1MU11, &b_trig_HLT_noalg_L1MU11);
   fChain->SetBranchAddress("trig_HLT_noalg_L1MU20", &trig_HLT_noalg_L1MU20, &b_trig_HLT_noalg_L1MU20);
   fChain->SetBranchAddress("trig_HLT_noalg_L1MU21", &trig_HLT_noalg_L1MU21, &b_trig_HLT_noalg_L1MU21);
   fChain->SetBranchAddress("vertex_density", &vertex_density, &b_vertex_density);
   Notify();
}

Bool_t PBClassTrig::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void PBClassTrig::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t PBClassTrig::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
