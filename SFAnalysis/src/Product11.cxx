#include "math.h"
#include <string.h>
#include <sstream>
#include <vector>
#include <algorithm>
#include <iostream>  
#include <stdlib.h>

using namespace std;

class Candidate  // trida subretezcu, pocitam si produkt subretezce, 
{ 
    public: 

    string CandidateString;  //subretezec
    double dProduct; // jeho produkt
    double dLogSum; // suma logaritmu
    int index; //index
  
    void ComputeProduct()  //vypocet produktu subretezce
    { 
       dLogSum = 0.0; 
       for (unsigned int i = 0; i < CandidateString.size(); i++ ){
        //    cout << "int " << (int)CandidateString.at(i) - 48 << " " << CandidateString.at(i) << endl;
            dLogSum = dLogSum + log((int)CandidateString.at(i) - 48);
            
       }  
       dProduct = round(exp(dLogSum)); // vypocet pres logaritmus

       //cout << CandidateString  << " " << dProduct << " " << dLogSum << endl;
    } 

    bool operator<(const Candidate & other) // operator
    {
        return dProduct < other.dProduct;
    }
    //bool operator>(const  Candidate & other) //(1)
    //{
    //    return dProduct > other.dProduct;
    //}
    //bool operator==(const  Candidate & other) //(1)
    //{
    //    return dProduct == other.dProduct;
    //}
}; 

bool isNumber(string s)  // function for checing the input
{ 
    for (unsigned int i = 0; i < s.length(); i++) 
        if (isdigit(s[i]) == false) 
            return false; 
  
    return true; 
}

vector<string> SplitZeros(string sIn, unsigned int iDefaultLenOfSerie) //function to split the series of numbers by zero
{
    stringstream sSerie(sIn);
    string sSubSerie;
    vector<string> sSubSerieList;

    if (sIn.find("0")){ 
        while(getline(sSerie, sSubSerie, '0'))
        {
           if (sSubSerie.size() >= iDefaultLenOfSerie){
                cout << "Substrings: " << sSubSerie << endl;
                sSubSerieList.push_back(sSubSerie);
           }
        }
    } else{
        sSubSerieList.push_back(sIn);
    }

 return sSubSerieList;
}

vector<Candidate> PickCandidates(vector<string> vsIn, unsigned int iDefaultLenOfSerie) //function to split the series of numbers by zero
{
    vector<Candidate> vsCandidates;
    Candidate TempCandidate;
    int k = 1;
    for(unsigned int i =0; i < vsIn.size(); i++){
    if (vsIn.at(i).size() == iDefaultLenOfSerie){
        TempCandidate.CandidateString = vsIn.at(i);
        TempCandidate.ComputeProduct();
        TempCandidate.index = k;
        k++;
        vsCandidates.push_back(TempCandidate);
        } else{
            for (unsigned int j =0; j <= (vsIn.at(i).size()-iDefaultLenOfSerie); j++){
                //cout << vsIn.at(i).substr(j,iDefaultLenOfSerie) << endl;
                TempCandidate.CandidateString = vsIn.at(i).substr(j,iDefaultLenOfSerie);
                TempCandidate.ComputeProduct();
                TempCandidate.index = k;
                k++;
                vsCandidates.push_back(TempCandidate);
            }
        
            
        }
    }
    

 return vsCandidates;
}

//vector<Candidate> SwapIndex(vector<Candidate> a, int index1, int index2){
//    Candidate c = a.at(index1);
//    cout << a.at(index1).index << " " << a.at(index2).index << endl;
//    a.at(index1).index = a.at(index2).index;
//    a.at(index2).index = c.index;
//    cout << a.at(index1).index << " " << a.at(index2).index << endl;
//    cout << "******************" << endl;
//    return a;
//}


bool compare(const Candidate a, const Candidate b)
{
    return a.index < b.index;
}


int main (int argc, char *argv[]) { 
    
    string sInput;
    unsigned int iDefaultLenOfSubSerie = 13; // delka retezce, defaultne 13

    if (argc == 1) { // defaultni retezec
        cout << "INFO: The input series of numbers is default. " << endl;    
        cout << "Input series of numbers = " << "1398024787037910369017901433749013239719493017201986983520312774506326239578318016984801869478851843758615607891129494954595017379583319528532088055110254069874715852386305071569329096329522744304355786696648950445244523161731856403098711121722383113522298934233803081353362766142828064444866452387492035890729629049156044077239071381051585930796086680172427121883998797908792274921901699720888093776757273330010533678812202354218097512545405947522436258490771167055601360483958644670632441572215539743697817977846174064955149290862569321978468622482339722413756570560574902614079729686524145351004744216637048440319989000889524345065854122758866688126427171479924442928230863465674813919123162824586078664583591245665294765456828489128831426076900421421902267105562632111110937054421750694165896040817198403850962455444362981230987879927244284909188745801561660979191338754992005240636899125607176063588611646710940507754100225698315520005593572972521636269561882670428252483600823257530420752963450" << endl;    
        sInput = "1398024787037910369017901433749013239719493017201986983520312774506326239578318016984801869478851843758615607891129494954595017379583319528532088055110254069874715852386305071569329096329522744304355786696648950445244523161731856403098711121722383113522298934233803081353362766142828064444866452387492035890729629049156044077239071381051585930796086680172427121883998797908792274921901699720888093776757273330010533678812202354218097512545405947522436258490771167055601360483958644670632441572215539743697817977846174064955149290862569321978468622482339722413756570560574902614079729686524145351004744216637048440319989000889524345065854122758866688126427171479924442928230863465674813919123162824586078664583591245665294765456828489128831426076900421421902267105562632111110937054421750694165896040817198403850962455444362981230987879927244284909188745801561660979191338754992005240636899125607176063588611646710940507754100225698315520005593572972521636269561882670428252483600823257530420752963450";
    } else {
        sInput = argv[1];
        if (isNumber(sInput)){ // zde na testovani dalsich retezcu
            cout << "WARNING: The input series of numbers is not default!" << endl;    
            cout << "Input series of numbers = " << sInput << endl;
        } else{
            cout << "ERROR: Your input contains characters! Please provide only numbers." << endl;
            cout << "INFO: End of the program." << endl;
            return 0;
        }
        if (argc == 3){
            iDefaultLenOfSubSerie = strtol(argv[2], NULL, 10);
        }
    }

    vector<string> vInput = SplitZeros(sInput, iDefaultLenOfSubSerie); //rozsekam si retezec podle nul
    vector<Candidate> vInputCandidates = PickCandidates(vInput, iDefaultLenOfSubSerie); // vyberu vsechny kandidaty, po sobe jdouci cisla
    for(unsigned int i =0; i < vInputCandidates.size(); i++){ // tisk kandidatu
            cout.precision(15);
            cout << "Candidate number " << vInputCandidates.at(i).index  << endl;
            cout << "Series of numbers: " <<  vInputCandidates.at(i).CandidateString << endl;
            cout << "Sum(log): "  << vInputCandidates.at(i).dLogSum << endl; 
            cout << "Product: " <<  vInputCandidates.at(i).dProduct  << endl;
            cout << "---------" << endl;
            cout << " " << endl;
    }
    cout << " ---------------------RESULTS---------------------------" << endl;
    sort(vInputCandidates.begin(), vInputCandidates.end()); // bohuzel trideni neni nejlepsi, nebot vhodni kadidati jsou dva.
    for(unsigned int i =0; i < vInputCandidates.size(); i++){
        cout.precision(15);
        cout << "Candidates after sorting: " << vInputCandidates.at(i).CandidateString << ", product: "  << vInputCandidates.at(i).dProduct << ", index: " << vInputCandidates.at(i).index << endl;
    }

    cout << "--------------------------------------------------------" << endl;
    cout << "Candidate: " << vInputCandidates.at(vInputCandidates.size()-1).CandidateString << " , Max product: "  << vInputCandidates.at(vInputCandidates.size()-1).dProduct << endl;

    return 0;
    

 } 
