#define PBClass_cxx
#include "PBClass.h"
#include "TH2.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TLorentzVector.h"
#include "TROOT.h"
#include "TString.h"
#include "TEfficiency.h"
#include "math.h"
#include "vector"
#include <iostream>
#include <vector>
using namespace std;

void PBClass::Loop(TString Output, TString Tag)
{
   cout << "SIGNe: Start of the function Loop()." << endl;
   if (fChain == 0) return;
   TFile *outfile = new TFile(Output, "recreate");
   cout << "Output: " << Output << endl;
   
   //MUMU HISTOGRAMS
   
   TH1D *Zmass_mumu = new TH1D("Zmass_mumu", "m_{Z mumu}; [GeV]; Events", 60, 60, 120);
   TH1D *Zmass_mumu_IDcuts = new TH1D("Zmass_mumu_IDcuts", "m_{Z mumu}_cuts; [GeV]; Events", 60, 60, 120);

   TH1D *Zmass_mumu_Calocuts = new TH1D("Zmass_mumu_Calocuts", "m_{Z mumu}_cuts; [GeV]; Events", 60, 60, 120);
   TH1D *Zmass_mumu_MEcuts = new TH1D("Zmass_mumu_MEcuts", "m_{Z mumu}_cuts; [GeV]; Events", 60, 60, 120);

   TH1D *Zmass_mumu_Calocuts_nom = new TH1D("Zmass_mumu_Calocuts_nom", "m_{Z mumu}_cuts_nom; [GeV]; Events", 60, 60, 120);
   TH1D *Zmass_mumu_Calocuts_denom = new TH1D("Zmass_mumu_Calocuts_denom", "m_{Z mumu}_cuts_denom; [GeV]; Events", 60, 60, 120);
   TH1D *Zmass_mumu_MEcuts_nom = new TH1D("Zmass_mumu_MEcuts_nom", "m_{Z mumu}_cuts_nom; [GeV]; Events", 60, 60, 120);
   TH1D *Zmass_mumu_MEcuts_denom = new TH1D("Zmass_mumu_MEcuts_denom", "m_{Z mumu}_cuts_denom; [GeV]; Events", 60, 60, 120);

   //TH1D *Zmass_mumu_Trig_nom = new TH1D("Zmass_mumu_Trig_nom", "m_{Z mumu}_cuts_nom; [GeV]; Events", 60, 60, 120);
   //TH1D *Zmass_mumu_Trig_denom = new TH1D("Zmass_mumu_Trig_denom", "m_{Z mumu}_cuts_denom; [GeV]; Events", 60, 60, 120);

   //TH2D *check = new TH2D("check","check",60,0.0,6.28,60,0.0,6.28);
    
    //TH1D *muon0_pt = new TH1D("muon0_pt","muon0_pt;[GeV]; Events",500,0,500);
    TH1D *muon0_pt_nomin_plus = new TH1D("muon0_pt_nomin_plus","muon0_pt_nomin_plus;[GeV]; Events",11,10,100);
    TH1D *muon0_pt_denomin_plus = new TH1D("muon0_pt_denomin_plus","muon0_pt_denomin_plus;[GeV]; Events",11,10,100);
    TH1D *muon0_pt_nomin_minus = new TH1D("muon0_pt_nomin_minus","muon0_pt_nomin_minus;[GeV]; Events",11,10,100);
    TH1D *muon0_pt_denomin_minus = new TH1D("muon0_pt_denomin_minus","muon0_pt_denomin_minus;[GeV]; Events",11,10,100);
    
    TH1D *muon0_eta_nomin_plus = new TH1D("muon0_eta_nomin_plus","muon0_eta_nomin_plus;[GeV]; Events",20,-2.5,2.5);
    TH1D *muon0_eta_denomin_plus = new TH1D("muon0_eta_denomin_plus","muon0_eta_denomin_plus;[GeV]; Events",20,-2.5,2.5);
    TH1D *muon0_eta_nomin_minus = new TH1D("muon0_eta_nomin_minus","muon0_eta_nomin_minus;[GeV]; Events",20,-2.5,2.5);
    TH1D *muon0_eta_denomin_minus = new TH1D("muon0_eta_denomin_minus","muon0_eta_denomin_minus;[GeV]; Events",20,-2.5,2.5);
    
    //TH1D *muon0_eta = new TH1D("muon0_eta","muon0_eta",60,-6,6);
    //TH1D *muon0_phi = new TH1D("muon0_phi","muon0_phi",60,-6.28,6.28);
    //TH1D *muon0_e = new TH1D("muon0_e","muon0_e",500,0,500);
    //TH1D *muon0_charge = new TH1D("muon0_charge","muon0_charge",10,-2,2);
 
    //TH1D *muon1_pt = new TH1D("muon1_pt","muon1_pt;[GeV]; Events",500,0,500);
    //TH1D *muon1_eta = new TH1D("muon1_eta","muon1_eta",60,-6,6);
    //TH1D *muon1_phi = new TH1D("muon1_phi","muon1_phi",60,-6.28,6.28);
    //TH1D *muon1_e = new TH1D("muon1_e","muon1_e",500,0,500);
    //TH1D *muon1_charge = new TH1D("muon1_charge","muon1_charge",10,-2,2);

   Long64_t nentries = fChain->GetEntriesFast();

   Long64_t nbytes = 0, nb = 0;
   
   // FOUR VECTORS Zmumu
   TLorentzVector mu0,mu1;
   //VARIABLES
   //float mu_pt0, mu_eta0, mu_phi0, mu_e0; // leading muon
   //float mu_pt1, mu_eta1, mu_phi1, mu_e1; // subleading muon
   //float Zmumu_M, Zmumu_pt, Zmumu_y, Zmumu_eta, Zmumu_phi; // Z boson mumu
   //CUTS
   //float DeltaPhiCutJet0Zee = 2.5;
   const float GeV = 0.001;
   bool CutTag, CutProbeID, CutProbeCalo, CutProbeME;

   // CYCLE OVER EVENTS
   for (Long64_t jentry=0; jentry<nentries;jentry++) {
      Long64_t ientry = LoadTree(jentry);
      if (ientry < 0) break;
      nb = fChain->GetEntry(jentry);   nbytes += nb;
	if (jentry % 10000 == 0) 
  		cout << "SIGN: Processing entry " << jentry << endl;
   Zmass_mumu->Fill(dilep_mll*GeV);
   
   //TAG CUTS are always the same

   // PROBE TAGS differes depending on Tree i am running, atlas note ATL-COM-PHYS-2018-024

   //CALO TREE

   
   
   CutTag =  (tag_pt*GeV > 24.0) && (fabs(tag_eta) < 2.5) && (tag_matched_IsoFCLoose) && (tag_matched_HLT_mu20_iloose_L1MU15) && (fabs(tag_d0/tag_d0err) < 3.0) && (tag_z0 < 10.0);

   CutProbeID = (probe_pt*GeV > 10.0) && (fabs(probe_eta) < 2.5) && (fabs(probe_d0/probe_d0err) < 3.0) && (probe_z0 < 10.0) && (probe_ptvarcone30/probe_pt < 0.06) && (probe_topoetcone20/probe_pt < 0.1) ;
   CutProbeCalo = (probe_pt*GeV > 10.0) && (fabs(probe_eta) < 2.5) && (probe_matched_IsoFCLoose) && (fabs(probe_d0/probe_d0err) < 3.0) && (probe_z0 < 10.0);
   CutProbeME = (probe_pt*GeV > 10.0) && (fabs(probe_eta) < 2.5) && (probe_matched_IsoFCLoose);

   if ((Tag == "TPTree_CaloProbes_OC")||(Tag == "TPTree_CaloProbes_SC")){
      //Plot Zmass note figure 15
      if ( (CutTag) && (CutProbeCalo) ) {      
         if ((fabs(probe_eta) > 0.1) && (fabs(probe_eta) < 2.4)){
            if (probe_matched_MediumMuons){
              Zmass_mumu_Calocuts_nom->Fill(dilep_mll*GeV); 
            }
            Zmass_mumu_Calocuts_denom->Fill(dilep_mll*GeV); 
            }
      }

      
      if ( (CutTag) && (CutProbeCalo) && (dilep_mll*GeV > 81) && (dilep_mll*GeV < 101.0) ) {
         Zmass_mumu_Calocuts->Fill(dilep_mll*GeV);
         // HERE FILL NOMINATOR AND DENOMINATOR
         

         //One more cut for plotting efficiencies
         if ((fabs(probe_eta) > 0.1) && (fabs(probe_eta) < 2.4) && (probe_q > 0.0)){
            if (probe_matched_MediumMuons){
              muon0_pt_nomin_plus->Fill(probe_pt*GeV); 
            }
         
            muon0_pt_denomin_plus->Fill(probe_pt*GeV);
            }

         if ((fabs(probe_eta) > 0.1) && (fabs(probe_eta) < 2.4) && (probe_q < 0.0)){
            if (probe_matched_MediumMuons){
              muon0_pt_nomin_minus->Fill(probe_pt*GeV); 
            }
         
            muon0_pt_denomin_minus->Fill(probe_pt*GeV);
            }

         if ((probe_pt*GeV > 25.0) && (probe_q > 0.0)){
            if (probe_matched_MediumMuons){
              muon0_eta_nomin_plus->Fill(probe_eta); 
            }
         
            muon0_eta_denomin_plus->Fill(probe_eta);
            }

         if ((probe_pt*GeV > 25.0) && (probe_q < 0.0)){
            if (probe_matched_MediumMuons){
              muon0_eta_nomin_minus->Fill(probe_eta); 
            }
         
            muon0_eta_denomin_minus->Fill(probe_eta);
            }
      }
      
   } 

   //MS TREE
   if ((Tag == "TPTree_MSProbes_OC")||(Tag =="TPTree_MSProbes_SC")){
      
      //Plot Zmass note figure 15
      if ( (CutTag) && (CutProbeME) ) {
         if ((fabs(probe_eta) > 0.1) && (fabs(probe_eta) < 2.4)){
            if (probe_matched_IDTracks){
              Zmass_mumu_MEcuts_nom->Fill(dilep_mll*GeV); 
            }
            Zmass_mumu_MEcuts_denom->Fill(dilep_mll*GeV); 
            }
      }
      
      if ( (CutTag) && (CutProbeME) && (dilep_mll*GeV > 81) && (dilep_mll*GeV < 101.0) ) {
         Zmass_mumu_MEcuts->Fill(dilep_mll*GeV);
         // HERE FILL NOMINATOR AND DENOMINATOR

         //One more cut for plotting efficiencies
         if ((fabs(probe_eta) > 0.1) && (fabs(probe_eta) < 2.4) && (probe_q > 0.0)){
            if (probe_matched_IDTracks){
              muon0_pt_nomin_plus->Fill(probe_pt*GeV); 
            }
         
            muon0_pt_denomin_plus->Fill(probe_pt*GeV);
            }

         if ((fabs(probe_eta) > 0.1) && (fabs(probe_eta) < 2.4) && (probe_q < 0.0)){
            if (probe_matched_IDTracks){
              muon0_pt_nomin_minus->Fill(probe_pt*GeV); 
            }
         
            muon0_pt_denomin_minus->Fill(probe_pt*GeV);
            }

         if ((probe_pt*GeV > 25.0) && (probe_q > 0.0)){
            if (probe_matched_IDTracks){
              muon0_eta_nomin_plus->Fill(probe_eta); 
            }
         
            muon0_eta_denomin_plus->Fill(probe_eta);
            }

         if ((probe_pt*GeV > 25.0) && (probe_q < 0.0)){
            if (probe_matched_IDTracks){
              muon0_eta_nomin_minus->Fill(probe_eta); 
            }
         
            muon0_eta_denomin_minus->Fill(probe_eta);
            }
      }
   }
   
   //ID TREE I do no use

   if ((Tag == "TPTree_IDProbes_OC")||(Tag == "TPTree_IDProbes_SC")){
      if ( (CutTag) && (CutProbeID) && (dilep_mll*GeV > 86) && (dilep_mll*GeV < 96.0) ) {
         Zmass_mumu_IDcuts->Fill(dilep_mll*GeV);
         // HERE FILL NOMINATOR AND DENOMINATOR
      }
   }


   }

   if ((Tag == "TPTree_CaloProbes_OC")||(Tag == "TPTree_CaloProbes_SC")){
   TEfficiency *eff_med_ct_pt_plus = 0;
   eff_med_ct_pt_plus = new TEfficiency((const TH1D)*muon0_pt_nomin_plus,(const TH1D)*muon0_pt_denomin_plus);
   eff_med_ct_pt_plus->SetName("eff_med_ct_pt_plus");
   eff_med_ct_pt_plus->Write();
   //TH1F *eff_med_ct_pt_plus = (TH1F*)muon0_pt_nomin_plus->Clone("eff_med_ct_pt_plus");
   //eff_med_ct_pt_plus->Divide(muon0_pt_denomin_plus);
   TEfficiency *eff_med_ct_pt_minus = 0;
   eff_med_ct_pt_minus = new TEfficiency((const TH1D)*muon0_pt_nomin_minus,(const TH1D)*muon0_pt_denomin_minus);
   eff_med_ct_pt_minus->SetName("eff_med_ct_pt_minus");
   eff_med_ct_pt_minus->Write();
   //TH1F *eff_med_ct_pt_minus = (TH1F*)muon0_pt_nomin_minus->Clone("eff_med_ct_pt_minus");
   //eff_med_ct_pt_minus->Divide(muon0_pt_denomin_minus);
   TEfficiency *eff_med_ct_eta_plus = 0;
   eff_med_ct_eta_plus = new TEfficiency((const TH1D)*muon0_eta_nomin_plus,(const TH1D)*muon0_eta_denomin_plus);
   eff_med_ct_eta_plus->SetName("eff_med_ct_eta_plus");
   eff_med_ct_eta_plus->Write();
   //TH1F *eff_med_ct_eta_plus = (TH1F*)muon0_eta_nomin_plus->Clone("eff_med_ct_eta_plus");
   //eff_med_ct_eta_plus->Divide(muon0_eta_denomin_plus);
   TEfficiency *eff_med_ct_eta_minus = 0;
   eff_med_ct_eta_minus = new TEfficiency((const TH1D)*muon0_eta_nomin_minus, (const TH1D)*muon0_eta_denomin_minus);
   eff_med_ct_eta_minus->SetName("eff_med_ct_eta_minus");
   eff_med_ct_eta_minus->Write();
   //TH1F *eff_med_ct_eta_minus = (TH1F*)muon0_eta_nomin_minus->Clone("eff_med_ct_eta_minus");
   //eff_med_ct_eta_minus->Divide(muon0_eta_denomin_minus);
   }
   if ((Tag == "TPTree_MSProbes_OC")||(Tag == "TPTree_MSProbes_SC")){
   TEfficiency *eff_id_ms_pt_plus = 0;
   eff_id_ms_pt_plus = new TEfficiency((const TH1D)*muon0_pt_nomin_plus,(const TH1D)*muon0_pt_denomin_plus);
   eff_id_ms_pt_plus->SetName("eff_id_ms_pt_plus");
   eff_id_ms_pt_plus->Write();
   //TH1F *eff_id_ms_pt_plus = (TH1F*)muon0_pt_nomin_plus->Clone("eff_id_ms_pt_plus");
   //eff_id_ms_pt_plus->Divide(muon0_pt_denomin_plus);
   TEfficiency *eff_id_ms_pt_minus = 0;
   eff_id_ms_pt_minus = new TEfficiency((const TH1D)*muon0_pt_nomin_minus, (const TH1D)*muon0_pt_denomin_minus);
   eff_id_ms_pt_minus->SetName("eff_id_ms_pt_minus");
   eff_id_ms_pt_minus->Write();
   //TH1F *eff_id_ms_pt_minus = (TH1F*)muon0_pt_nomin_minus->Clone("eff_id_ms_pt_minus");
   //eff_id_ms_pt_minus->Divide(muon0_pt_denomin_minus);
   TEfficiency *eff_id_ms_eta_plus = 0;
   eff_id_ms_eta_plus = new TEfficiency((const TH1D)*muon0_eta_nomin_plus,(const TH1D)*muon0_eta_denomin_plus);
   eff_id_ms_eta_plus->SetName("eff_id_ms_eta_plus");
   eff_id_ms_eta_plus->Write();
   //TH1F *eff_id_ms_eta_plus = (TH1F*)muon0_eta_nomin_plus->Clone("eff_id_ms_eta_plus");
   //eff_id_ms_eta_plus->Divide(muon0_eta_denomin_plus);
   TEfficiency *eff_id_ms_eta_minus = 0;
   eff_id_ms_eta_minus = new TEfficiency((const TH1D)*muon0_eta_nomin_minus, (const TH1D)*muon0_eta_denomin_minus);
   eff_id_ms_eta_minus->SetName("eff_id_ms_eta_minus");
   eff_id_ms_eta_minus->Write();
   //TH1F *eff_id_ms_eta_minus = (TH1F*)muon0_eta_nomin_minus->Clone("eff_id_ms_eta_minus");
   //eff_id_ms_eta_minus->Divide(muon0_eta_denomin_minus);
   }

   cout << "SIGN: End of the Loop() function." << endl;
   outfile->Write();
}
//EOD OF THE LOOP FUNCTION

//THIS WAS BEFOR IN THE HEADER FILE

PBClass::PBClass(TTree *tree, TString Input, TString NameOfTree, TString PathToTree): fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(Input);
      if (!f || !f->IsOpen()) {
         f = new TFile(Input);
      }
      TDirectory * dir = (TDirectory*)f->Get(Input+":/"+PathToTree);
      dir->GetObject(NameOfTree,tree);

   }
   Init(tree);
}

PBClass::~PBClass()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t PBClass::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t PBClass::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void PBClass::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("BCID", &BCID, &b_BCID);
   fChain->SetBranchAddress("EventBadMuonVetoVar_Higheta_ID", &EventBadMuonVetoVar_HighPt_ID, &b_EventBadMuonVetoVar_HighPt_ID);
   fChain->SetBranchAddress("EventBadMuonVetoVar_HighPt_ME", &EventBadMuonVetoVar_HighPt_ME, &b_EventBadMuonVetoVar_HighPt_ME);
   fChain->SetBranchAddress("EventBadMuonVetoVar_Loose_ID", &EventBadMuonVetoVar_Loose_ID, &b_EventBadMuonVetoVar_Loose_ID);
   fChain->SetBranchAddress("EventBadMuonVetoVar_Loose_ME", &EventBadMuonVetoVar_Loose_ME, &b_EventBadMuonVetoVar_Loose_ME);
   fChain->SetBranchAddress("EventBadMuonVetoVar_Medium_ID", &EventBadMuonVetoVar_Medium_ID, &b_EventBadMuonVetoVar_Medium_ID);
   fChain->SetBranchAddress("EventBadMuonVetoVar_Medium_ME", &EventBadMuonVetoVar_Medium_ME, &b_EventBadMuonVetoVar_Medium_ME);
   fChain->SetBranchAddress("EventBadMuonVetoVar_Tight_ID", &EventBadMuonVetoVar_Tight_ID, &b_EventBadMuonVetoVar_Tight_ID);
   fChain->SetBranchAddress("EventBadMuonVetoVar_Tight_ME", &EventBadMuonVetoVar_Tight_ME, &b_EventBadMuonVetoVar_Tight_ME);
   fChain->SetBranchAddress("EventBadMuonVetoVar_VeryLoose_ID", &EventBadMuonVetoVar_VeryLoose_ID, &b_EventBadMuonVetoVar_VeryLoose_ID);
   fChain->SetBranchAddress("EventBadMuonVetoVar_VeryLoose_ME", &EventBadMuonVetoVar_VeryLoose_ME, &b_EventBadMuonVetoVar_VeryLoose_ME);
   fChain->SetBranchAddress("PV_n", &PV_n, &b_PV_n);
   fChain->SetBranchAddress("actual_mu", &actual_mu, &b_actual_mu);
   fChain->SetBranchAddress("average_mu", &average_mu, &b_average_mu);
   fChain->SetBranchAddress("calib_jet_probe_pt", &calib_jet_probe_pt, &b_calib_jet_probe_pt);
   fChain->SetBranchAddress("calib_jet_probe_eta", &calib_jet_probe_eta, &b_calib_jet_probe_eta);
   fChain->SetBranchAddress("calib_jet_probe_phi", &calib_jet_probe_phi, &b_calib_jet_probe_phi);
   fChain->SetBranchAddress("calib_jet_probe_index", &calib_jet_probe_index, &b_calib_jet_probe_index);
   fChain->SetBranchAddress("calib_jet_probe_m", &calib_jet_probe_m, &b_calib_jet_probe_m);
   fChain->SetBranchAddress("calib_jet_probe_emfrac", &calib_jet_probe_emfrac, &b_calib_jet_probe_emfrac);
   fChain->SetBranchAddress("calib_jet_probe_jvt", &calib_jet_probe_jvt, &b_calib_jet_probe_jvt);
   fChain->SetBranchAddress("calib_jet_closest_ptrel_probe", &calib_jet_closest_ptrel_probe, &b_calib_jet_closest_ptrel_probe);
   fChain->SetBranchAddress("calib_jet_tag_pt", &calib_jet_tag_pt, &b_calib_jet_tag_pt);
   fChain->SetBranchAddress("calib_jet_tag_eta", &calib_jet_tag_eta, &b_calib_jet_tag_eta);
   fChain->SetBranchAddress("calib_jet_tag_phi", &calib_jet_tag_phi, &b_calib_jet_tag_phi);
   fChain->SetBranchAddress("calib_jet_tag_index", &calib_jet_tag_index, &b_calib_jet_tag_index);
   fChain->SetBranchAddress("calib_jet_tag_m", &calib_jet_tag_m, &b_calib_jet_tag_m);
   fChain->SetBranchAddress("calib_jet_dR_tag", &calib_jet_dR_tag, &b_calib_jet_dR_tag);
   fChain->SetBranchAddress("calib_jet_nJets20", &calib_jet_nJets20, &b_calib_jet_nJets20);
   fChain->SetBranchAddress("calib_jet_dR_probe", &calib_jet_dR_probe, &b_calib_jet_dR_probe);
   fChain->SetBranchAddress("calib_jet_closest_chargedfrac_probe", &calib_jet_closest_chargedfrac_probe, &b_calib_jet_closest_chargedfrac_probe);
   fChain->SetBranchAddress("calib_jet_ntrks500_probe", &calib_jet_ntrks500_probe, &b_calib_jet_ntrks500_probe);
   fChain->SetBranchAddress("calib_jet_n_bad_jets", &calib_jet_n_bad_jets, &b_calib_jet_n_bad_jets);
   fChain->SetBranchAddress("dilep_deta", &dilep_deta, &b_dilep_deta);
   fChain->SetBranchAddress("dilep_dphi", &dilep_dphi, &b_dilep_dphi);
   fChain->SetBranchAddress("dilep_mll", &dilep_mll, &b_dilep_mll);
   fChain->SetBranchAddress("dilep_pt", &dilep_pt, &b_dilep_pt);
   fChain->SetBranchAddress("energyDensity_central", &energyDensity_central, &b_energyDensity_central);
   fChain->SetBranchAddress("energyDensity_forward", &energyDensity_forward, &b_energyDensity_forward);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("lumiblock", &lumiblock, &b_lumiblock);
   fChain->SetBranchAddress("matchobj_muon_CaloLRLikelihood", &matchobj_muon_CaloLRLikelihood, &b_matchobj_muon_CaloLRLikelihood);
   fChain->SetBranchAddress("matchobj_muon_CaloMuonIDTag", &matchobj_muon_CaloMuonIDTag, &b_matchobj_muon_CaloMuonIDTag);
   fChain->SetBranchAddress("matchobj_muon_EnergyLoss", &matchobj_muon_EnergyLoss, &b_matchobj_muon_EnergyLoss);
   fChain->SetBranchAddress("matchobj_muon_MeasEnergyLoss", &matchobj_muon_MeasEnergyLoss, &b_matchobj_muon_MeasEnergyLoss);
   fChain->SetBranchAddress("matchobj_muon_ParamEnergyLoss", &matchobj_muon_ParamEnergyLoss, &b_matchobj_muon_ParamEnergyLoss);
   fChain->SetBranchAddress("matchobj_muon_allAuthors", &matchobj_muon_allAuthors, &b_matchobj_muon_allAuthors);
   fChain->SetBranchAddress("matchobj_muon_author", &matchobj_muon_author, &b_matchobj_muon_author);
   fChain->SetBranchAddress("matchobj_muon_momentumBalanceSignificance", &matchobj_muon_momentumBalanceSignificance, &b_matchobj_muon_momentumBalanceSignificance);
   fChain->SetBranchAddress("matchobj_muon_scatteringCurvatureSignificance", &matchobj_muon_scatteringCurvatureSignificance, &b_matchobj_muon_scatteringCurvatureSignificance);
   fChain->SetBranchAddress("matchobj_muon_scatteringNeighbourSignificance", &matchobj_muon_scatteringNeighbourSignificance, &b_matchobj_muon_scatteringNeighbourSignificance);
   fChain->SetBranchAddress("matchobj_muon_segmentDeltaEta", &matchobj_muon_segmentDeltaEta, &b_matchobj_muon_segmentDeltaEta);
   fChain->SetBranchAddress("matchobj_muon_segmentDeltaPhi", &matchobj_muon_segmentDeltaPhi, &b_matchobj_muon_segmentDeltaPhi);
   fChain->SetBranchAddress("matchobj_muon_type", &matchobj_muon_type, &b_matchobj_muon_type);
   fChain->SetBranchAddress("mcChannelNumber", &mcChannelNumber, &b_mcChannelNumber);
   fChain->SetBranchAddress("mcEventWeight", &mcEventWeight, &b_mcEventWeight);
   fChain->SetBranchAddress("n_Trks_PrimVtx", &n_Trks_PrimVtx, &b_n_Trks_PrimVtx);
   fChain->SetBranchAddress("probe_pt", &probe_pt, &b_probe_pt);
   fChain->SetBranchAddress("probe_eta", &probe_eta, &b_probe_eta);
   fChain->SetBranchAddress("probe_phi", &probe_phi, &b_probe_phi);
   fChain->SetBranchAddress("probe_index", &probe_index, &b_probe_index);
   fChain->SetBranchAddress("probe_d0", &probe_d0, &b_probe_d0);
   fChain->SetBranchAddress("probe_d0err", &probe_d0err, &b_probe_d0err);
   fChain->SetBranchAddress("probe_eta_exTP", &probe_eta_exTP, &b_probe_eta_exTP);
   fChain->SetBranchAddress("probe_etcone20", &probe_etcone20, &b_probe_etcone20);
   fChain->SetBranchAddress("probe_etcone30", &probe_etcone30, &b_probe_etcone30);
   fChain->SetBranchAddress("probe_etcone40", &probe_etcone40, &b_probe_etcone40);
   fChain->SetBranchAddress("probe_neflowisol20", &probe_neflowisol20, &b_probe_neflowisol20);
   fChain->SetBranchAddress("probe_neflowisol30", &probe_neflowisol30, &b_probe_neflowisol30);
   fChain->SetBranchAddress("probe_neflowisol40", &probe_neflowisol40, &b_probe_neflowisol40);
   fChain->SetBranchAddress("probe_phi_exTP", &probe_phi_exTP, &b_probe_phi_exTP);
   fChain->SetBranchAddress("probe_ptcone20", &probe_ptcone20, &b_probe_ptcone20);
   fChain->SetBranchAddress("probe_ptcone20_LooseTTVA_pt500", &probe_ptcone20_LooseTTVA_pt500, &b_probe_ptcone20_LooseTTVA_pt500);
   fChain->SetBranchAddress("probe_ptcone20_TightTTVA_pt1000", &probe_ptcone20_TightTTVA_pt1000, &b_probe_ptcone20_TightTTVA_pt1000);
   fChain->SetBranchAddress("probe_ptcone20_TightTTVA_pt500", &probe_ptcone20_TightTTVA_pt500, &b_probe_ptcone20_TightTTVA_pt500);
   fChain->SetBranchAddress("probe_ptcone30", &probe_ptcone30, &b_probe_ptcone30);
   fChain->SetBranchAddress("probe_ptcone30_LooseTTVA_pt500", &probe_ptcone30_LooseTTVA_pt500, &b_probe_ptcone30_LooseTTVA_pt500);
   fChain->SetBranchAddress("probe_ptcone30_TightTTVA_pt1000", &probe_ptcone30_TightTTVA_pt1000, &b_probe_ptcone30_TightTTVA_pt1000);
   fChain->SetBranchAddress("probe_ptcone30_TightTTVA_pt500", &probe_ptcone30_TightTTVA_pt500, &b_probe_ptcone30_TightTTVA_pt500);
   fChain->SetBranchAddress("probe_ptcone40", &probe_ptcone40, &b_probe_ptcone40);
   fChain->SetBranchAddress("probe_ptcone40_LooseTTVA_pt500", &probe_ptcone40_LooseTTVA_pt500, &b_probe_ptcone40_LooseTTVA_pt500);
   fChain->SetBranchAddress("probe_ptcone40_TightTTVA_pt1000", &probe_ptcone40_TightTTVA_pt1000, &b_probe_ptcone40_TightTTVA_pt1000);
   fChain->SetBranchAddress("probe_ptcone40_TightTTVA_pt500", &probe_ptcone40_TightTTVA_pt500, &b_probe_ptcone40_TightTTVA_pt500);
   fChain->SetBranchAddress("probe_ptvarcone20", &probe_ptvarcone20, &b_probe_ptvarcone20);
   fChain->SetBranchAddress("probe_ptvarcone20_LooseTTVA_pt500", &probe_ptvarcone20_LooseTTVA_pt500, &b_probe_ptvarcone20_LooseTTVA_pt500);
   fChain->SetBranchAddress("probe_ptvarcone20_TightTTVA_pt1000", &probe_ptvarcone20_TightTTVA_pt1000, &b_probe_ptvarcone20_TightTTVA_pt1000);
   fChain->SetBranchAddress("probe_ptvarcone20_TightTTVA_pt500", &probe_ptvarcone20_TightTTVA_pt500, &b_probe_ptvarcone20_TightTTVA_pt500);
   fChain->SetBranchAddress("probe_ptvarcone30", &probe_ptvarcone30, &b_probe_ptvarcone30);
   fChain->SetBranchAddress("probe_ptvarcone30_LooseTTVA_pt500", &probe_ptvarcone30_LooseTTVA_pt500, &b_probe_ptvarcone30_LooseTTVA_pt500);
   fChain->SetBranchAddress("probe_ptvarcone30_TightTTVA_pt1000", &probe_ptvarcone30_TightTTVA_pt1000, &b_probe_ptvarcone30_TightTTVA_pt1000);
   fChain->SetBranchAddress("probe_ptvarcone30_TightTTVA_pt500", &probe_ptvarcone30_TightTTVA_pt500, &b_probe_ptvarcone30_TightTTVA_pt500);
   fChain->SetBranchAddress("probe_ptvarcone40", &probe_ptvarcone40, &b_probe_ptvarcone40);
   fChain->SetBranchAddress("probe_ptvarcone40_LooseTTVA_pt500", &probe_ptvarcone40_LooseTTVA_pt500, &b_probe_ptvarcone40_LooseTTVA_pt500);
   fChain->SetBranchAddress("probe_ptvarcone40_TightTTVA_pt1000", &probe_ptvarcone40_TightTTVA_pt1000, &b_probe_ptvarcone40_TightTTVA_pt1000);
   fChain->SetBranchAddress("probe_ptvarcone40_TightTTVA_pt500", &probe_ptvarcone40_TightTTVA_pt500, &b_probe_ptvarcone40_TightTTVA_pt500);
   fChain->SetBranchAddress("probe_q", &probe_q, &b_probe_q);
   fChain->SetBranchAddress("probe_topocore", &probe_topocore, &b_probe_topocore);
   fChain->SetBranchAddress("probe_topoetcone20", &probe_topoetcone20, &b_probe_topoetcone20);
   fChain->SetBranchAddress("probe_topoetcone30", &probe_topoetcone30, &b_probe_topoetcone30);
   fChain->SetBranchAddress("probe_topoetcone40", &probe_topoetcone40, &b_probe_topoetcone40);
   fChain->SetBranchAddress("probe_truth_origin", &probe_truth_origin, &b_probe_truth_origin);
   fChain->SetBranchAddress("probe_truth_type", &probe_truth_type, &b_probe_truth_type);
   fChain->SetBranchAddress("probe_z0", &probe_z0, &b_probe_z0);
   fChain->SetBranchAddress("probe_dRMatch_CaloTag", &probe_dRMatch_CaloTag, &b_probe_dRMatch_CaloTag);
   fChain->SetBranchAddress("probe_dRMatch_CombinedMuons", &probe_dRMatch_CombinedMuons, &b_probe_dRMatch_CombinedMuons);
   fChain->SetBranchAddress("probe_dRMatch_HighPtMuons", &probe_dRMatch_HighPtMuons, &b_probe_dRMatch_HighPtMuons);
   fChain->SetBranchAddress("probe_dRMatch_LooseMuons", &probe_dRMatch_LooseMuons, &b_probe_dRMatch_LooseMuons);
   fChain->SetBranchAddress("probe_dRMatch_LooseMuons_noCaloTag", &probe_dRMatch_LooseMuons_noCaloTag, &b_probe_dRMatch_LooseMuons_noCaloTag);
   fChain->SetBranchAddress("probe_dRMatch_LowPtMuons", &probe_dRMatch_LowPtMuons, &b_probe_dRMatch_LowPtMuons);
   fChain->SetBranchAddress("probe_dRMatch_MediumMuons", &probe_dRMatch_MediumMuons, &b_probe_dRMatch_MediumMuons);
   fChain->SetBranchAddress("probe_dRMatch_StandaloneMuons", &probe_dRMatch_StandaloneMuons, &b_probe_dRMatch_StandaloneMuons);
   fChain->SetBranchAddress("probe_dRMatch_TightMuons", &probe_dRMatch_TightMuons, &b_probe_dRMatch_TightMuons);
   fChain->SetBranchAddress("probe_matched_CaloTag", &probe_matched_CaloTag, &b_probe_matched_CaloTag);
   fChain->SetBranchAddress("probe_matched_CaloTag_PtrMatching", &probe_matched_CaloTag_PtrMatching, &b_probe_matched_CaloTag_PtrMatching);
   fChain->SetBranchAddress("probe_matched_CombinedMuons", &probe_matched_CombinedMuons, &b_probe_matched_CombinedMuons);
   fChain->SetBranchAddress("probe_matched_HighPtMuons", &probe_matched_HighPtMuons, &b_probe_matched_HighPtMuons);
   fChain->SetBranchAddress("probe_matched_HighPtMuons_PtrMatching", &probe_matched_HighPtMuons_PtrMatching, &b_probe_matched_HighPtMuons_PtrMatching);
   fChain->SetBranchAddress("probe_matched_IsoFCLoose", &probe_matched_IsoFCLoose, &b_probe_matched_IsoFCLoose);
   fChain->SetBranchAddress("probe_matched_IsoFCLoose_FixedRad", &probe_matched_IsoFCLoose_FixedRad, &b_probe_matched_IsoFCLoose_FixedRad);
   fChain->SetBranchAddress("probe_matched_IsoFCTight", &probe_matched_IsoFCTight, &b_probe_matched_IsoFCTight);
   fChain->SetBranchAddress("probe_matched_IsoFCTightTrackOnly", &probe_matched_IsoFCTightTrackOnly, &b_probe_matched_IsoFCTightTrackOnly);
   fChain->SetBranchAddress("probe_matched_IsoFCTightTrackOnly_FixedRad", &probe_matched_IsoFCTightTrackOnly_FixedRad, &b_probe_matched_IsoFCTightTrackOnly_FixedRad);
   fChain->SetBranchAddress("probe_matched_IsoFCTight_FixedRad", &probe_matched_IsoFCTight_FixedRad, &b_probe_matched_IsoFCTight_FixedRad);
   fChain->SetBranchAddress("probe_matched_IsoFixedCutHighPtTrackOnly", &probe_matched_IsoFixedCutHighPtTrackOnly, &b_probe_matched_IsoFixedCutHighPtTrackOnly);
   fChain->SetBranchAddress("probe_matched_LooseMuons", &probe_matched_LooseMuons, &b_probe_matched_LooseMuons);
   fChain->SetBranchAddress("probe_matched_LooseMuons_PtrMatching", &probe_matched_LooseMuons_PtrMatching, &b_probe_matched_LooseMuons_PtrMatching);
   fChain->SetBranchAddress("probe_matched_LooseMuons_noCaloTag", &probe_matched_LooseMuons_noCaloTag, &b_probe_matched_LooseMuons_noCaloTag);
   fChain->SetBranchAddress("probe_matched_LooseMuons_noCaloTag_PtrMatching", &probe_matched_LooseMuons_noCaloTag_PtrMatching, &b_probe_matched_LooseMuons_noCaloTag_PtrMatching);
   fChain->SetBranchAddress("probe_matched_LowPtMuons", &probe_matched_LowPtMuons, &b_probe_matched_LowPtMuons);
   fChain->SetBranchAddress("probe_matched_LowPtMuons_PtrMatching", &probe_matched_LowPtMuons_PtrMatching, &b_probe_matched_LowPtMuons_PtrMatching);
   fChain->SetBranchAddress("probe_matched_MediumMuons", &probe_matched_MediumMuons, &b_probe_matched_MediumMuons);
   fChain->SetBranchAddress("probe_matched_MediumMuons_PtrMatching", &probe_matched_MediumMuons_PtrMatching, &b_probe_matched_MediumMuons_PtrMatching);
   fChain->SetBranchAddress("probe_matched_StandaloneMuons", &probe_matched_StandaloneMuons, &b_probe_matched_StandaloneMuons);
   fChain->SetBranchAddress("probe_matched_StandaloneMuons_PtrMatching", &probe_matched_StandaloneMuons_PtrMatching, &b_probe_matched_StandaloneMuons_PtrMatching);
   fChain->SetBranchAddress("probe_matched_TRTCut", &probe_matched_TRTCut, &b_probe_matched_TRTCut);
   fChain->SetBranchAddress("probe_matched_TightMuons", &probe_matched_TightMuons, &b_probe_matched_TightMuons);
   fChain->SetBranchAddress("probe_matched_TightMuons_PtrMatching", &probe_matched_TightMuons_PtrMatching, &b_probe_matched_TightMuons_PtrMatching);
   fChain->SetBranchAddress("probe_matched_IDTracks", &probe_matched_IDTracks, &b_probe_matched_IDTracks); //added by PB
   fChain->SetBranchAddress("probe_matched_HLT_mu10", &probe_matched_HLT_mu10, &b_probe_matched_HLT_mu10); //added by PB
   fChain->SetBranchAddress("probe_quality", &probe_quality, &b_probe_quality); //added by PB
   fChain->SetBranchAddress("probe_matched_HLT_mu20_iloose_L1MU15", &probe_matched_HLT_mu20_iloose_L1MU15, &b_probe_matched_HLT_mu20_iloose_L1MU15); //added by PB
   fChain->SetBranchAddress("prwWeight", &prwWeight, &b_prwWeight);
   fChain->SetBranchAddress("randomLumiBlockNumber", &randomLumiBlockNumber, &b_randomLumiBlockNumber);
   fChain->SetBranchAddress("randomRunNumber", &randomRunNumber, &b_randomRunNumber);
   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("tag_pt", &tag_pt, &b_tag_pt);
   fChain->SetBranchAddress("tag_eta", &tag_eta, &b_tag_eta);
   fChain->SetBranchAddress("tag_phi", &tag_phi, &b_tag_phi);
   fChain->SetBranchAddress("tag_index", &tag_index, &b_tag_index);
   fChain->SetBranchAddress("tag_d0", &tag_d0, &b_tag_d0);
   fChain->SetBranchAddress("tag_d0err", &tag_d0err, &b_tag_d0err);
   fChain->SetBranchAddress("tag_dR_exTP", &tag_dR_exTP, &b_tag_dR_exTP);
   fChain->SetBranchAddress("tag_deta_exTP", &tag_deta_exTP, &b_tag_deta_exTP);
   fChain->SetBranchAddress("tag_dphi_exTP", &tag_dphi_exTP, &b_tag_dphi_exTP);
   fChain->SetBranchAddress("tag_etcone20", &tag_etcone20, &b_tag_etcone20);
   fChain->SetBranchAddress("tag_etcone30", &tag_etcone30, &b_tag_etcone30);
   fChain->SetBranchAddress("tag_etcone40", &tag_etcone40, &b_tag_etcone40);
   fChain->SetBranchAddress("tag_matched_IsoFCLoose", &tag_matched_IsoFCLoose, &b_tag_matched_IsoFCLoose);
   fChain->SetBranchAddress("tag_matched_IsoFCLoose_FixedRad", &tag_matched_IsoFCLoose_FixedRad, &b_tag_matched_IsoFCLoose_FixedRad);
   fChain->SetBranchAddress("tag_matched_IsoFCTight", &tag_matched_IsoFCTight, &b_tag_matched_IsoFCTight);
   fChain->SetBranchAddress("tag_matched_IsoFCTightTrackOnly", &tag_matched_IsoFCTightTrackOnly, &b_tag_matched_IsoFCTightTrackOnly);
   fChain->SetBranchAddress("tag_matched_IsoFCTightTrackOnly_FixedRad", &tag_matched_IsoFCTightTrackOnly_FixedRad, &b_tag_matched_IsoFCTightTrackOnly_FixedRad);
   fChain->SetBranchAddress("tag_matched_IsoFCTight_FixedRad", &tag_matched_IsoFCTight_FixedRad, &b_tag_matched_IsoFCTight_FixedRad);
   fChain->SetBranchAddress("tag_matched_IsoFixedCutHighPtTrackOnly", &tag_matched_IsoFixedCutHighPtTrackOnly, &b_tag_matched_IsoFixedCutHighPtTrackOnly);
   fChain->SetBranchAddress("tag_neflowisol20", &tag_neflowisol20, &b_tag_neflowisol20);
   fChain->SetBranchAddress("tag_neflowisol30", &tag_neflowisol30, &b_tag_neflowisol30);
   fChain->SetBranchAddress("tag_neflowisol40", &tag_neflowisol40, &b_tag_neflowisol40);
   fChain->SetBranchAddress("tag_ptcone20", &tag_ptcone20, &b_tag_ptcone20);
   fChain->SetBranchAddress("tag_ptcone20_LooseTTVA_pt500", &tag_ptcone20_LooseTTVA_pt500, &b_tag_ptcone20_LooseTTVA_pt500);
   fChain->SetBranchAddress("tag_ptcone20_TightTTVA_pt1000", &tag_ptcone20_TightTTVA_pt1000, &b_tag_ptcone20_TightTTVA_pt1000);
   fChain->SetBranchAddress("tag_ptcone20_TightTTVA_pt500", &tag_ptcone20_TightTTVA_pt500, &b_tag_ptcone20_TightTTVA_pt500);
   fChain->SetBranchAddress("tag_ptcone30", &tag_ptcone30, &b_tag_ptcone30);
   fChain->SetBranchAddress("tag_ptcone30_LooseTTVA_pt500", &tag_ptcone30_LooseTTVA_pt500, &b_tag_ptcone30_LooseTTVA_pt500);
   fChain->SetBranchAddress("tag_ptcone30_TightTTVA_pt1000", &tag_ptcone30_TightTTVA_pt1000, &b_tag_ptcone30_TightTTVA_pt1000);
   fChain->SetBranchAddress("tag_ptcone30_TightTTVA_pt500", &tag_ptcone30_TightTTVA_pt500, &b_tag_ptcone30_TightTTVA_pt500);
   fChain->SetBranchAddress("tag_ptcone40", &tag_ptcone40, &b_tag_ptcone40);
   fChain->SetBranchAddress("tag_ptcone40_LooseTTVA_pt500", &tag_ptcone40_LooseTTVA_pt500, &b_tag_ptcone40_LooseTTVA_pt500);
   fChain->SetBranchAddress("tag_ptcone40_TightTTVA_pt1000", &tag_ptcone40_TightTTVA_pt1000, &b_tag_ptcone40_TightTTVA_pt1000);
   fChain->SetBranchAddress("tag_ptcone40_TightTTVA_pt500", &tag_ptcone40_TightTTVA_pt500, &b_tag_ptcone40_TightTTVA_pt500);
   fChain->SetBranchAddress("tag_ptvarcone20", &tag_ptvarcone20, &b_tag_ptvarcone20);
   fChain->SetBranchAddress("tag_ptvarcone20_LooseTTVA_pt500", &tag_ptvarcone20_LooseTTVA_pt500, &b_tag_ptvarcone20_LooseTTVA_pt500);
   fChain->SetBranchAddress("tag_ptvarcone20_TightTTVA_pt1000", &tag_ptvarcone20_TightTTVA_pt1000, &b_tag_ptvarcone20_TightTTVA_pt1000);
   fChain->SetBranchAddress("tag_ptvarcone20_TightTTVA_pt500", &tag_ptvarcone20_TightTTVA_pt500, &b_tag_ptvarcone20_TightTTVA_pt500);
   fChain->SetBranchAddress("tag_ptvarcone30", &tag_ptvarcone30, &b_tag_ptvarcone30);
   fChain->SetBranchAddress("tag_ptvarcone30_LooseTTVA_pt500", &tag_ptvarcone30_LooseTTVA_pt500, &b_tag_ptvarcone30_LooseTTVA_pt500);
   fChain->SetBranchAddress("tag_ptvarcone30_TightTTVA_pt1000", &tag_ptvarcone30_TightTTVA_pt1000, &b_tag_ptvarcone30_TightTTVA_pt1000);
   fChain->SetBranchAddress("tag_ptvarcone30_TightTTVA_pt500", &tag_ptvarcone30_TightTTVA_pt500, &b_tag_ptvarcone30_TightTTVA_pt500);
   fChain->SetBranchAddress("tag_ptvarcone40", &tag_ptvarcone40, &b_tag_ptvarcone40);
   fChain->SetBranchAddress("tag_ptvarcone40_LooseTTVA_pt500", &tag_ptvarcone40_LooseTTVA_pt500, &b_tag_ptvarcone40_LooseTTVA_pt500);
   fChain->SetBranchAddress("tag_ptvarcone40_TightTTVA_pt1000", &tag_ptvarcone40_TightTTVA_pt1000, &b_tag_ptvarcone40_TightTTVA_pt1000);
   fChain->SetBranchAddress("tag_ptvarcone40_TightTTVA_pt500", &tag_ptvarcone40_TightTTVA_pt500, &b_tag_ptvarcone40_TightTTVA_pt500);
   fChain->SetBranchAddress("tag_q", &tag_q, &b_tag_q);
   fChain->SetBranchAddress("tag_topocore", &tag_topocore, &b_tag_topocore);
   fChain->SetBranchAddress("tag_topoetcone20", &tag_topoetcone20, &b_tag_topoetcone20);
   fChain->SetBranchAddress("tag_topoetcone30", &tag_topoetcone30, &b_tag_topoetcone30);
   fChain->SetBranchAddress("tag_topoetcone40", &tag_topoetcone40, &b_tag_topoetcone40);
   fChain->SetBranchAddress("tag_truth_origin", &tag_truth_origin, &b_tag_truth_origin);
   fChain->SetBranchAddress("tag_truth_type", &tag_truth_type, &b_tag_truth_type);
   fChain->SetBranchAddress("tag_z0", &tag_z0, &b_tag_z0);
   fChain->SetBranchAddress("tag_dRMatch_HLT_2mu6_bJpsimumu", &tag_dRMatch_HLT_2mu6_bJpsimumu, &b_tag_dRMatch_HLT_2mu6_bJpsimumu);
   fChain->SetBranchAddress("tag_dRMatch_HLT_2mu6_bJpsimumui_noL2", &tag_dRMatch_HLT_2mu6_bJpsimumui_noL2, &b_tag_dRMatch_HLT_2mu6_bJpsimumui_noL2);
   fChain->SetBranchAddress("tag_dRMatch_HLT_AllTriggers", &tag_dRMatch_HLT_AllTriggers, &b_tag_dRMatch_HLT_AllTriggers);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu10", &tag_dRMatch_HLT_mu10, &b_tag_dRMatch_HLT_mu10);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu10_idperf", &tag_dRMatch_HLT_mu10_idperf, &b_tag_dRMatch_HLT_mu10_idperf);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu10_msonly", &tag_dRMatch_HLT_mu10_msonly, &b_tag_dRMatch_HLT_mu10_msonly);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu13_mu13_idperf_Zmumu", &tag_dRMatch_HLT_mu13_mu13_idperf_Zmumu, &b_tag_dRMatch_HLT_mu13_mu13_idperf_Zmumu);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu14", &tag_dRMatch_HLT_mu14, &b_tag_dRMatch_HLT_mu14);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu14_ivarloose", &tag_dRMatch_HLT_mu14_ivarloose, &b_tag_dRMatch_HLT_mu14_ivarloose);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu15noL1", &tag_dRMatch_HLT_mu15noL1, &b_tag_dRMatch_HLT_mu15noL1);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu18", &tag_dRMatch_HLT_mu18, &b_tag_dRMatch_HLT_mu18);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu18_2mu0noL1_JpsimumuFS", &tag_dRMatch_HLT_mu18_2mu0noL1_JpsimumuFS, &b_tag_dRMatch_HLT_mu18_2mu0noL1_JpsimumuFS);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu18_2mu4noL1_JpsimumuL2", &tag_dRMatch_HLT_mu18_2mu4noL1_JpsimumuL2, &b_tag_dRMatch_HLT_mu18_2mu4noL1_JpsimumuL2);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu18noL1", &tag_dRMatch_HLT_mu18noL1, &b_tag_dRMatch_HLT_mu18noL1);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu20", &tag_dRMatch_HLT_mu20, &b_tag_dRMatch_HLT_mu20);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu20_idperf", &tag_dRMatch_HLT_mu20_idperf, &b_tag_dRMatch_HLT_mu20_idperf);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu20_iloose_L1MU15", &tag_dRMatch_HLT_mu20_iloose_L1MU15, &b_tag_dRMatch_HLT_mu20_iloose_L1MU15);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu20_ivarloose_L1MU15", &tag_dRMatch_HLT_mu20_ivarloose_L1MU15, &b_tag_dRMatch_HLT_mu20_ivarloose_L1MU15);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu20_msonly", &tag_dRMatch_HLT_mu20_msonly, &b_tag_dRMatch_HLT_mu20_msonly);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu22", &tag_dRMatch_HLT_mu22, &b_tag_dRMatch_HLT_mu22);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu24", &tag_dRMatch_HLT_mu24, &b_tag_dRMatch_HLT_mu24);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu24_iloose", &tag_dRMatch_HLT_mu24_iloose, &b_tag_dRMatch_HLT_mu24_iloose);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu24_iloose_L1MU15", &tag_dRMatch_HLT_mu24_iloose_L1MU15, &b_tag_dRMatch_HLT_mu24_iloose_L1MU15);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu24_imedium", &tag_dRMatch_HLT_mu24_imedium, &b_tag_dRMatch_HLT_mu24_imedium);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu24_ivarloose", &tag_dRMatch_HLT_mu24_ivarloose, &b_tag_dRMatch_HLT_mu24_ivarloose);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu24_ivarloose_L1MU15", &tag_dRMatch_HLT_mu24_ivarloose_L1MU15, &b_tag_dRMatch_HLT_mu24_ivarloose_L1MU15);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu24_ivarmedium", &tag_dRMatch_HLT_mu24_ivarmedium, &b_tag_dRMatch_HLT_mu24_ivarmedium);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu26_imedium", &tag_dRMatch_HLT_mu26_imedium, &b_tag_dRMatch_HLT_mu26_imedium);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu26_ivarmedium", &tag_dRMatch_HLT_mu26_ivarmedium, &b_tag_dRMatch_HLT_mu26_ivarmedium);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu28_imedium", &tag_dRMatch_HLT_mu28_imedium, &b_tag_dRMatch_HLT_mu28_imedium);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu28_ivarmedium", &tag_dRMatch_HLT_mu28_ivarmedium, &b_tag_dRMatch_HLT_mu28_ivarmedium);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu4", &tag_dRMatch_HLT_mu4, &b_tag_dRMatch_HLT_mu4);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu40", &tag_dRMatch_HLT_mu40, &b_tag_dRMatch_HLT_mu40);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu4_idperf", &tag_dRMatch_HLT_mu4_idperf, &b_tag_dRMatch_HLT_mu4_idperf);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu4_msonly", &tag_dRMatch_HLT_mu4_msonly, &b_tag_dRMatch_HLT_mu4_msonly);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu4_mu4_idperf_bJpsimumu_noid", &tag_dRMatch_HLT_mu4_mu4_idperf_bJpsimumu_noid, &b_tag_dRMatch_HLT_mu4_mu4_idperf_bJpsimumu_noid);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu50", &tag_dRMatch_HLT_mu50, &b_tag_dRMatch_HLT_mu50);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu6", &tag_dRMatch_HLT_mu6, &b_tag_dRMatch_HLT_mu6);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu60", &tag_dRMatch_HLT_mu60, &b_tag_dRMatch_HLT_mu60);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu6_bJpsi_TrkPEB", &tag_dRMatch_HLT_mu6_bJpsi_TrkPEB, &b_tag_dRMatch_HLT_mu6_bJpsi_TrkPEB);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu6_bJpsi_Trkloose", &tag_dRMatch_HLT_mu6_bJpsi_Trkloose, &b_tag_dRMatch_HLT_mu6_bJpsi_Trkloose);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu6_idperf", &tag_dRMatch_HLT_mu6_idperf, &b_tag_dRMatch_HLT_mu6_idperf);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu6_msonly", &tag_dRMatch_HLT_mu6_msonly, &b_tag_dRMatch_HLT_mu6_msonly);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu6_mu4_bJpsimumu", &tag_dRMatch_HLT_mu6_mu4_bJpsimumu, &b_tag_dRMatch_HLT_mu6_mu4_bJpsimumu);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu6_mu4_bJpsimumu_noL2", &tag_dRMatch_HLT_mu6_mu4_bJpsimumu_noL2, &b_tag_dRMatch_HLT_mu6_mu4_bJpsimumu_noL2);
   fChain->SetBranchAddress("tag_dRMatch_HLT_mu8noL1", &tag_dRMatch_HLT_mu8noL1, &b_tag_dRMatch_HLT_mu8noL1);
   fChain->SetBranchAddress("tag_dRMatch_HLT_noalg_L1MU", &tag_dRMatch_HLT_noalg_L1MU, &b_tag_dRMatch_HLT_noalg_L1MU);
   fChain->SetBranchAddress("tag_dRMatch_HLT_noalg_L1MU10", &tag_dRMatch_HLT_noalg_L1MU10, &b_tag_dRMatch_HLT_noalg_L1MU10);
   fChain->SetBranchAddress("tag_dRMatch_HLT_noalg_L1MU11", &tag_dRMatch_HLT_noalg_L1MU11, &b_tag_dRMatch_HLT_noalg_L1MU11);
   fChain->SetBranchAddress("tag_dRMatch_HLT_noalg_L1MU20", &tag_dRMatch_HLT_noalg_L1MU20, &b_tag_dRMatch_HLT_noalg_L1MU20);
   fChain->SetBranchAddress("tag_dRMatch_HLT_noalg_L1MU21", &tag_dRMatch_HLT_noalg_L1MU21, &b_tag_dRMatch_HLT_noalg_L1MU21);
   fChain->SetBranchAddress("tag_matched_HLT_2mu6_bJpsimumu", &tag_matched_HLT_2mu6_bJpsimumu, &b_tag_matched_HLT_2mu6_bJpsimumu);
   fChain->SetBranchAddress("tag_matched_HLT_2mu6_bJpsimumui_noL2", &tag_matched_HLT_2mu6_bJpsimumui_noL2, &b_tag_matched_HLT_2mu6_bJpsimumui_noL2);
   fChain->SetBranchAddress("tag_matched_HLT_AllTriggers", &tag_matched_HLT_AllTriggers, &b_tag_matched_HLT_AllTriggers);
   fChain->SetBranchAddress("tag_matched_HLT_mu10", &tag_matched_HLT_mu10, &b_tag_matched_HLT_mu10);
   fChain->SetBranchAddress("tag_matched_HLT_mu10_idperf", &tag_matched_HLT_mu10_idperf, &b_tag_matched_HLT_mu10_idperf);
   fChain->SetBranchAddress("tag_matched_HLT_mu10_msonly", &tag_matched_HLT_mu10_msonly, &b_tag_matched_HLT_mu10_msonly);
   fChain->SetBranchAddress("tag_matched_HLT_mu13_mu13_idperf_Zmumu", &tag_matched_HLT_mu13_mu13_idperf_Zmumu, &b_tag_matched_HLT_mu13_mu13_idperf_Zmumu);
   fChain->SetBranchAddress("tag_matched_HLT_mu14", &tag_matched_HLT_mu14, &b_tag_matched_HLT_mu14);
   fChain->SetBranchAddress("tag_matched_HLT_mu14_ivarloose", &tag_matched_HLT_mu14_ivarloose, &b_tag_matched_HLT_mu14_ivarloose);
   fChain->SetBranchAddress("tag_matched_HLT_mu15noL1", &tag_matched_HLT_mu15noL1, &b_tag_matched_HLT_mu15noL1);
   fChain->SetBranchAddress("tag_matched_HLT_mu18", &tag_matched_HLT_mu18, &b_tag_matched_HLT_mu18);
   fChain->SetBranchAddress("tag_matched_HLT_mu18_2mu0noL1_JpsimumuFS", &tag_matched_HLT_mu18_2mu0noL1_JpsimumuFS, &b_tag_matched_HLT_mu18_2mu0noL1_JpsimumuFS);
   fChain->SetBranchAddress("tag_matched_HLT_mu18_2mu4noL1_JpsimumuL2", &tag_matched_HLT_mu18_2mu4noL1_JpsimumuL2, &b_tag_matched_HLT_mu18_2mu4noL1_JpsimumuL2);
   fChain->SetBranchAddress("tag_matched_HLT_mu18noL1", &tag_matched_HLT_mu18noL1, &b_tag_matched_HLT_mu18noL1);
   fChain->SetBranchAddress("tag_matched_HLT_mu20", &tag_matched_HLT_mu20, &b_tag_matched_HLT_mu20);
   fChain->SetBranchAddress("tag_matched_HLT_mu20_idperf", &tag_matched_HLT_mu20_idperf, &b_tag_matched_HLT_mu20_idperf);
   fChain->SetBranchAddress("tag_matched_HLT_mu20_iloose_L1MU15", &tag_matched_HLT_mu20_iloose_L1MU15, &b_tag_matched_HLT_mu20_iloose_L1MU15);
   fChain->SetBranchAddress("tag_matched_HLT_mu20_ivarloose_L1MU15", &tag_matched_HLT_mu20_ivarloose_L1MU15, &b_tag_matched_HLT_mu20_ivarloose_L1MU15);
   fChain->SetBranchAddress("tag_matched_HLT_mu20_msonly", &tag_matched_HLT_mu20_msonly, &b_tag_matched_HLT_mu20_msonly);
   fChain->SetBranchAddress("tag_matched_HLT_mu22", &tag_matched_HLT_mu22, &b_tag_matched_HLT_mu22);
   fChain->SetBranchAddress("tag_matched_HLT_mu24", &tag_matched_HLT_mu24, &b_tag_matched_HLT_mu24);
   fChain->SetBranchAddress("tag_matched_HLT_mu24_iloose", &tag_matched_HLT_mu24_iloose, &b_tag_matched_HLT_mu24_iloose);
   fChain->SetBranchAddress("tag_matched_HLT_mu24_iloose_L1MU15", &tag_matched_HLT_mu24_iloose_L1MU15, &b_tag_matched_HLT_mu24_iloose_L1MU15);
   fChain->SetBranchAddress("tag_matched_HLT_mu24_imedium", &tag_matched_HLT_mu24_imedium, &b_tag_matched_HLT_mu24_imedium);
   fChain->SetBranchAddress("tag_matched_HLT_mu24_ivarloose", &tag_matched_HLT_mu24_ivarloose, &b_tag_matched_HLT_mu24_ivarloose);
   fChain->SetBranchAddress("tag_matched_HLT_mu24_ivarloose_L1MU15", &tag_matched_HLT_mu24_ivarloose_L1MU15, &b_tag_matched_HLT_mu24_ivarloose_L1MU15);
   fChain->SetBranchAddress("tag_matched_HLT_mu24_ivarmedium", &tag_matched_HLT_mu24_ivarmedium, &b_tag_matched_HLT_mu24_ivarmedium);
   fChain->SetBranchAddress("tag_matched_HLT_mu26_imedium", &tag_matched_HLT_mu26_imedium, &b_tag_matched_HLT_mu26_imedium);
   fChain->SetBranchAddress("tag_matched_HLT_mu26_ivarmedium", &tag_matched_HLT_mu26_ivarmedium, &b_tag_matched_HLT_mu26_ivarmedium);
   fChain->SetBranchAddress("tag_matched_HLT_mu28_imedium", &tag_matched_HLT_mu28_imedium, &b_tag_matched_HLT_mu28_imedium);
   fChain->SetBranchAddress("tag_matched_HLT_mu28_ivarmedium", &tag_matched_HLT_mu28_ivarmedium, &b_tag_matched_HLT_mu28_ivarmedium);
   fChain->SetBranchAddress("tag_matched_HLT_mu4", &tag_matched_HLT_mu4, &b_tag_matched_HLT_mu4);
   fChain->SetBranchAddress("tag_matched_HLT_mu40", &tag_matched_HLT_mu40, &b_tag_matched_HLT_mu40);
   fChain->SetBranchAddress("tag_matched_HLT_mu4_idperf", &tag_matched_HLT_mu4_idperf, &b_tag_matched_HLT_mu4_idperf);
   fChain->SetBranchAddress("tag_matched_HLT_mu4_msonly", &tag_matched_HLT_mu4_msonly, &b_tag_matched_HLT_mu4_msonly);
   fChain->SetBranchAddress("tag_matched_HLT_mu4_mu4_idperf_bJpsimumu_noid", &tag_matched_HLT_mu4_mu4_idperf_bJpsimumu_noid, &b_tag_matched_HLT_mu4_mu4_idperf_bJpsimumu_noid);
   fChain->SetBranchAddress("tag_matched_HLT_mu50", &tag_matched_HLT_mu50, &b_tag_matched_HLT_mu50);
   fChain->SetBranchAddress("tag_matched_HLT_mu6", &tag_matched_HLT_mu6, &b_tag_matched_HLT_mu6);
   fChain->SetBranchAddress("tag_matched_HLT_mu60", &tag_matched_HLT_mu60, &b_tag_matched_HLT_mu60);
   fChain->SetBranchAddress("tag_matched_HLT_mu6_bJpsi_TrkPEB", &tag_matched_HLT_mu6_bJpsi_TrkPEB, &b_tag_matched_HLT_mu6_bJpsi_TrkPEB);
   fChain->SetBranchAddress("tag_matched_HLT_mu6_bJpsi_Trkloose", &tag_matched_HLT_mu6_bJpsi_Trkloose, &b_tag_matched_HLT_mu6_bJpsi_Trkloose);
   fChain->SetBranchAddress("tag_matched_HLT_mu6_idperf", &tag_matched_HLT_mu6_idperf, &b_tag_matched_HLT_mu6_idperf);
   fChain->SetBranchAddress("tag_matched_HLT_mu6_msonly", &tag_matched_HLT_mu6_msonly, &b_tag_matched_HLT_mu6_msonly);
   fChain->SetBranchAddress("tag_matched_HLT_mu6_mu4_bJpsimumu", &tag_matched_HLT_mu6_mu4_bJpsimumu, &b_tag_matched_HLT_mu6_mu4_bJpsimumu);
   fChain->SetBranchAddress("tag_matched_HLT_mu6_mu4_bJpsimumu_noL2", &tag_matched_HLT_mu6_mu4_bJpsimumu_noL2, &b_tag_matched_HLT_mu6_mu4_bJpsimumu_noL2);
   fChain->SetBranchAddress("tag_matched_HLT_mu8noL1", &tag_matched_HLT_mu8noL1, &b_tag_matched_HLT_mu8noL1);
   fChain->SetBranchAddress("tag_matched_HLT_noalg_L1MU", &tag_matched_HLT_noalg_L1MU, &b_tag_matched_HLT_noalg_L1MU);
   fChain->SetBranchAddress("tag_matched_HLT_noalg_L1MU10", &tag_matched_HLT_noalg_L1MU10, &b_tag_matched_HLT_noalg_L1MU10);
   fChain->SetBranchAddress("tag_matched_HLT_noalg_L1MU11", &tag_matched_HLT_noalg_L1MU11, &b_tag_matched_HLT_noalg_L1MU11);
   fChain->SetBranchAddress("tag_matched_HLT_noalg_L1MU20", &tag_matched_HLT_noalg_L1MU20, &b_tag_matched_HLT_noalg_L1MU20);
   fChain->SetBranchAddress("tag_matched_HLT_noalg_L1MU21", &tag_matched_HLT_noalg_L1MU21, &b_tag_matched_HLT_noalg_L1MU21);
   fChain->SetBranchAddress("trackMET_abs", &trackMET_abs, &b_trackMET_abs);
   fChain->SetBranchAddress("trackMET_phi", &trackMET_phi, &b_trackMET_phi);
   fChain->SetBranchAddress("trig_HLT_2mu6_bJpsimumu", &trig_HLT_2mu6_bJpsimumu, &b_trig_HLT_2mu6_bJpsimumu);
   fChain->SetBranchAddress("trig_HLT_2mu6_bJpsimumui_noL2", &trig_HLT_2mu6_bJpsimumui_noL2, &b_trig_HLT_2mu6_bJpsimumui_noL2);
   fChain->SetBranchAddress("trig_HLT_AllTriggers", &trig_HLT_AllTriggers, &b_trig_HLT_AllTriggers);
   fChain->SetBranchAddress("trig_HLT_mu10", &trig_HLT_mu10, &b_trig_HLT_mu10);
   fChain->SetBranchAddress("trig_HLT_mu10_idperf", &trig_HLT_mu10_idperf, &b_trig_HLT_mu10_idperf);
   fChain->SetBranchAddress("trig_HLT_mu10_msonly", &trig_HLT_mu10_msonly, &b_trig_HLT_mu10_msonly);
   fChain->SetBranchAddress("trig_HLT_mu13_mu13_idperf_Zmumu", &trig_HLT_mu13_mu13_idperf_Zmumu, &b_trig_HLT_mu13_mu13_idperf_Zmumu);
   fChain->SetBranchAddress("trig_HLT_mu14", &trig_HLT_mu14, &b_trig_HLT_mu14);
   fChain->SetBranchAddress("trig_HLT_mu14_ivarloose", &trig_HLT_mu14_ivarloose, &b_trig_HLT_mu14_ivarloose);
   fChain->SetBranchAddress("trig_HLT_mu15noL1", &trig_HLT_mu15noL1, &b_trig_HLT_mu15noL1);
   fChain->SetBranchAddress("trig_HLT_mu18", &trig_HLT_mu18, &b_trig_HLT_mu18);
   fChain->SetBranchAddress("trig_HLT_mu18_2mu0noL1_JpsimumuFS", &trig_HLT_mu18_2mu0noL1_JpsimumuFS, &b_trig_HLT_mu18_2mu0noL1_JpsimumuFS);
   fChain->SetBranchAddress("trig_HLT_mu18_2mu4noL1_JpsimumuL2", &trig_HLT_mu18_2mu4noL1_JpsimumuL2, &b_trig_HLT_mu18_2mu4noL1_JpsimumuL2);
   fChain->SetBranchAddress("trig_HLT_mu18noL1", &trig_HLT_mu18noL1, &b_trig_HLT_mu18noL1);
   fChain->SetBranchAddress("trig_HLT_mu20", &trig_HLT_mu20, &b_trig_HLT_mu20);
   fChain->SetBranchAddress("trig_HLT_mu20_idperf", &trig_HLT_mu20_idperf, &b_trig_HLT_mu20_idperf);
   fChain->SetBranchAddress("trig_HLT_mu20_iloose_L1MU15", &trig_HLT_mu20_iloose_L1MU15, &b_trig_HLT_mu20_iloose_L1MU15);
   fChain->SetBranchAddress("trig_HLT_mu20_ivarloose_L1MU15", &trig_HLT_mu20_ivarloose_L1MU15, &b_trig_HLT_mu20_ivarloose_L1MU15);
   fChain->SetBranchAddress("trig_HLT_mu20_msonly", &trig_HLT_mu20_msonly, &b_trig_HLT_mu20_msonly);
   fChain->SetBranchAddress("trig_HLT_mu22", &trig_HLT_mu22, &b_trig_HLT_mu22);
   fChain->SetBranchAddress("trig_HLT_mu24", &trig_HLT_mu24, &b_trig_HLT_mu24);
   fChain->SetBranchAddress("trig_HLT_mu24_iloose", &trig_HLT_mu24_iloose, &b_trig_HLT_mu24_iloose);
   fChain->SetBranchAddress("trig_HLT_mu24_iloose_L1MU15", &trig_HLT_mu24_iloose_L1MU15, &b_trig_HLT_mu24_iloose_L1MU15);
   fChain->SetBranchAddress("trig_HLT_mu24_imedium", &trig_HLT_mu24_imedium, &b_trig_HLT_mu24_imedium);
   fChain->SetBranchAddress("trig_HLT_mu24_ivarloose", &trig_HLT_mu24_ivarloose, &b_trig_HLT_mu24_ivarloose);
   fChain->SetBranchAddress("trig_HLT_mu24_ivarloose_L1MU15", &trig_HLT_mu24_ivarloose_L1MU15, &b_trig_HLT_mu24_ivarloose_L1MU15);
   fChain->SetBranchAddress("trig_HLT_mu24_ivarmedium", &trig_HLT_mu24_ivarmedium, &b_trig_HLT_mu24_ivarmedium);
   fChain->SetBranchAddress("trig_HLT_mu26_imedium", &trig_HLT_mu26_imedium, &b_trig_HLT_mu26_imedium);
   fChain->SetBranchAddress("trig_HLT_mu26_ivarmedium", &trig_HLT_mu26_ivarmedium, &b_trig_HLT_mu26_ivarmedium);
   fChain->SetBranchAddress("trig_HLT_mu28_imedium", &trig_HLT_mu28_imedium, &b_trig_HLT_mu28_imedium);
   fChain->SetBranchAddress("trig_HLT_mu28_ivarmedium", &trig_HLT_mu28_ivarmedium, &b_trig_HLT_mu28_ivarmedium);
   fChain->SetBranchAddress("trig_HLT_mu4", &trig_HLT_mu4, &b_trig_HLT_mu4);
   fChain->SetBranchAddress("trig_HLT_mu40", &trig_HLT_mu40, &b_trig_HLT_mu40);
   fChain->SetBranchAddress("trig_HLT_mu4_idperf", &trig_HLT_mu4_idperf, &b_trig_HLT_mu4_idperf);
   fChain->SetBranchAddress("trig_HLT_mu4_msonly", &trig_HLT_mu4_msonly, &b_trig_HLT_mu4_msonly);
   fChain->SetBranchAddress("trig_HLT_mu4_mu4_idperf_bJpsimumu_noid", &trig_HLT_mu4_mu4_idperf_bJpsimumu_noid, &b_trig_HLT_mu4_mu4_idperf_bJpsimumu_noid);
   fChain->SetBranchAddress("trig_HLT_mu50", &trig_HLT_mu50, &b_trig_HLT_mu50);
   fChain->SetBranchAddress("trig_HLT_mu6", &trig_HLT_mu6, &b_trig_HLT_mu6);
   fChain->SetBranchAddress("trig_HLT_mu60", &trig_HLT_mu60, &b_trig_HLT_mu60);
   fChain->SetBranchAddress("trig_HLT_mu6_bJpsi_TrkPEB", &trig_HLT_mu6_bJpsi_TrkPEB, &b_trig_HLT_mu6_bJpsi_TrkPEB);
   fChain->SetBranchAddress("trig_HLT_mu6_bJpsi_Trkloose", &trig_HLT_mu6_bJpsi_Trkloose, &b_trig_HLT_mu6_bJpsi_Trkloose);
   fChain->SetBranchAddress("trig_HLT_mu6_idperf", &trig_HLT_mu6_idperf, &b_trig_HLT_mu6_idperf);
   fChain->SetBranchAddress("trig_HLT_mu6_msonly", &trig_HLT_mu6_msonly, &b_trig_HLT_mu6_msonly);
   fChain->SetBranchAddress("trig_HLT_mu6_mu4_bJpsimumu", &trig_HLT_mu6_mu4_bJpsimumu, &b_trig_HLT_mu6_mu4_bJpsimumu);
   fChain->SetBranchAddress("trig_HLT_mu6_mu4_bJpsimumu_noL2", &trig_HLT_mu6_mu4_bJpsimumu_noL2, &b_trig_HLT_mu6_mu4_bJpsimumu_noL2);
   fChain->SetBranchAddress("trig_HLT_mu8noL1", &trig_HLT_mu8noL1, &b_trig_HLT_mu8noL1);
   fChain->SetBranchAddress("trig_HLT_noalg_L1MU", &trig_HLT_noalg_L1MU, &b_trig_HLT_noalg_L1MU);
   fChain->SetBranchAddress("trig_HLT_noalg_L1MU10", &trig_HLT_noalg_L1MU10, &b_trig_HLT_noalg_L1MU10);
   fChain->SetBranchAddress("trig_HLT_noalg_L1MU11", &trig_HLT_noalg_L1MU11, &b_trig_HLT_noalg_L1MU11);
   fChain->SetBranchAddress("trig_HLT_noalg_L1MU20", &trig_HLT_noalg_L1MU20, &b_trig_HLT_noalg_L1MU20);
   fChain->SetBranchAddress("trig_HLT_noalg_L1MU21", &trig_HLT_noalg_L1MU21, &b_trig_HLT_noalg_L1MU21);
   fChain->SetBranchAddress("vertex_density", &vertex_density, &b_vertex_density);
   Notify();
}

Bool_t PBClass::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void PBClass::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t PBClass::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
