#!/usr/bin/python

import argparse
from matplotlib import pyplot as plt
from ROOT import *
from array import array

parser = argparse.ArgumentParser(description="Plotting Z boson mass")
parser.add_argument('--x', '-x', type=str, default="M_{#mu#mu} [GeV]")
parser.add_argument('--y', '-y', type=str, default="Entries / NormFactor")
parser.add_argument('--t', '-t', type=str, default="pp collisions")
parser.add_argument('--type', '-type', type=str, default="mumu")
parser.add_argument('--ry', '-ry', type=str, default="#frac{Data}{Prediction}         ")
parser.add_argument('--nom', '-nom', type=str, default="Zmass_mumu")
parser.add_argument('--denom', '-denom', type=str, default="Zmass_mumu")
parser.add_argument('--legh1', '-legh1', type=str, default="Data")
parser.add_argument('--legh2', '-legh2', type=str, default="Z #rightarrow ee")
parser.add_argument('--log', '-log', type=int, default=1)
parser.add_argument('--rmax', '-rmax', type=float, default=2)
parser.add_argument('--rmin', '-rmin', type=float, default=0)
parser.add_argument('--maxy', '-maxy', type=float, default=2)
parser.add_argument('--miny', '-miny', type=float, default=1e-3)
parser.add_argument('--rangexmin', '-rangexmin', type=float, default=0)
parser.add_argument('--rangexmax', '-rangexmax', type=float, default=1000)
parser.add_argument('--nrebin', '-nrebin', type=int, default=2)
parser.add_argument('--lx1', '-lx1', type=float, default=0.15)
parser.add_argument('--ly1', '-ly1', type=float, default=0.6)
parser.add_argument('--lx2', '-lx2', type=float, default=0.4)
parser.add_argument('--ly2', '-ly2', type=float, default=0.85)
parser.add_argument('--fit', '-fit', type=float, default=2)
parser.add_argument('--mcoc', '-mcoc', type=str, default="/home/petr/Analysis_iwona/SFAnalysis/outputs/muontp_mc_pp_OC.root")
parser.add_argument('--mcsc', '-mcsc', type=str, default="/home/petr/Analysis_iwona/SFAnalysis/outputs/muontp_mc_pp_SC.root")
parser.add_argument('--dataoc', '-dataoc', type=str, default="/home/petr/Analysis_iwona/SFAnalysis/outputs/muontp_data_pp_OC.root")
parser.add_argument('--datasc', '-datasc', type=str, default="/home/petr/Analysis_iwona/SFAnalysis/outputs/muontp_data_pp_SC.root")
parser.add_argument('--o', '-o', type=str, default="")
parser.add_argument('--batch', '-batch', type=int, default=0)

args = parser.parse_args()

if (args.batch == 1):
    gROOT.SetBatch(1)

def DataVsMC( data, MC , MyXTitle="XTitle", MyYTitle="YTitle", MCLegend="MC Legend", DataLegend = "data legend",MyCanvasName="can",MyPad1Name= "pad1",MyPad2Name="pad2",X1Line=0,X2Line=200 ):

        can, pad1, pad2 = PrepareCanvas(MyCanvasName,MyCanvasName,MyPad2Name)

        PrepareHisogram(MC, LineColor=2, Title=MC.GetName(),YTitle=MyYTitle)
        MC.Draw("hist e1")
        PrepareHisogram(data, LineColor=1,MarkerColor=1,MarkerSize=1.5,LineWidth=2,Marker=22)
        data.Draw("p hist e0 x0 same")
        legend4 = TLegend(args.lx1,args.ly1,args.lx2,args.ly2)
        legend4.SetTextSize(0.04)
        legend4.AddEntry(MC,MCLegend)
        legend4.AddEntry(data,DataLegend,"p")
        legend4.SetBorderSize(0)
        legend4.Draw("same")
        MakeSmallLabel(0.15,0.78,analysis=args.t)
        pad2.cd()
        data_clone=data.Clone("")
        data_clone.Divide(MC)
        PrepareHisogram(data_clone, LineColor=2, Ratio=1, XTitle = MyXTitle)
        data_clone.Draw("p e x0")
        line = TLine(X1Line,1,X2Line,1)
        line.SetLineColor(2)
        line.SetLineWidth(2)
        line.Draw("hist same")
        data_clone.Draw("p e x0 same")
        PrintCan(can)

def PrepareCanvas(CanvasName="",Pad1Name="pad1", Pad2Name="pad2"):
    gStyle.SetPadLeftMargin(0.12)
    gStyle.SetOptStat(0)
    gStyle.SetPadRightMargin(0.12)
    can = TCanvas(args.o+CanvasName,"Results",0,0,1600, 800)
    can.Divide(2,1)
    return can

def PrepareHisogram( h1, LineColor = 1, LineWidth = 1, Marker = -1, MarkerColor = -1, MarkerSize = 1.1, TitleOffset = 1.3, Ratio = 0, Title = "" , YTitle = "none", XTitle = args.x, LineStyle = 1):
    if (Ratio == 1):
        h1.SetMaximum(args.rmax)
        h1.SetMinimum(args.rmin)
        h1.SetTitle("")
        h1.GetXaxis().SetTitle(args.x)
        h1.GetXaxis().SetTitleOffset(2.7)
        h1.GetYaxis().SetTitle(args.ry)
        h1.GetXaxis().SetTitle(XTitle)
        h1.GetYaxis().SetNdivisions(505)
        h1.GetXaxis().SetLabelFont(43)
        h1.GetXaxis().SetLabelSize(25)
        h1.GetXaxis().SetTitleSize(25)
        h1.GetXaxis().SetLabelOffset(0.01)
        h1.SetStats(0)
    else:
        #h1.SetTitle(Title)
        h1.SetLineStyle(LineStyle)
        h1.SetLineColor(LineColor)
        h1.SetLineWidth(LineWidth) 
        if (Marker!=-1):
            h1.SetMarkerStyle(Marker)
        if (MarkerColor!=-1):
            h1.SetMarkerColor(MarkerColor)
        h1.SetMarkerSize(MarkerSize)
        h1.SetMaximum(h1.GetMaximum()*args.maxy)
        h1.SetMinimum(args.miny)
        h1.GetXaxis().SetTitleSize(34)
        h1.GetXaxis().SetTitleFont(43)
        h1.GetYaxis().SetTitleSize(34)
        h1.GetYaxis().SetTitleFont(43)
        h1.GetYaxis().SetTitleOffset(TitleOffset)
        h1.GetYaxis().SetLabelFont(43)
        h1.GetYaxis().SetLabelSize(25)
        h1.SetTitle(Title)
        if (YTitle == "none"):
                h1.GetYaxis().SetTitle(h1.GetYaxis().GetName())
        else:
                h1.GetYaxis().SetTitle(YTitle)


def PrepareEff( h1, LineColor = 1, LineWidth = 1, Marker = -1, MarkerColor = -1, MarkerSize = 1.1, TitleOffset = 1.3, Ratio = 0, Title = "" , YTitle = "none", XTitle = args.x, LineStyle = 1):
    if (Ratio == 1):
        #h1.SetMaximum(args.rmax)
        #h1.SetMinimum(args.rmin)
        h1.SetTitle("")
        #h1.GetXaxis().SetTitle(args.x)
        h1.GetXaxis().SetTitleOffset(2.7)
        #h1.GetYaxis().SetTitle(args.ry)
        h1.GetXaxis().SetTitle(XTitle)
        h1.GetYaxis().SetNdivisions(505)
        h1.GetXaxis().SetLabelFont(43)
        h1.GetXaxis().SetLabelSize(25)
        h1.GetXaxis().SetTitleSize(25)
        h1.GetXaxis().SetLabelOffset(0.01)
        h1.GetYaxis().SetLabelFont(43)
        h1.GetYaxis().SetLabelSize(25)
        h1.GetYaxis().SetTitleSize(25)
        h1.GetYaxis().SetLabelOffset(0.01)
        h1.SetStats(0)
    else:
        #h1.SetTitle(Title)
        h1.SetLineStyle(LineStyle)
        h1.SetLineColor(LineColor)
        h1.SetLineWidth(LineWidth) 
        if (Marker!=-1):
            h1.SetMarkerStyle(Marker)
        if (MarkerColor!=-1):
            h1.SetMarkerColor(MarkerColor)
        h1.SetMarkerSize(MarkerSize)
        #h1.SetMaximum(h1.GetMaximum()*args.maxy)
        #h1.SetMinimum(args.miny)
        h1.GetXaxis().SetTitleSize(34)
        h1.GetXaxis().SetTitleFont(43)
        h1.GetYaxis().SetTitleSize(34)
        h1.GetYaxis().SetTitleFont(43)
        h1.GetYaxis().SetTitleOffset(TitleOffset)
        h1.GetYaxis().SetLabelFont(43)
        h1.GetYaxis().SetLabelSize(25)
        h1.SetTitle(Title)
        if (YTitle == "none"):
                h1.GetYaxis().SetTitle(h1.GetYaxis().GetName())
        else:
                h1.GetYaxis().SetTitle(YTitle)


def MakeProfileX( TH2DHist, Name = "" ):
  ArrayXBins = TH2DHist.GetXaxis().GetXbins()
  if (Name == ""):
          Name = TH2DHist.GetName()+"_profileX"
          print Name
  h1 = TH1D(Name,Name, TH2DHist.GetNbinsX(),ArrayXBins.GetArray())
  #h2 = TH1D(Name+"2",Name+"2", TH2DHist.GetNbinsX(),ArrayXBins.GetArray())
  
  for i in range(1,TH2DHist.GetNbinsX()+1):
        NumberOfEntries = 0
        SumOverY = 0.0
        sigma = 0.0
        for j in range(1,TH2DHist.GetNbinsY()+1):
                SumOverY = TH2DHist.GetBinContent(i,j)*TH2DHist.GetYaxis().GetBinCenter(j)+SumOverY
                NumberOfEntries = TH2DHist.GetBinContent(i,j) + NumberOfEntries
        #print "**************"
        #print SumOverY, NumberOfEntries
        if (NumberOfEntries != 0):
                Mean = SumOverY/NumberOfEntries
        else:
                Mean = SumOverY
                print "WARNING: Pokud je Mean = 0, je to ok, Mean = ", Mean
        #print "********************"
        #print Mean, h2.GetBinContent(i)
        if (Mean != 0.0):
                print "----------"
                print Mean
                print "----------"
                h1.SetBinContent(i,Mean)
        else:
                h1.SetBinContent(i,int(Mean))
        sigma=0.0
        for k in range(1,TH2DHist.GetNbinsY()+1):
                sigma=pow(TH2DHist.GetBinContent(i,j)*TH2DHist.GetYaxis().GetBinCenter(j)-Mean,2)
        if (NumberOfEntries != 1):
             sigma=pow(sigma/(NumberOfEntries-1),0.5)
        elif (NumberOfEntries !=0):
             if "_data" in h1.GetName():
                sigma=sigma/pow(NumberOfEntries,0.5)
             elif "_mc" in h1.GetName():
                sigma=sigma/pow(NumberOfEntries,0.5)
        else:
             sigma = 0
             print "WARNING: sigme nastavena na 0.", Mean
        if (sigma != 0):
                h1.SetBinError(i,sigma)

  return h1

def MakeATLASLabel( x, y, status = "Internal", iLumi = "22.1", ECM = "13", Sizer = 0.1):

 #set labels....
  l1 = TLatex()
  l1.SetTextAlign(9)
  l1.SetTextSize(Sizer)
  l1.SetNDC()
  text = "#sqrt{s} = %s TeV, %s fb^{-1}" % ( ECM, iLumi )
  l1.DrawLatex(0.2, 0.10, text)


  l2 = TLatex()
  l2.SetTextAlign(9)
  l2.SetTextFont(72)
  l2.SetTextSize(Sizer)
  l2.SetNDC()
  #l2.DrawLatex(0.5, 0.465, "ATLAS")
  l3 = TLatex()
  l3.SetTextAlign(9)
  l3.SetTextSize(Sizer)
  l3.SetNDC()
  #l3.DrawLatex(0.76, 0.465,  status)

def MakeSmallLabel( x, y, status = "Internal", iLumi = "160", ECM = "8.16", phase_space = "", analysis = "" , size = 0.06):
    l = TLatex()
    l.SetTextSize(size) 
    l.SetNDC()
    l.SetTextColor(kBlack)

    l1 = TLatex()
    l1.SetNDC()
    l1.SetTextFont(72)
    l1.SetTextSize(size)
    l1.SetTextColor(kBlack)
    #l1.DrawLatex(x,y-0.01,"ATLAS")
    
    
    l2 = TLatex()
    l2.SetTextSize(size)
    l2.SetNDC()
    l2.SetTextColor(kBlack)
    #l2.DrawLatex(x+0.12,y-0.01,status)


    ##l.DrawLatex( x, y-0.07, phase_space )

#    text = "#int Ldt = %s fb^{-1}   #sqrt{s} = %s TeV" % ( iLumi, ECM )
    text = "#sqrt{s} = %s TeV" % ( ECM )
    analysis2 = "%s nb^{-1}" % ( iLumi )
    #analysis = "Resoved"

    ##l.DrawLatex( x, y-0.08, text)
    l.DrawLatex( x + 0.35, y+0.05, analysis)
    ##l.DrawLatex( x, y-0.15, analysis2)

def DivideBinWidth(h):
    for j in range(1, h.GetNbinsX()+1):
        h.SetBinContent(j,((h.GetBinContent(j))/(h.GetBinWidth(j))))
        h.SetBinError(j,((h.GetBinError(j))/(h.GetBinWidth(j))))

def PrintCan(can):
    can.Print("/home/petr/Analysis_iwona/SFAnalysis/png/" + can.GetName() + '.png')
    can.Print("/home/petr/Analysis_iwona/SFAnalysis/eps/" + can.GetName() + '.eps')
    #can.Print(can.GetName() + '.root')

rfiles = []

def ReadFile( filename ):
   rfile = TFile(filename, 'read')
   #print "File was read."
   rfiles.append(rfile)
   return rfile

def DoAllTheBloodyWork( TGraph_eff_mc , TGraph_eff_data , TEff_mc , TEff_data , canvas , pad1Name = "pad1", pad1rName = "pad1r", XTitle = "p_{T}^{#mu}", YTitle = "Efficiency (ID | MS)" , MultiGraphName = "multi1", legend_mc = "MC, #mu^{+}", legend_data = "data, #mu^{+}" , data_h_passed_Name = "data_h_passed_Name", data_h_total_Name = "data_h_total_Name", mc_h_passed_Name = "mc_h_passed_Name", mc_h_total_Name = "mc_h_total_Name" , CanvasNumber = 3, YTitleR = "Data/MC         ", XTitleR = "p_{T}^{#mu} [GeV]", StartLine = 10, EndLine = 100, mg1Min = 0.9601, RatioMin = 0.95, RatioMax = 1.05 , YOffset = 1.05, Xaxis = 0):
        canvas.cd(CanvasNumber)
        print "canvas", canvas
        mg1 = TMultiGraph()
        mg1.Add(TGraph_eff_mc)
        mg1.Add(TGraph_eff_data)
        pad1 = TPad(pad1Name,pad1Name,0,0.40,1,1)
        pad1.SetTopMargin(0.15)
        pad1.SetBottomMargin(0.01)
        pad1.SetFillStyle(0)
        pad1.SetTicks(1,1)
        pad1.SetBorderMode(0)
        pad1.Draw()
        canvas.cd(CanvasNumber)
        pad1r = TPad(pad1rName,pad1rName,0,0.01,1,0.422)
        pad1r.SetFillStyle(0)
        pad1r.SetTopMargin(0.043)
        pad1r.SetBottomMargin(0.3)
        pad1r.SetBorderMode(0)
        pad1r.SetTicks(1,1)
        pad1r.Draw()
        pad1r.Modified()
        canvas.cd(CanvasNumber)
        pad1.cd()
        if Xaxis == 1:
                gPad.SetLogx()        
        mg1.GetYaxis().SetNdivisions(510)
        mg1.GetYaxis().SetLabelFont(43)
        mg1.GetYaxis().SetLabelSize(25)
        mg1.GetYaxis().SetTitleSize(0.06)
        mg1.GetYaxis().SetTitleOffset(YOffset)
        #mg1.GetYaxis().SetLabelOffset(0.01)
        mg1.SetMinimum(mg1Min)
        mg1.GetXaxis().SetTitle(XTitle)
        mg1.GetYaxis().SetTitle(YTitle)
        mg1.Draw("AP")
        legend1 = TLegend(0.2,0.2,0.4,0.4)
        legend1.SetBorderSize(0)
        legend1.AddEntry(TGraph_eff_data,legend_data,"p")
        legend1.AddEntry(TGraph_eff_mc,legend_mc,"p")
        legend1.Draw("same")
        #MakeATLASLabel(0.01,0.1)
        pad1r.cd()
        data_h_passed =  TEff_data.GetPassedHistogram()
        data_h_total =   TEff_data.GetTotalHistogram()
        data_h_passed.SetName(data_h_passed.GetName()+data_h_passed_Name)
        data_h_total.SetName(data_h_total.GetName()+data_h_total_Name)
        eff_ratio = data_h_passed.Clone(data_h_passed.GetName()+"_effraio")
        eff_ratio.Divide(data_h_total)
        for i in range(1,data_h_passed.GetXaxis().GetNbins()):
                help_list = []
                help_list.append( TEff_data.GetEfficiencyErrorUp(i))
                help_list.append( TEff_data.GetEfficiencyErrorLow(i))
                eff_ratio.SetBinError(i, max(help_list))

        mc_h_passed = TEff_mc.GetPassedHistogram()
        mc_h_total =  TEff_mc.GetTotalHistogram()
        mc_h_passed.SetName(mc_h_passed.GetName()+mc_h_passed_Name)
        mc_h_total.SetName(mc_h_total.GetName()+mc_h_total_Name)
        eff_ratio_mc = mc_h_passed.Clone(mc_h_passed.GetName()+"_clone")
        eff_ratio_mc.Divide(mc_h_total)
        for i in range(1,mc_h_passed.GetXaxis().GetNbins()):
                help_list = []
                help_list.append(TEff_mc.GetEfficiencyErrorUp(i))
                help_list.append(TEff_mc.GetEfficiencyErrorLow(i))
                eff_ratio_mc.SetBinError(i, max(help_list))
        #ACTUAL DIVISON
        eff_ratio.Divide(eff_ratio_mc)
        #PrepareEff(eff_ratio, LineColor=1, Marker = 20, MarkerColor = 1, Ratio= 1, YTitle = "Data/MC", XTitle="p_{T}^{#mu} [GeV]", LineStyle=-1)
        eff_ratio.SetLineColor(1)
        eff_ratio.SetLineStyle(-1)
        eff_ratio.SetMarkerStyle(20)
        eff_ratio.SetMarkerColor(1)
        eff_ratio.GetYaxis().SetTitle(YTitleR)
        eff_ratio.GetXaxis().SetTitle(XTitleR)
        #eff_ratio.GetXaxis().SetTitleSize(0.06)
        #eff_ratio.GetYaxis().SetNdivisions(510)
        eff_ratio.GetYaxis().SetLabelFont(43)
        eff_ratio.GetYaxis().SetLabelSize(25)
        eff_ratio.GetYaxis().SetTitleSize(0.08)
        eff_ratio.GetXaxis().SetTitleSize(0.08)
        eff_ratio.GetXaxis().SetTitleOffset(2.7)
        eff_ratio.SetTitle("")
        eff_ratio.GetYaxis().SetNdivisions(505)
        eff_ratio.GetXaxis().SetLabelFont(43)
        eff_ratio.GetXaxis().SetLabelSize(25)
        eff_ratio.GetXaxis().SetTitleSize(0.08)
        eff_ratio.GetXaxis().SetTitleOffset(1.5)
        eff_ratio.GetXaxis().SetLabelOffset(0.01)
        eff_ratio.GetYaxis().SetLabelFont(43)
        eff_ratio.GetYaxis().SetLabelSize(25)
        eff_ratio.GetYaxis().SetTitleOffset(0.8)
        eff_ratio.GetYaxis().SetLabelOffset(0.01)
        eff_ratio.SetStats(0)
        eff_ratio.SetMaximum(RatioMax)
        eff_ratio.SetMinimum(RatioMin)
        eff_ratio.GetXaxis().SetTitle(XTitleR)
        if Xaxis == 1:
                gPad.SetLogx()        
        eff_ratio.Draw("p e1")
        line1 = TLine(StartLine,1,EndLine,1)
        line1.SetLineStyle(2)
        line1.SetLineColor(4)
        line1.SetLineWidth(2)
        line1.Draw("same")
        print "canvas", canvas
        return canvas, pad1, pad1r, mg1, legend1, eff_ratio, line1

# LIST OF USED ROOT FILENAMES

filename = [args.mcoc,args.mcsc, args.dataoc, args.datasc]

# READING FILES AND SAV'/home/petr/Analysis_iwona/ZAnalysis/outputs/out_data_nominal.root', '/home/petr/Analysis_iwona/ZAnalysis/outputs/out_mc_Zee_nominal.root' , '/home/petr/Analysis_iwona/ZAnalysis/outputs/out_mc_Zmumu_nominal.root', '/home/petr/Analysis_iwona/ZAnalysis/outputs/out_data_nominal_Loose.root','/home/petr/Analysis_iwona/ZAnalysis/outputs/out_mc_Zee_nominal_Loose.root','/home/petr/Analysis_iwona/ZAnalysis/outputs/out_mc_Zmumu_nominal_Loose.root' ING THEM INTO THE LIST filename[]

for i in range(len(filename)):
    ReadFile(filename[i])

print("Filename", filename)
# INPUTS REFER TO THE HISTOGRAMS 

inputs = []

inputs.append(args.denom)
print(args.denom)
#inputs.append("Zmass_mumu_IDcuts")
#inputs.append("jet0_pT_div_ZpT_ee_vs_jet0_pT")
#inputs.append("jet0_pT_div_ZpT_mumu_vs_ZpT_mumu")
#inputs.append("jet0_pT_div_ZpT_mumu_vs_jet0_pT")

# SWITCHER BETWEEN DIFFERENT ROOT FILES AND TREES


if (args.type == "mumu"):
        h1 = rfiles[0].Get(inputs[0])
        h2 = rfiles[1].Get(inputs[0])
        h3 = rfiles[2].Get(inputs[0])
        h4 = rfiles[3].Get(inputs[0])

        h1.SetName(h1.GetName()+"_mc_OC")
        h2.SetName(h2.GetName()+"_mc_SC")
        h3.SetName(h3.GetName()+"_data_OC")
        h4.SetName(h4.GetName()+"_data_SC")

        h1.GetXaxis().SetTitle(args.x)
        h2.GetXaxis().SetTitle(args.x)
        h3.GetXaxis().SetTitle(args.x)
        h4.GetXaxis().SetTitle(args.x)

        h5 = rfiles[0].Get(args.nom)
        h6 = rfiles[1].Get(args.nom)
        h7 = rfiles[2].Get(args.nom)
        h8 = rfiles[3].Get(args.nom)

        h5.SetName(h5.GetName()+"_mc_OC")
        h6.SetName(h6.GetName()+"_mc_SC")
        h7.SetName(h7.GetName()+"_data_OC")
        h8.SetName(h8.GetName()+"_data_SC")

        h5.GetXaxis().SetTitle(args.x)
        h6.GetXaxis().SetTitle(args.x)
        h7.GetXaxis().SetTitle(args.x)
        h8.GetXaxis().SetTitle(args.x)

# END OF THE SWITCHER

# CANVAS SETTINGS 

can = PrepareCanvas()

# FUNCTION TO SET HISTOGRAMS, COLORS, AXIS, LINES, ...

PrepareHisogram(h1, LineColor=2, Marker = 20, MarkerColor = 2, YTitle = args.y, LineStyle=0)
PrepareHisogram(h2, LineColor=2, Marker = 4, MarkerColor = 2, YTitle = args.y, LineStyle=0)
PrepareHisogram(h3, LineColor=1, Marker = 20, MarkerColor = 1, YTitle = args.y, LineStyle=0)
PrepareHisogram(h4, LineColor=1, Marker = 4, MarkerColor = 1, YTitle = args.y, LineStyle=0)

PrepareHisogram(h5, LineColor=2, Marker = 20, MarkerColor = 2, YTitle = args.y, LineStyle=0)
PrepareHisogram(h6, LineColor=2, Marker = 4, MarkerColor = 2, YTitle = args.y, LineStyle=0)
PrepareHisogram(h7, LineColor=1, Marker = 20, MarkerColor = 1, YTitle = args.y, LineStyle=0)
PrepareHisogram(h8, LineColor=1, Marker = 4, MarkerColor = 1, YTitle = args.y, LineStyle=0)
# NORMALIZING HISTOGRAMS

Integral1 = h1.Integral(h1.FindBin(81.0),h1.FindBin(101.0))
Integral2 = h2.Integral(h2.FindBin(81.0),h2.FindBin(101.0))
Integral3 = h3.Integral(h3.FindBin(81.0),h3.FindBin(101.0))
Integral4 = h4.Integral(h4.FindBin(81.0),h4.FindBin(101.0))
Integral5 = h5.Integral(h5.FindBin(81.0),h5.FindBin(101.0))
Integral6 = h6.Integral(h6.FindBin(81.0),h6.FindBin(101.0))
Integral7 = h7.Integral(h7.FindBin(81.0),h7.FindBin(101.0))
Integral8 = h8.Integral(h8.FindBin(81.0),h8.FindBin(101.0))

if (h1.Integral() != 0.0):
        h1.Scale(h3.Integral()/h1.Integral())
if (h2.Integral() != 0.0):
        h2.Scale(h4.Integral()/h2.Integral()) # h2 and h4 are same charge

if (h5.Integral() != 0.0):
        h5.Scale(h7.Integral()/h5.Integral())
if (h6.Integral() != 0.0):
        h6.Scale(h8.Integral()/h6.Integral()) # h2 and h4 are same charge



## Number of events / Luminosity
## Number of events / ()
#if (h3.Integral() != 0.0):
#        h3.Scale(1./h1.Integral())
#if (h4.Integral() != 0.0):
###        h4.Scale(1./h1.Integral())
# DRAWING PART
can.cd(1)
gPad.SetLogy(1)


lineA = TLine(81.0,0.0,81.0,0.5*h1.GetMaximum())
lineA.SetLineColor(4)
lineA.SetLineStyle(2)
lineA.SetLineWidth(2)
lineB = TLine(101.0,0.0,101.0,0.5*h1.GetMaximum())
lineB.SetLineColor(4)
lineB.SetLineStyle(2)
lineB.SetLineWidth(2)
h1.SetMaximum(h1.GetMaximum()*100)
h1.SetMinimum(2e-1)

h1.Draw("p e1 same")
h2.Draw("p e1 same")
h3.Draw("p e1 same")
h4.Draw("p e1 same")
lineA.Draw("same")
lineB.Draw("same")

legend = TLegend(args.lx1,args.ly1,args.lx2,args.ly2)
legend.SetTextSize(0.04)
legend.AddEntry(h3,"OC data: "+str(int(Integral3)),"p")
legend.AddEntry(h4,"SC data: "+str(int(Integral4)),"p")
legend.AddEntry(h1,"OC MC: "+str(int(Integral1)),"p")
legend.AddEntry(h2,"SC MC: "+str(int(Integral2)),"p")
legend.SetBorderSize(0)
legend.Draw("same")

MakeSmallLabel(0.15,0.78,analysis=args.t)

can.cd(2)
gPad.SetLogy(1)
h5.SetMaximum(h1.GetMaximum()*100)
h5.SetMinimum(2e-1)

h5.Draw("p e1 ")
h6.Draw("p e1 same")
h7.Draw("p e1 same")
h8.Draw("p e1 same")
lineA.Draw("same")
lineB.Draw("same")

legend2 = TLegend(args.lx1,args.ly1,args.lx2,args.ly2)
legend2.SetTextSize(0.04)
legend2.AddEntry(h7,"OC data: "+str(int(Integral7)),"p")
legend2.AddEntry(h8,"SC data: "+str(int(Integral8)),"p")
legend2.AddEntry(h5,"OC MC: "+str(int(Integral5)),"p")
legend2.AddEntry(h6,"SC MC: "+str(int(Integral6)),"p")
legend2.SetBorderSize(0)
legend2.Draw("same")
MakeSmallLabel(0.15,0.78,analysis=args.t)
PrintCan(can)

# EFF BEGIN

outfile = TFile(can.GetName()+".root", "recreate")

if (args.o =="MSTree"):
        #Or_eff_id_ms_pt_plus_mc = rfiles[0].Get("eff_id_ms_pt_plus")
        #Or_eff_id_ms_pt_minus_mc = rfiles[0].Get("eff_id_ms_pt_minus")
        #Or_eff_id_ms_eta_plus_mc = rfiles[0].Get("eff_id_ms_eta_plus")
        #Or_eff_id_ms_eta_minus_mc = rfiles[0].Get("eff_id_ms_eta_minus")
        #Or_eff_id_ms_pt_plus_data = rfiles[2].Get("eff_id_ms_pt_plus")
        #Or_eff_id_ms_pt_minus_data = rfiles[2].Get("eff_id_ms_pt_minus")
        #Or_eff_id_ms_eta_plus_data = rfiles[2].Get("eff_id_ms_eta_plus")
        #Or_eff_id_ms_eta_minus_data = rfiles[2].Get("eff_id_ms_eta_minus")
        #gROOT.SetBatch(1)

        muon0_pt_nomin_plus_mcoc = rfiles[0].Get("muon0_pt_nomin_plus")
        muon0_pt_denomin_plus_mcoc = rfiles[0].Get("muon0_pt_denomin_plus")
        muon0_pt_nomin_minus_mcoc = rfiles[0].Get("muon0_pt_nomin_minus")
        muon0_pt_denomin_minus_mcoc = rfiles[0].Get("muon0_pt_denomin_minus")
        muon0_eta_nomin_plus_mcoc = rfiles[0].Get("muon0_eta_nomin_plus")
        muon0_eta_denomin_plus_mcoc = rfiles[0].Get("muon0_eta_denomin_plus")
        muon0_eta_nomin_minus_mcoc = rfiles[0].Get("muon0_eta_nomin_minus")
        muon0_eta_denomin_minus_mcoc = rfiles[0].Get("muon0_eta_denomin_minus")

        muon0_pt_nomin_plus_mcsc = rfiles[1].Get("muon0_pt_nomin_plus")
        muon0_pt_denomin_plus_mcsc = rfiles[1].Get("muon0_pt_denomin_plus")
        muon0_pt_nomin_minus_mcsc = rfiles[1].Get("muon0_pt_nomin_minus")
        muon0_pt_denomin_minus_mcsc = rfiles[1].Get("muon0_pt_denomin_minus")
        muon0_eta_nomin_plus_mcsc = rfiles[1].Get("muon0_eta_nomin_plus")
        muon0_eta_denomin_plus_mcsc = rfiles[1].Get("muon0_eta_denomin_plus")
        muon0_eta_nomin_minus_mcsc = rfiles[1].Get("muon0_eta_nomin_minus")
        muon0_eta_denomin_minus_mcsc = rfiles[1].Get("muon0_eta_denomin_minus")

        muon0_pt_nomin_plus_dataoc = rfiles[2].Get("muon0_pt_nomin_plus")
        muon0_pt_denomin_plus_dataoc = rfiles[2].Get("muon0_pt_denomin_plus")
        muon0_pt_nomin_minus_dataoc = rfiles[2].Get("muon0_pt_nomin_minus")
        muon0_pt_denomin_minus_dataoc = rfiles[2].Get("muon0_pt_denomin_minus")
        muon0_eta_nomin_plus_dataoc = rfiles[2].Get("muon0_eta_nomin_plus")
        muon0_eta_denomin_plus_dataoc = rfiles[2].Get("muon0_eta_denomin_plus")
        muon0_eta_nomin_minus_dataoc = rfiles[2].Get("muon0_eta_nomin_minus")
        muon0_eta_denomin_minus_dataoc = rfiles[2].Get("muon0_eta_denomin_minus")

        muon0_pt_nomin_plus_datasc = rfiles[3].Get("muon0_pt_nomin_plus")
        muon0_pt_denomin_plus_datasc = rfiles[3].Get("muon0_pt_denomin_plus")
        muon0_pt_nomin_minus_datasc = rfiles[3].Get("muon0_pt_nomin_minus")
        muon0_pt_denomin_minus_datasc = rfiles[3].Get("muon0_pt_denomin_minus")
        muon0_eta_nomin_plus_datasc = rfiles[3].Get("muon0_eta_nomin_plus")
        muon0_eta_denomin_plus_datasc = rfiles[3].Get("muon0_eta_denomin_plus")
        muon0_eta_nomin_minus_datasc = rfiles[3].Get("muon0_eta_nomin_minus")
        muon0_eta_denomin_minus_datasc = rfiles[3].Get("muon0_eta_denomin_minus")

        ##sub back

        ##DummyCanvas2 = TCanvas("DummyCanvas2","DummyCanvas2",0,0,800, 800)
        ##DummyCanvas2.Divide(4,4)


        ##muon0_pt_nomin_plus_mcoc.Add(muon0_pt_nomin_plus_mcsc, -1.0)
        ##muon0_pt_denomin_plus_mcoc.Add(muon0_pt_denomin_plus_mcsc, -1.0)
        ##muon0_pt_nomin_minus_mcoc.Add(muon0_pt_nomin_minus_mcsc, -1.0)
        ##muon0_pt_denomin_minus_mcoc.Add(muon0_pt_denomin_minus_mcsc, -1.0)
        ##muon0_eta_nomin_plus_mcoc.Add(muon0_eta_nomin_plus_mcsc, -1.0)
        ##muon0_eta_denomin_plus_mcoc.Add(muon0_eta_denomin_plus_mcsc, -1.0)
        ##muon0_eta_nomin_minus_mcoc.Add(muon0_eta_nomin_minus_mcsc, -1.0)
        ##muon0_eta_denomin_minus_mcoc.Add(muon0_eta_denomin_minus_mcsc, -1.0)
        ##muon0_pt_nomin_plus_dataoc.Add(muon0_pt_nomin_plus_datasc, -1.0)
        ##muon0_pt_denomin_plus_dataoc.Add(muon0_pt_denomin_plus_datasc, -1.0)
        ##muon0_pt_nomin_minus_dataoc.Add(muon0_pt_nomin_minus_datasc, -1.0)
        ##muon0_pt_denomin_minus_dataoc.Add(muon0_pt_denomin_minus_datasc, -1.0)
        ##muon0_eta_nomin_plus_dataoc.Add(muon0_eta_nomin_plus_datasc, -1.0)
        ##muon0_eta_denomin_plus_dataoc.Add(muon0_eta_denomin_plus_datasc, -1.0)
        ##muon0_eta_nomin_minus_dataoc.Add(muon0_eta_nomin_minus_datasc, -1.0)
        ##muon0_eta_denomin_minus_dataoc.Add(muon0_eta_denomin_minus_datasc, -1.0)

        ##muon0_pt_nomin_plus_mcoc.Divide(muon0_pt_denomin_plus_mcoc)
        ##muon0_pt_nomin_minus_mcoc.Divide(muon0_pt_denomin_minus_mcoc)
        ##muon0_eta_nomin_plus_mcoc.Divide(muon0_eta_denomin_plus_mcoc)
        ##muon0_eta_nomin_minus_mcoc.Divide(muon0_eta_denomin_minus_mcoc)
        ##muon0_pt_nomin_plus_dataoc.Divide(muon0_pt_denomin_plus_dataoc)
        ##muon0_pt_nomin_minus_dataoc.Divide(muon0_pt_denomin_minus_dataoc)
        ##muon0_eta_nomin_plus_dataoc.Divide(muon0_eta_denomin_plus_dataoc)
        ##muon0_eta_nomin_minus_dataoc.Divide(muon0_eta_denomin_minus_dataoc)


        #DummyCanvas2.cd(1)
        #muon0_pt_nomin_plus_mcoc.Draw()
        ##DummyCanvas2.cd(2)
        ##muon0_pt_denomin_plus_mcoc.Draw()
        #DummyCanvas2.cd(3)
        #muon0_pt_nomin_minus_mcoc.Draw()
        ##DummyCanvas2.cd(4)
        ##muon0_pt_denomin_minus_mcoc.Draw()
        #DummyCanvas2.cd(5)
        #muon0_eta_nomin_plus_mcoc.Draw()
        ##DummyCanvas2.cd(6)
        ##muon0_eta_denomin_plus_mcoc.Draw()      
        #DummyCanvas2.cd(7)
        #muon0_eta_nomin_minus_mcoc.Draw()
        ##DummyCanvas2.cd(8)
        ##muon0_eta_denomin_minus_mcoc.Draw()
        #DummyCanvas2.cd(9)
        #muon0_pt_nomin_plus_dataoc.Draw()
        ##DummyCanvas2.cd(10)
        ##muon0_pt_denomin_plus_dataoc.Draw()
        #DummyCanvas2.cd(11)
        #muon0_pt_nomin_minus_dataoc.Draw()
        ##DummyCanvas2.cd(12)
        ##muon0_pt_denomin_minus_dataoc.Draw()
        #DummyCanvas2.cd(13)
        #muon0_eta_nomin_plus_dataoc.Draw()
        ##DummyCanvas2.cd(14)
        ##muon0_eta_denomin_plus_dataoc.Draw()
        #DummyCanvas2.cd(15)
        #muon0_eta_nomin_minus_dataoc.Draw()
        ##DummyCanvas2.cd(16)
        ##muon0_eta_denomin_minus_dataoc.Draw()

        #gApplication.Run()
        DummyCanvas = TCanvas("DummyCanvas","DummyCanvas",0,0,800, 800)
        DummyCanvas.Divide(4,4)
        Or_eff_id_ms_pt_plus_mc = TEfficiency( muon0_pt_nomin_plus_mcoc , muon0_pt_denomin_plus_mcoc)
        DummyCanvas.cd(1)
        Or_eff_id_ms_pt_plus_mc.Draw()
        gPad.Update()
        eff_id_ms_pt_plus_mc = Or_eff_id_ms_pt_plus_mc.GetPaintedGraph()

        Or_eff_id_ms_pt_minus_mc = TEfficiency(muon0_pt_nomin_minus_mcoc, muon0_pt_denomin_minus_mcoc)
        DummyCanvas.cd(2)
        Or_eff_id_ms_pt_minus_mc.Draw()
        gPad.Update()
        eff_id_ms_pt_minus_mc = Or_eff_id_ms_pt_minus_mc.GetPaintedGraph()        
        
        Or_eff_id_ms_eta_plus_mc = TEfficiency(muon0_eta_nomin_plus_mcoc , muon0_eta_denomin_plus_mcoc)
        DummyCanvas.cd(3)
        Or_eff_id_ms_eta_plus_mc.Draw()
        gPad.Update()
        eff_id_ms_eta_plus_mc = Or_eff_id_ms_eta_plus_mc.GetPaintedGraph()
        
        Or_eff_id_ms_eta_minus_mc = TEfficiency(muon0_eta_nomin_minus_mcoc, muon0_eta_denomin_minus_mcoc)
        DummyCanvas.cd(4)
        Or_eff_id_ms_eta_minus_mc.Draw()
        gPad.Update()
        eff_id_ms_eta_minus_mc = Or_eff_id_ms_eta_minus_mc.GetPaintedGraph()
        
        Or_eff_id_ms_pt_plus_data = TEfficiency(muon0_pt_nomin_plus_dataoc, muon0_pt_denomin_plus_dataoc)
        DummyCanvas.cd(5)
        Or_eff_id_ms_pt_plus_data.Draw()
        gPad.Update()
        eff_id_ms_pt_plus_data = Or_eff_id_ms_pt_plus_data.GetPaintedGraph()
        
        Or_eff_id_ms_pt_minus_data = TEfficiency(muon0_pt_nomin_minus_dataoc, muon0_pt_denomin_minus_dataoc)
        DummyCanvas.cd(6)
        Or_eff_id_ms_pt_minus_data.Draw()
        gPad.Update()
        eff_id_ms_pt_minus_data = Or_eff_id_ms_pt_minus_data.GetPaintedGraph()
        
        Or_eff_id_ms_eta_plus_data = TEfficiency(muon0_eta_nomin_plus_dataoc, muon0_eta_denomin_plus_dataoc)
        DummyCanvas.cd(7)
        Or_eff_id_ms_eta_plus_data.Draw()
        gPad.Update()
        eff_id_ms_eta_plus_data = Or_eff_id_ms_eta_plus_data.GetPaintedGraph()
        
        Or_eff_id_ms_eta_minus_data = TEfficiency(muon0_eta_nomin_minus_dataoc, muon0_eta_denomin_minus_dataoc)
        DummyCanvas.cd(8)
        Or_eff_id_ms_eta_minus_data.Draw()
        gPad.Update()
        eff_id_ms_eta_minus_data = Or_eff_id_ms_eta_minus_data.GetPaintedGraph()
        #gROOT.SetBatch(0)
        #gApplication.Run()
        
        #eff_id_ms_pt_plus_mc.Draw()
        #gApplication.Run()
        
        PrepareEff(eff_id_ms_pt_plus_mc, LineColor=2, Marker = 4, MarkerColor = 2, YTitle = "Efficiency (ID | MS)", XTitle="p_{T}^{#mu} [GeV]", LineStyle=-1)
        PrepareEff(eff_id_ms_pt_plus_data, LineColor=1, Marker = 20, MarkerColor = 1, YTitle = "Efficiency (ID | MS)", XTitle="p_{T}^{#mu} [GeV]", LineStyle=-1)
        PrepareEff(eff_id_ms_pt_minus_mc, LineColor=2, Marker = 4, MarkerColor = 2, YTitle = "Efficiency (ID | MS)", XTitle="p_{T}^{#mu} [GeV]", LineStyle=-1)
        PrepareEff(eff_id_ms_pt_minus_data, LineColor=1, Marker = 20, MarkerColor = 1, YTitle = "Efficiency (ID | MS)", XTitle="p_{T}^{#mu} [GeV]", LineStyle=-1)
        PrepareEff(eff_id_ms_eta_plus_mc, LineColor=2, Marker = 4, MarkerColor = 2, YTitle = "Efficiency (ID | MS)", XTitle="#eta^{#mu}", LineStyle=-1)
        PrepareEff(eff_id_ms_eta_plus_data, LineColor=1, Marker = 20, MarkerColor = 1, YTitle = "Efficiency (ID | MS)", XTitle="#eta^{#mu}", LineStyle=-1)
        
        gStyle.SetPadLeftMargin(0.12)
        gStyle.SetOptStat(0)
        gStyle.SetPadRightMargin(0.12)
        can1 = TCanvas(args.o+"_id_ms_efficiency","Results",0,0,1600, 1600)
        can1.Divide(2,2)
        can1, pad1, pad1r, mg1, legend1, eff_ratio, line1 = DoAllTheBloodyWork( TGraph_eff_mc = eff_id_ms_pt_plus_mc, TGraph_eff_data = eff_id_ms_pt_plus_data , TEff_mc = Or_eff_id_ms_pt_plus_mc, TEff_data = Or_eff_id_ms_pt_plus_data , canvas = can1, pad1Name = "pad1", pad1rName = "pad1r", XTitle = "p_{T}^{#mu}", YTitle = "Efficiency (ID | MS)" , MultiGraphName = "multi1", legend_mc = "MC, #mu^{+}", legend_data = "data, #mu^{+}" , data_h_passed_Name = "data_h_passed_Name", data_h_total_Name = "data_h_total_Name", mc_h_passed_Name = "mc_h_passed_Name", mc_h_total_Name = "mc_h_total_Name" , CanvasNumber = 3, YTitleR = "Data/MC         ", XTitleR = "p_{T}^{#mu} [GeV]", StartLine = 10, EndLine = 100)
        can1.cd(3)
        pad1.cd()
        mg1.Draw("AP")
        legend1.Draw("same")
        MakeATLASLabel(0.01,0.1)
        pad1r.cd()
        line1.Draw()
        eff_ratio.Draw("p e1 same")
        can1, pad2, pad2r, mg2, legend2, eff_ratio2, line2 = DoAllTheBloodyWork( TGraph_eff_mc = eff_id_ms_pt_minus_mc, TGraph_eff_data = eff_id_ms_pt_minus_data , TEff_mc = Or_eff_id_ms_pt_minus_mc, TEff_data = Or_eff_id_ms_pt_minus_data , canvas = can1, pad1Name = "pad2", pad1rName = "pad2r", XTitle = "p_{T}^{#mu}", YTitle = "Efficiency (ID | MS)" , MultiGraphName = "multi2", legend_mc = "MC, #mu^{-}", legend_data = "data, #mu^{-}" , data_h_passed_Name = "data_h_passed_Name2", data_h_total_Name = "data_h_total_Name2", mc_h_passed_Name = "mc_h_passed_Name2", mc_h_total_Name = "mc_h_total_Name2" , CanvasNumber = 4, YTitleR = "Data/MC         ", XTitleR = "p_{T}^{#mu} [GeV]", StartLine = 10, EndLine = 100)
        can1.cd(4)
        pad2.cd()
        mg2.Draw("AP")
        legend2.Draw("same")
        MakeATLASLabel(0.01,0.1)
        pad2r.cd()
        line2.Draw()
        eff_ratio2.Draw("p e1 same")
        can1, pad3, pad3r, mg3, legend3, eff_ratio3, line3 = DoAllTheBloodyWork( TGraph_eff_mc = eff_id_ms_eta_plus_mc, TGraph_eff_data = eff_id_ms_eta_plus_data , TEff_mc = Or_eff_id_ms_eta_plus_mc, TEff_data = Or_eff_id_ms_eta_plus_data , canvas = can1, pad1Name = "pad3", pad1rName = "pad3r", XTitle = "#eta^{#mu}", YTitle = "Efficiency (ID | MS)" , MultiGraphName = "multi3", legend_mc = "MC, #mu^{+}", legend_data = "data, #mu^{+}" , data_h_passed_Name = "data_h_passed_Name3", data_h_total_Name = "data_h_total_Name3", mc_h_passed_Name = "mc_h_passed_Name3", mc_h_total_Name = "mc_h_total_Name3" , CanvasNumber = 1, YTitleR = "Data/MC         ", XTitleR = "#eta^{#mu}", StartLine = -2.5, EndLine = 2.5)
        can1.cd(1)
        pad3.cd()
        mg3.Draw("AP")
        legend3.Draw("same")
        MakeATLASLabel(0.01,0.1)
        pad3r.cd()
        line3.Draw("same")
        eff_ratio3.Draw("p e1 same")
        PrepareEff(eff_id_ms_eta_minus_mc, LineColor=2, Marker = 4, MarkerColor = 2, YTitle = "Efficiency (ID | MS)", XTitle="#eta^{#mu}", LineStyle=-1)
        PrepareEff(eff_id_ms_eta_minus_data, LineColor=1, Marker = 20, MarkerColor = 1, YTitle = "Efficiency (ID | MS)", XTitle="#eta^{#mu}", LineStyle=-1)
        can1, pad4, pad4r, mg4, legend4, eff_ratio4, line4 = DoAllTheBloodyWork( TGraph_eff_mc = eff_id_ms_eta_minus_mc, TGraph_eff_data = eff_id_ms_eta_minus_data , TEff_mc = Or_eff_id_ms_eta_minus_mc, TEff_data = Or_eff_id_ms_eta_minus_data , canvas = can1, pad1Name = "pad4", pad1rName = "pad4r", XTitle = "#eta^{#mu}", YTitle = "Efficiency (ID | MS)" , MultiGraphName = "multi4", legend_mc = "MC, #mu^{-}", legend_data = "data, #mu^{-}" , data_h_passed_Name = "data_h_passed_Name4", data_h_total_Name = "data_h_total_Name4", mc_h_passed_Name = "mc_h_passed_Name4", mc_h_total_Name = "mc_h_total_Name4" , CanvasNumber = 2, YTitleR = "Data/MC         ", XTitleR = "#eta^{#mu}", StartLine = -2.5, EndLine = 2.5)      
        can1.cd(2)
        pad4.cd()
        mg4.Draw("AP")
        legend4.Draw("same")
        MakeATLASLabel(0.01,0.1)
        pad4r.cd()
        line4.Draw("same")
        eff_ratio4.Draw("p e1 same")
        #gApplication.Run()
        PrintCan(can1)
if (args.o =="CaloTree"):
        Or_eff_med_ct_pt_plus_mc = rfiles[0].Get("eff_med_ct_pt_plus")
        Or_eff_med_ct_pt_minus_mc = rfiles[0].Get("eff_med_ct_pt_minus")
        Or_eff_med_ct_eta_plus_mc = rfiles[0].Get("eff_med_ct_eta_plus")
        Or_eff_med_ct_eta_minus_mc = rfiles[0].Get("eff_med_ct_eta_minus")
        Or_eff_med_ct_pt_plus_data = rfiles[2].Get("eff_med_ct_pt_plus")
        Or_eff_med_ct_pt_minus_data = rfiles[2].Get("eff_med_ct_pt_minus")
        Or_eff_med_ct_eta_plus_data = rfiles[2].Get("eff_med_ct_eta_plus")
        Or_eff_med_ct_eta_minus_data = rfiles[2].Get("eff_med_ct_eta_minus")
        Or_eff_med_ct_pt_plus_mc.Draw()
        gPad.Update()
        eff_med_ct_pt_plus_mc = Or_eff_med_ct_pt_plus_mc.GetPaintedGraph()
        Or_eff_med_ct_pt_minus_mc.Draw()
        gPad.Update()
        eff_med_ct_pt_minus_mc = Or_eff_med_ct_pt_minus_mc.GetPaintedGraph()        
        Or_eff_med_ct_eta_plus_mc.Draw()
        gPad.Update()
        eff_med_ct_eta_plus_mc = Or_eff_med_ct_eta_plus_mc.GetPaintedGraph()
        Or_eff_med_ct_eta_minus_mc.Draw()
        gPad.Update()
        eff_med_ct_eta_minus_mc = Or_eff_med_ct_eta_minus_mc.GetPaintedGraph()
        Or_eff_med_ct_pt_plus_data.Draw()
        gPad.Update()
        eff_med_ct_pt_plus_data = Or_eff_med_ct_pt_plus_data.GetPaintedGraph()
        Or_eff_med_ct_pt_minus_data.Draw()
        gPad.Update()
        eff_med_ct_pt_minus_data = Or_eff_med_ct_pt_minus_data.GetPaintedGraph()
        Or_eff_med_ct_eta_plus_data.Draw()
        gPad.Update()
        eff_med_ct_eta_plus_data = Or_eff_med_ct_eta_plus_data.GetPaintedGraph()
        Or_eff_med_ct_eta_minus_data.Draw()
        gPad.Update()
        eff_med_ct_eta_minus_data = Or_eff_med_ct_eta_minus_data.GetPaintedGraph()
        PrepareEff(eff_med_ct_pt_plus_mc, LineColor=2, Marker = 4, MarkerColor = 2, YTitle = "Efficiency (Medium | ID)", XTitle="p_{T}^{#mu} [GeV]", LineStyle=-1)
        PrepareEff(eff_med_ct_pt_plus_data, LineColor=1, Marker = 20, MarkerColor = 1, YTitle = "Efficiency (Medium | ID)", XTitle="p_{T}^{#mu} [GeV]", LineStyle=-1)
        PrepareEff(eff_med_ct_pt_minus_mc, LineColor=2, Marker = 4, MarkerColor = 2, YTitle = "Efficiency (Medium | ID)", XTitle="p_{T}^{#mu} [GeV]", LineStyle=-1)
        PrepareEff(eff_med_ct_pt_minus_data, LineColor=1, Marker = 20, MarkerColor = 1, YTitle = "Efficiency (Medium | ID)", XTitle="p_{T}^{#mu} [GeV]", LineStyle=-1)
        PrepareEff(eff_med_ct_eta_plus_mc, LineColor=2, Marker = 4, MarkerColor = 2, YTitle = "Efficiency (Medium | ID)", XTitle="#eta^{#mu}", LineStyle=-1)
        PrepareEff(eff_med_ct_eta_plus_data, LineColor=1, Marker = 20, MarkerColor = 1, YTitle = "Efficiency (Medium | ID)", XTitle="#eta^{#mu}", LineStyle=-1)
        
        gStyle.SetPadLeftMargin(0.12)
        gStyle.SetOptStat(0)
        gStyle.SetPadRightMargin(0.12)
        can1 = TCanvas(args.o+"_med_ct_efficiency","Results",0,0,1600, 1600)
        can1.Divide(2,2)
        can1, pad1, pad1r, mg1, legend1, eff_ratio, line1 = DoAllTheBloodyWork( TGraph_eff_mc = eff_med_ct_pt_plus_mc, TGraph_eff_data = eff_med_ct_pt_plus_data , TEff_mc = Or_eff_med_ct_pt_plus_mc, TEff_data = Or_eff_med_ct_pt_plus_data , canvas = can1, pad1Name = "pad1", pad1rName = "pad1r", XTitle = "p_{T}^{#mu}", YTitle = "Efficiency (Medium | ID)" , MultiGraphName = "multi1", legend_mc = "MC, #mu^{+}", legend_data = "data, #mu^{+}" , data_h_passed_Name = "data_h_passed_Name", data_h_total_Name = "data_h_total_Name", mc_h_passed_Name = "mc_h_passed_Name", mc_h_total_Name = "mc_h_total_Name" , CanvasNumber = 3, YTitleR = "Data/MC         ", XTitleR = "p_{T}^{#mu} [GeV]", StartLine = 10, EndLine = 100)
        can1.cd(3)
        pad1.cd()
        mg1.Draw("AP")
        legend1.Draw("same")
        MakeATLASLabel(0.01,0.1)
        pad1r.cd()
        line1.Draw()
        eff_ratio.Draw("p e1 same")
        can1, pad2, pad2r, mg2, legend2, eff_ratio2, line2 = DoAllTheBloodyWork( TGraph_eff_mc = eff_med_ct_pt_minus_mc, TGraph_eff_data = eff_med_ct_pt_minus_data , TEff_mc = Or_eff_med_ct_pt_minus_mc, TEff_data = Or_eff_med_ct_pt_minus_data , canvas = can1, pad1Name = "pad2", pad1rName = "pad2r", XTitle = "p_{T}^{#mu}", YTitle = "Efficiency (Medium | ID)" , MultiGraphName = "multi2", legend_mc = "MC, #mu^{-}", legend_data = "data, #mu^{-}" , data_h_passed_Name = "data_h_passed_Name2", data_h_total_Name = "data_h_total_Name2", mc_h_passed_Name = "mc_h_passed_Name2", mc_h_total_Name = "mc_h_total_Name2" , CanvasNumber = 4, YTitleR = "Data/MC         ", XTitleR = "p_{T}^{#mu} [GeV]", StartLine = 10, EndLine = 100)
        can1.cd(4)
        pad2.cd()
        mg2.Draw("AP")
        legend2.Draw("same")
        MakeATLASLabel(0.01,0.1)
        pad2r.cd()
        line2.Draw()
        eff_ratio2.Draw("p e1 same")
        can1, pad3, pad3r, mg3, legend3, eff_ratio3, line3 = DoAllTheBloodyWork( TGraph_eff_mc = eff_med_ct_eta_plus_mc, TGraph_eff_data = eff_med_ct_eta_plus_data , TEff_mc = Or_eff_med_ct_eta_plus_mc, TEff_data = Or_eff_med_ct_eta_plus_data , canvas = can1, pad1Name = "pad3", pad1rName = "pad3r", XTitle = "#eta^{#mu}", YTitle = "Efficiency (Medium | ID)" , MultiGraphName = "multi3", legend_mc = "MC, #mu^{+}", legend_data = "data, #mu^{+}" , data_h_passed_Name = "data_h_passed_Name3", data_h_total_Name = "data_h_total_Name3", mc_h_passed_Name = "mc_h_passed_Name3", mc_h_total_Name = "mc_h_total_Name3" , CanvasNumber = 1, YTitleR = "Data/MC         ", XTitleR = "#eta^{#mu}", StartLine = -2.5, EndLine = 2.5)
        can1.cd(1)
        pad3.cd()
        mg3.Draw("AP")
        legend3.Draw("same")
        MakeATLASLabel(0.01,0.1)
        pad3r.cd()
        line3.Draw("same")
        eff_ratio3.Draw("p e1 same")
        PrepareEff(eff_med_ct_eta_minus_mc, LineColor=2, Marker = 4, MarkerColor = 2, YTitle = "Efficiency (Medium | ID)", XTitle="#eta^{#mu}", LineStyle=-1)
        PrepareEff(eff_med_ct_eta_minus_data, LineColor=1, Marker = 20, MarkerColor = 1, YTitle = "Efficiency (Medium | ID)", XTitle="#eta^{#mu}", LineStyle=-1)
        can1, pad4, pad4r, mg4, legend4, eff_ratio4, line4 = DoAllTheBloodyWork( TGraph_eff_mc = eff_med_ct_eta_minus_mc, TGraph_eff_data = eff_med_ct_eta_minus_data , TEff_mc = Or_eff_med_ct_eta_minus_mc, TEff_data = Or_eff_med_ct_eta_minus_data , canvas = can1, pad1Name = "pad4", pad1rName = "pad4r", XTitle = "#eta^{#mu}", YTitle = "Efficiency (Medium | ID)" , MultiGraphName = "multi4", legend_mc = "MC, #mu^{-}", legend_data = "data, #mu^{-}" , data_h_passed_Name = "data_h_passed_Name4", data_h_total_Name = "data_h_total_Name4", mc_h_passed_Name = "mc_h_passed_Name4", mc_h_total_Name = "mc_h_total_Name4" , CanvasNumber = 2, YTitleR = "Data/MC         ", XTitleR = "#eta^{#mu}", StartLine = -2.5, EndLine = 2.5)      
        can1.cd(2)
        pad4.cd()
        mg4.Draw("AP")
        legend4.Draw("same")
        MakeATLASLabel(0.01,0.1)
        pad4r.cd()
        line4.Draw("same")
        eff_ratio4.Draw("p e1 same")
        PrintCan(can1)

if ("Triger" in args.o):
        muon0_pt_nomin_both_barrel_mcoc = rfiles[0].Get("muon0_pt_nomin_both_barrel") # MCOC
        muon0_pt_nomin_both_barrel_mcsc = rfiles[1].Get("muon0_pt_nomin_both_barrel") #MCSC
        muon0_pt_nomin_both_barrel_dataoc = rfiles[2].Get("muon0_pt_nomin_both_barrel") #DATAOC
        muon0_pt_nomin_both_barrel_datasc = rfiles[3].Get("muon0_pt_nomin_both_barrel") #DATASC

        muon0_pt_denomin_both_barrel_mcoc = rfiles[0].Get("muon0_pt_denomin_both_barrel") # MCOC
        muon0_pt_denomin_both_barrel_mcsc = rfiles[1].Get("muon0_pt_denomin_both_barrel") #MCSC
        muon0_pt_denomin_both_barrel_dataoc = rfiles[2].Get("muon0_pt_denomin_both_barrel") #DATAOC
        muon0_pt_denomin_both_barrel_datasc = rfiles[3].Get("muon0_pt_denomin_both_barrel") #DATASC

        muon0_pt_nomin_both_endcup_mcoc = rfiles[0].Get("muon0_pt_nomin_both_endcup") # MCOC
        muon0_pt_nomin_both_endcup_mcsc = rfiles[1].Get("muon0_pt_nomin_both_endcup") #MCSC
        muon0_pt_nomin_both_endcup_dataoc = rfiles[2].Get("muon0_pt_nomin_both_endcup") #DATAOC
        muon0_pt_nomin_both_endcup_datasc = rfiles[3].Get("muon0_pt_nomin_both_endcup") #DATASC

        muon0_pt_denomin_both_endcup_mcoc = rfiles[0].Get("muon0_pt_denomin_both_endcup") # MCOC
        muon0_pt_denomin_both_endcup_mcsc = rfiles[1].Get("muon0_pt_denomin_both_endcup") #MCSC
        muon0_pt_denomin_both_endcup_dataoc = rfiles[2].Get("muon0_pt_denomin_both_endcup") #DATAOC
        muon0_pt_denomin_both_endcup_datasc = rfiles[3].Get("muon0_pt_denomin_both_endcup") #DATASC

        # subtractiong background

        muon0_pt_nomin_both_barrel_mcoc.Add(muon0_pt_nomin_both_barrel_mcsc, -1.0)
        muon0_pt_nomin_both_barrel_dataoc.Add(muon0_pt_nomin_both_barrel_datasc, -1.0)
        muon0_pt_denomin_both_barrel_mcoc.Add(muon0_pt_denomin_both_barrel_mcsc, -1.0)
        muon0_pt_denomin_both_barrel_dataoc.Add(muon0_pt_denomin_both_barrel_datasc, -1.0)

        muon0_pt_nomin_both_endcup_mcoc.Add(muon0_pt_nomin_both_endcup_mcsc, -1.0)
        muon0_pt_nomin_both_endcup_dataoc.Add(muon0_pt_nomin_both_endcup_datasc, -1.0)
        muon0_pt_denomin_both_endcup_mcoc.Add(muon0_pt_denomin_both_endcup_mcsc, -1.0)
        muon0_pt_denomin_both_endcup_dataoc.Add(muon0_pt_denomin_both_endcup_datasc, -1.0)

        # creating TEfficiencies

        DummyCanvas = TCanvas("DummyCanvas","DummyCanvas",0,0,800, 800)
        DummyCanvas.cd()
        muon0_pt_Teff_both_barrel_mcoc = TEfficiency(muon0_pt_nomin_both_barrel_mcoc,muon0_pt_denomin_both_barrel_mcoc)
        muon0_pt_Teff_both_barrel_mcoc.Draw()
        gPad.Update()
        muon0_pt_eff_both_barrel_mcoc = muon0_pt_Teff_both_barrel_mcoc.GetPaintedGraph()

        muon0_pt_Teff_both_barrel_dataoc = TEfficiency(muon0_pt_nomin_both_barrel_dataoc,muon0_pt_denomin_both_barrel_dataoc)
        muon0_pt_Teff_both_barrel_dataoc.Draw()
        gPad.Update()
        muon0_pt_eff_both_barrel_dataoc = muon0_pt_Teff_both_barrel_dataoc.GetPaintedGraph()

        muon0_pt_Teff_both_endcup_mcoc = TEfficiency(muon0_pt_nomin_both_endcup_mcoc,muon0_pt_denomin_both_endcup_mcoc)
        muon0_pt_Teff_both_endcup_mcoc.Draw()
        gPad.Update()
        muon0_pt_eff_both_endcup_mcoc = muon0_pt_Teff_both_endcup_mcoc.GetPaintedGraph()

        muon0_pt_Teff_both_endcup_dataoc = TEfficiency(muon0_pt_nomin_both_endcup_dataoc,muon0_pt_denomin_both_endcup_dataoc)
        muon0_pt_Teff_both_endcup_dataoc.Draw()
        gPad.Update()
        muon0_pt_eff_both_endcup_dataoc = muon0_pt_Teff_both_endcup_dataoc.GetPaintedGraph()

        PrepareEff(muon0_pt_eff_both_barrel_mcoc, LineColor=2, Marker = 4, MarkerColor = 2, YTitle = "Trigger efficiency", XTitle="p_{T}^{#mu} [GeV]", LineStyle=-1)
        PrepareEff(muon0_pt_eff_both_barrel_dataoc, LineColor=1, Marker = 20, MarkerColor = 1, YTitle = "Trigger efficiency", XTitle="p_{T}^{#mu} [GeV]", LineStyle=-1)
        PrepareEff(muon0_pt_eff_both_endcup_mcoc, LineColor=2, Marker = 4, MarkerColor = 2, YTitle = "Trigger efficiency", XTitle="p_{T}^{#mu} [GeV]", LineStyle=-1)
        PrepareEff(muon0_pt_eff_both_endcup_dataoc, LineColor=1, Marker = 20, MarkerColor = 1, YTitle = "Trigger efficiency", XTitle="p_{T}^{#mu} [GeV]", LineStyle=-1)

        gStyle.SetPadLeftMargin(0.12)
        gStyle.SetOptStat(0)
        gStyle.SetPadRightMargin(0.12)
        can1 = TCanvas(args.o+"_and_endcup_pt_both_efficiency","Results",0,0,1600, 1600)
        can1.Divide(2,1)
        can1, pad1, pad1r, mg1, legend1, eff_ratio, line1 = DoAllTheBloodyWork( TGraph_eff_mc = muon0_pt_eff_both_barrel_mcoc, TGraph_eff_data = muon0_pt_eff_both_barrel_dataoc , TEff_mc = muon0_pt_Teff_both_barrel_mcoc, TEff_data = muon0_pt_Teff_both_barrel_dataoc , canvas = can1, pad1Name = "pad1", pad1rName = "pad1r", XTitle = "p_{T}^{#mu}", YTitle = "Trigger Efficiency" , MultiGraphName = "multi1", legend_mc = "MC, #mu^{#pm}", legend_data = "data, #mu^{#pm}" , data_h_passed_Name = "data_h_passed_Name", data_h_total_Name = "data_h_total_Name", mc_h_passed_Name = "mc_h_passed_Name", mc_h_total_Name = "mc_h_total_Name" , CanvasNumber = 1, YTitleR = "Data/MC         ", XTitleR = "p_{T}^{#mu} [GeV]", StartLine = 10, EndLine = 100, mg1Min=0.001, RatioMin=0.8, RatioMax=1.2, YOffset=1.04)
        can1.cd(1)
        pad1.cd()
        mg1.Draw("AP")
        legend1.Draw("same")
        MakeATLASLabel(0.01,0.1,Sizer=0.08)
        pad1r.cd()
        line1.Draw("same")
        eff_ratio.Draw("p e1 same")
        PrepareEff(muon0_pt_eff_both_endcup_mcoc, LineColor=2, Marker = 4, MarkerColor = 2, YTitle = "Trigger efficiency", XTitle="p_{T}^{#mu} [GeV]", LineStyle=-1)
        PrepareEff(muon0_pt_eff_both_endcup_dataoc, LineColor=1, Marker = 20, MarkerColor = 1, YTitle = "Trigger efficiency", XTitle="p_{T}^{#mu} [GeV]", LineStyle=-1)
        can1, pad2, pad2r, mg2, legend2, eff_ratio2, line2 = DoAllTheBloodyWork( TGraph_eff_mc = muon0_pt_eff_both_endcup_mcoc, TGraph_eff_data = muon0_pt_eff_both_endcup_dataoc , TEff_mc = muon0_pt_Teff_both_endcup_mcoc, TEff_data = muon0_pt_Teff_both_endcup_dataoc , canvas = can1, pad1Name = "pad2", pad1rName = "pad2r", XTitle = "p_{T}^{#mu}", YTitle = "Trigger Efficiency" , MultiGraphName = "multi2", legend_mc = "MC, #mu^{#pm}", legend_data = "data, #mu^{#pm}" , data_h_passed_Name = "data_h_passed_Name2", data_h_total_Name = "data_h_total_Name2", mc_h_passed_Name = "mc_h_passed_Name2", mc_h_total_Name = "mc_h_total_Name2" , CanvasNumber = 2, YTitleR = "Data/MC         ", XTitleR = "p_{T}^{#mu} [GeV]", StartLine = 10, EndLine = 100, mg1Min=0.001, RatioMin=0.8, RatioMax=1.2, YOffset=1.04)
        can1.cd(2)
        pad2.cd()
        mg2.Draw("AP")
        legend2.Draw("same")
        MakeATLASLabel(0.01,0.1,Sizer=0.08)
        pad2r.cd()
        line2.Draw()
        eff_ratio2.Draw("p e1 same")
        PrintCan(can1)

        DummyCanvas.cd()

        muon0_eta_nomin_both_mcoc = rfiles[0].Get("muon0_eta_nomin_both") # MCOC
        muon0_eta_nomin_both_mcsc = rfiles[1].Get("muon0_eta_nomin_both") #MCSC
        muon0_eta_nomin_both_dataoc = rfiles[2].Get("muon0_eta_nomin_both") #DATAOC
        muon0_eta_nomin_both_datasc = rfiles[3].Get("muon0_eta_nomin_both") #DATASC

        muon0_eta_denomin_both_mcoc = rfiles[0].Get("muon0_eta_denomin_both") # MCOC
        muon0_eta_denomin_both_mcsc = rfiles[1].Get("muon0_eta_denomin_both") #MCSC
        muon0_eta_denomin_both_dataoc = rfiles[2].Get("muon0_eta_denomin_both") #DATAOC
        muon0_eta_denomin_both_datasc = rfiles[3].Get("muon0_eta_denomin_both") #DATASC

        muon0_eta_nomin_both_mcoc.Add(muon0_eta_nomin_both_mcsc, -1.0)
        muon0_eta_nomin_both_dataoc.Add(muon0_eta_nomin_both_datasc, -1.0)
        muon0_eta_denomin_both_mcoc.Add(muon0_eta_denomin_both_mcsc, -1.0)
        muon0_eta_denomin_both_dataoc.Add(muon0_eta_denomin_both_datasc, -1.0)

        muon0_eta_Teff_both_mcoc = TEfficiency(muon0_eta_nomin_both_mcoc,muon0_eta_denomin_both_mcoc)
        muon0_eta_Teff_both_mcoc.Draw()
        gPad.Update()
        muon0_eta_eff_both_mcoc = muon0_eta_Teff_both_mcoc.GetPaintedGraph()

        muon0_eta_Teff_both_dataoc = TEfficiency(muon0_eta_nomin_both_dataoc,muon0_eta_denomin_both_dataoc)
        muon0_eta_Teff_both_dataoc.Draw()
        gPad.Update()
        muon0_eta_eff_both_dataoc = muon0_eta_Teff_both_dataoc.GetPaintedGraph()
        
        gStyle.SetPadLeftMargin(0.12)
        gStyle.SetOptStat(0)
        gStyle.SetPadRightMargin(0.12)
        can2 = TCanvas(args.o+"_and_endcup_eta_both_efficiency_here","Resultsb",0,0,800, 800)

        PrepareEff(muon0_eta_eff_both_mcoc, LineColor=2, Marker = 4, MarkerColor = 2, YTitle = "Trigger efficiency", XTitle="p_{T}^{#mu} [GeV]", LineStyle=-1)
        PrepareEff(muon0_eta_eff_both_dataoc, LineColor=1, Marker = 20, MarkerColor = 1, YTitle = "Trigger efficiency", XTitle="p_{T}^{#mu} [GeV]", LineStyle=-1)
        can2, pad1b, pad1rb, mg1b, legend1b, eff_ratiob, line1b = DoAllTheBloodyWork( TGraph_eff_mc = muon0_eta_eff_both_mcoc, TGraph_eff_data = muon0_eta_eff_both_dataoc , TEff_mc = muon0_eta_Teff_both_mcoc, TEff_data = muon0_eta_Teff_both_dataoc , canvas = can2, pad1Name = "pad1b", pad1rName = "pad1rb", XTitle = "p_{T}^{#mu}", YTitle = "Trigger Efficiency" , MultiGraphName = "multi1b", legend_mc = "MC, #mu^{#pm}", legend_data = "data, #mu^{#pm}" , data_h_passed_Name = "data_h_passed_Nameb", data_h_total_Name = "data_h_total_Nameb", mc_h_passed_Name = "mc_h_passed_Nameb", mc_h_total_Name = "mc_h_total_Nameb" , CanvasNumber = 1, YTitleR = "Data/MC         ", XTitleR = "#eta^{#mu}", StartLine = -2.5, EndLine = 2.5, mg1Min=0.001, RatioMin=0.8, RatioMax=1.2, YOffset=1.04)
        can2.cd()
        pad1b.cd()
        mg1b.Draw("AP")
        legend1b.Draw("same")
        MakeATLASLabel(0.01,0.1,Sizer=0.08)
        pad1rb.cd()
        line1b.Draw("same")
        eff_ratiob.Draw("p e1 same")
        #can2.SetName(args.o+"_and_endcup_eta_both_efficiency")
        PrintCan(can2)

        ## Figure 27

        muon0_pt_nomin_plus_barrel_dataoc = rfiles[2].Get("muon0_pt_nomin_plus_barrel") #DATAOC
        muon0_pt_nomin_plus_barrel_datasc = rfiles[3].Get("muon0_pt_nomin_plus_barrel") #DATASC
        muon0_pt_denomin_plus_barrel_dataoc = rfiles[2].Get("muon0_pt_denomin_plus_barrel") #DATAOC
        muon0_pt_denomin_plus_barrel_datasc = rfiles[3].Get("muon0_pt_denomin_plus_barrel") #DATASC

        muon0_pt_nomin_minus_barrel_dataoc = rfiles[2].Get("muon0_pt_nomin_minus_barrel") #DATAOC
        muon0_pt_nomin_minus_barrel_datasc = rfiles[3].Get("muon0_pt_nomin_minus_barrel") #DATASC
        muon0_pt_denomin_minus_barrel_dataoc = rfiles[2].Get("muon0_pt_denomin_minus_barrel") #DATAOC
        muon0_pt_denomin_minus_barrel_datasc = rfiles[3].Get("muon0_pt_denomin_minus_barrel") #DATASC

        muon0_pt_nomin_plus_endcup_dataoc = rfiles[2].Get("muon0_pt_nomin_plus_endcup") #DATAOC
        muon0_pt_nomin_plus_endcup_datasc = rfiles[3].Get("muon0_pt_nomin_plus_endcup") #DATASC
        muon0_pt_denomin_plus_endcup_dataoc = rfiles[2].Get("muon0_pt_denomin_plus_endcup") #DATAOC
        muon0_pt_denomin_plus_endcup_datasc = rfiles[3].Get("muon0_pt_denomin_plus_endcup") #DATASC

        muon0_pt_nomin_minus_endcup_dataoc = rfiles[2].Get("muon0_pt_nomin_minus_endcup") #DATAOC
        muon0_pt_nomin_minus_endcup_datasc = rfiles[3].Get("muon0_pt_nomin_minus_endcup") #DATASC
        muon0_pt_denomin_minus_endcup_dataoc = rfiles[2].Get("muon0_pt_denomin_minus_endcup") #DATAOC
        muon0_pt_denomin_minus_endcup_datasc = rfiles[3].Get("muon0_pt_denomin_minus_endcup") #DATASC

        ## Subtracting background

        muon0_pt_nomin_plus_barrel_dataoc.Add(muon0_pt_nomin_plus_barrel_datasc, -1.0)
        muon0_pt_denomin_plus_barrel_dataoc.Add(muon0_pt_denomin_plus_barrel_datasc, -1.0)
        muon0_pt_nomin_minus_barrel_dataoc.Add(muon0_pt_nomin_minus_barrel_datasc, -1.0)
        muon0_pt_denomin_minus_barrel_dataoc.Add(muon0_pt_denomin_minus_barrel_datasc, -1.0)
        muon0_pt_nomin_plus_endcup_dataoc.Add(muon0_pt_nomin_plus_endcup_datasc, -1.0)
        muon0_pt_denomin_plus_endcup_dataoc.Add(muon0_pt_denomin_plus_endcup_datasc, -1.0)
        muon0_pt_nomin_minus_endcup_dataoc.Add(muon0_pt_nomin_minus_endcup_datasc, -1.0)
        muon0_pt_denomin_minus_endcup_dataoc.Add(muon0_pt_denomin_minus_endcup_datasc, -1.0)

        DummyCanvas.cd()

        
        
        muon0_pt_Teff_plus_barrel_dataoc = TEfficiency(muon0_pt_nomin_plus_barrel_dataoc, muon0_pt_denomin_plus_barrel_dataoc)
        muon0_pt_Teff_plus_barrel_dataoc.Draw()
        gPad.Update()
        muon0_pt_eff_plus_barrel_dataoc = muon0_pt_Teff_plus_barrel_dataoc.GetPaintedGraph()
        
        muon0_pt_Teff_minus_barrel_dataoc = TEfficiency(muon0_pt_nomin_minus_barrel_dataoc, muon0_pt_denomin_minus_barrel_dataoc)
        muon0_pt_Teff_minus_barrel_dataoc.Draw()
        gPad.Update()
        muon0_pt_eff_minus_barrel_dataoc = muon0_pt_Teff_minus_barrel_dataoc.GetPaintedGraph()

        muon0_pt_Teff_plus_endcup_dataoc = TEfficiency(muon0_pt_nomin_plus_endcup_dataoc, muon0_pt_denomin_plus_endcup_dataoc)
        muon0_pt_Teff_plus_endcup_dataoc.Draw()
        gPad.Update()
        muon0_pt_eff_plus_endcup_dataoc = muon0_pt_Teff_plus_endcup_dataoc.GetPaintedGraph()

        muon0_pt_Teff_minus_endcup_dataoc = TEfficiency(muon0_pt_nomin_minus_endcup_dataoc, muon0_pt_denomin_minus_endcup_dataoc)
        muon0_pt_Teff_minus_endcup_dataoc.Draw()
        gPad.Update()
        muon0_pt_eff_minus_endcup_dataoc = muon0_pt_Teff_minus_endcup_dataoc.GetPaintedGraph()
        

        PrepareEff(muon0_pt_eff_minus_barrel_dataoc, LineColor=2, Marker = 4, MarkerColor = 2, YTitle = "Trigger efficiency", XTitle="p_{T}^{#mu} [GeV]", LineStyle=-1)
        PrepareEff(muon0_pt_eff_plus_barrel_dataoc, LineColor=1, Marker = 20, MarkerColor = 1, YTitle = "Trigger efficiency", XTitle="p_{T}^{#mu} [GeV]", LineStyle=-1)
        PrepareEff(muon0_pt_eff_minus_endcup_dataoc, LineColor=2, Marker = 4, MarkerColor = 2, YTitle = "Trigger efficiency", XTitle="p_{T}^{#mu} [GeV]", LineStyle=-1)
        PrepareEff(muon0_pt_eff_plus_endcup_dataoc, LineColor=1, Marker = 20, MarkerColor = 1, YTitle = "Trigger efficiency", XTitle="p_{T}^{#mu} [GeV]", LineStyle=-1)
        gStyle.SetPadLeftMargin(0.12)
        gStyle.SetOptStat(0)
        gStyle.SetPadRightMargin(0.12)
        can3 = TCanvas(args.o+"_and_endcup_pt_plus_misus_data_efficiency_check","Resultsd",0,0,1600, 1600)
        can3.Divide(2,1)
        can3, pad1c, pad1rc, mg1c, legend1c, eff_ratioc, line1c = DoAllTheBloodyWork( TGraph_eff_mc = muon0_pt_eff_minus_barrel_dataoc, TGraph_eff_data = muon0_pt_eff_plus_barrel_dataoc , TEff_mc = muon0_pt_Teff_minus_barrel_dataoc, TEff_data = muon0_pt_Teff_plus_barrel_dataoc , canvas = can3, pad1Name = "pad1c", pad1rName = "pad1rc", XTitle = "p_{T}^{#mu}", YTitle = "Trigger Efficiency" , MultiGraphName = "multi1c", legend_mc = "#mu^{-} data", legend_data = "#mu^{+} data" , data_h_passed_Name = "data_h_passed_Namec", data_h_total_Name = "data_h_total_Namec", mc_h_passed_Name = "mc_h_passed_Namec", mc_h_total_Name = "mc_h_total_Namec" , CanvasNumber = 1, YTitleR = "Data+ / Data-    ", XTitleR = "p_{T}^{#mu} [GeV]", StartLine = 10, EndLine = 100, mg1Min=0.001, RatioMin=0.8, RatioMax=1.2, YOffset=1.04)
        can3.cd(1)
        pad1c.cd()
        mg1c.Draw("AP")
        legend1c.Draw("same")
        MakeATLASLabel(0.01,0.1,Sizer=0.08)
        pad1rc.cd()
        line1c.Draw("same")
        eff_ratioc.Draw("p e1 same")
        PrepareEff(muon0_pt_eff_minus_endcup_dataoc, LineColor=2, Marker = 4, MarkerColor = 2, YTitle = "Trigger efficiency", XTitle="p_{T}^{#mu} [GeV]", LineStyle=-1)
        PrepareEff(muon0_pt_eff_plus_endcup_dataoc, LineColor=1, Marker = 20, MarkerColor = 1, YTitle = "Trigger efficiency", XTitle="p_{T}^{#mu} [GeV]", LineStyle=-1)
        can3, pad1d, pad1rd, mg1d, legend1d, eff_ratiod, line1d = DoAllTheBloodyWork( TGraph_eff_mc = muon0_pt_eff_minus_endcup_dataoc, TGraph_eff_data = muon0_pt_eff_plus_endcup_dataoc , TEff_mc =  muon0_pt_Teff_minus_endcup_dataoc, TEff_data =  muon0_pt_Teff_plus_endcup_dataoc , canvas = can3, pad1Name = "pad1d", pad1rName = "pad1rd", XTitle = "p_{T}^{#mu}", YTitle = "Trigger Efficiency" , MultiGraphName = "multi1d", legend_mc = "#mu^{-} data", legend_data = "#mu^{+} data" , data_h_passed_Name = "data_h_passed_Named", data_h_total_Name = "data_h_total_Named", mc_h_passed_Name = "mc_h_passed_Named", mc_h_total_Name = "mc_h_total_Named" , CanvasNumber = 2, YTitleR = "Data+ / Data-    ", XTitleR = "p_{T}^{#mu} [GeV]", StartLine = 10, EndLine = 100, mg1Min=0.001, RatioMin=0.8, RatioMax=1.2, YOffset=1.04)
        can3.cd(1)
        pad1d.cd()
        mg1d.Draw("AP")
        legend1d.Draw("same")
        MakeATLASLabel(0.01,0.1,Sizer=0.08)
        pad1rd.cd()
        line1d.Draw("same")
        eff_ratiod.Draw("p e1 same")
        PrintCan(can3)

        ## Figure 28

        muon0_eta_nomin_plus_dataoc = rfiles[2].Get("muon0_eta_nomin_plus") #DATAOC
        muon0_eta_nomin_plus_datasc = rfiles[3].Get("muon0_eta_nomin_plus") #DATASC
        muon0_eta_denomin_plus_dataoc = rfiles[2].Get("muon0_eta_denomin_plus") #DATAOC
        muon0_eta_denomin_plus_datasc = rfiles[3].Get("muon0_eta_denomin_plus") #DATASC

        muon0_eta_nomin_minus_dataoc = rfiles[2].Get("muon0_eta_nomin_minus") #DATAOC
        muon0_eta_nomin_minus_datasc = rfiles[3].Get("muon0_eta_nomin_minus") #DATASC
        muon0_eta_denomin_minus_dataoc = rfiles[2].Get("muon0_eta_denomin_minus") #DATAOC
        muon0_eta_denomin_minus_datasc = rfiles[3].Get("muon0_eta_denomin_minus") #DATASC

        ## Subtractiong background

        muon0_eta_nomin_plus_dataoc.Add(muon0_eta_nomin_plus_datasc, -1.0)
        muon0_eta_denomin_plus_dataoc.Add(muon0_eta_denomin_plus_datasc, -1.0)
        muon0_eta_nomin_minus_dataoc.Add(muon0_eta_nomin_minus_datasc, -1.0)
        muon0_eta_denomin_minus_dataoc.Add(muon0_eta_denomin_minus_datasc, -1.0)

        DummyCanvas.cd()

        muon0_eta_Teff_plus_dataoc = TEfficiency(muon0_eta_nomin_plus_dataoc, muon0_eta_denomin_plus_dataoc)
        muon0_eta_Teff_plus_dataoc.Draw()
        gPad.Update()
        muon0_eta_eff_plus_dataoc = muon0_eta_Teff_plus_dataoc.GetPaintedGraph()
        
        muon0_eta_Teff_minus_dataoc = TEfficiency(muon0_eta_nomin_minus_dataoc, muon0_eta_denomin_minus_dataoc)
        muon0_eta_Teff_minus_dataoc.Draw()
        gPad.Update()
        muon0_eta_eff_minus_dataoc = muon0_eta_Teff_minus_dataoc.GetPaintedGraph()

        gStyle.SetPadLeftMargin(0.12)
        gStyle.SetOptStat(0)
        gStyle.SetPadRightMargin(0.12)
        can4 = TCanvas(args.o+"_and_endcup_eta_both_efficiency","Resultse",0,0,800, 800)

        PrepareEff(muon0_eta_eff_minus_dataoc, LineColor=2, Marker = 4, MarkerColor = 2, YTitle = "Trigger efficiency", XTitle="p_{T}^{#mu} [GeV]", LineStyle=-1)
        PrepareEff(muon0_eta_eff_plus_dataoc, LineColor=1, Marker = 20, MarkerColor = 1, YTitle = "Trigger efficiency", XTitle="p_{T}^{#mu} [GeV]", LineStyle=-1)
        can4, pad1e, pad1re, mg1e, legend1e, eff_ratioe, line1e = DoAllTheBloodyWork( TGraph_eff_mc = muon0_eta_eff_minus_dataoc, TGraph_eff_data = muon0_eta_eff_plus_dataoc , TEff_mc = muon0_eta_Teff_minus_dataoc, TEff_data = muon0_eta_Teff_plus_dataoc , canvas = can4, pad1Name = "pad1e", pad1rName = "pad1re", XTitle = "#eta^{#mu}", YTitle = "Trigger Efficiency" , MultiGraphName = "multi1e", legend_mc = "#mu^{-} data", legend_data = "#mu^{+} data" , data_h_passed_Name = "data_h_passed_Namee", data_h_total_Name = "data_h_total_Namee", mc_h_passed_Name = "mc_h_passed_Namee", mc_h_total_Name = "mc_h_total_Namee" , CanvasNumber = 1, YTitleR = "Data+ / Data-    ", XTitleR = "#eta^{#mu}", StartLine = -2.5, EndLine = 2.5, mg1Min=0.001, RatioMin=0.8, RatioMax=1.2, YOffset=1.04)
        can4.cd()
        pad1e.cd()
        mg1e.Draw("AP")
        legend1e.Draw("same")
        MakeATLASLabel(0.01,0.1,Sizer=0.08)
        pad1re.cd()
        line1e.Draw("same")
        eff_ratioe.Draw("p e1 same")
        #can2.SetName(args.o+"_and_endcup_eta_both_efficiency")
        PrintCan(can4)

if ("Iso" in args.o):
        muon0_eta_nomin_both_iso_dataoc = rfiles[2].Get("muon0_eta_nomin_both_iso")
        muon0_eta_denomin_both_iso_dataoc = rfiles[2].Get("muon0_eta_denomin_both_iso")
        muon0_eta_nomin_both_iso_datasc = rfiles[3].Get("muon0_eta_nomin_both_iso")
        muon0_eta_denomin_both_iso_datasc = rfiles[3].Get("muon0_eta_denomin_both_iso")
        muon0_eta_nomin_both_iso_mcoc = rfiles[0].Get("muon0_eta_nomin_both_iso")
        muon0_eta_denomin_both_iso_mcoc = rfiles[0].Get("muon0_eta_denomin_both_iso")
        muon0_eta_nomin_both_iso_mcsc = rfiles[1].Get("muon0_eta_nomin_both_iso")
        muon0_eta_denomin_both_iso_mcsc = rfiles[1].Get("muon0_eta_denomin_both_iso")

        muon0_pt_nomin_both_iso_dataoc = rfiles[2].Get("muon0_pt_nomin_both_iso")
        muon0_pt_denomin_both_iso_dataoc = rfiles[2].Get("muon0_pt_denomin_both_iso")
        muon0_pt_nomin_both_iso_datasc = rfiles[3].Get("muon0_pt_nomin_both_iso")
        muon0_pt_denomin_both_iso_datasc = rfiles[3].Get("muon0_pt_denomin_both_iso")
        muon0_pt_nomin_both_iso_mcoc = rfiles[0].Get("muon0_pt_nomin_both_iso")
        muon0_pt_denomin_both_iso_mcoc = rfiles[0].Get("muon0_pt_denomin_both_iso")
        muon0_pt_nomin_both_iso_mcsc = rfiles[1].Get("muon0_pt_nomin_both_iso")
        muon0_pt_denomin_both_iso_mcsc = rfiles[1].Get("muon0_pt_denomin_both_iso")

        muon0_eta_nomin_both_iso_dataoc.Add(muon0_eta_nomin_both_iso_datasc, -1.0)
        muon0_eta_denomin_both_iso_dataoc.Add(muon0_eta_denomin_both_iso_datasc, -1.0)
        muon0_eta_nomin_both_iso_mcoc.Add(muon0_eta_nomin_both_iso_mcsc, -1.0)
        muon0_eta_denomin_both_iso_mcoc.Add(muon0_eta_denomin_both_iso_mcsc, -1.0)
        muon0_pt_nomin_both_iso_dataoc.Add(muon0_pt_nomin_both_iso_datasc, -1.0)
        muon0_pt_denomin_both_iso_dataoc.Add(muon0_pt_denomin_both_iso_datasc, -1.0)
        muon0_pt_nomin_both_iso_mcoc.Add(muon0_pt_nomin_both_iso_mcsc, -1.0)
        muon0_pt_denomin_both_iso_mcoc.Add(muon0_pt_denomin_both_iso_mcsc, -1.0)

        DummyCanvas = TCanvas("DummyCanvas","DummyCanvas",0,0,800, 800)
        DummyCanvas.Divide(2,2)
        DummyCanvas.cd(1)
        muon0_eta_Teff_both_iso_dataoc = TEfficiency(muon0_eta_nomin_both_iso_dataoc,muon0_eta_denomin_both_iso_dataoc)
        muon0_eta_Teff_both_iso_dataoc.Draw()
        gPad.Update()
        muon0_eta_eff_both_iso_dataoc = muon0_eta_Teff_both_iso_dataoc.GetPaintedGraph()
        DummyCanvas.cd(2)
        muon0_eta_Teff_both_iso_mcoc = TEfficiency(muon0_eta_nomin_both_iso_mcoc,muon0_eta_denomin_both_iso_mcoc)
        muon0_eta_Teff_both_iso_mcoc.Draw()
        gPad.Update()
        muon0_eta_eff_both_iso_mcoc = muon0_eta_Teff_both_iso_mcoc.GetPaintedGraph()
        DummyCanvas.cd(3)
        muon0_pt_Teff_both_iso_dataoc = TEfficiency(muon0_pt_nomin_both_iso_dataoc,muon0_pt_denomin_both_iso_dataoc)
        muon0_pt_Teff_both_iso_dataoc.Draw()
        gPad.Update()
        muon0_pt_eff_both_iso_dataoc = muon0_pt_Teff_both_iso_dataoc.GetPaintedGraph()
        DummyCanvas.cd(4)
        muon0_pt_Teff_both_iso_mcoc = TEfficiency(muon0_pt_nomin_both_iso_mcoc,muon0_pt_denomin_both_iso_mcoc)
        muon0_pt_Teff_both_iso_mcoc.Draw()
        gPad.Update()
        muon0_pt_eff_both_iso_mcoc = muon0_pt_Teff_both_iso_mcoc.GetPaintedGraph()
        #gApplication.Run()


        PrepareEff(muon0_pt_eff_both_iso_mcoc, LineColor=2, Marker = 4, MarkerColor = 2, YTitle = "Trigger efficiency", XTitle="p_{T}^{#mu} [GeV]", LineStyle=-1)
        PrepareEff(muon0_pt_eff_both_iso_dataoc, LineColor=1, Marker = 20, MarkerColor = 1, YTitle = "Trigger efficiency", XTitle="p_{T}^{#mu} [GeV]", LineStyle=-1)
        #PrepareEff(muon0_eta_eff_both_iso_mcoc, LineColor=2, Marker = 4, MarkerColor = 2, YTitle = "Trigger efficiency", XTitle="p_{T}^{#mu} [GeV]", LineStyle=-1)
        #PrepareEff(muon0_eta_eff_both_iso_dataoc, LineColor=1, Marker = 20, MarkerColor = 1, YTitle = "Trigger efficiency", XTitle="p_{T}^{#mu} [GeV]", LineStyle=-1)
        gStyle.SetPadLeftMargin(0.12)
        gStyle.SetOptStat(0)
        gStyle.SetPadRightMargin(0.12)
        can5 = TCanvas(args.o+"_both_eta_pt","Resultsd",0,0,1600, 1600)
        can5.Divide(2,1)
        PrepareEff(muon0_pt_eff_both_iso_mcoc, LineColor=2, Marker = 4, MarkerColor = 2, YTitle = "Trigger efficiency", XTitle="p_{T}^{#mu} [GeV]", LineStyle=-1)
        PrepareEff(muon0_pt_eff_both_iso_dataoc, LineColor=1, Marker = 20, MarkerColor = 1, YTitle = "Trigger efficiency", XTitle="p_{T}^{#mu} [GeV]", LineStyle=-1)
        can5, pad1c, pad1rc, mg1c, legend1c, eff_ratioc, line1c = DoAllTheBloodyWork( TGraph_eff_mc = muon0_pt_eff_both_iso_mcoc, TGraph_eff_data = muon0_pt_eff_both_iso_dataoc , TEff_mc = muon0_pt_Teff_both_iso_mcoc, TEff_data = muon0_pt_Teff_both_iso_dataoc , canvas = can5, pad1Name = "pad1c", pad1rName = "pad1rc", XTitle = "p_{T}^{#mu}", YTitle = "Isolation Efficiency" , MultiGraphName = "multi1c", legend_mc = "MC, #mu^{#pm}", legend_data = "data, #mu^{#pm}" , data_h_passed_Name = "data_h_passed_Name", data_h_total_Name = "data_h_total_Name", mc_h_passed_Name = "mc_h_passed_Name", mc_h_total_Name = "mc_h_total_Name" , CanvasNumber = 1, YTitleR = "Data/MC         ", XTitleR = "p_{T}^{#mu} [GeV]", StartLine = 10, EndLine = 500, mg1Min=0.001, RatioMin=0.8, RatioMax=1.2, YOffset=1.04, Xaxis = 1)
        can5.cd(1)
        pad1c.cd()
        mg1c.Draw("AP")
        legend1c.Draw("same")
        MakeATLASLabel(0.01,0.1,Sizer=0.08)
        pad1rc.cd()
        line1c.Draw("same")
        eff_ratioc.Draw("p e1 same")
        PrepareEff(muon0_eta_eff_both_iso_mcoc, LineColor=2, Marker = 4, MarkerColor = 2, YTitle = "Trigger efficiency", XTitle="p_{T}^{#mu} [GeV]", LineStyle=-1)
        PrepareEff(muon0_eta_eff_both_iso_dataoc, LineColor=1, Marker = 20, MarkerColor = 1, YTitle = "Trigger efficiency", XTitle="p_{T}^{#mu} [GeV]", LineStyle=-1)
        can5, pad1d, pad1rd, mg1d, legend1d, eff_ratiod, line1d = DoAllTheBloodyWork(  TGraph_eff_mc = muon0_eta_eff_both_iso_mcoc, TGraph_eff_data = muon0_eta_eff_both_iso_dataoc , TEff_mc = muon0_eta_Teff_both_iso_mcoc, TEff_data = muon0_eta_Teff_both_iso_dataoc  , canvas = can5, pad1Name = "pad1d", pad1rName = "pad1rd", XTitle = "#eta^{#mu}", YTitle = "Isolation Efficiency" , MultiGraphName = "multi1d", legend_mc = "MC, #mu^{#pm}", legend_data = "data, #mu^{#pm}" , data_h_passed_Name = "data_h_passed_Named", data_h_total_Name = "data_h_total_Named", mc_h_passed_Name = "mc_h_passed_Named", mc_h_total_Name = "mc_h_total_Named" , CanvasNumber = 2, YTitleR = "Data/MC         ", XTitleR = "#eta^{#mu}", StartLine = -2.5, EndLine = 2.5, mg1Min=0.001, RatioMin=0.8, RatioMax=1.2, YOffset=1.04)
        can5.cd(2)
        pad1d.cd()
        mg1d.SetMaximum(1)
        mg1d.Draw("AP")
        legend1d.Draw("same")
        MakeATLASLabel(0.01,0.1,Sizer=0.08)
        pad1rd.cd()
        line1d.Draw("same")
        eff_ratiod.Draw("p e1 same")
        PrintCan(can5)
        #gApplication.Run()        

        # Figure 33

        muon0_eta_nomin_plus_iso_dataoc = rfiles[2].Get("muon0_eta_nomin_plus_iso")
        muon0_eta_denomin_plus_iso_dataoc = rfiles[2].Get("muon0_eta_denomin_plus_iso")
        muon0_eta_nomin_minus_iso_dataoc = rfiles[2].Get("muon0_eta_nomin_minus_iso")
        muon0_eta_denomin_minus_iso_dataoc = rfiles[2].Get("muon0_eta_denomin_minus_iso")

        muon0_eta_nomin_plus_iso_datasc = rfiles[3].Get("muon0_eta_nomin_plus_iso")
        muon0_eta_denomin_plus_iso_datasc = rfiles[3].Get("muon0_eta_denomin_plus_iso")
        muon0_eta_nomin_minus_iso_datasc = rfiles[3].Get("muon0_eta_nomin_minus_iso")
        muon0_eta_denomin_minus_iso_datasc = rfiles[3].Get("muon0_eta_denomin_minus_iso")

        muon0_eta_nomin_plus_iso_mcoc = rfiles[0].Get("muon0_eta_nomin_plus_iso")
        muon0_eta_denomin_plus_iso_mcoc = rfiles[0].Get("muon0_eta_denomin_plus_iso")
        muon0_eta_nomin_minus_iso_mcoc = rfiles[0].Get("muon0_eta_nomin_minus_iso")
        muon0_eta_denomin_minus_iso_mcoc = rfiles[0].Get("muon0_eta_denomin_minus_iso")

        muon0_eta_nomin_plus_iso_mcsc = rfiles[1].Get("muon0_eta_nomin_plus_iso")
        muon0_eta_denomin_plus_iso_mcsc = rfiles[1].Get("muon0_eta_denomin_plus_iso")
        muon0_eta_nomin_minus_iso_mcsc = rfiles[1].Get("muon0_eta_nomin_minus_iso")
        muon0_eta_denomin_minus_iso_mcsc = rfiles[1].Get("muon0_eta_denomin_minus_iso")
        
        
        #Possibly subtract background

        muon0_eta_nomin_plus_iso_dataoc.Add(muon0_eta_nomin_plus_iso_datasc, -1)
        muon0_eta_denomin_plus_iso_dataoc.Add(muon0_eta_denomin_plus_iso_datasc, -1)
        muon0_eta_nomin_minus_iso_dataoc.Add(muon0_eta_nomin_minus_iso_datasc, -1)
        muon0_eta_denomin_minus_iso_dataoc.Add(muon0_eta_denomin_minus_iso_datasc, -1)
        muon0_eta_nomin_plus_iso_mcoc.Add(muon0_eta_nomin_plus_iso_mcsc, -1)
        muon0_eta_denomin_plus_iso_mcoc.Add(muon0_eta_denomin_plus_iso_mcsc, -1)
        muon0_eta_nomin_minus_iso_mcoc.Add(muon0_eta_nomin_minus_iso_mcsc, -1)
        muon0_eta_denomin_minus_iso_mcoc.Add(muon0_eta_denomin_minus_iso_mcsc, -1)

        DummyCanvas2 = TCanvas("DummyCanvas2","DummyCanvas2",0,0,800, 800)        
        DummyCanvas2.Divide(2,2)

        DummyCanvas2.cd(1)
        muon0_eta_Teff_plus_iso_dataoc = TEfficiency(muon0_eta_nomin_plus_iso_dataoc,muon0_eta_denomin_plus_iso_dataoc)
        muon0_eta_Teff_plus_iso_dataoc.Draw()
        gPad.Update()
        muon0_eta_eff_plus_iso_dataoc = muon0_eta_Teff_plus_iso_dataoc.GetPaintedGraph()
        DummyCanvas2.cd(2)
        muon0_eta_Teff_plus_iso_mcoc = TEfficiency(muon0_eta_nomin_plus_iso_mcoc,muon0_eta_denomin_plus_iso_mcoc)
        muon0_eta_Teff_plus_iso_mcoc.Draw()
        gPad.Update()
        muon0_eta_eff_plus_iso_mcoc = muon0_eta_Teff_plus_iso_mcoc.GetPaintedGraph()
        DummyCanvas2.cd(3)
        muon0_eta_Teff_minus_iso_dataoc = TEfficiency(muon0_eta_nomin_minus_iso_dataoc,muon0_eta_denomin_minus_iso_dataoc)
        muon0_eta_Teff_minus_iso_dataoc.Draw()
        gPad.Update()
        muon0_eta_eff_minus_iso_dataoc = muon0_eta_Teff_minus_iso_dataoc.GetPaintedGraph()
        DummyCanvas2.cd(4)
        muon0_eta_Teff_minus_iso_mcoc = TEfficiency(muon0_eta_nomin_minus_iso_mcoc,muon0_eta_denomin_minus_iso_mcoc)
        muon0_eta_Teff_minus_iso_mcoc.Draw()
        gPad.Update()
        muon0_eta_eff_minus_iso_mcoc = muon0_eta_Teff_minus_iso_mcoc.GetPaintedGraph()

        PrepareEff(muon0_eta_eff_plus_iso_mcoc, LineColor=2, Marker = 4, MarkerColor = 2, YTitle = "Isolation efficiency", XTitle="#eta^{#mu}", LineStyle=-1)
        PrepareEff(muon0_eta_eff_plus_iso_dataoc, LineColor=1, Marker = 20, MarkerColor = 1, YTitle = "Isolation efficiency", XTitle="#eta^{#mu}", LineStyle=-1)
        #PrepareEff(muon0_eta_eff_both_iso_mcoc, LineColor=2, Marker = 4, MarkerColor = 2, YTitle = "Trigger efficiency", XTitle="#eta^{#mu}", LineStyle=-1)
        #PrepareEff(muon0_eta_eff_both_iso_dataoc, LineColor=1, Marker = 20, MarkerColor = 1, YTitle = "Trigger efficiency", XTitle="#eta^{#mu}", LineStyle=-1)
        gStyle.SetPadLeftMargin(0.12)
        gStyle.SetOptStat(0)
        gStyle.SetPadRightMargin(0.12)
        can6 = TCanvas(args.o+"_iso_eta_plus_minus","Resultse",0,0,1600, 1600)
        can6.Divide(2,1)
        PrepareEff(muon0_eta_eff_plus_iso_mcoc, LineColor=2, Marker = 4, MarkerColor = 2, YTitle = "Isolation efficiency", XTitle="#eta^{#mu}", LineStyle=-1)
        PrepareEff(muon0_eta_eff_plus_iso_dataoc, LineColor=1, Marker = 20, MarkerColor = 1, YTitle = "Isolation efficiency", XTitle="#eta^{#mu}", LineStyle=-1)
        can6, pad1e, pad1re, mg1e, legend1e, eff_ratioe, line1e = DoAllTheBloodyWork( TGraph_eff_mc = muon0_eta_eff_plus_iso_mcoc, TGraph_eff_data = muon0_eta_eff_plus_iso_dataoc , TEff_mc = muon0_eta_Teff_plus_iso_mcoc, TEff_data = muon0_eta_Teff_plus_iso_dataoc , canvas = can6, pad1Name = "pad1e", pad1rName = "pad1re", XTitle = "#eta^{#mu}", YTitle = "Isolation Efficiency" , MultiGraphName = "multi1e", legend_mc = "MC, #mu^{+}", legend_data = "data, #mu^{+}" , data_h_passed_Name = "data_h_passed_Name", data_h_total_Name = "data_h_total_Name", mc_h_passed_Name = "mc_h_passed_Name", mc_h_total_Name = "mc_h_total_Name" , CanvasNumber = 1, YTitleR = "Data/MC         ", XTitleR = "#eta^{#mu}", StartLine = -2.5, EndLine = 2.5, mg1Min=0.001, RatioMin=0.8, RatioMax=1.2, YOffset=1.04)
        can6.cd(1)
        pad1e.cd()
        mg1e.SetMaximum(1)
        mg1e.Draw("AP")
        legend1e.Draw("same")
        MakeATLASLabel(0.01,0.1,Sizer=0.08)
        pad1re.cd()
        line1e.Draw("same")
        eff_ratioe.Draw("p e1 same")
        PrepareEff(muon0_eta_eff_minus_iso_mcoc, LineColor=2, Marker = 4, MarkerColor = 2, YTitle = "Isolation efficiency", XTitle="#eta^{#mu}", LineStyle=-1)
        PrepareEff(muon0_eta_eff_minus_iso_dataoc, LineColor=1, Marker = 20, MarkerColor = 1, YTitle = "Isolation efficiency", XTitle="#eta^{#mu}", LineStyle=-1)
        can6, pad1f, pad1rf, mg1f, legend1f, eff_ratiof, line1f = DoAllTheBloodyWork(  TGraph_eff_mc = muon0_eta_eff_minus_iso_mcoc, TGraph_eff_data = muon0_eta_eff_minus_iso_dataoc , TEff_mc = muon0_eta_Teff_minus_iso_mcoc, TEff_data = muon0_eta_Teff_minus_iso_dataoc , canvas = can6, pad1Name = "pad1f", pad1rName = "pad1rf", XTitle = "#eta^{#mu}", YTitle = "Isolation Efficiency" , MultiGraphName = "multi1f", legend_mc = "MC, #mu^{-}", legend_data = "data, #mu^{-}" , data_h_passed_Name = "data_h_passed_Named", data_h_total_Name = "data_h_total_Named", mc_h_passed_Name = "mc_h_passed_Named", mc_h_total_Name = "mc_h_total_Named" , CanvasNumber = 2, YTitleR = "Data/MC         ", XTitleR = "#eta^{#mu}", StartLine = -2.5, EndLine = 2.5, mg1Min=0.001, RatioMin=0.8, RatioMax=1.2, YOffset=1.04)
        can6.cd(2)
        pad1f.cd()
        mg1f.SetMaximum(1)
        mg1f.Draw("AP")
        legend1f.Draw("same")
        MakeATLASLabel(0.01,0.1,Sizer=0.08)
        pad1rf.cd()
        line1f.Draw("same")
        eff_ratiof.Draw("p e1 same")
        PrintCan(can6)

        #gApplication.Run()
        
outfile.Write()



